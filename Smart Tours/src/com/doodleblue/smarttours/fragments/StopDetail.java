package com.doodleblue.smarttours.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.fragments.Map.MapMode;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/27/13
 * Time: 7:04 PM
 * To change this template use File | Settings | File Templates.
 */
@SuppressLint("ValidFragment")
public class StopDetail extends BaseContainerFragment implements View.OnClickListener {

    private Stops mStop;
    private int mStopType;

    private String freeCallValue;
    private String callValue;
    private String webValue;

    StopDetail(Stops stops, int type){
        mStop = stops;
        mStopType = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.stops_description,
                container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView(){
        ((Button)mRootView.findViewById(R.id.freeCall)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.call)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.web)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.viewOnMap)).setOnClickListener(this);

        ((TextView)mRootView.findViewById(R.id.title)).setText(mStop.getName());
        Bitmap bitmap = Utils.getImageFromString(mStop.getImageWideLocal());
        ((ImageView)mRootView.findViewById(R.id.cover)).setImageBitmap(bitmap);

        ((TextView)mRootView.findViewById(R.id.hours)).setText(mStop.getTimeNeeded());
        ((TextView)mRootView.findViewById(R.id.dateTime)).setText(mStop.getSegmentId()); //Todo: retrieve Segment Name
        ((TextView)mRootView.findViewById(R.id.highlightContent)).setText(mStop.getHighlights());
        ((TextView)mRootView.findViewById(R.id.descriptionContent)).setText(mStop.getDescription());

    }

    private void replaceFragment(String url) {
        //TODO: any navigation of level deeper will go here
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Web(url), true);
    }
    
    private void replaceFragment(MapMode mode, String segmentId, String stopId, boolean isPlace) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Map(mode, segmentId, stopId, isPlace), true);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.freeCall:
                if(!TextUtils.isEmpty(freeCallValue)){
                    showDevMsg(freeCallValue);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + freeCallValue));
                    mActivity.startActivity(intent);
                }
                break;
            case R.id.call:
                if(!TextUtils.isEmpty(callValue) ) {
                    showDevMsg(callValue);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+callValue));
                    mActivity.startActivity(intent);
                }
                break;
            case R.id.web:
            	webValue = AppConstants.URL_DEVELOPERS;
                if(!TextUtils.isEmpty(webValue) ){
                    showDevMsg(webValue);
                    replaceFragment(webValue);
                }
                break;
            case R.id.viewOnMap:
            	//Stop Map
            	Map.setMapMode(MapMode.STOP, null, mStop.getSegmentId(), mStop.getId());
            	mActivity.mTabHost.setCurrentTab(1);
            	//replaceFragment(MapMode.STOP,mStop.getSegmentId(), mStop.getId(),true);
                break;
        }
    }
}
