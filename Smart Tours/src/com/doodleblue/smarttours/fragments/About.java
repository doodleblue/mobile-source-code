
package com.doodleblue.smarttours.fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class About extends BaseContainerFragment implements View.OnClickListener {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView =  inflater.inflate(R.layout.aboutus,
				container, false);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	private void initView() {

		String text = "<font color=#1b7fbe>doodle</font><font color=#161546>blue </font> <font color=#999897>Innovations Private Limited </font>";
		((TextView)mActivity.findViewById(R.id.devBy)).setText(Html.fromHtml(text));

		/*((TextView)mActivity.findViewById(R.id.header_text)).setText("Settings");
        ((TextView)mRootView.findViewById(R.id.about_text)).setText(
                mActivity.getResources().getString(R.string.lorem_ipsum)+" \n\n"+
                        mActivity.getResources().getString(R.string.lorem_ipsum_more));*/

		((TextView)mRootView.findViewById(R.id.terms)).setOnClickListener(this);
		((TextView)mRootView.findViewById(R.id.contributors)).setOnClickListener(this);
		((LinearLayout)mRootView.findViewById(R.id.developer)).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.terms:
			replaceFragment(AppConstants.URL_TERMS_OF_USE);
			Toast.makeText(mActivity, AppConstants.URL_TERMS_OF_USE, Toast.LENGTH_SHORT).show();
			break;

		case R.id.contributors:
			replaceFragment(AppConstants.URL_CONTRIBUTORS);
			Toast.makeText(mActivity, AppConstants.URL_CONTRIBUTORS, Toast.LENGTH_SHORT).show();
			break;

		case R.id.developer:
			replaceFragment(AppConstants.URL_DEVELOPERS);
			Toast.makeText(mActivity, AppConstants.URL_DEVELOPERS, Toast.LENGTH_SHORT).show();
			break;

		default:
			break;
		}
	}

	private void replaceFragment(String url) {
		((BaseContainerFragment)getParentFragment()).replaceFragment(new Web(url), true);
	}
}
