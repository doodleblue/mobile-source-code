package com.doodleblue.smarttours.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.util.BaseContainerFragment;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/4/13
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class RouteLevelOne extends BaseContainerFragment {

    private FragmentTabHost mTabHost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mTabHost = new FragmentTabHost(mActivity);
        mTabHost.setup(getActivity(), getChildFragmentManager(),
                R.id.menu_settings);

        initializeTabs();
        return mTabHost;
    }

    public void initializeTabs() {

        //TEMP
        /*Bundle b = new Bundle();
        b.putString("key", "Simple");
        mTabHost.addTab(mTabHost.newTabSpec("Simple").setIndicator("Simple"),
                Fragment1.class, b);*/

        mTabHost.addTab(setIndicator(mActivity,mTabHost.newTabSpec(AppConstants.SELECT_ROUTES),R.drawable.select_routes_tab_selector),
                SelectRouteContainer.class, null);

        mTabHost.addTab(setIndicator(mActivity,mTabHost.newTabSpec(AppConstants.MY_FAVOURITE),R.drawable.my_favourites_selector),
                FavouriteContainer.class, null);

        mTabHost.addTab(setIndicator(mActivity,mTabHost.newTabSpec(AppConstants.DOWNLOADS),R.drawable.my_downloads_selector),
                DownloadedContainer.class, null);


    }


    public TabHost.TabSpec setIndicator(Context ctx,TabHost.TabSpec spec, int resid) {

        View view = LayoutInflater.from(ctx).inflate(R.layout.tabs_icon, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
        imageView.setImageResource(resid);

        return spec.setIndicator(view);
    }

    public String getCurrentTab()
    {
        return mTabHost.getCurrentTabTag();
    }

}
