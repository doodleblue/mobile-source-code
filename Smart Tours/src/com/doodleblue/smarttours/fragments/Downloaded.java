package com.doodleblue.smarttours.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.doodleblue.smarttours.view.CoverFlow;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/27/13
 * Time: 3:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class Downloaded extends BaseContainerFragment {
	private ProgressBar mProgressBar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.downloaded, null);
        return mRootView;
    }
    

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView(){
    	mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progressBar1);
		mProgressBar.setVisibility(View.VISIBLE);
        fetchDownloadedRecord();
    }

    private void fetchDownloadedRecord(){
        FetchDownloadedTask task = new FetchDownloadedTask();
        task.execute();
    }

    public class FetchDownloadedTask extends AsyncTask<String,String,ArrayList<Route>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Route> doInBackground(String... params) {
            ArrayList<Route> masterRouteList = new ArrayList<Route>();
            for(String routeId:DatabaseHelper.getInstance(mActivity).fetchDownloaded(null)){
                for(Route route:DatabaseHelper.getInstance(mActivity).fetchRoutes(new String[]{routeId})){
                    masterRouteList.add(route);
                }
            }
            return masterRouteList;
        }

        @Override
        protected void onPostExecute(ArrayList<Route> routes) {
            super.onPostExecute(routes);
            mProgressBar.setVisibility(View.GONE);
            if(routes!=null&&routes.size()>0)
            {
                setupCustomLists(routes);
                ((TextView)mRootView.findViewById(R.id.tour_spot)).setText(routes.get(0).getName());
            } else {
            	mRootView.findViewById(R.id.noDownloadsFound).setVisibility(View.VISIBLE);
            }
        }
    }

    private void setupCustomLists(ArrayList<Route>  routeArrayList){

        CoverFlow coverFlow = (CoverFlow) mRootView.findViewById(R.id.favourite_rotes);
        ImageAdapter coverImageAdapter =  new ImageAdapter(mActivity, routeArrayList);
        //FavouriteSwipeAdapter coverImageAdapter = new FavouriteSwipeAdapter(mActivity, routeArrayList);
        coverFlow.setAdapter(coverImageAdapter);
        coverFlow.setSpacing(-25);
        coverFlow.setSelection(0, true);
        coverFlow.setAnimationDuration(1000);
        /*coverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Route route = (Route) parent.getAdapter().getItem(position);
                replaceFragment(route);

            }
        });*/

        //TODO: Initially setOnItemSelectedListener is not calling by default.

        coverFlow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Route route = (Route) parent.getAdapter().getItem(position);
                ((TextView)mRootView.findViewById(R.id.tour_spot)).setText(route.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void replaceFragment(Route route) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new TourStopList(route), true);
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private  ArrayList<Route>  mList;

        public ImageAdapter(Context c, ArrayList<Route>  list) {
            mContext = c;
            mList = list;
        }

        public int getCount() {
            return mList.size();
        }

        public Route getItem(int position) {
            return mList.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @SuppressWarnings("deprecation")
        public View getView(int position, View convertView, ViewGroup parent) {

            //Use this code if you want to load from resources
            /*ImageView i = new ImageView(mContext);
            i.setImageBitmap(
                    Utils.getImageFromString(mList.get(position).getImageLocal())
            );
            i.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            */
        	
            Bitmap bitmap = Utils.getRoundedCornerBitmap(
					Utils.getImageFromString(mList.get(position).getImageLocal()),
					5);
            ImageView i = new ImageView(mContext);
			i.setImageBitmap(bitmap);
			float scale = getResources().getDisplayMetrics().density;
			int width = (int) (175*scale + 0.5f); //350px
			int height = (int) (250*scale + 0.5f); //500px

			i.setLayoutParams(new CoverFlow.LayoutParams(width,height));
			i.setScaleType(ImageView.ScaleType.FIT_CENTER);

            //Make sure we set anti-aliasing otherwise we get jaggies
            BitmapDrawable drawable = (BitmapDrawable) i.getDrawable();
            drawable.setAntiAlias(true);

            return i;
        }
        /** Returns the size (0.0f to 1.0f) of the views
         * depending on the 'offset' to the center. */
        public float getScale(boolean focused, int offset) {
        /* Formula: 1 / (2 ^ offset) */
            return Math.max(0, 1.0f / (float)Math.pow(2, Math.abs(offset)));
        }

    }


}
