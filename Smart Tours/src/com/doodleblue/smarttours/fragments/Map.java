package com.doodleblue.smarttours.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.bonuspack.overlays.PlacesInfoWindow.OnAudioButtonClickedListener;
import org.osmdroid.bonuspack.overlays.PlacesOverlayWithBubble;
import org.osmdroid.bonuspack.overlays.TouchToPlayOverlay;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.tileprovider.MapTileProviderArray;
import org.osmdroid.tileprovider.modules.IArchiveFile;
import org.osmdroid.tileprovider.modules.MBTilesFileArchive;
import org.osmdroid.tileprovider.modules.MapTileFileArchiveProvider;
import org.osmdroid.tileprovider.modules.MapTileModuleProviderBase;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.capricorn.ArcMenu;
import com.doodleblue.smarttours.adapter.ThingsToDoDetailAdapter.PlayerState;
import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.Activity;
import com.doodleblue.smarttours.model.ActivityCount;
import com.doodleblue.smarttours.model.Downloaded;
import com.doodleblue.smarttours.model.Opus;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Segment;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.service.Actions;
import com.doodleblue.smarttours.service.GuideControllableAudioService;
import com.doodleblue.smarttours.service.MbtilesDownloadService;
import com.doodleblue.smarttours.service.Tracker;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.doodleblue.smarttours.view.SmartMapView;
import com.smarttours.audio.codec.OpusManager;
import com.smarttours.audio.codec.OpusManager.OnStateUpdateListener;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA. User: ruby Date: 11/5/13 Time: 7:40 PM To change
 * this template use File | Settings | File Templates.
 */
@SuppressLint("ValidFragment")
public class Map extends BaseContainerFragment implements 
View.OnClickListener, 
OnAudioButtonClickedListener, 
OnStateUpdateListener {

	private final String TAG = getClass().getSimpleName();
	// Location
	private final LocationManager mLocationManager = (LocationManager) SmartTours
			.getContext().getSystemService(Context.LOCATION_SERVICE);

	private ArrayList<Opus> mOpuses = new ArrayList<Opus>();
	private ArrayList<Opus> mRelevantOpuses = new ArrayList<Opus>();


	public static ArrayList<POI> mPOIs;
	public static ArrayList<POI> mAudioMarkers;
	public static ArrayList<POI> mPlaceMarkers;

	private boolean mIsMapConfigured;

	private ArrayList<OverlayItem> mOverlayItemArray;

	private ProgressBar mProgressBar;

	//private ProgressDialog mProgressDialog;

	private CheckBox mCurrentLocationSelector;
	private Tracker mTracker;

	private ArcMenu mActivitiesFilter;
	private ArcMenu mStopsFilter;
	private static boolean shouldHideArcMenu;

	// private StringBuffer mSegmentCoordinates;
	private ArrayList<String> mSegmentCoordinatesList = new ArrayList<String>();

	public enum MapMode {
		ROUTE, SEGMENT, STOP, ACTIVITY
	}

	private static MapMode mMapMode;
	private static ArrayList<String> mSegmentsList = new ArrayList<String>();
	private static String mRouteId;
	private static String mSegmentId;
	private static String mId;


	public static void setMapMode(MapMode mode, String routeId, String segmentId, String id){
		mId = id;
		mMapMode = mode;
		if(!TextUtils.isEmpty(routeId)){ 
			mRouteId = routeId;
			mSegmentsList.clear(); 
			shouldHideArcMenu = false;
		} 
		if(!TextUtils.isEmpty(segmentId)){
			mSegmentsList.clear(); 
			mSegmentsList.add(segmentId); 
			mSegmentId = segmentId; 
			switch (mMapMode) {
			case ACTIVITY:
				mCurrentActivityId = id;
				mRouteId = null;
				shouldHideArcMenu = true;
				break;

			case STOP:
				mCurrentStopId = id;
				mRouteId = null;
				shouldHideArcMenu = true;
				break;

			case SEGMENT:
				mRouteId = null;
				shouldHideArcMenu = false;
				break;

			default:
				shouldHideArcMenu = false;
				break;
			}
		} 

	}


	public Map(String routeId) {
		mRouteId = routeId;
	}

	public Map() {
		this(mMapMode, mSegmentId, mId, true);



		/*this("1");
		mMapMode = MapMode.ROUTE;*/
	}

	private boolean mIsPlaceMap;
	private static String mCurrentActivityId;
	private static String mCurrentStopId;

	public Map(MapMode mode, String segmentId, String id, boolean isPlaceMap) {
		mMapMode = mode;
		if(mMapMode==null){
			mMapMode = MapMode.ROUTE;
			mRouteId = "1";
		}
		else {
			mIsPlaceMap = isPlaceMap;
			mSegmentsList.clear();
			mSegmentsList.add(segmentId);
			mSegmentId = segmentId;
			switch (mMapMode) {
			case ACTIVITY:
				mCurrentActivityId = id;
				mRouteId = null;
				shouldHideArcMenu = true;
				break;

			case STOP:
				mCurrentStopId = id;
				mRouteId = null;
				shouldHideArcMenu = true;
				break;

			case SEGMENT:
				mRouteId = null;
				shouldHideArcMenu = false;
				break;

			default:
				shouldHideArcMenu = false;
				break;
			}
		}
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.map, null);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (mRootView != null)
			mRootView.findViewById(R.id.progressBar)
			.setVisibility(View.VISIBLE);
		initView();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mRootView != null)
			mRootView.findViewById(R.id.progressBar).setVisibility(View.GONE);
	}

	private void initView() {

		if(mOpusPlayer == null){
			mOpusPlayer = new OpusManager(mActivity, this);
			mPlayerState = PlayerState.INITIALIZED;
		}

		// Trigger it, only when coming here from route
		mCurrentLocationSelector = (CheckBox) mRootView
				.findViewById(R.id.currentLocationSelector);
		mCurrentLocationSelector
		.setOnCheckedChangeListener(mCurrentLocationChangedListener);

		mOverlayItemArray = new ArrayList<OverlayItem>();

		mActivitiesFilter = (ArcMenu) mRootView
				.findViewById(R.id.activity_filter);
		mStopsFilter = (ArcMenu) mRootView.findViewById(R.id.stop_filter);
		if(shouldHideArcMenu) hideArcMenu(true);
		else hideArcMenu(false);
		mActivitiesFilter.setCenterImage(R.drawable.map_info_btn);
		mStopsFilter.setCenterImage(R.drawable.map_stop_btn);

		initArcMenu(mActivitiesFilter, ACTIVITY_DRAWABLES);
		initArcMenu(mStopsFilter, STOP_DRAWABLES);

		mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progressBar);
		mProgressBar.setVisibility(View.VISIBLE);

		if(!shouldHideArcMenu){
			// To Set ArcMenu calculated Margin
			alignArcMenu();
		}
		
		MBTilesExistanceCheckTask mbTilesExistanceCheckTask = new MBTilesExistanceCheckTask();
		mbTilesExistanceCheckTask.execute();

		Intent intent = new Intent(SmartTours.getContext(),
				MbtilesDownloadService.class);
		switch (mMapMode) {
		case ROUTE:
			intent.putExtra("routeId", mRouteId);
			// Put Pins on Available Audio Points
			mTracker = new Tracker(mRouteId) {
				@Override
				protected void onCatchedAudio(Opus opus, Direction direction) {

				}
			};
			break;

		default:
			mCurrentLocationSelector.setVisibility(View.GONE);
			(mRootView.findViewById(R.id.layCurrentLocationSelector))
			.setVisibility(View.GONE);
			intent.putExtra("segmentId", mSegmentId);
			break;
		}

		/*
		 * if(mIsPlaceMap){ mCurrentLocationSelector.setVisibility(View.GONE);
		 * (mRootView
		 * .findViewById(R.id.layCurrentLocationSelector)).setVisibility
		 * (View.GONE); intent.putExtra("segmentId",mSegmentId); } else {
		 * 
		 * intent.putExtra("routeId",mRouteId); }
		 */
		intent.putExtra("receiver", new DownloadReceiver(new Handler()));

		if(SmartTours.isNetworkConnected()){
			SmartTours.getContext().startService(intent);
			Toast.makeText(mActivity, "Checking out Map data!", Toast.LENGTH_SHORT)
			.show();
		} else {
			Toast.makeText(mActivity, getResources().getString(R.string.check_network), Toast.LENGTH_SHORT)
			.show();
		}

		/*
		 * if(!mIsPlaceMap){ //Put Pins on Available Audio Points mTracker = new
		 * Tracker(mRouteId) {
		 * 
		 * @Override protected void onCatchedAudio(Opus opus,Direction
		 * direction) {
		 * 
		 * } }; }
		 */
	}
	
	public class MBTilesExistanceCheckTask extends AsyncTask<String, Integer, Boolean>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressBar.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			ArrayList<Route> routes;
			ArrayList<Segment> segments;
			switch (mMapMode) {
			case ROUTE:
				routes = DatabaseHelper.getInstance().fetchRoutes(new String[]{mRouteId});
				for (Route route : routes) {
					String segmentMeta = route.getSegmentMeta();
					try {
						JSONObject json = new JSONObject(segmentMeta);
						JSONArray segmentArray = new JSONArray(json.getString("segment_meta"));
						for (int i = 0; i < segmentArray.length(); i++) {
							if(!mSegmentsList.contains(segmentArray.getString(i))){
								mSegmentsList.add(segmentArray.getString(i));
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					for (String string : mSegmentsList) {
						for(Segment segment: DatabaseHelper.getInstance().fetchSegment(string)){
							isFileExists(segment.getMbTilesLocal());
						}
					}
				}
				break;
			default:
				segments = DatabaseHelper.getInstance().fetchSegment(mSegmentId);
				
				break;
			}
			return true;
		}
		
		private boolean isFileExists(String mbTilesLocal) {
			
			String path = AppConstants.ROOT_DIRECTORY + mbTilesLocal;
			String root = path.substring(0,path.lastIndexOf("/"));
			File dir = new File(root);
			if(!dir.exists()){
				dir.mkdirs();
			};
			String fileName = path.substring(path.lastIndexOf("/"),path.length());
			File file = new File(root,fileName);  //SDCardRoot,"1.jpg"
			return file.exists();
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			mProgressBar.setVisibility(View.GONE);
		}
		
	}

	/***************************************** Code to align Arc Menu ***************************************/
	private void alignArcMenu() {
		isActyArcMeasureReady = isStopArcMeasureReady = isControlLayoutMeasureReady = false;
		controlLayout = (FrameLayout) mRootView
				.findViewById(com.capricorn.R.id.control_layout);

		ViewTreeObserver viewTreeObserverControlLayout = controlLayout
				.getViewTreeObserver();
		if (viewTreeObserverControlLayout.isAlive()) {
			viewTreeObserverControlLayout
			.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					controlLayout.getViewTreeObserver()
					.removeGlobalOnLayoutListener(this);
					controlStopLayoutWidth = controlActyLayoutWidth = controlLayout
							.getWidth();
					controlStopLayoutHeight = controlActyLayoutHeight = controlLayout
							.getHeight();
					isControlLayoutMeasureReady = true;

					mActivity.runOnUiThread(runnable);

				}
			});
		}

		ViewTreeObserver viewTreeObserver = mActivitiesFilter
				.getViewTreeObserver();
		if (viewTreeObserver.isAlive()) {
			viewTreeObserver
			.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					mActivitiesFilter.getViewTreeObserver()
					.removeGlobalOnLayoutListener(this);
					arcWidthActy = mActivitiesFilter.getWidth();
					arcHeightActy = mActivitiesFilter.getHeight();
					isActyArcMeasureReady = true;

					mActivity.runOnUiThread(runnable);
				}
			});
		}

		ViewTreeObserver viewStopObserver = mStopsFilter.getViewTreeObserver();
		if (viewStopObserver.isAlive()) {
			viewStopObserver
			.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					mStopsFilter.getViewTreeObserver()
					.removeGlobalOnLayoutListener(this);
					arcWidthStop = mStopsFilter.getWidth();
					arcHeightStop = mStopsFilter.getHeight();
					isStopArcMeasureReady = true;

					mActivity.runOnUiThread(runnableStops);
				}
			});
		}
	}

	FrameLayout controlLayout; // the frame layout of the control from library
	int arcWidthActy, arcHeightActy, arcWidthStop, arcHeightStop; // arc menu
	// measurments
	// to be
	// calculated
	// ,
	// required
	// for
	// margins
	int controlActyLayoutWidth, controlActyLayoutHeight,
	controlStopLayoutWidth, controlStopLayoutHeight; // controlLayout
	// measurments
	// to be
	// calculated ,
	// required for
	// margins
	boolean isActyArcMeasureReady, isStopArcMeasureReady,
	isControlLayoutMeasureReady; // determine if measurements are ready
	// if so apply the margins with the
	// following runnable

	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			if (isActyArcMeasureReady && isControlLayoutMeasureReady) {
				RelativeLayout.LayoutParams params = (LayoutParams) mActivitiesFilter
						.getLayoutParams();
				int leftMargin = -((arcWidthActy - controlActyLayoutWidth) / 2);
				int bottomMargin = -((arcHeightActy - controlActyLayoutHeight) / 2);
				params.setMargins(leftMargin, 0, 0, bottomMargin);
				mActivitiesFilter.setLayoutParams(params);
			}
		}
	};

	Runnable runnableStops = new Runnable() {

		@Override
		public void run() {
			if (isStopArcMeasureReady && isControlLayoutMeasureReady) {
				RelativeLayout.LayoutParams params = (LayoutParams) mStopsFilter
						.getLayoutParams();
				int leftMargin = -((arcWidthStop - controlStopLayoutWidth) / 2);
				int bottomMargin = -((arcHeightStop - controlStopLayoutHeight) / 2);
				params.setMargins(leftMargin, 0, 0, bottomMargin);
				mStopsFilter.setLayoutParams(params);
			}
		}
	};
	/************************************************* ENDS *************************************************/

	private CompoundButton.OnCheckedChangeListener 
	mCurrentLocationChangedListener = new CompoundButton.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton checkboxView,
				boolean isChecked) {

			if (isChecked) {
				Toast.makeText(mActivity, "Starting Location Search...",
						Toast.LENGTH_SHORT).show();
				mLocationManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, 0, 1000,
						mLocationListener);
				setAudioMarkers();
			} else {
				Toast.makeText(mActivity, "Stopping Location Search...",
						Toast.LENGTH_SHORT).show();
				mLocationManager.removeUpdates(mLocationListener);
				mOverlayItemArray.clear();
				// audioMarkers.removeAllItems();
			}
		}
	};

	private void initArcMenu(ArcMenu menu, final int[] itemDrawables) {
		final int itemCount = itemDrawables.length;
		for (int i = 0; i < itemCount; i++) {
			ImageView item = new ImageView(mActivity);
			item.setImageResource(itemDrawables[i]);
			item.setTag(itemDrawables[i]);

			final int position = i;
			menu.addItem(item, new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// Toast.makeText(mActivity, "position:" + position +
					// "drawable :"+itemDrawables[position],
					// Toast.LENGTH_SHORT).show();
					if (osmMap != null)
						getPOIAsync(itemDrawables[position]);/*
						 * Toast.makeText(
						 * mActivity, text,
						 * duration)
						 */
				}
			});
		}
	}

	private static final int[] ACTIVITY_DRAWABLES = { R.drawable.map_paidbtn,
		R.drawable.map_attractbtn, R.drawable.map_foodbtn,
		R.drawable.map_accomodbtn, R.drawable.map_otherbtn };

	private static final int[] STOP_DRAWABLES = { R.drawable.map_lookoutbtn,
		R.drawable.map_picnicbtn, R.drawable.map_walkbtn,
		R.drawable.map_toiletbtn };

	public static ItemizedOverlayWithBubble<ExtendedOverlayItem> poiMarkers;
	public static PlacesOverlayWithBubble<ExtendedOverlayItem> placeMarkers;
	public static TouchToPlayOverlay<ExtendedOverlayItem> audioMarkers;

	private ArrayList<ExtendedOverlayItem> 
	mAudioItems = new ArrayList<ExtendedOverlayItem>();

	// ------------ Route and Directions

	protected ItemizedOverlayWithBubble<ExtendedOverlayItem> roadNodeMarkers;

	private void putRoadNodes(Road road) {
		/*
		 * roadNodeMarkers.removeAllItems(); Drawable marker =
		 * getResources().getDrawable(R.drawable.marker_node); int n =
		 * road.mNodes.size(); TypedArray iconIds =
		 * getResources().obtainTypedArray(R.array.direction_icons); for (int
		 * i=0; i<n; i++){ RoadNode node = road.mNodes.get(i); String
		 * instructions = (node.mInstructions==null ? "" : node.mInstructions);
		 * ExtendedOverlayItem nodeMarker = new ExtendedOverlayItem("Step " +
		 * (i+1), instructions,node.mLocation,
		 * mActivity.getApplicationContext());
		 * nodeMarker.setSubDescription(road.getLengthDurationText(node.mLength,
		 * node.mDuration));
		 * nodeMarker.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
		 * nodeMarker.setMarker(marker); int iconId =
		 * iconIds.getResourceId(node.mManeuverType, R.drawable.ic_empty); if
		 * (iconId != R.drawable.ic_empty){ Drawable icon =
		 * getResources().getDrawable(iconId); nodeMarker.setImage(icon); }
		 * roadNodeMarkers.addItem(nodeMarker); }
		 */
	}

	public void getRoadAsync() {
		try{
			for (String coordinates : mSegmentCoordinatesList) {
				new UpdateRoadTask().execute(coordinates);
			}
		} catch(final Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Async task to get the road in a separate thread.
	 */
	private class UpdateRoadTask extends AsyncTask<Object, Void, Road> {
		private List<Overlay> mapOverlays;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mapOverlays = osmMap.getOverlays();
			if (roadOverlay != null) {
				mapOverlays.remove(roadOverlay);
			}
		}

		protected Road doInBackground(Object... params) {
			// ArrayList<GeoPoint> waypoints = (ArrayList<GeoPoint>)params[0];
			ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>();
			// RoadManager roadManager = new OSRMRoadManager();//new
			// MapQuestRoadManager();//new GoogleRoadManager();

			String coordinates = (String) params[0];// mSegmentCoordinates.toString();

			String[] coordinatesArray = coordinates.split(" ");
			String[] values;
			for (int i = 0; i < coordinatesArray.length; i++) {

				values = coordinatesArray[i].split(",");

				/*
				 * Log.i("UpdateRoadTask", "Lat: "+values[1]+",");
				 * Log.i("UpdateRoadTask", "Long: "+values[0]+",");
				 * Log.i("UpdateRoadTask", "Alt: "+values[2]+",");
				 */

				waypoints.add(new GeoPoint(Double.parseDouble(values[1]),
						Double.parseDouble(values[0]), Double
						.parseDouble(values[2])));
			}

			StringBuffer latStringBuffer = new StringBuffer();
			for (int i = 0; i < coordinatesArray.length; i++) {
				values = coordinatesArray[i].split(",");
				latStringBuffer.append(values[1] + ",");
			}
			// Log.i("UpdateRoadTask", "Lat: "+latStringBuffer.toString());

			StringBuffer longStringBuffer = new StringBuffer();
			for (int i = 0; i < coordinatesArray.length; i++) {
				values = coordinatesArray[i].split(",");
				longStringBuffer.append(values[0] + ",");
			}
			// Log.i("UpdateRoadTask", "Long: "+longStringBuffer.toString());

			StringBuffer altStringBuffer = new StringBuffer();
			for (int i = 0; i < coordinatesArray.length; i++) {
				values = coordinatesArray[i].split(",");
				altStringBuffer.append(values[2] + ",");
			}
			// Log.i("UpdateRoadTask", "Alt: "+altStringBuffer.toString());

			Road road = new Road(waypoints);
			return road;
		}

		protected void onPostExecute(Road result) {
			mRoad = result;
			updateUIWithRoad1(result);
		}

		private void updateUIWithRoad1(Road road) {
			if (road == null)
				return;
			/*
			 * if (road.mStatus == Road.STATUS_DEFAULT)
			 * Toast.makeText(osmMap.getContext(),
			 * "We have a problem to get the route", Toast.LENGTH_SHORT).show();
			 */

			roadOverlay = RoadManager.buildRoadOverlay(road,
					osmMap.getContext());
			Overlay removedOverlay = mapOverlays.set(0, roadOverlay);
			// we set the road overlay at the "bottom", just above the
			// MapEventsOverlay,
			// to avoid covering the other overlays.
			mapOverlays.add(removedOverlay);
			putRoadNodes(road);
			osmMap.invalidate();
		}
	}

	protected ArrayList<GeoPoint> viaPoints = new ArrayList<GeoPoint>();
	public static SmartMapView osmMap;
	protected Road mRoad;
	protected PathOverlay roadOverlay;

	private final LocationListener mLocationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			updateLoc(location);
			Toast.makeText(mActivity, "Location : " + location,
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	private void updateLoc(Location loc) {
		if (osmMap != null) {
			GeoPoint locGeoPoint = new GeoPoint(loc.getLatitude(),
					loc.getLongitude());
			osmMap.getController().setCenter(locGeoPoint);
			setOverlayLoc(loc);
			osmMap.invalidate();
		}
	}

	private void setOverlayLoc(Location overlayloc) {
		GeoPoint overlocGeoPoint = new GeoPoint(overlayloc);
		// ---
		mOverlayItemArray.clear();
		OverlayItem newMyLocationItem = new OverlayItem("My Location",
				"My Location", overlocGeoPoint);
		mOverlayItemArray.add(newMyLocationItem);
		// ---
	}

	private class MyItemizedIconOverlay extends
	ItemizedIconOverlay<OverlayItem> {
		//org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener
		public MyItemizedIconOverlay(
				List<OverlayItem> pList,
				OnItemGestureListener<OverlayItem> pOnItemGestureListener,
				ResourceProxy pResourceProxy) {
			super(pList, pOnItemGestureListener, pResourceProxy);
		}

		@Override
		public void draw(Canvas canvas, org.osmdroid.views.MapView mapview,
				boolean arg2) {
			// TODO Auto-generated method stub
			super.draw(canvas, mapview, arg2);

			if (!mOverlayItemArray.isEmpty()) {

				// overlayItemArray have only ONE element only, so I hard code
				// to get(0)
				GeoPoint in = mOverlayItemArray.get(0).getPoint();

				Point out = new Point();
				osmMap.getProjection().toPixels(in, out);

				Bitmap bm = BitmapFactory.decodeResource(SmartTours
						.getContext().getResources(), R.drawable.i_am_here_1);
				canvas.drawBitmap(bm, out.x - bm.getWidth() / 2, // shift the
						// bitmap
						// center
						out.y - bm.getHeight() / 2, // shift the bitmap center
						null);
			}
		}

		@Override
		public boolean onSingleTapUp(MotionEvent event, MapView mapView) {
			// TODO Auto-generated method stub
			// return super.onSingleTapUp(event, mapView);
			return false;
		}
	}

	ArrayList<MBTilesFileArchive> mbTilesFileArchives;

	/*
	 * @Override public void onPause() { for (int i =
	 * mbTilesFileArchives.size()-1; i >= 0; i--) {
	 * mbTilesFileArchives.remove(i); } super.onPause(); }
	 */

	@Override
	public void onDetach() {
		if (mbTilesFileArchives != null) {
			for (int i = mbTilesFileArchives.size() - 1; i >= 0; i--) {
				mbTilesFileArchives.remove(i);
			}
			mbTilesFileArchives.clear();
			if (mProvider != null)
				mProvider.detach();
		}
		super.onDetach();
	}

	class FetchMBTilesTask extends AsyncTask<String, String, Boolean> {

		Double latitude = null;
		Double longitude = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			boolean isFetched = false;

			mbTilesFileArchives = new ArrayList<MBTilesFileArchive>();

			//Adding Base Map
			File fileBase = new File(AppConstants.ROOT_DIRECTORY,AppConstants.BASE_MBTILE);
			if(fileBase.exists()){ mbTilesFileArchives.add(
					MBTilesFileArchive.getDatabaseFileArchive( fileBase ) ); }


			for (String segmentId : mSegmentsList) {
				for (Segment segment : DatabaseHelper.getInstance()
						.fetchSegment(segmentId)) {
					Log.i("Map",
							"segment.getMbTilesLocal(): "
									+ segment.getMbTilesLocal());
					File file = new File(AppConstants.ROOT_DIRECTORY,
							segment.getMbTilesLocal());
					if (latitude == null && longitude == null) {
						latitude = segment.getLatitudeInDouble();
						longitude = segment.getLongitudeInDouble();
					}
					if (file.exists()) {
						mbTilesFileArchives.add(MBTilesFileArchive
								.getDatabaseFileArchive(file));
					}

					switch (mMapMode) {
					case ROUTE:
						mSegmentCoordinatesList.add(segment.getCoordinates());
						break;
					default:
						break;
					}
				}

			}

			mResourceProxy = new DefaultResourceProxyImpl(
					SmartTours.getContext());
			SimpleRegisterReceiver simpleReceiver = new SimpleRegisterReceiver(
					SmartTours.getContext());

			if (mbTilesFileArchives.size() > 0) {
				IArchiveFile[] files = (IArchiveFile[]) mbTilesFileArchives
						.toArray(new IArchiveFile[mbTilesFileArchives.size()]);
				MapTileModuleProviderBase moduleProvider = new MapTileFileArchiveProvider(
						simpleReceiver, MBTILESRENDER, files);
				mProvider = new MapTileProviderArray(MBTILESRENDER, null,
						new MapTileModuleProviderBase[] { moduleProvider });
				isFetched = true;
			}

			switch (mMapMode) {
			case ROUTE:

				break;

			case SEGMENT:
				if(mSegmentId!=null){
					for(Segment segment:DatabaseHelper.getInstance().fetchSegment(mSegmentId)){
						latitude = segment.getLatitudeInDouble();
						longitude = segment.getLongitudeInDouble();
					}
				}
				break;

			case ACTIVITY:
				if(mCurrentActivityId!=null){
					Activity activity= DatabaseHelper.getInstance().fetchActivityfromActyId(mCurrentActivityId);
					latitude = activity.getLatitudeInDouble();
					longitude = activity.getLongitudeInDouble();
				}
				break;

			case STOP:
				if(mCurrentStopId!=null){
					Stops stop = DatabaseHelper.getInstance().fetchStop(mCurrentStopId);
					latitude = stop.getLatitudeInDouble();
					longitude = stop.getLongitudeInDouble();
				}
				break;
			default:
				break;
			}

			return isFetched;
		}

		@Override
		protected void onPostExecute(Boolean isFetched) {
			super.onPostExecute(isFetched);

			if (isFetched) {
				osmMap = new SmartMapView(SmartTours.getContext(), 256,
						mResourceProxy, mProvider);
			}
			mapEndConfig(latitude, longitude);
		}

	}

	// Most of this is useless
	private XYTileSource MBTILESRENDER = new XYTileSource("mbtiles",
			ResourceProxy.string.offline_mode, 11, 16, // zoom min/max <- should
			// be taken from
			// metadata if available
			256, ".png", new String[] { "http://i.dont.care.org/" });

	// private MapView mOsmv;
	private ResourceProxy mResourceProxy;

	private MapTileProviderArray mProvider;

	private static final ITileSource TILE_SOURCE_SPEC = TileSourceFactory.MAPQUESTOSM;

	private boolean mapBeginConfig() {
		FetchMBTilesTask task = new FetchMBTilesTask();
		task.execute();
		return true;
	}




	private synchronized void mapEndConfig(double latitude, double longitude) {
		if (osmMap != null) {
			osmMap.setBuiltInZoomControls(true);
			osmMap.setMultiTouchControls(true);

			osmMap.getController().setZoom(AppConstants.DEFAULT_ZOOM_LEVEL);
			// IGeoPoint point = new GeoPoint(-45.878776,170.59499); // lat lon
			// and not inverse
			IGeoPoint point = new GeoPoint(latitude, longitude);
			osmMap.getController().setCenter(point);

			loadUI();
		}
	}

	private void loadUI() {

		RelativeLayout rl = (RelativeLayout) mRootView
				.findViewById(R.id.mapLayout);

		osmMap.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT));
		rl.addView(osmMap);

		final ArrayList<ExtendedOverlayItem> poiItems = new ArrayList<ExtendedOverlayItem>();
		poiMarkers = new ItemizedOverlayWithBubble<ExtendedOverlayItem>(
				mActivity, poiItems, osmMap);// , new POIInfoWindow(osmMap)
		osmMap.getOverlays().add(poiMarkers);
		poiMarkers.removeAllItems();

		final ArrayList<ExtendedOverlayItem> audItems = new ArrayList<ExtendedOverlayItem>();
		audioMarkers = new TouchToPlayOverlay<ExtendedOverlayItem>(
				mActivity, audItems, osmMap, this); // , new POIInfoWindow(osmMap)
		osmMap.getOverlays().add(audioMarkers);
		audioMarkers.removeAllItems();

		final ArrayList<ExtendedOverlayItem> placeItems = new ArrayList<ExtendedOverlayItem>();
		placeMarkers = new PlacesOverlayWithBubble<ExtendedOverlayItem>(
				mActivity, placeItems, osmMap, this); // , new POIInfoWindow(osmMap)
		osmMap.getOverlays().add(placeMarkers);
		placeMarkers.removeAllItems();
		/**************************************** DRAW ROUTES ********************************************/
		switch (mMapMode) {
		case ROUTE:
			setPlaceMarkers(); // Setting Place Marker
			getRoadAsync(); // Drawing Route
			break;

		case SEGMENT:
			setPlaceMarkers(); // Setting Place Marker
			break;

		default:
			break;
		}
		/*
		 * if(!mIsPlaceMap) getRoadAsync(); //To Draw Routes
		 *//***********************************************************************************************/

		DefaultResourceProxyImpl defaultResourceProxyImpl = new DefaultResourceProxyImpl(
				mActivity);
		MyItemizedIconOverlay myItemizedIconOverlay = new MyItemizedIconOverlay(
				mOverlayItemArray, null, defaultResourceProxyImpl);
		osmMap.getOverlays().add(myItemizedIconOverlay);


		switch (mMapMode) { 
		case ROUTE: 
			getPOIAsync(-1); //To draw all POI's
			break;

		case SEGMENT: 
			//getPOIAsync(-1); //To draw all POI's by default 
			break;

		case STOP: 
			Stops stops =
			DatabaseHelper.getInstance().fetchStop(mCurrentStopId); 
			int tag = getTagFromType(stops.getType()); 
			getPOIAsync(tag); //To draw all POI's by default 
			break;

		case ACTIVITY: 
			Activity activity = DatabaseHelper.getInstance().fetchActivityfromActyId
			(mCurrentActivityId); 
			tag = getTagFromType(activity.getType());
			getPOIAsync(tag); 
			break;

		default: 
			break; 
		}
		mProgressBar.setVisibility(View.INVISIBLE);
	}

	private int getTagFromType(String type) {
		if (type.equals(getResources().getString(R.string.type_acty_paid)))
			return R.drawable.map_paidbtn;
		else if (type.equals(getResources().getString(R.string.type_acty_free)))
			return R.drawable.map_attractbtn;
		else if (type.equals(getResources().getString(R.string.type_acty_food)))
			return R.drawable.map_foodbtn;
		else if (type.equals(getResources().getString(R.string.type_acty_accm)))
			return R.drawable.map_accomodbtn;
		else if (type
				.equals(getResources().getString(R.string.type_acty_other)))
			return R.drawable.map_otherbtn;
		else if (type.equals(getResources().getString(
				R.string.type_stop_lookout)))
			return R.drawable.map_lookoutbtn;
		else if (type.equals(getResources().getString(R.string.type_stop_walk)))
			return R.drawable.map_walkbtn;
		else if (type.equals(getResources()
				.getString(R.string.type_stop_toilet)))
			return R.drawable.map_toiletbtn;
		else if (type.equals(getResources()
				.getString(R.string.type_stop_picnic)))
			return R.drawable.map_picnicbtn;
		return 0;
	}

	/********************************* CODE TO PUT MARKERS ************************************/
	/**************************************** START *******************************************/
	void getPOIAsync(int tag) {
		// poiMarkers.removeAllItems(); //TODO: Put it inside onPreExecute
		new POITask().execute(tag);
	}

	private class POITask extends AsyncTask<Object, Void, ArrayList<POI>> {
		Integer mTag;
		String type = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			poiMarkers.removeAllItems();
		}

		protected ArrayList<POI> doInBackground(Object... params) {

			ArrayList<POI> pois = new ArrayList<POI>();
			mTag = (Integer) params[0];

			if (mTag == null)
				return pois;

			if (mTag == -1) {
				for (String segmentId : mSegmentsList) {
					for (Stops stop : DatabaseHelper.getInstance().fetchStops(
							segmentId)) {
						POI poi = null;
						if (stop.getType().equals("Lookout")) {
							poi = new POI(POI.LOOK_OUT);
						} else if (stop.getType().equals("Toilet")) {
							poi = new POI(POI.TOILET);
						} else if (stop.getType().equals("Walk")) {
							poi = new POI(POI.WALKS);
						} else if (stop.getType().equals("Picnic")) {
							poi = new POI(POI.PICNIC);
						}
						if (poi != null) {
							poi.mLocation = new GeoPoint(
									Double.parseDouble(stop.getLatitude()),
									Double.parseDouble(stop.getLongitude())); // (45.02876,168.65623);
							poi.mCategory = stop.getType();
							poi.mType = stop.getName(); // "Sample Type";
							poi.mDescription = stop.getHighlights(); // "Sample Summary";
							poi.mThumbnailPath = stop.getImageLocal(); // "Sample thumbnail";
							pois.add(poi);
						}
					}

					for (Activity activity : DatabaseHelper.getInstance(
							mActivity).fetchActivity(segmentId)) {
						POI poi = null;
						if (activity.getType().equals("Paid Activity")) {
							poi = new POI(POI.PAID_ACTIVITIES);
						} else if (activity.getType().equals("Free Activity")) {
							poi = new POI(POI.ATTRACTIONS);
						} else if (activity.getType().equals("Food & Drink")) {
							poi = new POI(POI.FOOD_AND_DRINK);
						} else if (activity.getType().equals("Accommodation")) {
							poi = new POI(POI.ACCOMODATIONS);
						} else if (activity.getType().equals("Other")) {
							poi = new POI(POI.OTHER);
						}
						if (poi != null) {
							poi.mLocation = new GeoPoint(
									Double.parseDouble(activity.getLatitude()),
									Double.parseDouble(activity.getLongitude())); // (45.02876,168.65623);
							poi.mCategory = activity.getType();
							poi.mType = activity.getTitle(); // "Sample Type";
							poi.mDescription = activity.getHighlights(); // "Sample Summary";
							poi.mThumbnailPath = activity.getImageLocal(); // "Sample thumbnail";
							pois.add(poi);
						}
					}
				}
			} else {
				switch (mTag) {
				case R.drawable.map_paidbtn: {
					type = "Paid Activity";
					break;
				}
				case R.drawable.map_attractbtn: {
					type = "Free Activity";
					break;
				}
				case R.drawable.map_foodbtn: {
					type = "Food & Drink";
					break;
				}
				case R.drawable.map_accomodbtn: {
					type = "Accommodation";
					break;
				}
				case R.drawable.map_otherbtn: {
					type = "Other";
					break;
				}
				case R.drawable.map_lookoutbtn: {
					type = "Lookout";
					break;
				}
				case R.drawable.map_picnicbtn: {
					type = "Picnic";
					break;
				}
				case R.drawable.map_walkbtn: {
					type = "Walk";
					break;
				}
				case R.drawable.map_toiletbtn: {
					type = "Toilet";
					break;
				}
				}

				if (type == null)
					return null;
				if (type.equals("Lookout") || type.equals("Picnic")
						|| type.equals("Walk") || type.equals("Toilet")) {

					for (String segmentId : mSegmentsList) {
						ArrayList<Stops> stopsList = null;
						switch (mMapMode) {
						case ROUTE:
							stopsList = DatabaseHelper.getInstance()
							.fetchStops(segmentId, type);
							break;
						case SEGMENT:
							stopsList = DatabaseHelper.getInstance()
							.fetchStops(segmentId, type);
							break;
						case STOP:
							stopsList = new ArrayList<Stops>();
							stopsList.add(DatabaseHelper.getInstance()
									.fetchStop(mCurrentStopId));
							break;
						default:
							break;
						}
						if (stopsList != null)
							for (Stops stops : stopsList) {
								POI poi = null;
								switch (mTag) {
								case R.drawable.map_lookoutbtn: {
									poi = new POI(POI.LOOK_OUT);
									break;
								}
								case R.drawable.map_picnicbtn: {
									poi = new POI(POI.PICNIC);
									break;
								}
								case R.drawable.map_walkbtn: {
									poi = new POI(POI.WALKS);
									break;
								}
								case R.drawable.map_toiletbtn: {
									poi = new POI(POI.TOILET);
									break;
								}
								}
								if (poi != null) {
									poi.mLocation = new GeoPoint(
											Double.parseDouble(stops
													.getLatitude()),
													Double.parseDouble(stops
															.getLongitude()));
									poi.mType = stops.getName();
									poi.mThumbnailPath = stops.getImageLocal();
									poi.mCategory = stops.getType();

									if (!TextUtils.isEmpty(stops
											.getHighlights())) {
										poi.mDescription = stops
												.getHighlights();
										if (poi.mDescription.length() > 25)
											poi.mDescription = poi.mDescription
											.substring(0, 25) + "...";
									}
									pois.add(poi);
								}
							}
					}
				} else {
					for (String segmentId : mSegmentsList) {
						ArrayList<com.doodleblue.smarttours.model.Activity> activityList = null;
						switch (mMapMode) {
						case ACTIVITY:
							activityList = new ArrayList<Activity>();
							activityList
							.add(DatabaseHelper.getInstance()
									.fetchActivityfromActyId(
											mCurrentActivityId));
							break;
						case ROUTE:
							activityList = DatabaseHelper.getInstance()
							.fetchActivity(segmentId, type);
							break;
						case SEGMENT:
							activityList = DatabaseHelper.getInstance()
							.fetchActivity(segmentId, type);
							break;
						default:
							break;
						}
						if (activityList != null)
							for (com.doodleblue.smarttours.model.Activity activity : activityList) {
								POI poi = null;
								switch (mTag) {
								case R.drawable.map_paidbtn: {
									poi = new POI(POI.PAID_ACTIVITIES);
									break;
								}
								case R.drawable.map_attractbtn: {
									poi = new POI(POI.ATTRACTIONS);
									break;
								}
								case R.drawable.map_foodbtn: {
									poi = new POI(POI.FOOD_AND_DRINK);
									break;
								}
								case R.drawable.map_accomodbtn: {
									poi = new POI(POI.ACCOMODATIONS);
									break;
								}
								case R.drawable.map_otherbtn: {
									poi = new POI(POI.OTHER);
									break;
								}
								}
								if (poi != null) {
									poi.mLocation = new GeoPoint(
											Double.parseDouble(activity
													.getLatitude()),
													Double.parseDouble(activity
															.getLongitude()));
									poi.mType = activity.getTitle();
									poi.mCategory = activity.getType();
									poi.mThumbnailPath = activity.getImageLocal();
									if (!TextUtils.isEmpty(activity
											.getHighlights())) {
										poi.mDescription = activity
												.getHighlights();
										if (poi.mDescription.length() > 25)
											poi.mDescription = poi.mDescription
											.substring(0, 25) + "...";
									}

									pois.add(poi);
								}
							}
					}
				}
			}
			return pois;
		}

		protected void onPostExecute(ArrayList<POI> pois) {
			mPOIs = pois;
			if (mTag.equals("")) {
				// no search, no message
			} else if (mPOIs == null) {
				Toast.makeText(mActivity,
						"Technical issue when getting " + mTag + " POI.",
						Toast.LENGTH_LONG).show();
			}
			if (!TextUtils.isEmpty(type)) {
				Toast.makeText(mActivity, type, Toast.LENGTH_LONG).show();
			}
			updateUIWithPOI(mPOIs);
		}
	}

	void updateUIWithPOI(ArrayList<POI> pois) {
		if (pois != null) {
			for (POI poi : pois) {

				Drawable marker = null;

				switch (poi.mServiceId) {
				case POI.PAID_ACTIVITIES: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_paid_img);
					break;
				}
				case POI.ATTRACTIONS: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_attract_img);
					break;
				}
				case POI.FOOD_AND_DRINK: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_food_img);
					break;
				}
				case POI.ACCOMODATIONS: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_accomod_img);
					break;
				}
				case POI.OTHER: {
					marker = getResources()
							.getDrawable(R.drawable.map_otherbtn);
					break;
				}
				case POI.LOOK_OUT: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_lookout_img);
					break;
				}
				case POI.PICNIC: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_picnic_img);
					break;
				}
				case POI.WALKS: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_walks_img);
					break;
				}
				case POI.TOILET: {
					marker = SmartTours.getContext().getResources()
							.getDrawable(R.drawable.map_toilet_img);
					break;
				}
				}
				ExtendedOverlayItem poiMarker = new ExtendedOverlayItem(
						poi.mType, poi.mDescription, poi.mLocation, mActivity);
				poiMarker.setSubDescription(poi.mCategory);
				poiMarker.setMarker(marker);
				poiMarker.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
				// thumbnail loading moved in POIInfoWindow.onOpen for better
				// performances.
				poiMarker.setRelatedObject(poi);

				if (!TextUtils.isEmpty(poi.mThumbnailPath)) {
					Log.e(TAG, "mThumbnailPath: " + poi.mThumbnailPath);
					Drawable thumbImage = new BitmapDrawable(getResources(),
							Utils.getImageFromString(poi.mThumbnailPath));
					poiMarker.setImage(thumbImage);
				}



				int count = 0;
				switch (osmMap.getZoomLevel()) {
				case 10:
					count = 10;
					break;
				case 11:
					count = 15;
					break;
				case 12:
					count = 20;
					break;
				case 13:
					count = 25;
					break;
				case 14:
					count = 30;
					break;
				case 15:
					count = 35;
					break;
				case 16:
					count = 40;
					break;
				case 17:
					count = 45;
					break;
				case 18:
					count = 50;
					break;
				default:
					count = 1;
					break;
				}
				if (osmMap.getBoundingBox().contains(poi.mLocation))
					if (poiMarkers != null) {
						if (poiMarkers.size() < count)
							poiMarkers.addItem(poiMarker);
						else
							break;
					}
			}
		}
		osmMap.invalidate();

	}

	/**************************************** END *******************************************/

	private class DownloadReceiver extends ResultReceiver {
		public DownloadReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultCode == MbtilesDownloadService.UPDATE_PROGRESS) {
				
				int progress = resultData.getInt("progress");
				mSegmentsList = resultData
						.getStringArrayList("segmentIDList");

				mProgressBar.setVisibility(View.VISIBLE);
				mProgressBar.setProgress(progress);
				if (progress >= 100) {
					//if (!mIsMapConfigured) {

					mIsMapConfigured = mapBeginConfig();

					if(mRouteId != null) {
						Intent intent = new Intent(SmartTours.getContext(),
								GuideControllableAudioService.class);
						intent.setAction(Actions.PLAY_AUDIO);
						intent.putExtra("routeId", mRouteId);
						intent.putExtra("receiver",
								new AudioPointsReceiver(new Handler()));
						SmartTours.getContext().startService(intent);
					}
					//}
				}
			}
		}
	}

	private void setAudioMarkers() {
		// audioMarkers.removeAllItems();
		AudioPointMarkerTask audioPointMarkerTask = new AudioPointMarkerTask();
		audioPointMarkerTask.execute();
	}

	private void setPlaceMarkers() {

		PlaceMarkerTask task = new PlaceMarkerTask();
		task.execute();
	}

	private class AudioPointsReceiver extends ResultReceiver {

		public AudioPointsReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultCode == GuideControllableAudioService.UPDATE_AUDIO_POINTS) {

			}
		}

	}

	private class AudioPointMarkerTask extends
	AsyncTask<String, Void, ArrayList<POI>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			audioMarkers.removeAllItems();
			mProgressBar.setVisibility(View.VISIBLE);

		}

		@Override
		protected ArrayList<POI> doInBackground(String... params) {
			ArrayList<POI> audioMarkerList = new ArrayList<POI>();
			ArrayList<Opus> opuses = mTracker.getmOpuses();
			for (Opus opus : opuses) {
				POI poi = new POI(POI.AUDIO);
				poi.mLocation = new GeoPoint(opus.getLatitudeInDouble(),
						opus.getLongitudeInDouble());
				poi.mCategory = "Sample Category";
				poi.mType = "audio";
				poi.mDescription = "";
				poi.mThumbnailPath = "";
				audioMarkerList.add(poi);
			}
			return audioMarkerList;
		}

		@Override
		protected void onPostExecute(ArrayList<POI> result) {

			super.onPostExecute(result);
			mAudioMarkers = result;
			for (POI poi : mAudioMarkers) {
				//if (osmMap.getBoundingBox().contains(poi.mLocation)) {
				ExtendedOverlayItem extendedOverlayItem = new ExtendedOverlayItem(
						poi.mType, poi.mDescription, poi.mLocation,
						mActivity);
				extendedOverlayItem.setSubDescription(poi.mCategory);
				extendedOverlayItem.setMarker(SmartTours.getContext()
						.getResources()
						.getDrawable(R.drawable.map_info_img));
				extendedOverlayItem
				.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
				extendedOverlayItem.setRelatedObject(poi);
				audioMarkers.addItem(extendedOverlayItem);
				//}
			}
			// audioMarkers.size();
			osmMap.invalidate();
			mProgressBar.setVisibility(View.GONE);
		}

	}

	private class PlaceMarkerTask extends
	AsyncTask<String, Void, ArrayList<POI>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressBar.setVisibility(View.VISIBLE);
			placeMarkers.removeAllItems();
		}

		@Override
		protected ArrayList<POI> doInBackground(String... params) {

			ArrayList<POI> markerList = new ArrayList<POI>();
			for (String segmentID : mSegmentsList) {
				for (Segment segment : DatabaseHelper.getInstance()
						.fetchSegment(segmentID)) {
					if (segment.isPlace().equals("TRUE")) {
						POI poi = new POI(POI.PLACE);
						poi.mLocation = new GeoPoint(
								segment.getLatitudeInDouble(),
								segment.getLongitudeInDouble());
						markerList.add(poi);
						ActivityCount activityCount = DatabaseHelper
								.getInstance(mActivity).fetchActivityCount(
										segment.getId());
						poi.mCategory = segment.getName();
						poi.mDescription = segment.getAudioLocal();
						poi.mThumbnailPath = segment.getImageLocal();
						poi.mString1 = "Paid Activities: "
								+ activityCount.getnPaidActivity();
						poi.mString2 = "Free Activities: "
								+ activityCount.getnAttraction();
						poi.mString3 = "Food & Drink: "
								+ activityCount.getnFoodAndDrink();
						poi.mString4 = "Accomodations: "
								+ activityCount.getnAccomodation();
						poi.mString5 = "Others: " + activityCount.getnOther();
						markerList.add(poi);
					}
				}
			}
			return markerList;
		}

		@Override
		protected void onPostExecute(ArrayList<POI> result) {

			super.onPostExecute(result);
			mPlaceMarkers = result;
			for (POI poi : mPlaceMarkers) {
				ExtendedOverlayItem extendedOverlayItem = new ExtendedOverlayItem(
						poi.mType, poi.mDescription, poi.mLocation,
						mActivity);
				extendedOverlayItem.setSubDescription(poi.mCategory);
				extendedOverlayItem.setMarker(SmartTours.getContext()
						.getResources()
						.getDrawable(R.drawable.map_info_img));
				extendedOverlayItem
				.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
				extendedOverlayItem.setRelatedObject(poi);

				if (!TextUtils.isEmpty(poi.mThumbnailPath)) {
					Log.e(TAG, "mThumbnailPath: " + poi.mThumbnailPath);
					Drawable thumbImage = new BitmapDrawable(
							getResources(),
							Utils.getImageFromString(poi.mThumbnailPath));
					extendedOverlayItem.setImage(thumbImage);
				}
				extendedOverlayItem.setTitle(poi.mCategory); // used as
				// title
				extendedOverlayItem.setDescription(poi.mDescription); // used
				// as
				// opus
				// file
				// path

				extendedOverlayItem.setmString1(poi.mString1);
				extendedOverlayItem.setmString2(poi.mString2);
				extendedOverlayItem.setmString3(poi.mString3);
				extendedOverlayItem.setmString4(poi.mString4);
				extendedOverlayItem.setmString5(poi.mString5);
				placeMarkers.addItem(extendedOverlayItem);
			}
			osmMap.invalidate();
			mProgressBar.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		
	}

	private OpusManager mOpusPlayer;
	private PlayerState mPlayerState = null;
	private Segment mCurrentAudioAccessingSegment;
	private String mCurrentPlayingAudioLocalPath;


	@Override
	public void onButtonPressed(String path) {
		onAudioButtonPress(path);
	}

	private synchronized void onAudioButtonPress(String path){
		if(TextUtils.isEmpty(mCurrentPlayingAudioLocalPath)){
			mCurrentPlayingAudioLocalPath = path;
		}
		//Toast.makeText(mActivity, "<-Audio Path-> : "+path, Toast.LENGTH_SHORT).show();

		for(Segment segment:DatabaseHelper.getInstance().fetchSegment(DatabaseHelper.KEY_AUDIO_LOCAL, path)){
			mCurrentAudioAccessingSegment = segment;
		}

		switch (mPlayerState) {
		case PLAYING:
			mOpusPlayer.stopPlaying();
			if(!mCurrentPlayingAudioLocalPath.equals(path)){
				if(mCurrentAudioAccessingSegment!=null){
					playSong(
							mCurrentAudioAccessingSegment.getId(),
							mCurrentAudioAccessingSegment.getAudio(),
							mCurrentAudioAccessingSegment.getAudioLocal());
				}
			}
			break;

		case STOPPED:
			if(mCurrentAudioAccessingSegment!=null){
				playSong(
						mCurrentAudioAccessingSegment.getId(),
						mCurrentAudioAccessingSegment.getAudio(),
						mCurrentAudioAccessingSegment.getAudioLocal());
			}
			break;

		case INITIALIZED:
			if(mCurrentAudioAccessingSegment!=null){
				playSong(
						mCurrentAudioAccessingSegment.getId(),
						mCurrentAudioAccessingSegment.getAudio(),
						mCurrentAudioAccessingSegment.getAudioLocal());
			}
			break;

		default:
			break;
		}
		mCurrentPlayingAudioLocalPath = path;
	}

	@Override
	public void onStateUpdate(int state) {
		//state 0-STOPPED, 1-PLAYING
		switch (state) {
		case 0:
			mPlayerState = PlayerState.STOPPED;
			break;
		case 1:
			mPlayerState = PlayerState.PLAYING;
			break;

		default:
			break;
		}
	}

	private void playSong(String id, String httpPath, String path){

		String audioPath = AppConstants.ROOT_DIRECTORY + path;
		File file = new File(audioPath);
		if(!file.exists()){
			if(SmartTours.isNetworkConnected()){
				PlayPlaceAudioTask task = new PlayPlaceAudioTask(mActivity);
				task.execute(new String[]{id, httpPath, path});
			} else {
				Toast.makeText(mActivity, getResources().getString(R.string.check_network),Toast.LENGTH_SHORT).show();
			}
		}
		else {
			playDownloadedAudio(path);
		}
	}

	private void playDownloadedAudio(String path) {

		String opusPath = AppConstants.ROOT_DIRECTORY + path;
		mOpusPlayer.startPlaying(opusPath);
	}

	public class PlayPlaceAudioTask extends AsyncTask<String,String,Object> {

		private Context mContext;
		private String path;

		public PlayPlaceAudioTask(Context context){
			mContext = context;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Object doInBackground(String... params) {

			String id 		= params[0];
			String httpPath = params[1];
			path 			= params[2];

			Utils.downloadItem(id, httpPath, path);

			return null;
		}


		@Override
		protected void onPostExecute(Object o) {
			super.onPostExecute(o);

			playDownloadedAudio(path);
		}
	}

	private void hideArcMenu(boolean flag) {
		if(flag){
			mActivitiesFilter.setVisibility(View.GONE);
			mStopsFilter.setVisibility(View.GONE);
		} else { 
			mActivitiesFilter.setVisibility(View.VISIBLE);
			mStopsFilter.setVisibility(View.VISIBLE);
		}
	}
}





















/*

//TODO: Move this into a AsyncTask
	private boolean mapBeginConfig1() {

		mbTilesFileArchives = new ArrayList<MBTilesFileArchive>();
		Double latitude = null;
		Double longitude = null;

		File fileBase = new File(AppConstants.ROOT_DIRECTORY,AppConstants.BASE_MBTILE);
		if(fileBase.exists()){ mbTilesFileArchives.add(
				MBTilesFileArchive.getDatabaseFileArchive( fileBase ) ); }


		for (String segmentId : mSegmentsList) {
			// mSegmentCoordinates = new StringBuffer();
			for (Segment segment : DatabaseHelper.getInstance().fetchSegment(
					segmentId)) {
				Log.i("Map",
						"segment.getMbTilesLocal(): "
								+ segment.getMbTilesLocal());
				File file = new File(AppConstants.ROOT_DIRECTORY,
						segment.getMbTilesLocal());
				if (latitude == null && longitude == null) {
					latitude = segment.getLatitudeInDouble();
					longitude = segment.getLongitudeInDouble();
				}
				if (file.exists()) {
					mbTilesFileArchives.add(MBTilesFileArchive
							.getDatabaseFileArchive(file));
				}

				// mSegmentCoordinates =
				// mSegmentCoordinates.append(segment.getCoordinates());

				switch (mMapMode) {
				case ROUTE:
					mSegmentCoordinatesList.add(segment.getCoordinates());
					break;

				default:
					break;
				}

 * if(!mIsPlaceMap)
 * mSegmentCoordinatesList.add(segment.getCoordinates());

			}

		}

		mResourceProxy = new DefaultResourceProxyImpl(SmartTours.getContext());
		SimpleRegisterReceiver simpleReceiver = new SimpleRegisterReceiver(
				SmartTours.getContext());

		if (mbTilesFileArchives.size() > 0) {
			IArchiveFile[] files = (IArchiveFile[]) mbTilesFileArchives
					.toArray(new IArchiveFile[mbTilesFileArchives.size()]);
			MapTileModuleProviderBase moduleProvider = new MapTileFileArchiveProvider(
					simpleReceiver, MBTILESRENDER, files);
			mProvider = new MapTileProviderArray(MBTILESRENDER, null,
					new MapTileModuleProviderBase[] { moduleProvider });
			osmMap = new SmartMapView(SmartTours.getContext(), 64,
					mResourceProxy, mProvider);
		}
		mapEndConfig(latitude, longitude);
		return true;
	}

 */