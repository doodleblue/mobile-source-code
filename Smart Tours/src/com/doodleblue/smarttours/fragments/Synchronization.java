package com.doodleblue.smarttours.fragments;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.doodleblue.smarttours.activity.MainActivity;
import com.doodleblue.smarttours.activity.SplashActivity;
import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.Activity;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Segment;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.service.SyncService;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.GetFont;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class Synchronization extends BaseContainerFragment implements View.OnClickListener{

	private ProgressBar mProgressBar;
	private Button mSyncButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView =  inflater.inflate(R.layout.sync, null);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	private void initView() {
		mSyncButton = (Button) mRootView.findViewById(R.id.sync);
		mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progressBar1);
		mSyncButton.setTypeface(GetFont.getInstance(getActivity()).getFont(GetFont.HELVITICA_TT));
		mSyncButton.setOnClickListener(this);
	}

	class SyncTask extends AsyncTask<String,String,String> {

		private Context mContext;

		public SyncTask(Context context){
			mContext = context;
			
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressBar.setVisibility(View.VISIBLE);
			mSyncButton.setEnabled(false);
		}

		@Override
		protected String doInBackground(String... params) {
			StringBuffer routeIds 	= new StringBuffer();
			StringBuffer segmentIds = new StringBuffer();
			StringBuffer stopIds 	= new StringBuffer();
			StringBuffer activityIds = new StringBuffer();

			String routeLast 	= DatabaseHelper.getInstance().fetchLastModifiedDate(DatabaseHelper.TABLE_ROUTE);
			String segmentLast	= DatabaseHelper.getInstance().fetchLastModifiedDate(DatabaseHelper.TABLE_SEGMENT);
			String stopLast 	= DatabaseHelper.getInstance().fetchLastModifiedDate(DatabaseHelper.TABLE_STOPS);
			String activityLast = DatabaseHelper.getInstance().fetchLastModifiedDate(DatabaseHelper.TABLE_ACTIVITY);

			//get All Route IDs
			for(Route route: DatabaseHelper.getInstance().fetchRoutes(null)){
				if(routeIds.length()!=0)
					routeIds.append(",");
				routeIds.append(route.getId().trim());
			}

			//get All Segment IDs
			for(Segment segment: DatabaseHelper.getInstance().fetchSegment(null)){
				if(segmentIds.length()!=0)
					segmentIds.append(",");
				segmentIds.append(segment.getId().trim());
			}

			//get All Stop IDs
			for(Stops stop: DatabaseHelper.getInstance().fetchStops(null)){
				if(stopIds.length()!=0)
					stopIds.append(",");
				stopIds.append(stop.getId().trim());
			}	

			//get All Activity IDs
			for(Activity activity: DatabaseHelper.getInstance().fetchActivity(null)){
				if(activityIds.length()!=0)
					activityIds.append(",");
				activityIds.append(activity.getId().trim());
			}	

			String url = AppConstants.SYNC_URL;
			try {
				url = url.replace("value1", URLEncoder.encode(segmentIds.toString(), "UTF-8")); //segment_id
				url = url.replace("value2", URLEncoder.encode(segmentLast, "UTF-8")); //segment_lastmodified
				url = url.replace("value3",URLEncoder.encode(activityIds.toString(), "UTF-8")); //activity_id
				url = url.replace("value4",URLEncoder.encode(activityLast, "UTF-8")); //activity_lastmodified
				url = url.replace("value5",URLEncoder.encode(stopIds.toString(), "UTF-8")); //stop_id
				url = url.replace("value6",URLEncoder.encode(stopLast, "UTF-8")); //stop_lastmodified
				url = url.replace("value7",URLEncoder.encode(routeIds.toString(), "UTF-8")); //route_id
				url = url.replace("value8",URLEncoder.encode(routeLast, "UTF-8")); //route_lastmodified
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			Log.i(getClass().getSimpleName(), "SYNC URL: "+url);
			Log.i(getClass().getSimpleName(), "SYNC : Route Last Modified Date: "+routeLast);
			Log.i(getClass().getSimpleName(), "SYNC : Route Ids: "+routeIds.toString());

			return url;
		}

		/*private InputStream retrieveStream(String url) {
            Log.i("retrieveStream", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet(url);
            try {
                HttpResponse getResponse = client.execute(getRequest);
                final int statusCode = getResponse.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    Log.w(getClass().getSimpleName(),
                            "Error " + statusCode + " for URL " + url);
                    return null;
                }
                HttpEntity getResponseEntity = getResponse.getEntity();
                return getResponseEntity.getContent();
            }
            catch (IOException e) {
                getRequest.abort();
                Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
            }
            return null;
        }

		 */
		@Override
		protected void onPostExecute(String url) {
			super.onPostExecute(url);

			Intent intent = new Intent(mContext, SyncService.class);
			intent.putExtra("receiver", new DownloadReceiver(new Handler()));
			intent.putExtra("url", url);
			mContext.startService(intent);
			mSyncButton.setEnabled(true);
		}
	}


	private class DownloadReceiver extends ResultReceiver {
		private Intent intent;

		public DownloadReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultCode == SyncService.UPDATE_PROGRESS) {
				int progress = resultData.getInt("progress");
				mProgressBar.setProgress(progress);
				if (progress >= 100) {
					/*if(intent == null){
						intent = new Intent(SplashActivity.this,MainActivity.class);
						startActivity(intent);
						finish();
					}*/
					mProgressBar.setVisibility(View.GONE);
					Toast.makeText(mActivity, "Synchronization Completed!", Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	/*private void replaceFragment() {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new RouteDetail(), true);
    }*/

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.sync:
			if(SmartTours.isNetworkConnected()){
				SyncTask syncTask = new SyncTask(mActivity);
				syncTask.execute();
			} else {
				Toast.makeText(mActivity, getResources().getString(R.string.check_network), Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}

}
