package com.doodleblue.smarttours.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

public class Fragment2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        MapView mapView = new MapView(getActivity(), 256); //constructor

        mapView.setClickable(true);

        mapView.setBuiltInZoomControls(true);



        mapView.getController().setZoom(15); //set initial zoom-level, depends on your need

        mapView.getController().setCenter(new GeoPoint(52.221, 6.893)); //This point is in Enschede, Netherlands. You should select a point in your map or get it from user's location.

        //mapView.setUseDataConnection(false); //keeps the mapView from loading online tiles using network connection.

        mapView.setTileSource(TileSourceFactory.MAPNIK);
        return mapView;

    }
}
