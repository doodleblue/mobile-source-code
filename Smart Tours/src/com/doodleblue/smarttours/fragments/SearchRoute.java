package com.doodleblue.smarttours.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.doodleblue.smarttours.adapter.CustomArrayAdapter;
import com.doodleblue.smarttours.adapter.CustomArrayAdapter.FavSelectedListener;
import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.model.Favorite;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.service.DownloadAudioPointsService;
import com.doodleblue.smarttours.service.MbtilesDownloadService;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.GetFont;
import com.doodleblue.smarttours.view.SmartSpinner;
import com.doodleblue.smarttours.view.SmartTextView;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA. User: ruby Date: 11/4/13 Time: 6:21 PM To change
 * this template use File | Settings | File Templates.
 */
public class SearchRoute extends BaseContainerFragment implements
View.OnClickListener, FavSelectedListener {

	private ArrayList<String> mStartPointList;
	private ArrayList<String> mDestPointList;
	private ArrayList<String> mDaysList;

	private Spinner mStartSpinner;
	private Spinner mDestSpinner;
	private Spinner mDaysSpinner;

	private String mSelectedStartPoint = "";
	private String mSelectedDestPoint = "";
	private String mSelectedDays = "";

	private int nRoutes;
	private int nRouteStops;
	private int nPOI;
	private int nPlaces;
	private int nSize;
	private int nDownloads;
	private ProgressBar mProgressBar;
	
	public static ArrayList<Boolean> mIsDownloadedList = new ArrayList<Boolean>();
	public static ArrayList<String> mFavoritedRouteList;
	
	private ArrayList<Route> queriedArrayList = new ArrayList<Route>();

	private CustomArrayAdapter mAdapter;
	//public ArrayList<Favorite> mFavoriteList;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.select_route, null);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		initView();

	}

	private void initView() {
		
		mIsDownloadedList = new ArrayList<Boolean>();
		
		mProgressBar = (ProgressBar)mRootView.findViewById(R.id.progressBar1);
		mProgressBar.setVisibility(View.VISIBLE);
		Log.i(getActivity().getClass().getSimpleName(), "Bytes: "+SmartTours.getContext().getPackageName().getBytes());
		Log.i(getActivity().getClass().getSimpleName(), "Package Days: "+SmartTours.getContext().getPackageName());

		mStartSpinner = (SmartSpinner) mRootView
				.findViewById(R.id.startSpinner);
		mDestSpinner = (SmartSpinner) mRootView.findViewById(R.id.destSpinner);
		mDaysSpinner = (SmartSpinner) mRootView.findViewById(R.id.daysSpinner);

		((ImageView) mRootView.findViewById(R.id.download))
		.setOnClickListener(this);
		((TextView) mRootView.findViewById(R.id.result_header))
		.setOnClickListener(this);

		mStartSpinner
		.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,
					View view, int position, long id) {
				((SmartTextView) mRootView
						.findViewById(R.id.result_header))
						.setText("Go");
				mSelectedStartPoint = (String) mStartSpinner
						.getAdapter().getItem(position);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		mDestSpinner
		.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,
					View view, int position, long id) {

				((SmartTextView) mRootView
						.findViewById(R.id.result_header))
						.setText("Go");
				mSelectedDestPoint = (String) mDestSpinner.getAdapter()
						.getItem(position);

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		mDaysSpinner
		.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,
					View view, int position, long id) {

				((SmartTextView) mRootView
						.findViewById(R.id.result_header))
						.setText("Go");
				mSelectedDays = (String) mDaysSpinner.getAdapter()
						.getItem(position);
				if(mSelectedDays.equals("30+")){
					mSelectedDays = "30";
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		((HorizontalListView) mRootView.findViewById(R.id.routes_list))
		.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				replaceFragment(mAdapter.getItem(position));
			}
		});
		fetchRouteRecord();
	}

	private void replaceFragment(Route route) {
		((BaseContainerFragment) getParentFragment()).replaceFragment(
				new RouteDetail(route), true);
	}

	private void replaceFragment() {
		((BaseContainerFragment) getParentFragment()).replaceFragment(
				new Inapp(), true);
	}

	private void fetchRouteRecord() {
		FetchRecordTask task = new FetchRecordTask(false);
		task.execute(new String[] { "" });
	}

	public class FetchRecordTask extends
	AsyncTask<String, String, ArrayList<Route>> {

		private ArrayAdapter<String> startAdapter;
		private ArrayAdapter<String> destAdapter;
		private ArrayAdapter<String> dayAdapter;
		private boolean isSearchResult;


		//ProgressDialog dialog;

		FetchRecordTask(boolean isSearch) {
			mStartPointList = new ArrayList<String>();
			mDestPointList = new ArrayList<String>();
			mDaysList = new ArrayList<String>();
			isSearchResult = isSearch;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/*dialog = new ProgressDialog(mActivity);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setCancelable(false);
			dialog.setMessage("Loading...");
			dialog.show();*/

			mProgressBar.setVisibility(View.VISIBLE);

			nRoutes = 0;
			nRouteStops = 0;
			nPOI = 0;
			nPlaces = 0;
			nSize = 0;
			nDownloads = 0;

			queriedArrayList.clear();
		}

		@Override
		protected ArrayList<Route> doInBackground(String... params) {
			if (params[0].equals("")) {
				queriedArrayList = DatabaseHelper.getInstance(mActivity)
						.fetchRoutes(null);
			} else {
				queriedArrayList = DatabaseHelper.getInstance(mActivity)
						.fetchRoutes(
								(DatabaseHelper.getInstance().fetchSegment(
										DatabaseHelper.KEY_NAME, params[0])
										)
										.get(0).getId(),
										(DatabaseHelper.getInstance().fetchSegment(
												DatabaseHelper.KEY_NAME, params[1])
												)
												.get(0).getId(), 
												params[2]);
			}

			for (Route route : queriedArrayList) {
				String start = DatabaseHelper.getInstance(mActivity)
						.fetchSegmentName(route.getStart());
				String dest = DatabaseHelper.getInstance(mActivity)
						.fetchSegmentName(route.getEnd());
				String day = route.getMaxDay();
				if (start != null) {
					if (!mStartPointList.contains(start))
						mStartPointList.add(start);
				}
				if (dest != null) {
					if (!mDestPointList.contains(dest))
						mDestPointList.add(dest);
				}
				/*if (day != null) {
					if (!mDaysList.contains(day)) {
						mDaysList.add(day);
					}
				}*/

				Favorite favorite = DatabaseHelper.getInstance(
						).fetchFavorite(route.getId());
				if (favorite != null)
					route.setFavourite(true);
				else
					route.setFavourite(false);
			}

			ArrayList<Favorite> favoriteList = fetchFavList();
			if(mFavoritedRouteList==null){
				mFavoritedRouteList = new ArrayList<String>();
			} else {
				mFavoritedRouteList.clear();
			}
			//mFavoriteList = favoriteList;
			for(Favorite favorite2:favoriteList){
				String routeId = favorite2.getRouteId();
				mFavoritedRouteList.add(routeId);
				for(Route route2: DatabaseHelper.getInstance().fetchRoutes(new String[]{routeId})){
					if(queriedArrayList.contains(route2)){
						if(!TextUtils.isEmpty(route2.getnStops()))
							nRouteStops += Integer.parseInt(route2.getnStops());
						if(!TextUtils.isEmpty(route2.getnPOI()))
							nPlaces += Integer.parseInt(route2.getnPlaces());
						if(!TextUtils.isEmpty(route2.getnPlaces()))
							nPOI += Integer.parseInt(route2.getnPOI());
					}
				}
			}

			return queriedArrayList;
		}



		@Override
		protected void onPostExecute(ArrayList<Route> routeArrayList) {
			super.onPostExecute(routeArrayList);
			/*if(dialog!=null){
				if(dialog.isShowing())
					dialog.cancel();
			}*/
			mProgressBar.setVisibility(View.GONE);
			mAdapter = new CustomArrayAdapter(mActivity, (FavSelectedListener)SearchRoute.this, queriedArrayList);
			((HorizontalListView) mRootView.findViewById(R.id.routes_list))
			.setAdapter(mAdapter);

			if (queriedArrayList.size() == 0) {
				((HorizontalListView) mRootView.findViewById(R.id.routes_list))
				.setVisibility(View.GONE);
				((SmartTextView) mRootView.findViewById(R.id.noResultFound))
				.setVisibility(View.VISIBLE);
			} else {
				((HorizontalListView) mRootView.findViewById(R.id.routes_list))
				.setVisibility(View.VISIBLE);
				((SmartTextView) mRootView.findViewById(R.id.noResultFound))
				.setVisibility(View.GONE);
			}

			if (!isSearchResult) {
				if (mStartPointList.size() > 0) {
					if (startAdapter == null) {
						startAdapter = new CustomSpinnerAdapter(mActivity,
								android.R.layout.simple_spinner_item,
								mStartPointList);
						startAdapter
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						mStartSpinner.setAdapter(startAdapter);
					} else {
						startAdapter.notifyDataSetChanged();
					}
				}
				if (mDestPointList.size() > 0) {
					if (destAdapter == null) {
						destAdapter = new CustomSpinnerAdapter(mActivity,
								android.R.layout.simple_spinner_item,
								mDestPointList);
						destAdapter
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						mDestSpinner.setAdapter(destAdapter);
					} else {
						destAdapter.notifyDataSetChanged();
					}
				}

				for(int i=1;i<32;i++){
					if(i!=31)
						mDaysList.add(String.valueOf(i));
					else
						mDaysList.add("30+");
				}
				if (dayAdapter == null) {
					dayAdapter = new CustomSpinnerAdapter(mActivity,
							android.R.layout.simple_spinner_item, mDaysList);
					dayAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					mDaysSpinner.setAdapter(dayAdapter);
				}
				else {
					dayAdapter.notifyDataSetChanged();
				}
			}
			populateFavoriteBannerValues();
		}
	}

	private ArrayList<Favorite> fetchFavList() {
		/*ArrayList<Favorite> favoriteList = new ArrayList<Favorite>();
		for(Favorite favorite:DatabaseHelper.getInstance().fetchFavorite()){
			for(Route route:routes){

			}
		}*/
		ArrayList<Favorite> favoriteList = DatabaseHelper.getInstance().fetchFavorite();
		ArrayList<String> downloadedList = DatabaseHelper.getInstance().fetchDownloaded(null);
		nDownloads = 0;
		for(Favorite fav:favoriteList){
			if(!downloadedList.contains(fav.getRouteId())){
				nDownloads+=1;
			}
		}
		//nDownloads = favoriteList.size() - downloadedList.size();
		nRoutes = favoriteList.size();
		return favoriteList;
	}



	class CustomSpinnerAdapter extends ArrayAdapter<String> {

		private ArrayList<String> list;

		public CustomSpinnerAdapter(Context context, int textViewResourceId,
				ArrayList<String> objects) {
			super(context, textViewResourceId, objects);
			list = objects;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public String getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final TextView v = (TextView) ((LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(android.R.layout.simple_spinner_item, parent,
							false);
			v.setText(getItem(position));

			float scale = getResources().getDisplayMetrics().density;
			int dpAsPixels = (int) (33*scale + 0.5f);

			v.setPadding(dpAsPixels, 0, 0, 0);
			v.setTypeface(GetFont.getInstance(getContext()).getFont(
					GetFont.HELVITICA_CONDENSED));
			v.setTextSize(TypedValue.COMPLEX_UNIT_DIP,getResources().getInteger(R.integer.select_route_spinner_text_size));
			v.setTextColor(getResources().getColor(
					R.color.search_routes_spinner_item));
			return v;
		}
	}

	private void downloadCheckedOutAudios() {

		ArrayList<String> routes = new ArrayList<String>();
		routes.add(String.valueOf(1));
		routes.add(String.valueOf(2));
		routes.add(String.valueOf(3));

		for(String routeId:routes){
			Intent intent = new Intent(SmartTours.getContext(), DownloadAudioPointsService.class);
			intent.putExtra("receiver", new DownloadReceiver(new Handler()));
			intent.putExtra("routeId", routeId);
			SmartTours.getContext().startService(intent);
		}
	}

	private class DownloadReceiver extends ResultReceiver {
		private Intent intent;

		public DownloadReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultCode == DownloadAudioPointsService.UPDATE_PROGRESS) {
				int progress = resultData.getInt("progress");
				//splashProgressBar.setProgress(progress);
				if (progress >= 100) {
					if(intent == null){
						/*intent = new Intent(SmartTours.getContext(),MainActivity.class);
						startActivity(intent);*/
						Toast.makeText(SmartTours.getContext(), "Checked out Commentary!!!", Toast.LENGTH_SHORT).show();
					}
				}
			} else if(resultCode == MbtilesDownloadService.UPDATE_PROGRESS) {
				int progress = resultData.getInt("progress");
				if (progress >= 100) {
					fetchFavList();
					Toast.makeText(mActivity, "Completed", Toast.LENGTH_SHORT).show();
					populateFavoriteBannerValues();
				}
			}
		}
	}

	

	private void downloadFavoritedMaps() {
		if(mIsDownloadedList.contains(new Boolean(false))){
			Intent intent = new Intent(SmartTours.getContext(),
					MbtilesDownloadService.class);
			intent.putExtra("routeIds", mFavoritedRouteList);
			intent.putExtra("receiver", new DownloadReceiver(new Handler()));
			if(SmartTours.isNetworkConnected()){
				SmartTours.getContext().startService(intent);
				Toast.makeText(mActivity, "Downloading Map data!", Toast.LENGTH_SHORT)
				.show();
			} else {
				Toast.makeText(mActivity, getResources().getString(R.string.check_network), Toast.LENGTH_SHORT)
				.show();
			}
		}
		else {
			Toast.makeText(mActivity, getResources().getString(R.string.downloaded_already), Toast.LENGTH_SHORT)
			.show();
		}
		/*if(mIsDownloadedList!=null)
			Toast.makeText(mActivity, "mIsDownloadedList SIZE: "+mIsDownloadedList.size(), Toast.LENGTH_SHORT).show();*/
	}

	@Override
	public void onFavoriteSelected(CompoundButton buttonView, boolean isChecked) {

		Route route = (Route) buttonView.getTag();
		String routeId = route.getId();

		boolean isDownloaded = DatabaseHelper.getInstance().fetchIsDownloaded(routeId);
		
		
		//Query routeId to Favorite
		if(isChecked){
			DatabaseHelper.getInstance().insertOrUpdateFavorite(new Favorite(routeId,false)); //TODO: Update while in-app purchase is introduced
			route.setFavourite(true);
			if(!TextUtils.isEmpty(route.getnStops()))
				nRouteStops += Integer.parseInt(route.getnStops());
			if(!TextUtils.isEmpty(route.getnPOI()))
				nPlaces += Integer.parseInt(route.getnPlaces());
			if(!TextUtils.isEmpty(route.getnPlaces()))
				nPOI += Integer.parseInt(route.getnPOI());
			mIsDownloadedList.add(new Boolean(isDownloaded));
			if(!isDownloaded) nDownloads+=1;
			if(!mFavoritedRouteList.contains(route.getId()))
				mFavoritedRouteList.add(route.getId());
		}
		else {
			DatabaseHelper.getInstance().deleteRecord(DatabaseHelper.TABLE_FAVORITE,
					DatabaseHelper.KEY_ROUTE_ID,routeId);
			route.setFavourite(false);
			if(!TextUtils.isEmpty(route.getnStops()))
				nRouteStops -= Integer.parseInt(route.getnStops());
			if(!TextUtils.isEmpty(route.getnPOI()))
				nPlaces -= Integer.parseInt(route.getnPlaces());
			if(!TextUtils.isEmpty(route.getnPlaces()))
				nPOI -= Integer.parseInt(route.getnPOI());
			if(mIsDownloadedList.contains(new Boolean(isDownloaded))){
				mIsDownloadedList.remove(new Boolean(isDownloaded));
			}
			if(isDownloaded) nDownloads-=1;
			if(mFavoritedRouteList.contains(route.getId()))
				mFavoritedRouteList.remove(route.getId());
		}
		
		fetchFavList();
		populateFavoriteBannerValues();

	}

	private void populateFavoriteBannerValues() {
		try {
			((TextView) mRootView.findViewById(R.id.route_stop_value))
			.setText(": "+nRouteStops);
			((TextView) mRootView.findViewById(R.id.towns_cities_value))
			.setText(": "+nPlaces);
			((TextView) mRootView.findViewById(R.id.poi_value))
			.setText(": "+nPOI);
			((TextView) mRootView.findViewById(R.id.routes_value))
			.setText(": "+nRoutes);
			((TextView) mRootView.findViewById(R.id.left_to_download_value))
			.setText(": "+nDownloads);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.download:
			//replaceFragment(); //TODO: Uncomment this line and comment next line
			//downloadCheckedOutAudios();
			downloadFavoritedMaps();
			break;

		case R.id.result_header:

			Log.e("SearchRoute",
					"Start Point: " + mStartSpinner.getSelectedItemPosition());
			Log.e("SearchRoute",
					"Dest Point: " + mDestSpinner.getSelectedItemPosition());

			if (((SmartTextView) mRootView.findViewById(R.id.result_header))
					.getText().equals("Go")) {
				if (mStartSpinner.getSelectedItemPosition() != -1
						&& mDestSpinner.getSelectedItemPosition() != -1) {
					FetchRecordTask task = new FetchRecordTask(true);
					task.execute(new String[] { mSelectedStartPoint,
							mSelectedDestPoint, mSelectedDays });
					((SmartTextView) mRootView.findViewById(R.id.result_header))
					.setText("X");
					mDaysSpinner.setSelection(-1);
				}
			} else {
				FetchRecordTask task = new FetchRecordTask(false);
				task.execute(new String[] { "" });
				((SmartTextView) mRootView.findViewById(R.id.result_header))
				.setText("Go");
			}
			break;
		}
	}
}
