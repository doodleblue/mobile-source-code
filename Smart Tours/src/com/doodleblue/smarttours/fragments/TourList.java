package com.doodleblue.smarttours.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.adapter.ToursAdapter;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Tours;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 7:11 PM
 * To change this template use File | Settings | File Templates.
 */
@SuppressLint("ValidFragment")
public class TourList extends BaseContainerFragment
{
    private Route mRoute;
    private ToursAdapter mToursAdapter;

    
	public TourList(Route route, ToursAdapter adapter){
        mRoute = route;
        mToursAdapter = adapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.tour_list, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       
        	initView();
    }

    private void initView() {
        ((TextView)mRootView.findViewById(R.id.route_name)).setText(mRoute.getName());
        ((ImageView)mRootView.findViewById(R.id.route_image)).setImageBitmap(
        		Utils.getImageFromString(mRoute.getImage2Local()));
        ExpandableListView tourExpListView = ((ExpandableListView)mRootView.findViewById(R.id.tours_list));
        tourExpListView.setAdapter(mToursAdapter);
        tourExpListView.setGroupIndicator(null);
        if(mToursAdapter!=null){
        		tourExpListView.expandGroup(0); //0,true
        }
        tourExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                replaceFragment(mToursAdapter.getChild(groupPosition,childPosition));
                return true;
            }
        });
        tourExpListView.setGroupIndicator(null);
       /* ((RelativeLayout)mRootView.findViewById(R.id.stops_list_lay)).setVisibility(View.GONE);*/
    }

    private void replaceFragment(Tours tours) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new TourDetail(tours), true);
    }

}
