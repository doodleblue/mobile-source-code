package com.doodleblue.smarttours.fragments;

import java.util.ArrayList;

import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.service.DownloadAudioPointsService;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.view.SmartTextView;
import com.smarttoursnz.guide.R;

public class Inapp extends BaseContainerFragment implements OnClickListener {

	private static final String TAG = "Inapp";

	// (arbitrary) request code for the purchase flow
	static final int RC_REQUEST = 10001;

	// The helper object
	IabHelper mHelper;

	// Does the user have the premium upgrade?
	boolean mIsPremium = false;

	private SmartTextView mTextBuyZone1;
	private SmartTextView mTextBuyZone2;
	private SmartTextView mTextBuyZone3;
	private SmartTextView mTextBuyZone4;
	private SmartTextView mTextBuyZone5;
	private SmartTextView mTextBuyZone6;

	private SmartTextView mTextBuyNorth;
	private SmartTextView mTextBuySouth;

	private SmartTextView mTextBuyNewz;



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.inapp, container, false);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initView();
		initInApp();
	}


	// Listener that's called when we finish querying the items we own
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
			Log.d(TAG, "Query inventory finished.");

			// Have we been disposed of in the meantime? If so, quit.
			if (mHelper == null) return;

			// Is it a failure?
			if (result.isFailure()) {
				complain("Failed to query inventory: " + result);
				return;
			}

			Log.d(TAG, "Query inventory was successful.");
			//inventory.erasePurchase(SKU_ZONE_1);

			Purchase zone1Purchase = inventory.getPurchase(SKU_ZONE_1);
			savePreferences(SKU_ZONE_1, (zone1Purchase != null && verifyDeveloperPayload(zone1Purchase)));

			Purchase zone2Purchase = inventory.getPurchase(SKU_ZONE_2);
			savePreferences(SKU_ZONE_2, zone2Purchase != null && verifyDeveloperPayload(zone2Purchase));

			Purchase zone3Purchase = inventory.getPurchase(SKU_ZONE_3);
			savePreferences(SKU_ZONE_3, zone3Purchase != null && verifyDeveloperPayload(zone3Purchase));

			Purchase zone4Purchase = inventory.getPurchase(SKU_ZONE_4);
			savePreferences(SKU_ZONE_4, zone4Purchase != null && verifyDeveloperPayload(zone4Purchase));

			Purchase zone5Purchase = inventory.getPurchase(SKU_ZONE_5);
			savePreferences(SKU_ZONE_5, zone5Purchase != null && verifyDeveloperPayload(zone5Purchase));

			Purchase zone6Purchase = inventory.getPurchase(SKU_ZONE_6);
			savePreferences(SKU_ZONE_6, zone6Purchase != null && verifyDeveloperPayload(zone6Purchase));

			Purchase northPurchase = inventory.getPurchase(SKU_NORTH);
			savePreferences(SKU_NORTH, northPurchase != null && verifyDeveloperPayload(northPurchase));

			Purchase northUpgradePurchase = inventory.getPurchase(SKU_NORTH_UPGRADE);
			savePreferences(SKU_NORTH_UPGRADE, northUpgradePurchase != null && verifyDeveloperPayload(northUpgradePurchase));

			Purchase southPurchase = inventory.getPurchase(SKU_SOUTH);
			savePreferences(SKU_SOUTH, southPurchase != null && verifyDeveloperPayload(southPurchase));

			Purchase southUpgradePurchase = inventory.getPurchase(SKU_SOUTH_UPGRADE);
			savePreferences(SKU_SOUTH_UPGRADE, southUpgradePurchase != null && verifyDeveloperPayload(southUpgradePurchase));

			Purchase countryPurchase = inventory.getPurchase(SKU_COUNTRY);
			savePreferences(SKU_COUNTRY, countryPurchase != null && verifyDeveloperPayload(countryPurchase));

			Purchase countryUpgrade1Purchase = inventory.getPurchase(SKU_COUNTRY_UPGRADE_1);
			savePreferences(SKU_COUNTRY_UPGRADE_1, countryUpgrade1Purchase != null && verifyDeveloperPayload(countryUpgrade1Purchase));

			Purchase countryUpgrade2Purchase = inventory.getPurchase(SKU_COUNTRY_UPGRADE_2);
			savePreferences(SKU_COUNTRY_UPGRADE_2, countryUpgrade2Purchase != null && verifyDeveloperPayload(countryUpgrade2Purchase));


			/********************** Not needed in this module ***********************
            ArrayList<String> mSkuList = new ArrayList<String>();
            //Get all sku
			for(Purchase purchase : inventory.getAllPurchases()){
            	mSkuList.add(purchase.getSku());
            }

			//get purchased sku
			ArrayList<String> purchasedProductList = new ArrayList<String>();
			//update in shared preferences
			for(String sku : mSkuList){
				if (inventory.hasPurchase("productId")){
					purchasedProductList.add(sku);
					savePreferences(sku, true);
				} else {
	            	savePreferences(sku, false);
				}
			}
			 *****************can include in purchase verifying module*******************/

			//update purchasedProductList in db


			//notify if needed

			restorePreviousPurchases();
			setWaitScreen(false);
			Log.d(TAG, "Initial inventory query finished; enabling main UI.");
		}
	};

	/** Verifies the developer payload of a purchase. */
	boolean verifyDeveloperPayload(Purchase p) {
		String payload = p.getDeveloperPayload();
		return true;
	}

	//Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

			// if we were disposed of in the meantime, quit.
			if (mHelper == null) return;

			if (result.isFailure()) {
				complain("Error purchasing: " + result);
				setWaitScreen(false);
				return;
			}
			if (!verifyDeveloperPayload(purchase)) {
				complain("Error purchasing. Authenticity verification failed.");
				setWaitScreen(false);
				return;
			}

			Log.d(TAG, "Purchase successful.");

			if (purchase.getSku().equals(SKU_ZONE_1)) {
				Log.d(TAG, "Purchase is zone 1. Congratulating user.");
				alert("Thank you for purchasing zone 1!");
				savePreferences(SKU_ZONE_1, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_ZONE_1));
			}
			if (purchase.getSku().equals(SKU_ZONE_2)) {
				Log.d(TAG, "Purchase is zone 2. Congratulating user.");
				alert("Thank you for purchasing zone 2!");
				savePreferences(SKU_ZONE_2, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_ZONE_2));
			}
			if (purchase.getSku().equals(SKU_ZONE_3)) {
				Log.d(TAG, "Purchase is zone 3. Congratulating user.");
				alert("Thank you for purchasing zone 3!");
				savePreferences(SKU_ZONE_3, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_ZONE_3));
			}
			if (purchase.getSku().equals(SKU_ZONE_4)) {
				Log.d(TAG, "Purchase is zone 4. Congratulating user.");
				alert("Thank you for purchasing zone 4!");
				savePreferences(SKU_ZONE_4, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_ZONE_4));
			}
			if (purchase.getSku().equals(SKU_ZONE_5)) {
				Log.d(TAG, "Purchase is zone 5. Congratulating user.");
				alert("Thank you for purchasing zone 5!");
				savePreferences(SKU_ZONE_5, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_ZONE_5));
			}
			if (purchase.getSku().equals(SKU_ZONE_6)) {
				Log.d(TAG, "Purchase is zone 6. Congratulating user.");
				alert("Thank you for purchasing zone 6!");
				savePreferences(SKU_ZONE_6, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_ZONE_6));
			}
			if (purchase.getSku().equals(SKU_NORTH)) {
				Log.d(TAG, "Purchase is North Island. Congratulating user.");
				alert("Thank you for purchasing North Island!");
				savePreferences(SKU_NORTH, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_NORTH));
			}
			if (purchase.getSku().equals(SKU_NORTH_UPGRADE)) {
				Log.d(TAG, "Purchase is North Island. Congratulating user.");
				alert("Thank you for purchasing North Island!");
				savePreferences(SKU_NORTH_UPGRADE, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_NORTH));
			}
			if (purchase.getSku().equals(SKU_SOUTH)) {
				Log.d(TAG, "Purchase is South Island. Congratulating user.");
				alert("Thank you for purchasing South Island!");
				savePreferences(SKU_SOUTH, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_SOUTH));
			}
			if (purchase.getSku().equals(SKU_SOUTH_UPGRADE)) {
				Log.d(TAG, "Purchase is South Island. Congratulating user.");
				alert("Thank you for purchasing South Island!");
				savePreferences(SKU_SOUTH_UPGRADE, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_SOUTH));
			}
			if (purchase.getSku().equals(SKU_COUNTRY)) {
				Log.d(TAG, "Purchase is full Newzealand. Congratulating user.");
				alert("Thank you for purchasing Newzealand!");
				savePreferences(SKU_COUNTRY, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_COUNTRY));
			}
			if (purchase.getSku().equals(SKU_COUNTRY_UPGRADE_1)) {
				Log.d(TAG, "Purchase is full Newzealand. Congratulating user.");
				alert("Thank you for purchasing Newzealand upgrade!");
				savePreferences(SKU_COUNTRY_UPGRADE_1, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_COUNTRY));
			}
			if (purchase.getSku().equals(SKU_COUNTRY_UPGRADE_2)) {
				Log.d(TAG, "Purchase is full Newzealand. Congratulating user.");
				alert("Thank you for purchasing Newzealand upgrade!");
				savePreferences(SKU_COUNTRY_UPGRADE_2, true);
				downloadCheckedOutAudios(getCorrespondingRoutes(SKU_COUNTRY));
			}

			restorePreviousPurchases();
			setWaitScreen(false);
		}

		private void downloadCheckedOutAudios(
				ArrayList<String> correspondingRoutes) {

			Intent intent = new Intent(SmartTours.getContext(), DownloadAudioPointsService.class);
			intent.putExtra("receiver", new DownloadReceiver(new Handler()));
			SmartTours.getContext().startService(intent);
		}

		private ArrayList<String> getCorrespondingRoutes(String sku) {
			ArrayList<String> routes = new ArrayList<String>();
			if(sku.equals(SKU_ZONE_1)){
				routes.add(String.valueOf(1));
				routes.add(String.valueOf(2));
				routes.add(String.valueOf(3));
			} else if(sku.equals(SKU_ZONE_2)){
				routes.add(String.valueOf(4));
				routes.add(String.valueOf(5));
				routes.add(String.valueOf(6));
			} else if(sku.equals(SKU_ZONE_3)){
				routes.add(String.valueOf(7));
				routes.add(String.valueOf(8));
				routes.add(String.valueOf(9));
			} else if(sku.equals(SKU_ZONE_4)){
				routes.add(String.valueOf(10));
				routes.add(String.valueOf(11));
				routes.add(String.valueOf(12));
			} else if(sku.equals(SKU_ZONE_5)){
				routes.add(String.valueOf(13));
				routes.add(String.valueOf(14));
				routes.add(String.valueOf(15));
			} else if(sku.equals(SKU_ZONE_6)){
				routes.add(String.valueOf(16));
				routes.add(String.valueOf(17));
				routes.add(String.valueOf(18));
			} else if(sku.equals(SKU_NORTH)){
				for(int i=0;i<15;i++){
					routes.add(String.valueOf(i+1));
				}
			} else if(sku.equals(SKU_SOUTH)){
				for(int i=14;i<30;i++){
					routes.add(String.valueOf(i+1));
				}
			}  else if(sku.equals(SKU_COUNTRY)){
				for(int i=0;i<30;i++){
					routes.add(String.valueOf(i+1));
				}
			} 
			return routes;
		}
	};

	private class DownloadReceiver extends ResultReceiver {
		private Intent intent;

		public DownloadReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultCode == DownloadAudioPointsService.UPDATE_PROGRESS) {
				int progress = resultData.getInt("progress");
				//splashProgressBar.setProgress(progress);
				if (progress == 100) {
					if(intent == null){
						/*intent = new Intent(SmartTours.getContext(),MainActivity.class);
						startActivity(intent);*/
						Toast.makeText(SmartTours.getContext(), "Checked out Commentary!!!", Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
	}


	// We're being destroyed. It's important to dispose of the helper here!
	@Override
	public void onDestroy() {
		super.onDestroy();

		// very important:
		Log.d(TAG, "Destroying helper.");
		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
	}

	// Enables or disables the "please wait" screen.
	void setWaitScreen(boolean set) {
		mRootView.findViewById(R.id.progressBar).setVisibility(set ? View.GONE : View.VISIBLE);
		mRootView.findViewById(R.id.progressBar).setVisibility(set ? View.VISIBLE : View.GONE);
	}

	void complain(String message) {
		Log.e(TAG, "**** TrivialDrive Error: " + message);
		alert("Error: " + message);
	}

	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(getActivity());
		bld.setMessage(message);
		bld.setNeutralButton("OK", null);
		Log.d(TAG, "Showing alert dialog: " + message);
		bld.create().show();
	}

	void saveData() {

		/*
		 * WARNING: on a real application, we recommend you save data in a secure way to
		 * prevent tampering. For simplicity in this sample, we simply store the data using a
		 * SharedPreferences.
		 */

		SharedPreferences.Editor spe = ((Activity)SmartTours.getContext()).getPreferences(Context.MODE_PRIVATE).edit();
		spe.putBoolean("PURCHASED", mIsPremium);
		spe.commit();
		Log.d(TAG, "Saved data: PURCHASED = " + mIsPremium);
	}

	void loadData() {
		SharedPreferences sp = ((Activity)SmartTours.getContext()).getPreferences(Context.MODE_PRIVATE);
		mIsPremium = sp.getBoolean("PURCHASED", false);
		Log.d(TAG, "Loaded data: PURCHASED = " + mIsPremium);
	}

	// User clicked the "buy" button.
	public void onBuyButtonClicked(String sku) {
		Log.d(TAG, "Upgrade button clicked; launching purchase flow for upgrade.");
		setWaitScreen(true);

		/* TODO: for security, generate your payload here for verification. See the comments on
		 *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
		 *        an empty string, but on a production app you should carefully generate this. */
		String payload = "";

		mHelper.launchPurchaseFlow((Activity)SmartTours.getContext(), sku, RC_REQUEST,
				mPurchaseFinishedListener, payload);
	}

	private void initView() {

		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(SmartTours.getContext());

		mTextBuyZone1 = (SmartTextView)mRootView.findViewById(R.id.priceZone1);
		mTextBuyZone2 = (SmartTextView)mRootView.findViewById(R.id.priceZone2);
		mTextBuyZone3 = (SmartTextView)mRootView.findViewById(R.id.priceZone3);
		mTextBuyZone4 = (SmartTextView)mRootView.findViewById(R.id.priceZone4);
		mTextBuyZone5 = (SmartTextView)mRootView.findViewById(R.id.priceZone5);
		mTextBuyZone6 = (SmartTextView)mRootView.findViewById(R.id.priceZone6);

		mTextBuyNorth = (SmartTextView)mRootView.findViewById(R.id.priceNorth);
		mTextBuySouth = (SmartTextView)mRootView.findViewById(R.id.priceSouth);

		mTextBuyNewz = (SmartTextView)mRootView.findViewById(R.id.country);

		restorePreviousPurchases();

		mTextBuyZone1.setOnClickListener(this);
		mTextBuyZone2.setOnClickListener(this);
		mTextBuyZone3.setOnClickListener(this);
		mTextBuyZone4.setOnClickListener(this);
		mTextBuyZone5.setOnClickListener(this);
		mTextBuyZone6.setOnClickListener(this);

		mTextBuyNorth.setOnClickListener(this);
		mTextBuySouth.setOnClickListener(this);

		mTextBuyNewz.setOnClickListener(this);

		(mRootView.findViewById(R.id.clearPurchase)).setOnClickListener(this);

	}


	private void restorePreviousPurchases() {
		if(hasPreferences(SKU_ZONE_1)){
			if(getPreferences(SKU_ZONE_1)){
				togglePurchased(mTextBuyZone1);
				mTextBuySouth.setText("Buy $15");
				mTextBuyNewz.setText("Newzealand Buy $25");
			}
		}

		if(hasPreferences(SKU_ZONE_2)){
			if(getPreferences(SKU_ZONE_2)){
				togglePurchased(mTextBuyZone2);
				mTextBuySouth.setText("Buy $15");
				mTextBuyNewz.setText("Newzealand Buy $25");
			}
		}

		if(hasPreferences(SKU_ZONE_3)){
			if(getPreferences(SKU_ZONE_3)){
				togglePurchased(mTextBuyZone3);
				mTextBuySouth.setText("Buy $15");
				mTextBuyNewz.setText("Newzealand Buy $25");
			}
		}

		if(hasPreferences(SKU_ZONE_4)){
			if(getPreferences(SKU_ZONE_4)){
				togglePurchased(mTextBuyZone4);
				mTextBuyNorth.setText("Buy $15");
				mTextBuyNewz.setText("Newzealand Buy $25");
			}
		}

		if(hasPreferences(SKU_ZONE_5)){
			if(getPreferences(SKU_ZONE_5)){
				togglePurchased(mTextBuyZone5);
				mTextBuyNorth.setText("Buy $15");
				mTextBuyNewz.setText("Newzealand Buy $25");
			}
		}

		if(hasPreferences(SKU_ZONE_6)){
			if(getPreferences(SKU_ZONE_6)){
				togglePurchased(mTextBuyZone6);
				mTextBuyNorth.setText("Buy $15");
				mTextBuyNewz.setText("Newzealand Buy $25");
			}
		}

		if(hasPreferences(SKU_NORTH)||hasPreferences(SKU_NORTH_UPGRADE)){
			if(getPreferences(SKU_NORTH)||getPreferences(SKU_NORTH_UPGRADE)){
				togglePurchased(mTextBuyNorth);
				mTextBuyNewz.setText("Newzealand Buy $10");
				togglePurchased(mTextBuyZone4);
				togglePurchased(mTextBuyZone5);
				togglePurchased(mTextBuyZone6);
			}
		}

		if(hasPreferences(SKU_SOUTH)||hasPreferences(SKU_SOUTH_UPGRADE)){
			if(getPreferences(SKU_SOUTH)||getPreferences(SKU_SOUTH_UPGRADE)){
				togglePurchased(mTextBuySouth);
				mTextBuyNewz.setText("Newzealand Buy $10");
				togglePurchased(mTextBuyZone1);
				togglePurchased(mTextBuyZone2);
				togglePurchased(mTextBuyZone3);
			}
		}

		if(hasPreferences(SKU_NORTH)||hasPreferences(SKU_NORTH_UPGRADE) 
				|| hasPreferences(SKU_SOUTH)||hasPreferences(SKU_SOUTH_UPGRADE)){
			if((getPreferences(SKU_NORTH)||getPreferences(SKU_NORTH_UPGRADE)) 
					&& (getPreferences(SKU_SOUTH)||getPreferences(SKU_SOUTH_UPGRADE))){
				mTextBuyNewz.setText("Newzealand Purchased");
				mTextBuyNewz.setTextColor(getResources().getColor(R.color.inapp_purchased));
				mTextBuyNewz.setEnabled(false);

				togglePurchased(mTextBuySouth);
				togglePurchased(mTextBuyZone1);
				togglePurchased(mTextBuyZone2);
				togglePurchased(mTextBuyZone3);
				togglePurchased(mTextBuyNorth);
				togglePurchased(mTextBuyZone4);
				togglePurchased(mTextBuyZone5);
				togglePurchased(mTextBuyZone6);
			}
		}

		if(hasPreferences(SKU_COUNTRY)){
			if(getPreferences(SKU_COUNTRY)){
				mTextBuyNewz.setText("Newzealand Purchased");
				mTextBuyNewz.setTextColor(getResources().getColor(R.color.inapp_purchased));
				mTextBuyNewz.setEnabled(false);

				togglePurchased(mTextBuySouth);
				togglePurchased(mTextBuyZone1);
				togglePurchased(mTextBuyZone2);
				togglePurchased(mTextBuyZone3);
				togglePurchased(mTextBuyNorth);
				togglePurchased(mTextBuyZone4);
				togglePurchased(mTextBuyZone5);
				togglePurchased(mTextBuyZone6);
			}
		}

	}

	private void togglePurchased(SmartTextView textView) {
		textView.setText("Purchased");
		textView.setTextColor(getResources().getColor(R.color.inapp_purchased));
		textView.setEnabled(false);
	}

	/*
	 * private void replaceFragment(String url) { //TODO: any navigation of
	 * level deeper will go here
	 * ((BaseContainerFragment)getParentFragment()).replaceFragment(new
	 * Web(url), true); }
	 */

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {

		case R.id.priceZone1:
			if(!mTextBuyZone1.getText().equals("Purchased")){
				purchase(SKU_ZONE_1);
			}
			break;

		case R.id.priceZone2:
			if(!mTextBuyZone2.getText().equals("Purchased"))
				purchase(SKU_ZONE_2);

			break;

		case R.id.priceZone3:
			if(!mTextBuyZone3.getText().equals("Purchased"))
				purchase(SKU_ZONE_3);
			break;

		case R.id.priceZone4:
			if(!mTextBuyZone4.getText().equals("Purchased"))
				purchase(SKU_ZONE_4);
			break;

		case R.id.priceZone5:
			if(!mTextBuyZone5.getText().equals("Purchased"))
				purchase(SKU_ZONE_5);
			break;

		case R.id.priceZone6:
			if(!mTextBuyZone6.getText().equals("Purchased"))
				purchase(SKU_ZONE_6);
			break;

		case R.id.priceSouth:
			if(!mTextBuySouth.getText().equals("Purchased")){
				if(mTextBuySouth.getText().equals("Buy $35"))
					purchase(SKU_SOUTH);
				else if(mTextBuySouth.getText().equals("Buy $15"))
					purchase(SKU_SOUTH_UPGRADE);
			}
			break;

		case R.id.priceNorth:
			if(!mTextBuyNorth.getText().equals("Purchased")){
				if(mTextBuyNorth.getText().equals("Buy $35"))
					purchase(SKU_NORTH);
				else if(mTextBuyNorth.getText().equals("Buy $15"))
					purchase(SKU_NORTH_UPGRADE);
			}
			break;

		case R.id.country:
			if(!mTextBuyNewz.getText().equals("Newzealand Purchased")){
				if(mTextBuyNewz.getText().equals("Newzealand Buy $45"))
					purchase(SKU_COUNTRY);
				else if(mTextBuyNewz.getText().equals("Newzealand Buy $25"))
					purchase(SKU_COUNTRY_UPGRADE_1);
				else if(mTextBuyNewz.getText().equals("Newzealand Buy $10"))
					purchase(SKU_COUNTRY_UPGRADE_2);
			}
			break;

		case R.id.clearPurchase:
			Toast.makeText(mActivity, "Cleared Past Purchases!", Toast.LENGTH_SHORT)
			.show();
			savePreferences(SKU_ZONE_1, false);
			savePreferences(SKU_ZONE_2, false);
			savePreferences(SKU_ZONE_3, false);
			savePreferences(SKU_ZONE_4, false);
			savePreferences(SKU_ZONE_5, false);
			savePreferences(SKU_ZONE_6, false);

			savePreferences(SKU_NORTH, false);
			savePreferences(SKU_NORTH_UPGRADE, false);

			savePreferences(SKU_SOUTH, false);
			savePreferences(SKU_SOUTH_UPGRADE, false);

			savePreferences(SKU_COUNTRY, false);
			savePreferences(SKU_COUNTRY_UPGRADE_1, false);
			savePreferences(SKU_COUNTRY_UPGRADE_2, false);
			break;
		default:
			break;
		}
		restorePreviousPurchases();
	}


	// SKUs for our products
	//static final String SKU_TEST = "android.test.purchased";

	//static final String SKU_ZONE_1 = "android.test.purchased";//commentary.zone.1.milfordsound
	//static final String SKU_ZONE_2 = "android.test.canceled";//commentary.zone.2.westcoast
	//static final String SKU_ZONE_3 = "android.test.refunded";//commentary.zone.3.canterbury
	//static final String SKU_ZONE_4 = "android.test.item_unavailable";//commentary.zone.4.wellington

	static final String SKU_ZONE_1 = "commentary.zone.1.milfordsound";					//NZ$20.00
	static final String SKU_ZONE_2 = "commentary.zone.2.westcoast";
	static final String SKU_ZONE_3 = "commentary.zone.3.canterbury";
	static final String SKU_ZONE_4 = "commentary.zone.4.wellington";
	static final String SKU_ZONE_5 = "commentary.zone.5.eastcape";
	static final String SKU_ZONE_6 = "commentary.zone.6.farnorth";

	static final String SKU_NORTH 			= "commentary.island.456.north";			//NZ$35.00
	static final String SKU_NORTH_UPGRADE 	= "commentary.upgrade.456.north";			//NZ$15.00
	static final String SKU_SOUTH 			= "commentary.island.123.south";	
	static final String SKU_SOUTH_UPGRADE 	= "commentary.upgrade.123.south";

	static final String SKU_COUNTRY				= "commentary.country.123456.nz";		//NZ$45.00
	static final String SKU_COUNTRY_UPGRADE_1	= "commentary.upgrade.123456.nz"; 		//NZ$25.00
	static final String SKU_COUNTRY_UPGRADE_2	= "commentary.upgrade.southnorth.nz";	//NZ$10.00



	private void purchase(String sku) {
		//dummyPurchase(sku);
		if (!mIsBillingAvailable) {
			alert("InApp billing is unavailable. Upgrade your Google Play!");
			return;
		}
		String payLoad = "";
		if (mActivity != null && mActivity instanceof Activity) {
			mHelper.launchPurchaseFlow(mActivity, sku, 0,
					mPurchaseFinishedListener, payLoad);
		} else {
			Log.w("InApp", "mActivity in purchase() is null");
		}
	}

	private boolean mIsBillingAvailable = true;

	private static SharedPreferences mSharedPreferences;

	private void savePreferences(String key, boolean value) {
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	private void savePreferences(String key, String value) {
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static boolean getPreferences(String key) {
		return mSharedPreferences.getBoolean(key, false);
	}

	public static boolean hasPreferences(String key) {
		return mSharedPreferences.contains(key);
	}

	private void initInApp(){

		//String base64EncodedPublicKey = 
		//	"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlLwOuR9MvGTkm9JHZEoZJe2yufUozMX4vj467o02eHEL1zrPBu6fXnxS+ESJt9VE4FQuaR4xlBeYlyp5ZrbVgEdglrj14hib4e8XXkzcD3/DOoYCD84W6hSUDxbZpcZYz2XQhPWQpz4UkGWB05IcwTrLV5DuLt4pivSTo1rtexx/UbTvNL/z9YdIRl0NGE7nijRqeKjLKFpzp2Zps89w0QR584CUdXtMdqhwYNtKDOMYDgFeNnuY/GKoVlwFIpxjPwSklCaRdLmtqz29co5twQfkGanV7Pq4gvkRwgtNEeRovpU2+9JzE41B35aYLMzuW9OVnirHN7RXNbjmFWDzjwIDAQAB";
		String base64EncodedPublicKey = 

				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1t1xyPSHiRvhooD8kQAtk3hnmSwkibhh9Ow6xZJ3PSJG95YswEb/9ah4rWTY7iGBX+ncYY8yMa2N56b4rENUbrcEsZ4NEPz+66LscbAn3cIAS7ZpblQkyPSK2hWaTS35FOLunzKCyZwutM8oi6ePkcM/flaMUhepF9DX7gXC0uLwcCDJEzjpm0RRc+FG1Qy5Ps6anUrY8OtmNUdyCkYWughpquhm+vQzugMdld+eL5OkFGx3XdRfgd6ehpUwMBMKh6BT/wK7AzlnTCnVe+InKdfq62x/oMOqw/pSNpweHZbuUyWf9ncNpRteJ57U9VqGTTLFiffCa+ihre07E0kBbQIDAQAB";
		//"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlLwOuR9MvGTkm9JHZEoZJe2yufUozMX4vj467o02eHEL1zrPBu6fXnxS+ESJt9VE4FQuaR4xlBeYlyp5ZrbVgEdglrj14hib4e8XXkzcD3/DOoYCD84W6hSUDxbZpcZYz2XQhPWQpz4UkGWB05IcwTrLV5DuLt4pivSTo1rtexx/UbTvNL/z9YdIRl0NGE7nijRqeKjLKFpzp2Zps89w0QR584CUdXtMdqhwYNtKDOMYDgFeNnuY/GKoVlwFIpxjPwSklCaRdLmtqz29co5twQfkGanV7Pq4gvkRwgtNEeRovpU2+9JzE41B35aYLMzuW9OVnirHN7RXNbjmFWDzjwIDAQAB";

		// Create the helper, passing it our context and the public key to verify signatures with
		Log.d(TAG, "Creating IAB helper.");
		mHelper = new IabHelper(SmartTours.getContext(), base64EncodedPublicKey);

		// enable debug logging (for a production application, you should set this to false).
		mHelper.enableDebugLogging(true);

		// Start setup. This is asynchronous and the specified listener
		// will be called once setup completes.
		Log.d(TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				Log.d(TAG, "Setup finished.");
				if (result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE) {
					mIsBillingAvailable = false;
				}

				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					complain("Problem setting up in-app billing: " + result);
					return;
				}

				// Have we been disposed of in the meantime? If so, quit.
				if (mHelper == null) return;

				// IAB is fully set up. Now, let's get an inventory of stuff we own.
				Log.d(TAG, "Setup successful. Querying inventory.");
				mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});

	}
	class PurchaseItem{
		PurchaseItem(String name, String price){
			this.name = name;
			this.price = price;
		}
		String name;
		String price;
	}
	class PurchaseArrayAdapter extends ArrayAdapter<PurchaseItem>{

		private ArrayList<PurchaseItem> mPurchaseItems;
		private Context mContext;
		private int mLayoutResourceId;


		public PurchaseArrayAdapter(Context context, int textViewResourceId,
				ArrayList<PurchaseItem> objects) {
			super(context, textViewResourceId, objects);
			mContext = context;
			mLayoutResourceId = textViewResourceId;
			mPurchaseItems = objects;
		}

		@Override
		public int getCount() {
			return mPurchaseItems.size();
		}

		@Override
		public PurchaseItem getItem(int position) {
			return mPurchaseItems.get(position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if(convertView == null){
				LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
				convertView = inflater.inflate(mLayoutResourceId, parent, false);
			}

			((TextView)convertView.findViewById(R.id.item)).setText(getItem(position).name);
			((TextView)convertView.findViewById(R.id.price)).setText(getItem(position).price);

			return convertView;
		}
	}

	public static String getPreferredPack(ArrayList<String> favIdList){


		if(favIdList==null || favIdList.size() == 0)
			return null;

		String preferredPack = null;
		boolean z1 = false;
		boolean z2 = false;
		boolean z3 = false;
		boolean z4 = false;
		boolean z5 = false;
		boolean z6 = false;
		boolean sI,nI,sIU,nIU,c,cU1,cU2;


		if(favIdList.contains("1") 
				|| favIdList.contains("2") 
				|| favIdList.contains("3")){
			z1 = true;
			preferredPack = SKU_ZONE_1;
		} 
		if(favIdList.contains("4")
				|| favIdList.contains("5")
				|| favIdList.contains("6")){
			z2 = true; 
			preferredPack = SKU_ZONE_2;
		} 
		if(favIdList.contains("7")
				|| favIdList.contains("8")
				|| favIdList.contains("9")){
			z3 = true; 
			preferredPack = SKU_ZONE_3;
		} 
		if(favIdList.contains("10")
				|| favIdList.contains("11")
				|| favIdList.contains("12")){
			z4 = true; 
			preferredPack = SKU_ZONE_4;
		} 
		if(favIdList.contains("13")
				|| favIdList.contains("14")
				|| favIdList.contains("15")){
			z5 = true; 
			preferredPack = SKU_ZONE_5;
		} 
		if(favIdList.contains("13")
				|| favIdList.contains("14")
				|| favIdList.contains("15")
				|| favIdList.contains("16")
				|| favIdList.contains("17")
				|| favIdList.contains("18")
				|| favIdList.contains("19")
				|| favIdList.contains("20")
				|| favIdList.contains("21")
				|| favIdList.contains("22")
				|| favIdList.contains("23")
				|| favIdList.contains("24")
				|| favIdList.contains("25")
				|| favIdList.contains("26")
				|| favIdList.contains("27")
				|| favIdList.contains("28")
				|| favIdList.contains("29")
				|| favIdList.contains("30")){
			z6 = true; 
			preferredPack = SKU_ZONE_6;
		} 

		if(z1||z2||z3){
			if(hasPreferences(SKU_ZONE_1)
					|| hasPreferences(SKU_ZONE_2)
					|| hasPreferences(SKU_ZONE_3)
					){
				if(getPreferences(SKU_ZONE_1)
						|| getPreferences(SKU_ZONE_2)
						|| getPreferences(SKU_ZONE_3)
						){
					nIU = true;
				}
				else
					nI = true;
			}
			else
				nI = true;
		}

		if(z4||z5||z6){
			sI = true;
		}

		if((z1||z2||z3)&&(z4||z5||z6)){
			c = true;
		}

		return preferredPack;
	}

	/*private void dummyPurchase(String key) {
		savePreferences(key, true);
	}*/

}
