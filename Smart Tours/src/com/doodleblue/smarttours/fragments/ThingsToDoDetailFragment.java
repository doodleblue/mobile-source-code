package com.doodleblue.smarttours.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.doodleblue.smarttours.adapter.ThingsToDoDetailAdapter;
import com.doodleblue.smarttours.fragments.Map.MapMode;
import com.doodleblue.smarttours.model.Activity;
import com.doodleblue.smarttours.model.PlacePropertyBean;
import com.doodleblue.smarttours.model.Segment;
import com.doodleblue.smarttours.model.ThingsToDoDetailBean;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.view.SmartTextView;
import com.smarttoursnz.guide.R;

@SuppressLint("ValidFragment")
public class ThingsToDoDetailFragment extends BaseContainerFragment
implements View.OnClickListener {

	private ExpandableListView mExpandableListView;
	private ThingsToDoDetailAdapter mAdapter;

	private static ArrayList<Activity> mArrayList;
	private static String mSegmentId;
	private ProgressBar mProgressBar;

	public ThingsToDoDetailFragment(String segmentId, ArrayList<Activity> arrayList)
	{
		mSegmentId = segmentId;
		mArrayList = arrayList;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView =  inflater.inflate(R.layout.things_to_do_main,
				container, false);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	@Override
	public void onResume() {
		super.onResume();
		((SmartTextView)mRootView.findViewById(R.id.place_name)).setVisibility(View.VISIBLE);
		((Button)mRootView.findViewById(R.id.view_on_map)).setVisibility(View.VISIBLE);



	}

	@Override
	public void onPause() {
		super.onPause();
		((SmartTextView)mRootView.findViewById(R.id.place_name)).setVisibility(View.GONE);
		((Button)mRootView.findViewById(R.id.view_on_map)).setVisibility(View.GONE);
	}

	private void initView()
	{
		mProgressBar = (ProgressBar)mRootView.findViewById(R.id.progressBar);
		((Button)mRootView.findViewById(R.id.view_on_map)).setOnClickListener(this);
		mExpandableListView = (ExpandableListView)mRootView.findViewById(R.id.expandableListView1);

		mExpandableListView.setGroupIndicator(null);
		mExpandableListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				if(mAdapter.getChildType(groupPosition,childPosition)!=0){
					replaceFragment(
							mAdapter.getChild(groupPosition,childPosition),
							mAdapter.getChildType(groupPosition,childPosition));
				}
				return true;
			}
		});
		/*expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if(parent.isGroupExpanded(groupPosition)){
                    parent.collapseGroup(groupPosition);
                }else{
                    boolean animateExpansion = false;
                    parent.expandGroup(groupPosition,animateExpansion);
                }
                //telling the listView we have handled the group click, and don't want the default actions.
                return true;
            }
        });*/

		fetchInformation();
	}

	private void fetchInformation(){
		FetchRecordTask fetchRecordTask = new FetchRecordTask();
		fetchRecordTask.execute();
	}

	public class FetchRecordTask extends AsyncTask<String,String,Segment> {
		
		ArrayList<PlacePropertyBean> listHeader = new ArrayList<PlacePropertyBean>();
		ArrayList<ArrayList<ThingsToDoDetailBean>> listChild = new ArrayList<ArrayList<ThingsToDoDetailBean>>();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Segment doInBackground(String... params) {
			Segment segment =  DatabaseHelper.getInstance().fetchSegment(mSegmentId).get(0);
			
			//Code to data feed
			
			listHeader.add(new PlacePropertyBean("Information",R.drawable.info_img,String.valueOf(0)));
			listHeader.add(new PlacePropertyBean("Paid Activities",R.drawable.paidact_img,String.valueOf(0)));
			listHeader.add(new PlacePropertyBean("Attractions",R.drawable.attraction_img,String.valueOf(0)));
			listHeader.add(new PlacePropertyBean("Food & Drink",R.drawable.food_drink_img,String.valueOf(0)));
			listHeader.add(new PlacePropertyBean("Accommodation",R.drawable.accomodation_img,String.valueOf(0)));
			listHeader.add(new PlacePropertyBean("Other",R.drawable.others_img,String.valueOf(0)));

			ArrayList<ThingsToDoDetailBean> list1 = new ArrayList<ThingsToDoDetailBean>();
			ArrayList<ThingsToDoDetailBean> list2 = new ArrayList<ThingsToDoDetailBean>();
			ArrayList<ThingsToDoDetailBean> list3 = new ArrayList<ThingsToDoDetailBean>();
			ArrayList<ThingsToDoDetailBean> list4 = new ArrayList<ThingsToDoDetailBean>();
			ArrayList<ThingsToDoDetailBean> list5 = new ArrayList<ThingsToDoDetailBean>();
			ArrayList<ThingsToDoDetailBean> list6 = new ArrayList<ThingsToDoDetailBean>();
			//if(activity.getType().equals("0"))      //    Information
			{
				ThingsToDoDetailBean bean1 = new ThingsToDoDetailBean(
						"Information", segment.getName(), segment.getImageLocal());
				bean1.setDescription(segment.getDescription());
				bean1.setSegmentId(segment.getId());
				bean1.setCoverImage(segment.getImage());
				bean1.setCoverImageLocal(segment.getImageLocal());
				bean1.setAudio(segment.getAudio());
				bean1.setAudioLocal(segment.getAudioLocal());
				bean1.setId(segment.getId());
				bean1.setHyperlink(segment.getHyperLink());
				bean1.setnStops(segment.getnStops());
				bean1.setnActivities(segment.getnActivities());
				list1.add(bean1);
			}
			for(Activity activity : mArrayList)
			{
				if(activity.getType().equals("Paid Activity"))    // Paid Activities
				{
					ThingsToDoDetailBean bean2 = new ThingsToDoDetailBean(
							"Paid Activities", activity.getTitle(), activity.getImageWideLocal());
					bean2.setOperator(activity.getOperator());
					bean2.setCoverImage(activity.getImage());
					bean2.setSegmentId(segment.getId());
					bean2.setCoverImageLocal(activity.getImageLocal());
					bean2.setCoverImageLarge(activity.getImageWide());
					bean2.setCoverImageLargeLocal(activity.getImageWideLocal());
					bean2.setDurationImageResId(activity.getDurationImageResId());
					bean2.setDuration(activity.getDuration());
					bean2.setOpenedTime(activity.getTime());
					bean2.setPriceImageResId(activity.getPriceImageResId());
					bean2.setPrice(activity.getPrice());
					bean2.setHighlights(activity.getHighlights());
					bean2.setDescription(activity.getDescription());

					bean2.setId(activity.getId());
					list2.add(bean2);
				}
				else  if(activity.getType().equals("Free Activity"))    //  Attractions
				{
					ThingsToDoDetailBean bean3 = new ThingsToDoDetailBean(
							"Paid Activities", activity.getTitle(), activity.getImageWideLocal());
					bean3.setOperator(activity.getOperator());
					bean3.setCoverImage(activity.getImage());
					bean3.setCoverImageLocal(activity.getImageLocal());
					bean3.setSegmentId(segment.getId());
					bean3.setCoverImageLarge(activity.getImageWide());
					bean3.setCoverImageLargeLocal(activity.getImageWideLocal());
					bean3.setDurationImageResId(activity.getDurationImageResId());
					bean3.setDuration(activity.getDuration());
					bean3.setOpenedTime(activity.getTime());
					bean3.setPriceImageResId(activity.getPriceImageResId());
					bean3.setPrice(activity.getPrice());
					bean3.setHighlights(activity.getHighlights());
					bean3.setDescription(activity.getDescription());

					bean3.setId(activity.getId());
					list3.add(bean3);
				}
				else if(activity.getType().equals("Food & Drink"))     //   Food & Drink
				{
					ThingsToDoDetailBean bean4 = new ThingsToDoDetailBean(
							"Food & Drink", activity.getTitle(), activity.getImageWideLocal());
					bean4.setType(activity.getType());
					bean4.setSubType(activity.getSubtitle());
					bean4.setSegmentId(segment.getId());
					bean4.setOpenedTime(activity.getTime());
					bean4.setCoverImage(activity.getImage());
					bean4.setCoverImageLocal(activity.getImageLocal());
					bean4.setCoverImageLarge(activity.getImageWide());
					bean4.setCoverImageLargeLocal(activity.getImageWideLocal());
					bean4.setPriceImageResId(activity.getPriceImageResId());
					bean4.setPrice(activity.getPrice());
					bean4.setCuisine(activity.getCuisine());
					bean4.setDescription(activity.getDescription());
					bean4.setId(activity.getId());
					list4.add(bean4);
				}
				else if(activity.getType().equals("Accommodation"))     //Accommodation
				{
					ThingsToDoDetailBean bean6 = new ThingsToDoDetailBean(
							"Accommodation", activity.getTitle(), activity.getImageWideLocal());
					bean6.setDurationImageResId(activity.getRateImageResId());
					bean6.setSubType(activity.getSubtitle());
					bean6.setCoverImage(activity.getImage());
					bean6.setCoverImageLocal(activity.getImageLocal());
					bean6.setSegmentId(segment.getId());
					bean6.setCoverImageLarge(activity.getImageWide());
					bean6.setCoverImageLargeLocal(activity.getImageWideLocal());
					bean6.setType(activity.getType());
					bean6.setRating(activity.getRatings());
					bean6.setPriceImageResId(activity.getPriceImageResId());
					bean6.setPrice(activity.getPrice());
					bean6.setFacility(activity.getFacility());
					bean6.setDescription(activity.getDescription());
					bean6.setId(activity.getId());
					list5.add(bean6);
				}
				else if(activity.getType().equals("Other"))     //Others
				{
					ThingsToDoDetailBean bean9 = new ThingsToDoDetailBean(
							"Other", activity.getTitle(), activity.getImageWideLocal());
					bean9.setType(activity.getType());
					bean9.setSubType(activity.getSubtitle());
					bean9.setCoverImage(activity.getImage());
					bean9.setSegmentId(segment.getId());
					bean9.setCoverImageLocal(activity.getImageLocal());
					bean9.setCoverImageLarge(activity.getImageWide());
					bean9.setCoverImageLargeLocal(activity.getImageWideLocal());
					bean9.setDescription(activity.getDescription());
					bean9.setHighlights(activity.getHighlights());
					bean9.setOpenedTime(activity.getTime());
					bean9.setPriceImageResId(activity.getPriceImageResId());
					bean9.setPrice(activity.getPrice());
					bean9.setFacility(activity.getFacility());
					bean9.setId(activity.getId());
					list6.add(bean9);
				}

			}

			
			listChild.add(list1);
			listChild.add(list2);
			listChild.add(list3);
			listChild.add(list4);
			listChild.add(list5);
			listChild.add(list6);
			
			return segment;
		}

		@Override
		protected void onPostExecute(Segment segment) {
			super.onPostExecute(segment);
			((SmartTextView)mRootView.findViewById(R.id.place_name)).setText(segment.getName());
			
			//if(mAdapter == null)
			//{
			mAdapter = new ThingsToDoDetailAdapter(mActivity,listHeader, listChild, new HyperlinkListener());
			mExpandableListView.setAdapter(mAdapter);
			//}
			//else
			//{
			//    mAdapter.notifyDataSetChanged();
			//    mExpandableListView.setGroupIndicator(null);
			//}
			mProgressBar.setVisibility(View.GONE);
		}
		
		
		
	}



	private void replaceFragment(ThingsToDoDetailBean bean,int type) {
		((BaseContainerFragment)getParentFragment()).replaceFragment(new Description(bean, type), true);
	}

	private void replaceFragment(MapMode mode, String segmentId, boolean isPlace) {
		((BaseContainerFragment)getParentFragment()).replaceFragment(new Map(mode, segmentId, null, isPlace), true);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
		case R.id.view_on_map:
		{
			Map.setMapMode(MapMode.SEGMENT, null, mSegmentId,null);
        	mActivity.mTabHost.setCurrentTab(1);
			//replaceFragment(MapMode.SEGMENT,mSegmentId,true);
			break;
		}
		}
	}

	public class HyperlinkListener implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			if(!TextUtils.isEmpty((String) v.getTag())){
				Toast.makeText(mActivity, (String) v.getTag(), Toast.LENGTH_SHORT).show();
				replaceFragment((String) v.getTag());
			}
		}
	}

	private void replaceFragment(String url) {
		((BaseContainerFragment)getParentFragment()).replaceFragment(new Web(url), true);
	}

}
