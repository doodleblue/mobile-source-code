package com.doodleblue.smarttours.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doodleblue.smarttours.fragments.Map.MapMode;
import com.doodleblue.smarttours.model.Favorite;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Tours;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.doodleblue.smarttours.view.Info;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */

@SuppressLint("ValidFragment")
public class RouteDetail extends BaseContainerFragment implements View.OnClickListener {

    private Route mRoute;
    
    public RouteDetail(Route route)
    {
        mRoute = route;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.route_detail, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((ProgressBar)mRootView.findViewById(R.id.progressBar)).setVisibility(View.VISIBLE);
        initView();
        //((ProgressBar)mRootView.findViewById(R.id.progressBar)).setVisibility(View.INVISIBLE);
    }
    
    @Override
    public void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	
    }

    private void initView() {
        
        ((ImageView)mRootView.findViewById(R.id.route_thumb_image)).setImageBitmap(
        		Utils.getRoundedCornerBitmap(
        				Utils.getImageFromString(mRoute.getImageLocal()),
        				10)
        		);

        if(mRoute.isFavourite()){
            ((ImageView)mRootView.findViewById(R.id.favHeart)).setImageDrawable(getResources().getDrawable(R.drawable.profile_fav_select_btn));
            ((Button)mRootView.findViewById(R.id.addFavorites)).setVisibility(View.GONE);
            ((Button)mRootView.findViewById(R.id.remFavorites)).setVisibility(View.VISIBLE);
        } else {
            ((ImageView)mRootView.findViewById(R.id.favHeart)).setImageDrawable(getResources().getDrawable(R.drawable.profile_fav_unselect_btn));
            ((Button)mRootView.findViewById(R.id.addFavorites)).setVisibility(View.VISIBLE);
            ((Button)mRootView.findViewById(R.id.remFavorites)).setVisibility(View.GONE);
        }
        fetchRecord();
        
        ((Button)mRootView.findViewById(R.id.addFavorites)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.remFavorites)).setOnClickListener(this);
    }

    private void fetchRecord()
    {
        FetchRecordTask task = new FetchRecordTask();
        task.execute();
    }

    public class FetchRecordTask extends AsyncTask<String,String,String> {

        private String nTours;
        private String nStops;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            //Generate No of Places / Tours
            nTours = String.valueOf(((ArrayList<Tours>) DatabaseHelper.getInstance(mActivity).fetchTours(mRoute.getId())).size());

            //Generate No of Stops //TODO: Retrieve Stops here
            //nStops =

            return null;
        }

        @Override
        protected void onPostExecute(String text) {
            super.onPostExecute(text);
            if(nTours!=null)
                ((Info)mRootView.findViewById(R.id.places)).setText(nTours+" Places");
            if(nStops!=null)
                ((Info)mRootView.findViewById(R.id.stops)).setText(nStops+" Stops");
            initAll();
        }
    }

    private void initAll(){
        
        ((TextView)mRootView.findViewById(R.id.route_name)).setText(mRoute.getName());
        ((TextView)mRootView.findViewById(R.id.start_value)).setText(mRoute.getStartName());
        ((TextView)mRootView.findViewById(R.id.dest_value)).setText(mRoute.getEndName());
        ((Info)mRootView.findViewById(R.id.duration)).setText(mRoute.getMinDay() + " to " + mRoute.getMaxDay());
        ((Info)mRootView.findViewById(R.id.distance)).setText(mRoute.getDistance());
        ((Info)mRootView.findViewById(R.id.poi)).setText(mRoute.getnPOI()+" POI");
        ((Info)mRootView.findViewById(R.id.stops)).setText(mRoute.getnStops()+" Stops");
        ((Info)mRootView.findViewById(R.id.places)).setText(mRoute.getnPlaces()+" Places");
        ((TextView)mRootView.findViewById(R.id.highlights_value)).setText(mRoute.getHighlights());
        ((TextView)mRootView.findViewById(R.id.description_value)).setText(mRoute.getDescription());

        //((ImageView)mRootView.findViewById(R.id.route_image)).setImageURI();

        (mRootView.findViewById(R.id.view_on_map)).setOnClickListener(this);
        (mRootView.findViewById(R.id.total_dwld_size)).setOnClickListener(this);
        ((ProgressBar)mRootView.findViewById(R.id.progressBar)).setVisibility(View.INVISIBLE);
    }

    private void replaceFragment() {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Tab1AddOnFragment(), true);
    }
    
    private void replaceFragment(int i) {
		((BaseContainerFragment) getParentFragment()).replaceFragment(
				new Inapp(), true);
	}

    private void replaceToMapFragment(String routeId) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Map(routeId), true);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.view_on_map:
            {
                //showDevMsg();
            	Map.setMapMode(MapMode.ROUTE, mRoute.getId(), null,null);
            	mActivity.mTabHost.setCurrentTab(1);
                //replaceToMapFragment(mRoute.getId());
                
                break;
            }
            case R.id.addFavorites:
            {
                DatabaseHelper.getInstance(mActivity).insertOrUpdateFavorite(new Favorite(mRoute.getId(),false)); //TODO: Update while in-app purchase is introduced
                v.setVisibility(View.GONE);
                mRoute.setFavourite(true);
                ((Button)mRootView.findViewById(R.id.remFavorites)).setVisibility(View.VISIBLE);
                ((ImageView)mRootView.findViewById(R.id.favHeart)).setImageDrawable(getResources().getDrawable(R.drawable.profile_fav_select_btn));
                break;
            }
            case R.id.remFavorites:
            {
                DatabaseHelper.getInstance(mActivity).deleteRecord(DatabaseHelper.TABLE_FAVORITE,DatabaseHelper.KEY_ROUTE_ID,mRoute.getId());
                v.setVisibility(View.GONE);
                mRoute.setFavourite(false);
                ((Button)mRootView.findViewById(R.id.addFavorites)).setVisibility(View.VISIBLE);
                ((ImageView)mRootView.findViewById(R.id.favHeart)).setImageDrawable(getResources().getDrawable(R.drawable.profile_fav_unselect_btn));
                break;
            }

            case R.id.total_dwld_size:
                /*Toast.makeText(mActivity, getResources().getString(R.string.download_alert), Toast.LENGTH_SHORT).show();
                DownloadTask downloadTask = new DownloadTask(mActivity);
                downloadTask.execute(new String[]{mRoute.getId()});*/
            	
            	//replaceFragment(0);
                break;
        }
    }



}
