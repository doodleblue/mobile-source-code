package com.doodleblue.smarttours.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.adapter.StopsAdapter;
import com.doodleblue.smarttours.adapter.ToursAdapter;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.Favorite;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.model.Tours;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/30/13
 * Time: 12:02 PM
 * To change this template use File | Settings | File Templates.
 */
@SuppressLint("ValidFragment")
public class TourStopList extends BaseContainerFragment implements View.OnClickListener{

	private Route mRoute;
	private StopsAdapter mStopsAdapter;
	private ToursAdapter mToursAdapter;
	private boolean isCommentaryPurchased;

	public TourStopList(Route route){
		mRoute = route;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView =  inflater.inflate(R.layout.tour_stop_list, null);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	private void initView() {
		((TextView)mRootView.findViewById(R.id.route_name)).setText(mRoute.getName());

		((ImageView)mRootView.findViewById(R.id.route_image)).setImageBitmap(
				Utils.getImageFromString(mRoute.getImage2Local())
				);

		mRootView.findViewById(R.id.tours_list_lay).setOnClickListener(this);
		mRootView.findViewById(R.id.stops_list_lay).setOnClickListener(this);


	}

	private void fetchToursRecord()
	{
		FetchRecordTask task = new FetchRecordTask();
		task.execute();
	}

	public class FetchRecordTask extends AsyncTask<String,String,ArrayList<Tours>> {

		private Favorite favorite;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected ArrayList<Tours> doInBackground(String... params) {
			ArrayList<Tours> arrayList = DatabaseHelper.getInstance().fetchTours(mRoute.getId());
			favorite = DatabaseHelper.getInstance(mActivity).fetchFavorite(mRoute.getId());
			return arrayList;
		}

		@Override
		protected void onPostExecute(ArrayList<Tours>  arrayList) {
			super.onPostExecute(arrayList);
			/*if(favorite!=null)
                if(!favorite.isCommentaryPurchased()){
                    ((TextView)mRootView.findViewById(R.id.commentary_text)).setVisibility(View.VISIBLE);
                    isCommentaryPurchased = false;
                }*/
			if(arrayList!=null&&arrayList.size()>0)
			{
				if(mToursAdapter == null)
					mToursAdapter = new ToursAdapter(mActivity,arrayList);
				else
					mToursAdapter.notifyDataSetChanged();

				replaceFragment(mRoute,mToursAdapter);
			}

		}
	}

	private void fetchStopsRecord(){
		FetchStopsRecordTask task = new FetchStopsRecordTask();
		task.execute();
	}

	public class FetchStopsRecordTask extends AsyncTask<String,String,HashMap<String,ArrayList<Stops>>> {

		private Favorite favorite;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected HashMap<String,ArrayList<Stops>> doInBackground(String... params) {
			ArrayList<Stops> arrayList = new ArrayList<Stops>();
			ArrayList<String> segmentList = new ArrayList<String>();
			ArrayList<Route> routesList = DatabaseHelper.getInstance().fetchRoutes(new String[]{mRoute.getId()});
			for(Route route:routesList){
				try {
					JSONObject jsonObject = new JSONObject(route.getSegmentMeta());
					JSONArray segmentsList = new JSONArray(jsonObject.getString("segment_meta"));
					for(int i=0;i<segmentsList.length();i++){
						if(!segmentList.contains(segmentsList.getString(i))){
							segmentList.add(segmentsList.getString(i));
							for(Stops stops:DatabaseHelper.getInstance(mActivity).fetchStops((String) segmentsList.getString(i))){
								arrayList.add(stops);
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			HashMap<String,ArrayList<Stops>> map = new HashMap<String, ArrayList<Stops>>();
			ArrayList<Stops> lookOuts = new ArrayList<Stops>();
			ArrayList<Stops> picnic = new ArrayList<Stops>();
			ArrayList<Stops> walks = new ArrayList<Stops>();
			ArrayList<Stops> toilets = new ArrayList<Stops>();

			for(Stops stop:arrayList){
				if(stop.getType().equals("Lookout")){
					lookOuts.add(stop);
				}else if(stop.getType().equals("Picnic")){
					picnic.add(stop);
				}else if(stop.getType().equals("Walk")){
					walks.add(stop);
				}else if(stop.getType().equals("Toilet")){
					toilets.add(stop);
				}
			}
			map.put(Stops.TYPES[0],lookOuts);
			map.put(Stops.TYPES[1],picnic);
			map.put(Stops.TYPES[2],walks);
			map.put(Stops.TYPES[3],toilets);

			//ToDo: Fetch Segment meta table to generate stops
			//favorite = DatabaseHelper.getInstance(mActivity).fetchFavorite(mRoute.getId());
			return map;
		}

		@Override
		protected void onPostExecute(HashMap<String,ArrayList<Stops>> map) {
			super.onPostExecute(map);
			/*if(favorite!=null)
                if(!favorite.isCommentaryPurchased()){
                    ((TextView)mRootView.findViewById(R.id.commentary_text)).setVisibility(View.VISIBLE);
                    isCommentaryPurchased = false;
                }*/
			if(map!=null&&map.size()>0)
			{
				if(mStopsAdapter == null)
					mStopsAdapter = new StopsAdapter(mActivity,map);
				else
					mStopsAdapter.notifyDataSetChanged();

				replaceFragment(mRoute,mStopsAdapter);
			}
		}
	}

	private void replaceFragment(Route route, ToursAdapter adapter) {
		((BaseContainerFragment)getParentFragment()).replaceFragment(new TourList(route,adapter), true);
	}

	private void replaceFragment(Route route, StopsAdapter adapter) {
		((BaseContainerFragment)getParentFragment()).replaceFragment(new StopList(route,adapter), true);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.tours_list_lay:
			fetchToursRecord();
			break;

		case R.id.stops_list_lay:
			fetchStopsRecord();
			break;

		}
	}
}
