package com.doodleblue.smarttours.fragments;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doodleblue.smarttours.adapter.StopsAdapter;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.doodleblue.smarttours.view.CoverImageView;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/30/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
@SuppressLint("ValidFragment")
public class StopList extends BaseContainerFragment {

	private Route mRoute;
	private StopsAdapter mStopsAdapter;

	public StopList(Route route, StopsAdapter adapter){
		mRoute = route;
		mStopsAdapter = adapter;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView =  inflater.inflate(R.layout.stop_list_, null);
		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	private void initView() {
		((TextView)mRootView.findViewById(R.id.route_name)).setText(mRoute.getName());
		Bitmap bitmap = Utils.getImageFromString(mRoute.getImage2Local());
		((CoverImageView)mRootView.findViewById(R.id.route_image)).setImageBitmap(bitmap);
		ExpandableListView stopsExpandableListView = ((ExpandableListView)mRootView.findViewById(R.id.stops_list));
		stopsExpandableListView.setGroupIndicator(null);
		stopsExpandableListView.setAdapter(mStopsAdapter);
		stopsExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				replaceFragment(
						mStopsAdapter.getChild(groupPosition, childPosition),
						mStopsAdapter.getChildType(groupPosition, childPosition)
						);
				return true;
			}
		});

		((RelativeLayout)mRootView.findViewById(R.id.stops_list_lay)).setVisibility(View.GONE);
	}

	private void replaceFragment(Stops stop, int type) {
		((BaseContainerFragment)getParentFragment()).replaceFragment(new StopDetail(stop, type), true);
	}
}
