package com.doodleblue.smarttours.opus.helper;

import android.content.Context;
import android.util.Log;

import com.smarttours.audio.codec.OpusManager;

public class SmartOpusManager extends OpusManager 
implements OpusManager.OnStateUpdateListener {

	public SmartOpusManager(Context context,OnStateUpdateListener listener) {
		super(context, listener);
	}

	@Override
	public void onStateUpdate(int state) {
		// TODO Auto-generated method stub
		Log.i(getClass().getSimpleName(), "OpusManager State: "+state);
	}

}
