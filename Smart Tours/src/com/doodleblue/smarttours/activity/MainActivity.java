package com.doodleblue.smarttours.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.fragments.MapContainer;
import com.doodleblue.smarttours.fragments.RouteLevelOne;
import com.doodleblue.smarttours.fragments.SettingsLevelOne;
import com.doodleblue.smarttours.fragments.ThingsToDoContainer;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.smarttoursnz.guide.R;

public class MainActivity extends FragmentActivity {

	public FragmentTabHost mTabHost;
	public SmartTours mApp = new SmartTours();

	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bottom_tabs);


		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		
		initializeTabs();
	}

	public void initializeTabs() {

		final TabHost.TabSpec routeTabTag       = mTabHost.newTabSpec(AppConstants.ROUTES);
		final TabHost.TabSpec mapTabTag         = mTabHost.newTabSpec(AppConstants.MAPS);
		final TabHost.TabSpec thingstodoTabTag  = mTabHost.newTabSpec(AppConstants.THINGS_TO_DO);
		final TabHost.TabSpec settingsTabTag    = mTabHost.newTabSpec(AppConstants.SETTINGS);

		mTabHost.addTab(
				setIndicator(
						getApplicationContext(),
						routeTabTag,
						R.drawable.route_tab_selector
						),
						RouteLevelOne.class,
						null);

		mTabHost.addTab(setIndicator(getApplicationContext(), mapTabTag, R.drawable.map_tab_selector),
				MapContainer.class, null);

		mTabHost.addTab(setIndicator(getApplicationContext(), thingstodoTabTag, R.drawable.things_to_do_selector),
				ThingsToDoContainer.class, null);

		mTabHost.addTab(setIndicator(getApplicationContext(), settingsTabTag, R.drawable.settings_tab_selector),
				SettingsLevelOne.class, null);

		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				if(routeTabTag.getTag().equals(tabId)){
					((TextView)findViewById(R.id.header_text))
					.setText(getResources().getString(R.string.route_tab));
				}else if(mapTabTag.getTag().equals(tabId)){
					((TextView)findViewById(R.id.header_text))
					.setText(getResources().getString(R.string.map_tab));
				}else if(thingstodoTabTag.getTag().equals(tabId)){
					((TextView)findViewById(R.id.header_text))
					.setText(getResources().getString(R.string.things_to_do_tab));
				}else if(settingsTabTag.getTag().equals(tabId)){
					((TextView)findViewById(R.id.header_text))
					.setText(getResources().getString(R.string.settings_tab));
				}
			}
		});


	}

	public TabHost.TabSpec setIndicator(Context ctx,TabHost.TabSpec spec, int resid) {
		View view = LayoutInflater.from(ctx).inflate(R.layout.tabs_icon, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
		imageView.setImageResource(resid);
		return spec.setIndicator(view);
	}




	@Override
	public void onBackPressed() {
		boolean isPopFragment = false;
		String currentTabTag = mTabHost.getCurrentTabTag();
		if (currentTabTag.equals(AppConstants.ROUTES)) {
			RouteLevelOne fragment3 = (RouteLevelOne) getSupportFragmentManager().findFragmentByTag(AppConstants.ROUTES);
			isPopFragment = ((BaseContainerFragment)fragment3.getChildFragmentManager().findFragmentByTag(fragment3.getCurrentTab())).popFragment();
		} else if (currentTabTag.equals(AppConstants.THINGS_TO_DO)) {
			isPopFragment = ((BaseContainerFragment)getSupportFragmentManager().findFragmentByTag(AppConstants.THINGS_TO_DO)).popFragment();
		} else if (currentTabTag.equals(AppConstants.SETTINGS)) {
			SettingsLevelOne fragmentSettings = (SettingsLevelOne) getSupportFragmentManager().findFragmentByTag(AppConstants.SETTINGS);
			isPopFragment = ((BaseContainerFragment)fragmentSettings.getChildFragmentManager().findFragmentByTag(fragmentSettings.getCurrentTab())).popFragment();
		}
		if (!isPopFragment) {
			finish();
		}
	}

}
