package com.doodleblue.smarttours.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.service.SplashDownloadService;
import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 12/9/13
 * Time: 9:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class SplashActivity extends Activity {

	private SeekBar splashProgressBar;
	private SmartTours mApp = new SmartTours();
	private final String NO_OF_TIMES_OPENED = "nOpened";
	private static final int SPLASH_TIME_OUT = 2000;
	private int nOpened;
	private TextView mProgressText;

	private SharedPreferences mSharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		initializeSplashLoader();
	}
	
	

	private void initializeSplashLoader(){
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mProgressText = (TextView)findViewById(R.id.textProgress2);

		if(hasPreferences(NO_OF_TIMES_OPENED)){
			nOpened = getPreferences(NO_OF_TIMES_OPENED);
		} else {
			nOpened = 0;
		}
		splashProgressBar = (SeekBar)findViewById(R.id.splashProgress);
		if(nOpened == 0){
			//Code to Download INITIAL DATA / ASSETS
			/*nOpened += 1;
			savePreferences(NO_OF_TIMES_OPENED, nOpened);*/
			if(SmartTours.isNetworkConnected()){
				Intent intent = new Intent(this, SplashDownloadService.class);
				intent.putExtra("receiver", new DownloadReceiver(new Handler()));
				startService(intent);
			} else {
				Toast.makeText(getApplicationContext(), 
						getResources().getString(R.string.check_network), 
						Toast.LENGTH_SHORT).show();
			}
		} else {
			//Code to Normal SPLASH
			nOpened += 1;
			savePreferences(NO_OF_TIMES_OPENED, nOpened);
			splashProgressBar.setVisibility(View.GONE);
			findViewById(R.id.laySplashProgress).setVisibility(View.GONE);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent i = new Intent(SplashActivity.this, MainActivity.class);
					startActivity(i);

					// close this activity
					finish();
				}
			}, SPLASH_TIME_OUT);
		}
	}

	private class DownloadReceiver extends ResultReceiver {
		private Intent intent;

		public DownloadReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultCode == SplashDownloadService.UPDATE_PROGRESS) {
				int progress = resultData.getInt("progress");
				splashProgressBar.setProgress(progress);
				//mProgressText.setText(resultData.getString("item"));
				if (progress >= 100) {
					if(intent == null){
						//mProgressText.setText(resultData.getString("item"));
						nOpened += 1;
						savePreferences(NO_OF_TIMES_OPENED, nOpened);
						intent = new Intent(SplashActivity.this,MainActivity.class);
						startActivity(intent);
						finish();
					}
				}
			}
		}
	}

	/*private void savePreferences(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }*/

	private void savePreferences(String key, int value) {
		SharedPreferences.Editor editor = mSharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	private int getPreferences(String key) {
		return mSharedPreferences.getInt(key, 0);
	}

	private boolean hasPreferences(String key) {
		return mSharedPreferences.contains(key);
	}


}
