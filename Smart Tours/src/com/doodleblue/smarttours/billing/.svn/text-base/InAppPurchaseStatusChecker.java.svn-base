package com.doodleblue.smarttours.billing;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.smarttoursnz.guide.BuildConfig;

public class InAppPurchaseStatusChecker {

	private static final String LOG_TAG = InAppPurchaseStatusChecker.class
			.getSimpleName();
	private final boolean DEBUG = BuildConfig.DEBUG;
	private IabHelper mHelper;
	private SharedPreferences mSharedPreferences;
	
	private final IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			Log.d(LOG_TAG, "Query inventory finished.");
			if (result.isFailure()) {
				Log.e(LOG_TAG, "Failed to query inventory: " + result);
				return;
			}

			if (inventory == null) {
				Log.e(LOG_TAG,
						"Can't check the in-app purchases. inventory==null");
				return;
			}
			ArrayList<String> mSkuList = new ArrayList<String>();
			//Get all sku
			for(Purchase purchase : inventory.getAllPurchases()){
            	mSkuList.add(purchase.getSku());
            }
			
			//get purchased sku
			ArrayList<String> purchasedProductList = new ArrayList<String>();
			//update in shared preferences
			for(String sku : mSkuList){
				if (inventory.hasPurchase("productId")){
					purchasedProductList.add(sku);
					savePreferences(sku, true);
				} else {
	            	savePreferences(sku, false);
				}
			}
			
			//update purchasedProductList in db
			
			
			//notify if needed

			if (mHelper != null)
				mHelper.dispose();
			mHelper = null;
		}
	};

	private final Context mContext;

	public InAppPurchaseStatusChecker(Context context) {
		mContext = context;
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public void check() {

		String base64 = 
				
				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1t1xyPSHiRvhooD8kQAtk3hnmSwkibhh9Ow6xZJ3PSJG95YswEb/9ah4rWTY7iGBX+ncYY8yMa2N56b4rENUbrcEsZ4NEPz+66LscbAn3cIAS7ZpblQkyPSK2hWaTS35FOLunzKCyZwutM8oi6ePkcM/flaMUhepF9DX7gXC0uLwcCDJEzjpm0RRc+FG1Qy5Ps6anUrY8OtmNUdyCkYWughpquhm+vQzugMdld+eL5OkFGx3XdRfgd6ehpUwMBMKh6BT/wK7AzlnTCnVe+InKdfq62x/oMOqw/pSNpweHZbuUyWf9ncNpRteJ57U9VqGTTLFiffCa+ihre07E0kBbQIDAQAB";
		//"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlLwOuR9MvGTkm9JHZEoZJe2yufUozMX4vj467o02eHEL1zrPBu6fXnxS+ESJt9VE4FQuaR4xlBeYlyp5ZrbVgEdglrj14hib4e8XXkzcD3/DOoYCD84W6hSUDxbZpcZYz2XQhPWQpz4UkGWB05IcwTrLV5DuLt4pivSTo1rtexx/UbTvNL/z9YdIRl0NGE7nijRqeKjLKFpzp2Zps89w0QR584CUdXtMdqhwYNtKDOMYDgFeNnuY/GKoVlwFIpxjPwSklCaRdLmtqz29co5twQfkGanV7Pq4gvkRwgtNEeRovpU2+9JzE41B35aYLMzuW9OVnirHN7RXNbjmFWDzjwIDAQAB";
		mHelper = new IabHelper(mContext, base64);

		mHelper.enableDebugLogging(DEBUG);

		// Start setup. This is asynchronous and the specified listener
		// will be called once setup completes.
		Log.d(LOG_TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				Log.d(LOG_TAG, "Setup finished.");

				if (!result.isSuccess()) {
					Log.e(LOG_TAG, "Problem setting up in-app billing: "
							+ result);
					return;
				}

				Log.d(LOG_TAG, "Setup successful. Querying inventory.");
				mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});
	}
	
	private void savePreferences(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
