package com.doodleblue.smarttours.service;

import java.util.ArrayList;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.Opus;
import com.doodleblue.smarttours.service.Tracker.Direction;
import com.smarttours.audio.codec.OpusManager;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 12/16/13
 * Time: 11:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class GuideControllableAudioService extends Service implements
AudioManager.OnAudioFocusChangeListener, OpusManager.OnStateUpdateListener {

	private final static String LOG_TAG = GuideControllableAudioService.class
			.getSimpleName();

	private Direction mCurrentDirection;

	private TelephonyManager mTelephonyManager;
	private PhoneStateListener mListener;
	private AudioManager mAudioManager;
	private PlayState mPlayState = PlayState.COMPLETED;
	private PlayerState mPlayerState = PlayerState.INTRODUCTION;
	int introDuration = 0;
	int mainDuraion = 0;
	private Tracker mTracker;
	private OpusManager mIntroPlayer;// = new OpusManager(this, this);
	private OpusManager mMainPlayer;// = new OpusManager(this, this);
	private Opus mCurrentOpus;
	private String mRouteId;

	public static final int UPDATE_AUDIO_POINTS = 2014;
	private enum PlayerState{
		INTRODUCTION, DESCRIPTION
	}
	private enum PlayState {
		INTRO, AUDIO, COMPLETED
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		try {
			if(intent.hasExtra("routeId"))
				mRouteId = intent.getStringExtra("routeId");
			else
				stopSelf();
		} catch (Exception e) {
			e.printStackTrace();
			this.stopSelf();
		}

		if(mRouteId!=null){
			mTracker = new Tracker(mRouteId) {
				@Override
				protected void onCatchedAudio(Opus opus, Direction direction) {
					playAudio(opus, direction);
				}
			};
			mTracker.getmOpuses();
			ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
			Bundle resultData = new Bundle();
			//resultData.putStringArrayList("segmentIDList", mSegmentsList);
			resultData.putParcelableArrayList("audioPoints", (ArrayList<? extends Parcelable>) mTracker.getmOpuses());

			receiver.send(UPDATE_AUDIO_POINTS, resultData);
			mTracker.startRoute(Long.parseLong(mRouteId));

		}


	}

	@Override
	public void onCreate() {
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		mTelephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		// Create a PhoneStateListener to watch for off-hook and idle events
		mListener = new PhoneStateListener() {

			private boolean mIsPausedInCall;

			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				switch (state) {
				case TelephonyManager.CALL_STATE_OFFHOOK:
				case TelephonyManager.CALL_STATE_RINGING:
					// Phone going off-hook or ringing, pause the player.
					if (mPlayState != PlayState.COMPLETED) {
						pauseAudio();
						mIsPausedInCall = true;
					}
					break;
				case TelephonyManager.CALL_STATE_IDLE:
					// Phone idle. Rewind a couple of seconds and start playing.
					if (mIsPausedInCall) {
						mIsPausedInCall = false;
						resumeAudio();
					}
					break;
				}
			}
		};

		// Register the listener with the telephony manager.
		mTelephonyManager.listen(mListener,
				PhoneStateListener.LISTEN_CALL_STATE);

	}

	@Override
	public synchronized int onStartCommand(Intent intent, int flags, int startId) {
		if (intent == null) {
			return super.onStartCommand(intent, flags, startId);
		}

		String action = intent.getAction();

		/*if (Actions.START_ROUTE.equals(action)) {
            mRouteId = GuideManager.getId(intent);
            mDataAdapter.setRouteId(mRouteId);
            mTracker.startRoute(mRouteId);
        } else if (Actions.STOP_ROUTE.equals(action)) {
            mTracker.stopRoute();
        } else*/ if (Actions.PLAY_AUDIO.equals(action)) {
        	//long audioId = GuideManager.getId(intent);
        	//playAudio(audioId);
        	//playAudio(mCurrentOpus);
        	Log.i(LOG_TAG,"Actions.PLAY_AUDIO"+Actions.PLAY_AUDIO);
        } /*else if (Actions.PAUSE_AUDIO.equals(action)) {
            pauseAudio();
        } else if (Actions.RESUME_AUDIO.equals(action)) {
            resumeAudio();
        } else if (Actions.NEXT_AUDIO.equals(action)) {
            playNextByDirecton(mTracker.currentDirection);
        } else if (Actions.PREV_AUDIO.equals(action)) {
            playNextByDirecton(mTracker.currentDirection.opposite());
        } else if (Actions.PLAYING_AUDIO_RESTORED.equals(action)) {
            if (mPlayState != PlayState.COMPLETED) {
                notifyPlayingAudioRestored();
            }
        }*/

        return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		switch (focusChange) {
		case AudioManager.AUDIOFOCUS_GAIN:
			resumeAudio();
			break;

		case AudioManager.AUDIOFOCUS_LOSS:
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			pauseAudio();
			break;

		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			// Lost focus for a short time, but it's ok to keep playing
			// at an attenuated level
			//audioLowSound();
			break;
		}
	}

	@Override
	public void onDestroy() {
		mTelephonyManager.listen(mListener, PhoneStateListener.LISTEN_NONE);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;  //To change body of implemented methods use File | Settings | File Templates.
	}


	private void playAudio(Opus opus, Direction direction) {
		if (opus == null) {
			return;
		}
		mCurrentOpus = opus;
		if (mCurrentOpus == null) {
			throw new RuntimeException("Audio for path point " + opus.getId()
					+ " not found");
		}
		mCurrentDirection = direction;
		playAudio();
	}

	private synchronized void playAudio() {
		String audioPath = null;

		switch (mPlayerState) {
		case INTRODUCTION:
			switch (mCurrentDirection) {
			case FORWARD:
				audioPath = mCurrentOpus.getAudioForwardLocal();
				break;
			case REVERSE:
				audioPath = mCurrentOpus.getAudioReverseLocal();
				break;

			default:
				break;
			}
			if (mCurrentOpus == null) {
				return;	
			}
			if(mIntroPlayer == null) mIntroPlayer = new OpusManager(this, this);
			mIntroPlayer.startPlaying(AppConstants.ROOT_DIRECTORY+audioPath);
			mPlayerState = PlayerState.DESCRIPTION;

			break;
		case DESCRIPTION:
			audioPath = mCurrentOpus.getAudioMainLocal();
			if (mCurrentOpus == null) {
				return;	
			}
			if(mMainPlayer == null) mMainPlayer = new OpusManager(this, this);
			mMainPlayer.startPlaying(AppConstants.ROOT_DIRECTORY+audioPath);
			mPlayerState = PlayerState.INTRODUCTION;

			break;

		default:
			break;
		}
	}

	@SuppressWarnings("incomplete-switch")
	private synchronized void pauseAudio() {
		switch (mPlayState) {
		case INTRO:
			mIntroPlayer.stopPlaying();
			notify(Actions.STOP_AUDIO);
			break;
		case AUDIO:
			mMainPlayer.stopPlaying();
			notify(Actions.STOP_AUDIO);
			break;
		}
		releaseAudioFocus();
	}
	private void releaseAudioFocus() {
		mAudioManager.abandonAudioFocus(this);
		
	}

	private synchronized void resumeAudio() {
		/*switch (mPlayState) {
		case INTRO:
			//mIntroPlayer.setVolume(1f, 1f);
			mIntroPlayer.startPlaying(mCurrentOpus.getAudioForwardLocal());
			notify(Actions.RESUME_AUDIO);
			break;
		case AUDIO:
			//mMainPlayer.setVolume(1f, 1f);
			mMainPlayer.startPlaying(mCurrentOpus.getAudioForwardLocal());
			notify(Actions.RESUME_AUDIO);
			break;
		case COMPLETED:
			playAudio();
			break;
		}*/
		playAudio();
		notify(Actions.PLAY_AUDIO);
	}

	/*private class TimeUpdater extends Thread {
        private boolean mIsCompleted = false;
        private int mSize;

        public void complete() {
            mIsCompleted = true;
        }

        @Override
        public void run() {
            while (!mIsCompleted) {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                if (mPlayState != PlayState.COMPLETED) {
                    updateProgress();
                }
            }
        }

        private void updateProgress() {
            mSize = introDuration + mainDuraion;
            final int current = (mPlayState == PlayState.INTRO) ? mIntroPlayer
                    .getCurrentPosition() : introDuration
                    + mMainPlayer.getCurrentPosition();

            notifyPlaying(mSize, current);
        }
    }
	 */
	private void notify(String action) {
		Intent notif = new Intent(action);
		sendBroadcast(notif);
	}

	private void notifyAudio(String action, long audioId) {
		Intent notif = new Intent(action);
		GuideObserver.putAudioId(notif, audioId);
		sendBroadcast(notif);
	}

	private void notifyPlaying(int size, int position) {
		Intent notif = new Intent(Actions.PLAYING);
		GuideObserver.putProgress(notif, size, position);
		sendBroadcast(notif);
	}

	@Override
	public void onStateUpdate(int state) {
		// TODO Auto-generated method stub

		//state 0-STOPPED, 1-PLAYING
		Log.i(getClass().getSimpleName(), "OpusManager State: "+state);

		switch (state) {
		case 0:
			switch (mPlayerState) {
			case DESCRIPTION:
				playAudio();
				break;
			default:
				break;
			}
			break;
		case 1:

			break;

		default:
			break;
		}


	}

}
