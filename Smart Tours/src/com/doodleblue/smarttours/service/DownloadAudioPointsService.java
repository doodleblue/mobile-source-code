package com.doodleblue.smarttours.service;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.doodleblue.smarttours.model.Downloaded;
import com.doodleblue.smarttours.model.Opus;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Segment;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;

/**
 * To Download Audio Points
 * @author Doodle
 *
 */
public class DownloadAudioPointsService extends IntentService {

	private static final String TAG = "AudioPointService";
	public static final String BROADCAST_ACTION = "com.doodleblue.smarttours.service.displayevents.audio";
	public static final int UPDATE_PROGRESS = 2015;

	private String mRouteId;

	public DownloadAudioPointsService() {
		super("AudioPointService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		double currentProgress = 1;
		ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
		
		if(intent.hasExtra("routeId"))
			mRouteId = (String)intent.getStringExtra("routeId");
		
		
		Bundle resultData = new Bundle();
		resultData.putInt("progress" ,(int)currentProgress);
		receiver.send(UPDATE_PROGRESS, resultData);

		if(mRouteId!=null){

			ArrayList<Route> routesList = DatabaseHelper.getInstance().fetchRoutes(new String[]{mRouteId});
			double nRouteProgress = (double)100/(double)routesList.size();
			for(Route route:routesList){
				if(route.getIsCommentaryPurchased().equals("true")){
					try {

						JSONObject jsonObject = new JSONObject(route.getSegmentMeta());
						JSONArray segmentsList = new JSONArray(jsonObject.getString("segment_meta"));

						double nAudioPointProgress = (double)nRouteProgress / (double)segmentsList.length();

						for(int i=0;i<segmentsList.length();i++){
							String segmentId = (String) segmentsList.getString(i);
							for(Opus opus: DatabaseHelper.getInstance().fetchOpus(segmentId)){
								Utils.downloadItem(opus.getId(),opus.getAudioForward(),opus.getAudioForwardLocal());
								Utils.downloadItem(opus.getId(),opus.getAudioReverse(),opus.getAudioReverseLocal());
								Utils.downloadItem(opus.getId(),opus.getAudioMain(),opus.getAudioMainLocal());

								resultData = new Bundle();
								currentProgress += nAudioPointProgress;
								resultData.putInt("progress" ,(int)currentProgress);
								receiver.send(UPDATE_PROGRESS, resultData);
							}
							for(Segment segment:DatabaseHelper.getInstance().fetchSegment(segmentId)){
								Utils.downloadItem(segment.getId(), segment.getAudio(), segment.getAudioLocal());
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				/*Downloaded downloaded = new Downloaded();
				downloaded.setRouteId(route.getId());
				DatabaseHelper.getInstance().insertOrUpdateDownloaded(downloaded);*/
			}
			if((int)currentProgress <100){
				resultData = new Bundle();
				resultData.putInt("progress" ,(int)100);
				receiver.send(UPDATE_PROGRESS, resultData);
			}
		}
	}

}
