package com.doodleblue.smarttours.service;

import android.app.IntentService;
import android.content.Intent;

import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.billing.InAppPurchaseStatusChecker;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/29/13
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckPurchasesService extends IntentService {

    public CheckPurchasesService(){
        super("CheckPurchasesService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    	
    	new InAppPurchaseStatusChecker(SmartTours.getContext()).check();

    }
}
