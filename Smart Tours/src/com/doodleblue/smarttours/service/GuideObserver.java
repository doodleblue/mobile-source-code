package com.doodleblue.smarttours.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 12/16/13
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class GuideObserver extends BroadcastReceiver {

    private static final String KEY_AUDIO_ID = "audio_id";
    private static final String KEY_SIZE = "size";
    private static final String KEY_POSITION = "position";

    public static void putAudioId(Intent intent, long audioId) {
        intent.putExtra(KEY_AUDIO_ID, audioId);
    }

    public static void putProgress(Intent intent, int size, int position) {
        intent.putExtra(KEY_SIZE, size);
        intent.putExtra(KEY_POSITION, position);
    }

    @Override
    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (Actions.PLAYING.equals(action)) {
            int size = intent.getIntExtra(KEY_SIZE, 0);
            int position = intent.getIntExtra(KEY_POSITION, 0);
            onPlaying(size, position);
            return;
        }

        if (Actions.PLAY_AUDIO.equals(action)) {
            long audioId = intent.getLongExtra(KEY_AUDIO_ID, 0);
            onAudioStarted(audioId);
            return;
        }

        if (Actions.PLAYING_AUDIO_RESTORED.equals(action)) {
            long audioId = intent.getLongExtra(KEY_AUDIO_ID, 0);
            onPlayingAudioRestored(audioId);
            return;
        }

        if (Actions.STOP_AUDIO.equals(action)) {
            long audioId = intent.getLongExtra(KEY_AUDIO_ID, 0);
            onAudioStopped(audioId);
            return;
        }


        if (Actions.PAUSE_AUDIO.equals(action)) {
            onPaused();
            return;
        }

        if (Actions.RESUME_AUDIO.equals(action)) {
            onResumed();
            return;
        }
    }

    public void register(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Actions.PLAY_AUDIO);
        intentFilter.addAction(Actions.STOP_AUDIO);
        intentFilter.addAction(Actions.PLAYING);
        intentFilter.addAction(Actions.PAUSE_AUDIO);
        intentFilter.addAction(Actions.RESUME_AUDIO);
        intentFilter.addAction(Actions.PLAYING_AUDIO_RESTORED);

        context.registerReceiver(this, intentFilter);
    }

    public void unregister(Context context) {
        context.unregisterReceiver(this);
    }

    protected abstract void onAudioStarted(long audioId);

    protected abstract void onPlayingAudioRestored(long audioId);

    protected abstract void onAudioStopped(long audioId);

    protected abstract void onPlaying(int size, int position);

    protected abstract void onPaused();

    protected abstract void onResumed();

}
