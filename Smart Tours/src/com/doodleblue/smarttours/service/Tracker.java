package com.doodleblue.smarttours.service;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.model.Opus;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.SegmentEntry;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;


public abstract class Tracker {

	public enum Direction {
		FORWARD,
		REVERSE
	}

	private Direction mCurrentDirection = Direction.FORWARD;
	private ArrayList<String> mPathOpusIds = new ArrayList<String>();

	private final static String LOG_TAG = Tracker.class.getSimpleName();
	private final LocationManager mLocationManager = (LocationManager) SmartTours.getContext().getSystemService(Context.LOCATION_SERVICE);

	private final static int CHECK_DIRECTION_PERIODICITY = 5 * 1000; //5mins
	private final static int CHECK_AUDIO_PERIODICY = 5 * 1000; //5mins
	private final static float LAMBDA = 0.382f;// value from 0.1 to 1.0
	private final static float RECHECK_DISTANCE_REDUCER = 0.4f;

	private final static long RECHECK_DISTANCE_REDUCER_IN_MILLIS = (long) (RECHECK_DISTANCE_REDUCER * 1000L);

	private final TimeChangeDetector mDirectionCheckThreshold = new TimeChangeDetector(CHECK_DIRECTION_PERIODICITY);
	private final TimeChangeDetector mAudioCheckThreshold = new TimeChangeDetector(CHECK_AUDIO_PERIODICY);

	//private final TrackerDataAdapter mDataAdapter;
	//private final DatabaseHelper mDatabaseHelper;

	private float mAverageSpeed;

	private long mLastTime;
	private Location mLastLocation;
	private int mLastOpusIndex;
	private Opus currentOpus;
	private Opus mPrevOpus;


	private String mPlayedAudioIds = "";
	private JSONArray mSegmentsList;
	private ArrayList<String> mSegmentIDList = new ArrayList<String>();

	//Direction currentDirection = Direction.FORWARD;
	private ArrayList<SegmentEntry> mSegmentWithOrderArrayList;

	private Route mRoute;
	private ArrayList<Opus> mOpuses;
	private String mStartSegment = "";
	private String mAssociateSegment = "";
	private String mEndSegment = "";

	private long mLastPathPointId;

	/*public Tracker(TrackerDataAdapter dataAdapter) {
        mDataAdapter = dataAdapter;
    }*/

	public Tracker(String routeId) {
		getPathSegments(routeId);
		generateOpuses();
	}

	private synchronized boolean getPathSegments(String routeId){
		//mSegmentWithOrderArrayList;
		mRoute = DatabaseHelper.getInstance().fetchRoutes(new String[]{routeId}).get(0);
		try {
			JSONObject json = new JSONObject(mRoute.getSegmentMeta());
			mSegmentsList = new JSONArray(json.getString("segment_meta"));
			for(int i=0;i<mSegmentsList.length();i++){
				if(!mSegmentIDList.contains(mSegmentsList.getString(i))){
					mSegmentIDList.add(mSegmentsList.getString(i));
				}
			}
			//mSegmentsList = json.optJSONArray("segment_meta");
			//Log.i(LOG_TAG,"mSegmentsList.length(): "+mSegmentsList.length());
			//mSegmentsList = new JSONArray(json.getString("segment_meta"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}


	private synchronized boolean generateOpuses(){
		mOpuses = new ArrayList<Opus>();

		for(String segmentId:mSegmentIDList){
			for(Opus opus:DatabaseHelper.getInstance().fetchOpus(segmentId)){
				if(!mOpuses.contains(opus)){
					mOpuses.add(opus);
				}
			}
		}
		Log.w(LOG_TAG,"mOpuses SIZE: "+mOpuses.size());

		/*for(int i=0;i<mSegmentsList.length();i++){

        	if(!mAssociateSegment.equals("")){
                mEndSegment = mAssociateSegment;
            }
            if(!mStartSegment.equals("")){
                mAssociateSegment = mStartSegment;
            }
            try {
				mStartSegment = (String) mSegmentsList.getString(i);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            //Log.i(LOG_TAG,"mStartSegment: "+mStartSegment);
            //Log.i(LOG_TAG,"mAssociateSegment: "+mAssociateSegment);
            //Log.i(LOG_TAG,"mEndSegment: "+mEndSegment);
            for(Opus opus: DatabaseHelper.getInstance().fetchOpus(mStartSegment,mAssociateSegment,mEndSegment)){
                if(!mOpuses.contains(opus))
                	mOpuses.add(opus);
                Log.w(LOG_TAG,"mOpuses ADDING OPUS: "+mOpuses);
            }
            Log.w(LOG_TAG,"mOpuses SIZE: "+mOpuses.size());
        }*/
		/* for (String segmentId:mSegmentsList){
            if(!mAssociateSegment.equals("")){
                mEndSegment = mAssociateSegment;
            }
            if(!mStartSegment.equals("")){
                mAssociateSegment = mStartSegment;
            }
            mStartSegment = segmentId;//segmentEntry.getSegmentId();
            Log.i(LOG_TAG,"mStartSegment: "+mStartSegment);
            Log.i(LOG_TAG,"mAssociateSegment: "+mAssociateSegment);
            Log.i(LOG_TAG,"mEndSegment: "+mEndSegment);
            for(Opus opus: DatabaseHelper.getInstance().fetchOpus(mStartSegment,mAssociateSegment,mEndSegment)){
                mOpuses.add(opus);
            }
        }*/
		return true;
	}

	public ArrayList<Opus> getmOpuses(){
		return mOpuses;
	}


	public void startRoute(long routeId) {
		try {
			mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
			//mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void stopRoute() {
		mLocationManager.removeUpdates(mLocationListener);
	}

	//protected abstract void onCatchedAudio(AudioPoint audio);
	protected abstract void onCatchedAudio(Opus opus, Direction direction);

	private void updateLocation(Location location) {
		long now = System.currentTimeMillis();
		if (mLastTime > 0) {
			updateLocation(location, now);
		}

		mLastTime = now;
		mLastLocation = location;
	}

	private synchronized void updateLocation(Location location, long byTime) {
		if (byTime == mLastTime) {
			return;
		}

		float distance = mLastLocation.distanceTo(location);
		float speed = (1000 * distance) / (byTime - mLastTime);

		mAverageSpeed = mLastTime == 0 ?
				speed :
					LAMBDA * speed + (1 - LAMBDA) * mAverageSpeed;

		//Log.i(LOG_TAG, "distance: "+distance);
		//Log.i(LOG_TAG, "speed: "+speed);
		//Log.i(LOG_TAG, "mAverageSpeed: "+mAverageSpeed);

		if (mAudioCheckThreshold.isChangedEnough(byTime)) {
			checkDistanceToNearestAudio(location);
		}

		/*if (mDirectionCheckThreshold.isChangedEnough(byTime)) {
            checkDirection(location);
        }*/

		/*GeoPoint currentPoint = new GeoPoint(location);
        for(Opus opus:mOpuses){
        	Log.i(LOG_TAG, "Lat: "+opus.getLatitudeInDouble());
        	Log.i(LOG_TAG, "Long: "+opus.getLongitudeInDouble());
            GeoPoint audioPoint = new GeoPoint(opus.getLatitudeInDouble(), opus.getLongitudeInDouble());
            Log.i(LOG_TAG, "Distance b/w AudioPoint & Current Locn: "+currentPoint.distanceTo(audioPoint));
        }
        Log.e(LOG_TAG, "Distance b/w AudioPoint & Current Locn: ********************************************");*/
	}

	/*private synchronized Opus getNearestAudio(Location location){
    	Opus opusNearest = null;
    	GeoPoint geoPoint = new GeoPoint(location);
        for(Opus opus:mOpuses){
            GeoPoint audioPoint = new GeoPoint(opus.getLatitudeInDouble(), opus.getLongitudeInDouble());
            if(audioPoint.distanceTo(geoPoint) < 30){
            	opusNearest = opus;
            }
        }
        return opusNearest;
    }
	 */
	private synchronized void checkDistanceToNearestAudio(Location location) {
		Log.e(LOG_TAG, "Checking Distance To Nearest Audio Point...");
		GeoPoint point = new GeoPoint(location);
		//AudioPoint audio = mDataAdapter.getNearestAudio(point, mPlayedAudioIds);
		//Opus opus = getNearestAudio(location);
		if(mOpuses!=null && mOpuses.size()>0){
			ArrayList<Integer> listDistance = new ArrayList<Integer>();
			for(Opus opus:mOpuses){
				GeoPoint audioPoint = new GeoPoint(opus.getLatitudeInDouble(), opus.getLongitudeInDouble());
				int distance = audioPoint.distanceTo(point);
				if(listDistance.size()==0){
					listDistance.add(distance);
					setCurrentOpus(opus);
				}
				else {
					Log.i(LOG_TAG, "Current Opus Info - distance: "+distance);
					Log.i(LOG_TAG, "Current Opus Info - Collections.min(listDistance): "+Collections.min(listDistance));
					if(distance < Collections.min(listDistance)){
						listDistance.add(distance);
						setCurrentOpus(opus);
					}
				}
			}
		}

		//Opus opus = null;    //Todo:Feed data source here.

		Log.e(LOG_TAG, "Current Opus Info : "+currentOpus.getId());
		Log.w(LOG_TAG, "3 Checking Distance To Nearest Audio Point...");

		if (currentOpus == null) {
			return;
		}
		Log.w(LOG_TAG, "4 Checking Distance To Nearest Audio Point...");
		GeoPoint audioPoint = new GeoPoint(currentOpus.getLatitudeInDouble(), 
				currentOpus.getLongitudeInDouble());
		int distance = point.distanceTo(audioPoint);
		
		//determine radius along with direction
		int radiusInSeconds = 0;
		switch (mCurrentDirection) {
			case FORWARD:
				radiusInSeconds = currentOpus.getRadiusForwardInSeconds();
				break;
			case REVERSE:
				radiusInSeconds = currentOpus.getRadiusReverseInSeconds();
				break;
			default:
				break;
		}
		
		if (isTooFar(distance, radiusInSeconds)) { 	//ToDo: Alter Forward and Reverse Correctly **********************
			Log.w(LOG_TAG, "5 Checking Distance To Nearest Audio Point...");
			return;
		}
		Log.w(LOG_TAG, "6 Checking Distance To Nearest Audio Point...");
		//		Log.d(LOG_TAG, "Catched audio: "+audio.audioId+". Prev: "+mPlayedAudioIds);

		if (!TextUtils.isEmpty(mPlayedAudioIds)) {
			mPlayedAudioIds += ",";
		}
		//mPlayedAudioIds += audio.audioId;
		mPlayedAudioIds += currentOpus.getId();

		//onCatchedAudio(audio);
		if(mPrevOpus == null || !mPrevOpus.equals(currentOpus)){
			onCatchedAudio(currentOpus, mCurrentDirection);
			mPrevOpus = currentOpus;
			
			mPathOpusIds.add(currentOpus.getId());
			for(String id:mPathOpusIds){
				Log.i(getClass().getName(), "mPathOpusIds: "+id);
			}
			
		}
		Log.w(LOG_TAG, "7 Checking Distance To Nearest Audio Point...");
	}

	private void setCurrentOpus(Opus opus){ 
		currentOpus = opus;
		mLastOpusIndex = Integer.parseInt(currentOpus.getId());
		mStartSegment = opus.getStartSegment();
		mEndSegment = opus.getEndSegment();
		mAssociateSegment = opus.getAssociateSegment();
	}

	/*private void checkDirection(Location location) {
        GeoPoint point = new GeoPoint(location);
        long pathPointId = mDataAdapter.getNearestPathPointId(point);

        if (pathPointId == mLastPathPointId) {
            return;
        }

        //Direction newDirection = pathPointId > mLastPathPointId ? Direction.FORWARD : Direction.REVERSE;
        mLastPathPointId = pathPointId;

        if (currentDirection == newDirection) {
            return;
        }

        currentDirection = newDirection;
    }*/

	private boolean isTooFar(int distanceMeters, int delaySeconds) {
		//delaySeconds = 100000;
		Log.e(LOG_TAG, "isTooFar");
		int arrivalIn = (int) (distanceMeters / mAverageSpeed);
		int crossZoneIn = arrivalIn - delaySeconds;
		Log.d(LOG_TAG, "arrivalIn: " + arrivalIn);
		Log.d(LOG_TAG, "crossZoneIn: " + crossZoneIn);

		if (crossZoneIn < 0) {
			Log.w(LOG_TAG, "crossZoneIn Inn");
			return false;
		}

		long nextCheckInMillis = RECHECK_DISTANCE_REDUCER_IN_MILLIS * crossZoneIn;
		if (nextCheckInMillis > CHECK_AUDIO_PERIODICY) {
			nextCheckInMillis = CHECK_AUDIO_PERIODICY;
		}

		Log.d(LOG_TAG, "Speed (m/s): " + mAverageSpeed);
		Log.d(LOG_TAG, "Delay (s): "+delaySeconds);
		Log.d(LOG_TAG, "Distance (m): "+distanceMeters);
		Log.d(LOG_TAG, "arrivalIn (s): "+arrivalIn);
		Log.d(LOG_TAG, "crossZoneIn (s): "+crossZoneIn);
		Log.d(LOG_TAG, "nextCheckInMillis (ms): "+nextCheckInMillis);

		mAudioCheckThreshold.setMinTimeChange(nextCheckInMillis);
		return true;
	}

	private final LocationListener mLocationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			updateLocation(location);
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};
}

