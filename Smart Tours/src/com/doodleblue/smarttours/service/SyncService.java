package com.doodleblue.smarttours.service;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.Activity;
import com.doodleblue.smarttours.model.ActivityAccessory;
import com.doodleblue.smarttours.model.JActivity;
import com.doodleblue.smarttours.model.JRoutes;
import com.doodleblue.smarttours.model.JSegments;
import com.doodleblue.smarttours.model.JStops;
import com.doodleblue.smarttours.model.JSync;
import com.doodleblue.smarttours.model.JSyncSet;
import com.doodleblue.smarttours.model.JTours;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Segment;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.model.Tours;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;
import com.google.gson.Gson;

public class SyncService extends IntentService{

	private static final String TAG = "SyncService";
	public static final String BROADCAST_ACTION = "com.doodleblue.smarttours.service.displayevent";
	public static final int UPDATE_PROGRESS = 2013;
	
	private Bundle resultData;
	private Intent intent;
	private double currentProgress = 1;
	private ResultReceiver receiver;

	public SyncService(){
		super("SyncService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
		/*resultData = new Bundle();
		resultData.putInt("progress" ,(int)currentProgress);
		receiver.send(UPDATE_PROGRESS, resultData);*/
		
		updateProgress(1);

		if(intent.hasExtra("url")){
			String url = intent.getStringExtra("url");
			InputStream inputStream = Utils.retrieveStream(url);
			
			if(inputStream!=null){
				Gson gson = new Gson();
				Reader reader = new InputStreamReader(inputStream);
				JSync response = gson.fromJson(reader, JSync.class);
				JSyncSet createdSet = response.created;
				JSyncSet modifiedSet = response.modified;

				double progressTotal = 
						createdSet.routes.size() +
						createdSet.segment.size() +
						createdSet.tours.size() +
						createdSet.stops.size() +
						createdSet.activity.size() +
						modifiedSet.routes.size() +
						modifiedSet.segment.size() +
						modifiedSet.tours.size() +
						modifiedSet.stops.size() +
						modifiedSet.activity.size();

				double nRouteCreatedProgress 	= (double)progressTotal/(double)createdSet.routes.size();
				double nTourCreatedProgress 	= (double)progressTotal/(double)createdSet.tours.size();
				double nActivityCreatedProgress = (double)progressTotal/(double)createdSet.activity.size();
				double nStopCreatedProgress 	= (double)progressTotal/(double)createdSet.stops.size();
				double nSegmentCreatedProgress 	= (double)progressTotal/(double)createdSet.segment.size();
				
				double nRouteModifiedProgress 	= (double)progressTotal/(double)modifiedSet.routes.size();
				double nSegmentModifiedProgress = (double)progressTotal/(double)modifiedSet.segment.size();
				double nStopModifiedProgress 	= (double)progressTotal/(double)modifiedSet.stops.size();
				double nTourModifiedProgress 	= (double)progressTotal/(double)modifiedSet.tours.size();
				double nActivityModifiedProgress= (double)progressTotal/(double)modifiedSet.activity.size();
				
				//HANDLE CREATED
				for(JRoutes jRoutes:createdSet.routes){
					Route route = new Route();
					route.setId(jRoutes.id);
					route.setName(jRoutes.name);

					route.setImage(AppConstants.BASE_URL_IMAGE+jRoutes.image); //jRoutes.image
					route.setImage2(AppConstants.BASE_URL_IMAGE+jRoutes.image2); //jRoutes.image2

					route.setImageLocal("route" + File.separator + jRoutes.id + File.separator + "thumb.png");
					route.setImage2Local("route" + File.separator + jRoutes.id + File.separator + "wide.png");

					route.setImageSize(jRoutes.imagesize);
					route.setImage2Size(jRoutes.image2size);

					route.setProductId(jRoutes.productId);
					route.setStart(jRoutes.start);
					route.setEnd(jRoutes.end);
					route.setMinDay(jRoutes.minDay);
					route.setMaxDay(jRoutes.maxDay);
					route.setDistance(jRoutes.distance);
					route.setHighlights(jRoutes.highlights);
					route.setLastModified(jRoutes.lastModified);
					route.setDescription(jRoutes.description);
					route.setZone(getZone(Integer.parseInt(jRoutes.id)));
					route.setnPlaces(jRoutes.nplaces);
					route.setnPOI(jRoutes.poi);
					route.setnStops(jRoutes.nstops);

					if(route.getId().equals("1"))    // TODO: Remove this after development! ******************************************
						route.setIsCommentaryPurchased(String.valueOf(true));
					else
						route.setIsCommentaryPurchased(String.valueOf(false));

					JSONObject jsonObject = new JSONObject();
					try {
						jsonObject.put("segment_meta", jRoutes.segments);
						route.setSegmentMeta(jsonObject.toString());
					} catch (JSONException e) {
						e.printStackTrace();
					}

					if(DatabaseHelper.getInstance()==null){
						Log.e("error","I'm DatabaseHelper - Null");
					}
					DatabaseHelper.getInstance().insertOrUpdateRoute(route);

					Utils.downloadItem(route.getId(),route.getImage(),route.getImageLocal());
					Utils.downloadItem(route.getId(),route.getImage2(),route.getImage2Local());

					/*resultData = new Bundle();
					currentProgress += nRouteCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nRouteCreatedProgress);
				}
				
				for(JTours jTours:createdSet.tours){
					Tours tour = new Tours();
					tour.setId(jTours.id);
					tour.setName(jTours.name);
					tour.setImage(AppConstants.BASE_URL_IMAGE+jTours.image);//jTours.image
					tour.setImageLocal("tour"+ File.separator + jTours.id + File.separator + "thumb.png");
					tour.setImageSize(jTours.imagesize);
					tour.setDuration(jTours.duration);
					tour.setCommentary(jTours.commentary);
					tour.setDistance(jTours.distance);
					tour.setHighlights(jTours.highlights);
					tour.setDescription(jTours.description);
					tour.setRouteId(jTours.routeId);
					tour.setStart(jTours.start);
					tour.setEnd(jTours.finish);
					tour.setLastModified(jTours.lastModified);
					DatabaseHelper.getInstance().insertOrUpdateTours(tour);
					Utils.downloadItem(tour.getId(),tour.getImage(),tour.getImageLocal());

					/*resultData = new Bundle();
					currentProgress += nTourCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nTourCreatedProgress);
				}
				
				for(JActivity jActivity:createdSet.activity){
					Activity activity = new Activity();

					activity.setId(jActivity.id);
					activity.setUserId(jActivity.email);
					activity.setSegmentId(jActivity.segmentId);
					activity.setPlaceId(jActivity.placeId);
					activity.setLatitude(jActivity.latitude);

					activity.setLongitude(jActivity.longitude);
					activity.setFacility(jActivity.info2);
					activity.setHighlights(jActivity.info2);
					activity.setOperator(jActivity.operator);
					activity.setType(jActivity.type);

					activity.setPlaceName(jActivity.place);
					activity.setTitle(jActivity.title);
					activity.setSubtitle(jActivity.subTitle); //Use it as subType for F&D and Others
					activity.setPrice(jActivity.price);
					activity.setDuration(jActivity.info1);

					activity.setCuisine(jActivity.info2);
					activity.setTime(jActivity.info3);
					activity.setRatings(jActivity.info1);
					activity.setDescription(jActivity.description);
					activity.setInnerAddOnTypes(jActivity.info1); //for F&D

					activity.setLastModified(jActivity.date);
					activity.setAddress(jActivity.address);
					activity.setImage(AppConstants.BASE_URL_IMAGE+jActivity.image);//jActivity.image
					activity.setImageSize(jActivity.imagesize);
					activity.setImageLocal("activity" + File.separator + jActivity.id + File.separator + "thumb.png");

					activity.setImageWide(AppConstants.BASE_URL_IMAGE+jActivity.image2);//jActivity.image2
					activity.setImageWideSize(jActivity.image2size);
					activity.setImageWideLocal("activity" + File.separator + jActivity.id + File.separator + "wide.png");

					DatabaseHelper.getInstance().insertOrUpdateActivity(activity);

					Utils.downloadItem(activity.getId(),activity.getImage(),activity.getImageLocal());
					Utils.downloadItem(activity.getId(),activity.getImageWide(),activity.getImageWideLocal());

					for(int i=0;i<3;i++){
						ActivityAccessory activityAccessory = new ActivityAccessory();
						activityAccessory.setActivityId(jActivity.id);
						activityAccessory.setType(String.valueOf(i+1));
						switch (i){
						case 0:
							activityAccessory.setValue(jActivity.phone);
							break;
						case 1:
							activityAccessory.setValue(jActivity.web);
							break;
						case 2:
							activityAccessory.setValue(jActivity.otherPhone);
							break;
						}
						DatabaseHelper.getInstance().insertOrUpdateActivityAccessory(activityAccessory);
					}
					
					/*resultData = new Bundle();
					currentProgress += nActivityCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nActivityCreatedProgress);
				}
					
				for(JStops jStops:createdSet.stops){
					Stops stops = new Stops();
					stops.setId(jStops.id);
					stops.setName(jStops.name);
					stops.setSegmentId(jStops.segmentId);
					stops.setType(jStops.types);
					stops.setAddress(jStops.address);
					stops.setTimeNeeded(jStops.timeNeeded);
					stops.setHighlights(jStops.highlights);
					stops.setDescription(jStops.description);
					stops.setLastModified(jStops.lastModified);
					stops.setImage(AppConstants.BASE_URL_IMAGE+jStops.image);//jStops.image
					stops.setImageSize(jStops.imageSize);
					stops.setImageLocal("stop"+ File.separator + jStops.id + File.separator + "thumb.png");

					stops.setImageWide(AppConstants.BASE_URL_IMAGE+jStops.image2);//jStops.image2
					stops.setImageWideSize(jStops.image2Size);
					stops.setImageWideLocal("stop"+ File.separator + jStops.id + File.separator + "wide.png");

					stops.setLatitude(jStops.latitude);
					stops.setLongitude(jStops.longitude);

					DatabaseHelper.getInstance().insertOrUpdateStops(stops);
					Utils.downloadItem(stops.getId(),stops.getImage(),stops.getImageLocal());
					Utils.downloadItem(stops.getId(),stops.getImageWide(),stops.getImageWideLocal());
					
					/*resultData = new Bundle();
					currentProgress += nStopCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nStopCreatedProgress);
				}
				
				for(JSegments jSegments:createdSet.segment){
					Segment segment = new Segment();
					segment.setId(jSegments.id);
					segment.setName(jSegments.name);
					segment.setPlace(jSegments.isPlace);
					segment.setIslandType(jSegments.island);
					segment.setLatitude(jSegments.latitude);
					segment.setLongitude(jSegments.langitude);
					segment.setDescription(jSegments.description);
					segment.setAudio(jSegments.audio);
					segment.setAudioLocal("segment" + File.separator + jSegments.id + File.separator + "audio.mp3");
					segment.setCoordinates(jSegments.coorinates);
					segment.setMbTiles(jSegments.mbTiles);
					segment.setMbTilesLocal("segment" + File.separator + jSegments.id + File.separator + "tiles.mbtiles");
					segment.setImage(jSegments.image); //jSegments.image
					segment.setImageLocal("segment" + File.separator + jSegments.id + File.separator + "thumb.png");
					segment.setLastModified(jSegments.lastModified);
					segment.setHyperLink(jSegments.hyperlink);

					Utils.downloadItem(segment.getId(),segment.getImage(),segment.getImageLocal());
					Utils.downloadItem(segment.getId(),segment.getAudio(),segment.getAudioLocal());
					DatabaseHelper.getInstance().insertOrUpdateSegment(segment);

					/*resultData = new Bundle();
					currentProgress += nSegmentCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nSegmentCreatedProgress);
				}
				
				//HANDLE MODIFIED
				for(JRoutes jRoutes:modifiedSet.routes){
					Route route = new Route();
					route.setId(jRoutes.id);
					route.setName(jRoutes.name);

					route.setImage(AppConstants.BASE_URL_IMAGE+jRoutes.image); //jRoutes.image
					route.setImage2(AppConstants.BASE_URL_IMAGE+jRoutes.image2); //jRoutes.image2

					route.setImageLocal("route" + File.separator + jRoutes.id + File.separator + "thumb.png");
					route.setImage2Local("route" + File.separator + jRoutes.id + File.separator + "wide.png");

					route.setImageSize(jRoutes.imagesize);
					route.setImage2Size(jRoutes.image2size);

					route.setProductId(jRoutes.productId);
					route.setStart(jRoutes.start);
					route.setEnd(jRoutes.end);
					route.setMinDay(jRoutes.minDay);
					route.setMaxDay(jRoutes.maxDay);
					route.setDistance(jRoutes.distance);
					route.setHighlights(jRoutes.highlights);
					route.setLastModified(jRoutes.lastModified);
					route.setDescription(jRoutes.description);
					route.setZone(getZone(Integer.parseInt(jRoutes.id)));
					route.setnPlaces(jRoutes.nplaces);
					route.setnPOI(jRoutes.poi);
					route.setnStops(jRoutes.nstops);

					if(route.getId().equals("1"))    // TODO: Remove this after development! ******************************************
						route.setIsCommentaryPurchased(String.valueOf(true));
					else
						route.setIsCommentaryPurchased(String.valueOf(false));

					JSONObject jsonObject = new JSONObject();
					try {
						jsonObject.put("segment_meta", jRoutes.segments);
						route.setSegmentMeta(jsonObject.toString());
					} catch (JSONException e) {
						e.printStackTrace();
					}

					if(DatabaseHelper.getInstance()==null){
						Log.e("error","I'm DatabaseHelper - Null");
					}
					DatabaseHelper.getInstance().insertOrUpdateRoute(route);

					Utils.downloadItem(route.getId(),route.getImage(),route.getImageLocal());
					Utils.downloadItem(route.getId(),route.getImage2(),route.getImage2Local());

					/*resultData = new Bundle();
					currentProgress += nRouteCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nRouteModifiedProgress);
				}
				
				for(JTours jTours:modifiedSet.tours){
					Tours tour = new Tours();
					tour.setId(jTours.id);
					tour.setName(jTours.name);
					tour.setImage(AppConstants.BASE_URL_IMAGE+jTours.image);//jTours.image
					tour.setImageLocal("tour"+ File.separator + jTours.id + File.separator + "thumb.png");
					tour.setImageSize(jTours.imagesize);
					tour.setDuration(jTours.duration);
					tour.setCommentary(jTours.commentary);
					tour.setDistance(jTours.distance);
					tour.setHighlights(jTours.highlights);
					tour.setDescription(jTours.description);
					tour.setRouteId(jTours.routeId);
					tour.setStart(jTours.start);
					tour.setEnd(jTours.finish);
					tour.setLastModified(jTours.lastModified);
					DatabaseHelper.getInstance().insertOrUpdateTours(tour);
					Utils.downloadItem(tour.getId(),tour.getImage(),tour.getImageLocal());

					/*resultData = new Bundle();
					currentProgress += nTourCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nTourModifiedProgress);
				}
				
				for(JActivity jActivity:modifiedSet.activity){
					Activity activity = new Activity();

					activity.setId(jActivity.id);
					activity.setUserId(jActivity.email);
					activity.setSegmentId(jActivity.segmentId);
					activity.setPlaceId(jActivity.placeId);
					activity.setLatitude(jActivity.latitude);

					activity.setLongitude(jActivity.longitude);
					activity.setFacility(jActivity.info2);
					activity.setHighlights(jActivity.info2);
					activity.setOperator(jActivity.operator);
					activity.setType(jActivity.type);

					activity.setPlaceName(jActivity.place);
					activity.setTitle(jActivity.title);
					activity.setSubtitle(jActivity.subTitle); //Use it as subType for F&D and Others
					activity.setPrice(jActivity.price);
					activity.setDuration(jActivity.info1);

					activity.setCuisine(jActivity.info2);
					activity.setTime(jActivity.info3);
					activity.setRatings(jActivity.info1);
					activity.setDescription(jActivity.description);
					activity.setInnerAddOnTypes(jActivity.info1); //for F&D

					activity.setLastModified(jActivity.date);
					activity.setAddress(jActivity.address);
					activity.setImage(AppConstants.BASE_URL_IMAGE+jActivity.image);//jActivity.image
					activity.setImageSize(jActivity.imagesize);
					activity.setImageLocal("activity" + File.separator + jActivity.id + File.separator + "thumb.png");

					activity.setImageWide(AppConstants.BASE_URL_IMAGE+jActivity.image2);//jActivity.image2
					activity.setImageWideSize(jActivity.image2size);
					activity.setImageWideLocal("activity" + File.separator + jActivity.id + File.separator + "wide.png");

					DatabaseHelper.getInstance().insertOrUpdateActivity(activity);

					Utils.downloadItem(activity.getId(),activity.getImage(),activity.getImageLocal());
					Utils.downloadItem(activity.getId(),activity.getImageWide(),activity.getImageWideLocal());

					for(int i=0;i<3;i++){
						ActivityAccessory activityAccessory = new ActivityAccessory();
						activityAccessory.setActivityId(jActivity.id);
						activityAccessory.setType(String.valueOf(i+1));
						switch (i){
						case 0:
							activityAccessory.setValue(jActivity.phone);
							break;
						case 1:
							activityAccessory.setValue(jActivity.web);
							break;
						case 2:
							activityAccessory.setValue(jActivity.otherPhone);
							break;
						}
						DatabaseHelper.getInstance().insertOrUpdateActivityAccessory(activityAccessory);
					}
					
					/*resultData = new Bundle();
					currentProgress += nActivityCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nActivityModifiedProgress);
				}
					
				for(JStops jStops:modifiedSet.stops){
					Stops stops = new Stops();
					stops.setId(jStops.id);
					stops.setName(jStops.name);
					stops.setSegmentId(jStops.segmentId);
					stops.setType(jStops.types);
					stops.setAddress(jStops.address);
					stops.setTimeNeeded(jStops.timeNeeded);
					stops.setHighlights(jStops.highlights);
					stops.setDescription(jStops.description);
					stops.setLastModified(jStops.lastModified);
					stops.setImage(AppConstants.BASE_URL_IMAGE+jStops.image);//jStops.image
					stops.setImageSize(jStops.imageSize);
					stops.setImageLocal("stop"+ File.separator + jStops.id + File.separator + "thumb.png");

					stops.setImageWide(AppConstants.BASE_URL_IMAGE+jStops.image2);//jStops.image2
					stops.setImageWideSize(jStops.image2Size);
					stops.setImageWideLocal("stop"+ File.separator + jStops.id + File.separator + "wide.png");

					stops.setLatitude(jStops.latitude);
					stops.setLongitude(jStops.longitude);

					DatabaseHelper.getInstance().insertOrUpdateStops(stops);
					Utils.downloadItem(stops.getId(),stops.getImage(),stops.getImageLocal());
					Utils.downloadItem(stops.getId(),stops.getImageWide(),stops.getImageWideLocal());
					
					/*resultData = new Bundle();
					currentProgress += nStopCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nStopModifiedProgress);
				}
				
				for(JSegments jSegments:modifiedSet.segment){
					Segment segment = new Segment();
					segment.setId(jSegments.id);
					segment.setName(jSegments.name);
					segment.setPlace(jSegments.isPlace);
					segment.setIslandType(jSegments.island);
					segment.setLatitude(jSegments.latitude);
					segment.setLongitude(jSegments.langitude);
					segment.setDescription(jSegments.description);
					segment.setAudio(jSegments.audio);
					segment.setAudioLocal("segment" + File.separator + jSegments.id + File.separator + "audio.opus");
					segment.setCoordinates(jSegments.coorinates);
					segment.setMbTiles(jSegments.mbTiles);
					segment.setMbTilesLocal("segment" + File.separator + jSegments.id + File.separator + "tiles.mbtiles");
					segment.setImage(jSegments.image); //jSegments.image
					segment.setImageLocal("segment" + File.separator + jSegments.id + File.separator + "thumb.png");
					segment.setLastModified(jSegments.lastModified);
					segment.setHyperLink(jSegments.hyperlink);

					Utils.downloadItem(segment.getId(),segment.getImage(),segment.getImageLocal());
					Utils.downloadItem(segment.getId(),segment.getAudio(),segment.getAudioLocal());
					DatabaseHelper.getInstance().insertOrUpdateSegment(segment);

					/*resultData = new Bundle();
					currentProgress += nSegmentCreatedProgress;
					resultData.putInt("progress" ,(int)currentProgress);
					receiver.send(UPDATE_PROGRESS, resultData);*/
					
					updateProgress(nSegmentModifiedProgress);
				}
				
			}
		}
		//Hold a Second!
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Stop Service
		if((int)currentProgress <100){
			resultData = new Bundle();
			resultData.putInt("progress" ,100);
			receiver.send(UPDATE_PROGRESS, resultData);
			stopSelf();
		}
	}

	private String getZone(int routeId){
		int zoneId = -1;
		if(routeId>0 && routeId <3){ //1 & 2
			zoneId = 1;
		}else if(routeId>2 && routeId<7){ //3,4,5,6
			zoneId = 2;
		}else if(routeId>6 && routeId<11){ //7,8,9,10
			zoneId = 3;
		}else if(routeId>10 && routeId<14){ //11,12,13
			zoneId = 6;
		}else if(routeId>13 && routeId<18){ //14,15,16,17
			zoneId = 5;
		}else if(routeId>17){ //18,19,20...30
			zoneId = 4;
		}
		return String.valueOf(zoneId);
	}
	
	private void updateProgress(double incrementValue){
		
		resultData = new Bundle();
		currentProgress += incrementValue;
		resultData.putInt("progress" ,(int)currentProgress);
		receiver.send(UPDATE_PROGRESS, resultData);
	}

}
