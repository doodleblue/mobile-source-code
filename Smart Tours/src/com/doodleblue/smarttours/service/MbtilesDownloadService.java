package com.doodleblue.smarttours.service;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.widget.RemoteViews;

import com.doodleblue.smarttours.model.Downloaded;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.model.Segment;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;
import com.smarttoursnz.guide.R;

public class MbtilesDownloadService extends IntentService {

	public static final String BROADCAST_ACTION = "com.doodleblue.smarttours.service.displayevents";
	public static final int UPDATE_PROGRESS = 2014;

	private static final String TAG = "MbtilesDownloadService";
	private static final String MY_CONTEXT = "context";

	private ArrayList<String> mSegmentsList = new ArrayList<String>();

	private boolean handleNotification = false;
	private NotificationManager notificationManager;
	private Notification 		notification;
	private PendingIntent 		contentIntent;
	private Intent notificationIntent = new Intent();
	private DecimalFormat mFormat = new DecimalFormat("00.00");

	private double currentProgress = 1;
	private Bundle resultData;
	private ResultReceiver receiver;

	public MbtilesDownloadService(){
		super("MbtilesDownloadService");
	}

	private void updateProgress(double progress){
		resultData = new Bundle();
		currentProgress += progress;
		resultData.putStringArrayList("segmentIDList", mSegmentsList);
		resultData.putInt("progress" ,(int)currentProgress);
		resultData.putStringArrayList("segmentIDList", mSegmentsList);
		receiver.send(UPDATE_PROGRESS, resultData);

		//Updating Notification Manager
		if(handleNotification){
			notification.contentView.setProgressBar(R.id.progressBar1, 100, (int)currentProgress, false);
			notification.contentView.setTextViewText(R.id.textView2, mFormat.format(currentProgress)+" %");
			if(currentProgress>=100){
				notification.setLatestEventInfo(getApplicationContext(), 
						"Maps Downloaded!", "Touch to cancel", contentIntent);
				notification.flags |= Notification.FLAG_AUTO_CANCEL;
			}
			notificationManager.notify(1, notification);
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

		try{
			contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);

			//Preparing Notification Manager
			if(handleNotification){
				notificationManager = (NotificationManager) 
						getApplicationContext().getSystemService(
								getApplicationContext().NOTIFICATION_SERVICE
								);
				notification = new Notification(R.drawable.ic_launcher, "Downloading Map Data", System.currentTimeMillis());
				notification.flags = notification.flags	| Notification.FLAG_ONGOING_EVENT;
				notification.contentView = new RemoteViews(getApplicationContext()
						.getPackageName(), R.layout.mbtiles_download_notification);
				notification.icon = R.drawable.ic_launcher;
				notification.contentView.setTextViewText(R.id.textView1, "Downloading Map data...");
				notification.contentIntent = contentIntent;
			}


			receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
			updateProgress(currentProgress);

			if(intent.hasExtra("routeId")||intent.hasExtra("routeIds")){
				ArrayList<Route> routes = new ArrayList<Route>();
				if(intent.hasExtra("routeIds")){
					ArrayList<String> routeIdList = intent.getStringArrayListExtra("routeIds");
					String[] array = new String[routeIdList.size()];
					for(int i=0;i<routeIdList.size();i++){
						array[i] = routeIdList.get(i);
					}
					routes = DatabaseHelper.getInstance().fetchRoutes(array);
				}
				else{
					routes = DatabaseHelper.getInstance().fetchRoutes(new String[]{intent.getStringExtra("routeId")});
				}
				double nRouteProgress = (double)100/(double)routes.size();
				for (Route route : routes) {
					String segmentMeta = route.getSegmentMeta();
					try {
						JSONObject json = new JSONObject(segmentMeta);
						JSONArray segmentArray = new JSONArray(json.getString("segment_meta"));
						for (int i = 0; i < segmentArray.length(); i++) {
							if(!mSegmentsList.contains(segmentArray.getString(i))){
								mSegmentsList.add(segmentArray.getString(i));
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					double nSegmentProgress = (double)nRouteProgress/(double)mSegmentsList.size();
					for (String string : mSegmentsList) {
						for(Segment segment: DatabaseHelper.getInstance().fetchSegment(string)){
							Utils.downloadItem(segment.getId(), segment.getMbTiles(), segment.getMbTilesLocal());
							updateProgress(nSegmentProgress);
						}
					}
					DatabaseHelper.getInstance().insertOrUpdateDownloaded(new Downloaded(route.getId()));
				}

			}
			else if(intent.hasExtra("segmentId")){
				for(Segment segment: DatabaseHelper.getInstance().fetchSegment(intent.getStringExtra("segmentId"))){
					Utils.downloadItem(segment.getId(), segment.getMbTiles(), segment.getMbTilesLocal());
					mSegmentsList.add(intent.getStringExtra("segmentId"));
					resultData = new Bundle();
					updateProgress(50);
				}

			}
		}
		catch(final Exception e){
			e.printStackTrace();
		}
		finally{
			if((int)currentProgress <100){
				resultData = new Bundle();
				resultData.putInt("progress" ,100);
				resultData.putStringArrayList("segmentIDList", mSegmentsList);
				receiver.send(UPDATE_PROGRESS, resultData);
			}
		}
	}
}
