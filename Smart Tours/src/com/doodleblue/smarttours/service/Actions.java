package com.doodleblue.smarttours.service;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 12/16/13
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class Actions {

    public static final String START_ROUTE = Actions.class.getName()
            + ".START_ROUTE";
    public static final String STOP_ROUTE = Actions.class.getName()
            + ".STOP_ROUTE";
    public static final String PLAY_AUDIO = Actions.class.getName()
            + ".PLAY_AUDIO";
    public static final String STOP_AUDIO = Actions.class.getName()
            + ".STOP_AUDIO";
    public static final String PAUSE_AUDIO = Actions.class.getName()
            + ".PAUSE_AUDIO";
    public static final String RESUME_AUDIO = Actions.class.getName()
            + ".RESUME_AUDIO";
    public static final String NEXT_AUDIO = Actions.class.getName()
            + ".NEXT_AUDIO";
    public static final String PREV_AUDIO = Actions.class.getName()
            + ".PREV_AUDIO";
    public static final String PLAYING = Actions.class.getName() + ".PLAYING";
    public static final String PLAYING_AUDIO_RESTORED = Actions.class.getName()
            + ".PLAYING_AUDIO_RESTORED";

}
