package com.doodleblue.smarttours.service;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 12/12/13
 * Time: 6:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class TimeChangeDetector {
    private long mMinTimeChange;
    private long mLastTime = 0;

    public TimeChangeDetector(long minTimeChange) {
        mMinTimeChange = minTimeChange;
    }

    public void setMinTimeChange(long minTimeChange) {
        mMinTimeChange = minTimeChange;
    }

    public boolean isChangedEnough(long newTime) {
        if (newTime - mLastTime < mMinTimeChange) {
            return false;
        }
        mLastTime = newTime;
        return true;
    }
}
