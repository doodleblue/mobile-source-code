package com.doodleblue.smarttours.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.*;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/29/13
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class SplashDownloadService extends IntentService {

	private static final String TAG = "SplashDownloadService";

	public static final String BROADCAST_ACTION = "com.doodleblue.smarttours.service.displayevent";
	public static final int UPDATE_PROGRESS = 2013;
	private Bundle resultData;
	private ResultReceiver receiver;
	private double currentProgress = 1;
	private Intent intent;

	public SplashDownloadService(){
		super("SplashDownloadService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {


		receiver = (ResultReceiver) intent.getParcelableExtra("receiver");

		updateProgress(1, "Initializing...");

		resultData = new Bundle();
		resultData.putInt("progress" ,(int)currentProgress);
		receiver.send(UPDATE_PROGRESS, resultData);

		//Code to fetch Routes
		InputStream inputStream = Utils.retrieveStream(AppConstants.GET_ALL_ROUTES_URL);
		if(inputStream==null){
			Log.e("error","I'm Null");
		}else{
			Log.e("error","I'm not Null");
		}
		Gson gson = new Gson();
		Reader reader = new InputStreamReader(inputStream);
		JSearchRoutes response = gson.fromJson(reader, JSearchRoutes.class);
		double nRouteProgress = (double)15/(double)response.routes.size();
		for(JRoutes jRoutes:response.routes){
			Route route = new Route();
			route.setId(jRoutes.id);
			route.setName(jRoutes.name);

			route.setImage(AppConstants.BASE_URL_IMAGE+jRoutes.image); //jRoutes.image
			route.setImage2(AppConstants.BASE_URL_IMAGE+jRoutes.image2); //jRoutes.image2

			route.setImageLocal("route" + File.separator + jRoutes.id + File.separator + "thumb.png");
			route.setImage2Local("route" + File.separator + jRoutes.id + File.separator + "wide.png");

			route.setImageSize(jRoutes.imagesize);
			route.setImage2Size(jRoutes.image2size);

			route.setProductId(jRoutes.productId);
			route.setStart(jRoutes.start);
			route.setEnd(jRoutes.end);
			route.setMinDay(jRoutes.minDay);
			route.setMaxDay(jRoutes.maxDay);
			route.setDistance(jRoutes.distance);
			route.setHighlights(jRoutes.highlights);
			route.setLastModified(jRoutes.lastModified);
			route.setDescription(jRoutes.description);
			route.setZone(getZone(Integer.parseInt(jRoutes.id)));
			route.setnPlaces(jRoutes.nplaces);
			route.setnPOI(jRoutes.poi);
			route.setnStops(jRoutes.nstops);

			if(route.getId().equals("1"))    // TODO: Remove this after development! ******************************************
				route.setIsCommentaryPurchased(String.valueOf(true));
			else
				route.setIsCommentaryPurchased(String.valueOf(false));
			//route.setSegmentList(jRoutes.segments);

			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("segment_meta", jRoutes.segments);
				route.setSegmentMeta(jsonObject.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if(DatabaseHelper.getInstance()==null){
				Log.e("error","I'm DatabaseHelper - Null");
			}
			DatabaseHelper.getInstance().insertOrUpdateRoute(route);

			Utils.downloadItem(route.getId(),route.getImage(),route.getImageLocal());
			Utils.downloadItem(route.getId(),route.getImage2(),route.getImage2Local());

			updateProgress(nRouteProgress, route.getName());
			/*resultData = new Bundle();
			currentProgress += nRouteProgress;
			resultData.putInt("progress" ,(int)currentProgress);
			resultData.putString("item", route.getName());
			receiver.send(UPDATE_PROGRESS, resultData);*/
		}


		double nTourProgress = (double)15/(double)response.tours.size();
		for(JTours jTours:response.tours){
			Tours tour = new Tours();
			tour.setId(jTours.id);
			tour.setName(jTours.name);
			tour.setImage(AppConstants.BASE_URL_IMAGE+jTours.image);//jTours.image
			tour.setImageLocal("tour"+ File.separator + jTours.id + File.separator + "thumb.png");
			tour.setImageSize(jTours.imagesize);
			tour.setDuration(jTours.duration);
			tour.setCommentary(jTours.commentary);
			tour.setDistance(jTours.distance);
			tour.setHighlights(jTours.highlights);
			tour.setDescription(jTours.description);
			tour.setRouteId(jTours.routeId);
			tour.setStart(jTours.start);
			tour.setEnd(jTours.finish);
			tour.setLastModified(jTours.lastModified);
			//Author and Status left
			DatabaseHelper.getInstance().insertOrUpdateTours(tour);
			Utils.downloadItem(tour.getId(),tour.getImage(),tour.getImageLocal());

			updateProgress(nTourProgress, tour.getName());
			/*resultData = new Bundle();
			currentProgress += nTourProgress;
			resultData.putInt("progress" ,(int)currentProgress);
			receiver.send(UPDATE_PROGRESS, resultData);*/
		}

		try {
			reader.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Code to fetch Segments
		inputStream = Utils.retrieveStream(AppConstants.GET_SEGMENTS_URL);
		if(inputStream!=null){
			gson = new Gson();
			reader = new InputStreamReader(inputStream);
			JGetSegments responseSegment = gson.fromJson(reader, JGetSegments.class);
			double nSegmentProgress = (double)70/(double)responseSegment.segment.size();
			for(JSegments jSegments:responseSegment.segment){
				Segment segment = new Segment();
				segment.setId(jSegments.id);
				segment.setName(jSegments.name);
				segment.setPlace(jSegments.isPlace);
				segment.setIslandType(jSegments.island);
				segment.setLatitude(jSegments.latitude);
				segment.setLongitude(jSegments.langitude);
				segment.setDescription(jSegments.description);
				segment.setAudio(jSegments.audio);
				segment.setAudioLocal("segment" + File.separator + jSegments.id + File.separator + "audio.opus");
				segment.setCoordinates(jSegments.coorinates);
				segment.setMbTiles(jSegments.mbTiles);
				segment.setMbTilesLocal("segment" + File.separator + jSegments.id + File.separator + "tiles.mbtiles");
				segment.setImage(jSegments.image); //jSegments.image
				segment.setImageLocal("segment" + File.separator + jSegments.id + File.separator + "thumb.png");
				segment.setLastModified(jSegments.lastModified);
				segment.setHyperLink(jSegments.hyperlink);

				segment.setnStops(jSegments.stops.size());
				segment.setnActivities(jSegments.activity.size());

				Utils.downloadItem(segment.getId(),segment.getImage(),segment.getImageLocal());
				//Utils.downloadItem(segment.getId(),segment.getAudio(),segment.getAudioLocal()); //Do it when needed
				DatabaseHelper.getInstance().insertOrUpdateSegment(segment);


				for(JActivity jActivity:jSegments.activity){

					Activity activity = new Activity();

					activity.setId(jActivity.id);
					activity.setUserId(jActivity.email);
					activity.setSegmentId(jActivity.segmentId);
					activity.setPlaceId(jActivity.placeId);
					activity.setLatitude(jActivity.latitude);

					activity.setLongitude(jActivity.longitude);
					activity.setFacility(jActivity.info2);
					activity.setHighlights(jActivity.info2);
					activity.setOperator(jActivity.operator);
					activity.setType(jActivity.type);

					activity.setPlaceName(jActivity.place);
					activity.setTitle(jActivity.title);
					activity.setSubtitle(jActivity.subTitle); //Use it as subType for F&D and Others
					activity.setPrice(jActivity.price);
					activity.setDuration(jActivity.info1);

					activity.setCuisine(jActivity.info2);
					activity.setTime(jActivity.info3);
					activity.setRatings(jActivity.info1);
					activity.setDescription(jActivity.description);
					activity.setInnerAddOnTypes(jActivity.info1); //for F&D

					activity.setLastModified(jActivity.date);
					activity.setAddress(jActivity.address);
					activity.setImage(AppConstants.BASE_URL_IMAGE+jActivity.image);//jActivity.image
					activity.setImageSize(jActivity.imagesize);
					activity.setImageLocal("activity" + File.separator + jActivity.id + File.separator + "thumb.png");

					activity.setImageWide(AppConstants.BASE_URL_IMAGE+jActivity.image2);//jActivity.image2
					activity.setImageWideSize(jActivity.image2size);
					activity.setImageWideLocal("activity" + File.separator + jActivity.id + File.separator + "wide.png");

					DatabaseHelper.getInstance().insertOrUpdateActivity(activity);

					Utils.downloadItem(activity.getId(),activity.getImage(),activity.getImageLocal());
					Utils.downloadItem(activity.getId(),activity.getImageWide(),activity.getImageWideLocal());

					for(int i=0;i<3;i++){
						ActivityAccessory activityAccessory = new ActivityAccessory();
						activityAccessory.setActivityId(jActivity.id);
						activityAccessory.setType(String.valueOf(i+1));
						switch (i){
						case 0:
							activityAccessory.setValue(jActivity.phone);
							break;
						case 1:
							activityAccessory.setValue(jActivity.web);
							break;
						case 2:
							activityAccessory.setValue(jActivity.otherPhone);
							break;
						}
						DatabaseHelper.getInstance().insertOrUpdateActivityAccessory(activityAccessory);

					}
				}

				for(JStops jStops:jSegments.stops){
					Stops stops = new Stops();
					stops.setId(jStops.id);
					stops.setName(jStops.name);
					stops.setSegmentId(jStops.segmentId);
					stops.setType(jStops.types);
					stops.setAddress(jStops.address);
					stops.setTimeNeeded(jStops.timeNeeded);
					stops.setHighlights(jStops.highlights);
					stops.setDescription(jStops.description);
					stops.setLastModified(jStops.lastModified);
					stops.setImage(AppConstants.BASE_URL_IMAGE+jStops.image);//jStops.image
					stops.setImageSize(jStops.imageSize);
					stops.setImageLocal("stop"+ File.separator + jStops.id + File.separator + "thumb.png");

					stops.setImageWide(AppConstants.BASE_URL_IMAGE+jStops.image2);//jStops.image2
					stops.setImageWideSize(jStops.image2Size);
					stops.setImageWideLocal("stop"+ File.separator + jStops.id + File.separator + "wide.png");

					stops.setLatitude(jStops.latitude);
					stops.setLongitude(jStops.longitude);

					DatabaseHelper.getInstance().insertOrUpdateStops(stops);
					Utils.downloadItem(stops.getId(),stops.getImage(),stops.getImageLocal());
					Utils.downloadItem(stops.getId(),stops.getImageWide(),stops.getImageWideLocal());
				}

				for(JAudioPoints audioPoints:jSegments.audiopoints){
					Opus opus = new Opus();

					opus.setId(audioPoints.id);

					opus.setStartSegment(audioPoints.startSegment);
					opus.setEndSegment(audioPoints.endSegment);
					opus.setAssociateSegment(audioPoints.associateSegment);

					opus.setLatitude(audioPoints.latitude);
					opus.setLongitude(audioPoints.longitude);

					opus.setAudioForward(audioPoints.audioFwd);
					opus.setAudioReverse(audioPoints.audioRev);
					opus.setAudioMain(audioPoints.audioMain);

					opus.setRadiusForward(audioPoints.radiusFwd);
					opus.setRadiusReverse(audioPoints.radiusRev);
					opus.setRadiusMain(audioPoints.radiusFwd);

					opus.setLastModified(audioPoints.lastModified);

					opus.setAudioForwardLocal("segment"+File.separator+jSegments.id+File.separator+audioPoints.id+File.separator+"fwd.opus");
					opus.setAudioReverseLocal("segment"+File.separator+jSegments.id+File.separator+audioPoints.id+File.separator+"rev.opus");
					opus.setAudioMainLocal("segment"+File.separator+jSegments.id+File.separator+audioPoints.id+File.separator+"main.opus");

					DatabaseHelper.getInstance().insertOrUpdateOpus(opus);

				}



				updateProgress(nSegmentProgress, segment.getName());
				/*resultData = new Bundle();
			currentProgress += nSegmentProgress;
			resultData.putInt("progress" ,(int)currentProgress);
			receiver.send(UPDATE_PROGRESS, resultData);*/
			}
		} else {
			Toast.makeText(getApplicationContext(), "Unable to download segment data", Toast.LENGTH_SHORT).show();
		}
		Utils.downloadItem(null,AppConstants.BASE_URL_BASEMAP,AppConstants.BASE_MBTILE);

		/*for(JRoutes jRoutes:response.routes){
        	for(Route route : DatabaseHelper.getInstance().fetchRoutes(jRoutes.id)){
        		route.getSegmentMeta();
        		try {
        			JSONObject json = new JSONObject(route.getSegmentMeta());
        			JSONArray segmentsList = new JSONArray(json.getString("segment_meta"));
        			for(int i=0;i<segmentsList.length();i++){
        				String segmentId = (String) segmentsList.getString(i);
        				DatabaseHelper.getInstance().fet
        			}

        		} catch (JSONException e) {
        			e.printStackTrace();
        		}
        	}
        }*/

		try {
			reader.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if((int)currentProgress <100){
			resultData = new Bundle();
			resultData.putInt("progress" ,100);
			resultData.putString("item", "finished...");
			receiver.send(UPDATE_PROGRESS, resultData);
		}
		Log.i(getClass().getSimpleName(), "End of Splash Download!!!");
	}

	private String getZone(int routeId){
		int zoneId = -1;
		if(routeId>0 && routeId <3){ //1 & 2
			zoneId = 1;
		}else if(routeId>2 && routeId<7){ //3,4,5,6
			zoneId = 2;
		}else if(routeId>6 && routeId<11){ //7,8,9,10
			zoneId = 3;
		}else if(routeId>10 && routeId<14){ //11,12,13
			zoneId = 6;
		}else if(routeId>13 && routeId<18){ //14,15,16,17
			zoneId = 5;
		}else if(routeId>17){ //18,19,20...30
			zoneId = 4;
		}
		return String.valueOf(zoneId);
	}

	private void updateProgress(double nRouteProgress, String downloadingItem){
		resultData = new Bundle();
		currentProgress += nRouteProgress;
		resultData.putInt("progress" ,(int)currentProgress);
		resultData.putString("item", downloadingItem);
		receiver.send(UPDATE_PROGRESS, resultData);
	}
}
