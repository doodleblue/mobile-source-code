package com.doodleblue.smarttours.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/14/13
 * Time: 5:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class StopsAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<String,ArrayList<Stops>> mStopsMap;

    public StopsAdapter(Context context,HashMap<String,ArrayList<Stops>> map)
    {
        mContext    = context;
        mStopsMap   = map;
    }

    @Override
    public int getGroupCount() {
        return Stops.TYPES.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mStopsMap.get(Stops.TYPES[groupPosition]).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Stops getChild(int groupPosition, int childPosition) {
        return mStopsMap.get(Stops.TYPES[groupPosition]).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {

        return 0;
    }

    @Override
    public boolean hasStableIds() {

        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.tour_section_header,null);
        }
        ((ImageView)convertView.findViewById(R.id.tour_cover))
                .setImageDrawable(
                        mContext.getResources().getDrawable(Stops.TYPE_DRAWABLES[groupPosition])
                );
        ((TextView)convertView.findViewById(R.id.tour_header)).setText(
                Stops.TYPES[groupPosition]
        );
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Stops stops = getChild(groupPosition,childPosition);
        if(convertView == null){
            convertView = View.inflate(mContext, R.layout.stops_row,null);
        }


        ((ImageView)convertView.findViewById(R.id.stop_image)).setImageBitmap(
        		Utils.getRoundedCornerBitmap(Utils.getImageFromString(stops.getImageLocal()),10)
        );
        ((TextView)convertView.findViewById(R.id.stop_name)).setText(stops.getName());
        ((TextView)convertView.findViewById(R.id.stop_description)).setText(stops.getHighlights());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return true;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        Stops stops = getChild(groupPosition,childPosition);
        if(stops.getType().equals("1")){
            return 0;
        } else if(stops.getType().equals("2")){
            return 1;
        } else if(stops.getType().equals("3")){
            return 2;
        } else if(stops.getType().equals("4")){
            return 3;
        }
        return 0;
    }

    @Override
    public int getGroupType(int groupPosition) {
        return super.getGroupType(groupPosition);
    }
}
