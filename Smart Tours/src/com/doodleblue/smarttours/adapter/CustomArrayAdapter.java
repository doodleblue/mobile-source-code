package com.doodleblue.smarttours.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.model.Favorite;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;

/** An array adapter that knows how to render views when given CustomData classes */
public class CustomArrayAdapter extends ArrayAdapter<Route> {
	
	private LayoutInflater mInflater;
	private ArrayList<Route> mRouteArrayList;
	private Context mContext;
	
	public interface FavSelectedListener{
		void onFavoriteSelected(CompoundButton buttonView, boolean isChecked);
	}
	
	private FavSelectedListener mFavoriteCheckedChangeListener;

	public CustomArrayAdapter(Context context,FavSelectedListener listener, ArrayList<Route> routeArrayList) {
		super(context, R.layout.custom_data_view, routeArrayList);
		mContext = context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mRouteArrayList = routeArrayList;
		mFavoriteCheckedChangeListener = listener;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;

		if (convertView == null) {
			// Inflate the view since it does not exist
			convertView = mInflater.inflate(R.layout.custom_data_view, parent, false);

			// Create and save off the holder in the tag so we get quick access to inner fields
			// This must be done for performance reasons
			holder = new Holder();
			holder.textView = (TextView) convertView.findViewById(R.id.textView);
			holder.route = (ImageView) convertView.findViewById(R.id.route_map);
			holder.favourite = (CheckBox) convertView.findViewById(R.id.favourite1);
			holder.favourite.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
					mFavoriteCheckedChangeListener.onFavoriteSelected(buttonView, isChecked);
				}
			});
			
			holder.favourite.setTag(getItem(position));
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		// Populate the text
		holder.favourite.setTag(getItem(position));
		holder.favourite.setChecked(getItem(position).isFavourite());
		holder.textView.setText(getItem(position).getName());

		//holder.route.setImageURI(getItem(position).getImage());
		//holder.route.setImageDrawable(mContext.getResources().getDrawable(R.drawable.route_img_big));

		holder.route.setImageBitmap(
				Utils.getRoundedCornerBitmap(
						Utils.getImageFromString(getItem(position).getImageLocal()),
						5
						)
				);

		return convertView;
	}

	@Override
	public int getCount() {
		return mRouteArrayList.size();
	}

	@Override
	public Route getItem(int position) {
		return mRouteArrayList.get(position);    //To change body of overridden methods use File | Settings | File Templates.
	}

	/** View holder for the views we need access to */
	private static class Holder {
		public TextView textView;
		public CheckBox favourite;
		public ImageView route;
	}
}
