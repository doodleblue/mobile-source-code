package com.doodleblue.smarttours.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.model.PlacePropertyBean;

public class ImageTextLayout extends FrameLayout {
	
	ImageView imageView;
	TextView titleView;
	TextView countView;

	public ImageTextLayout(Context context) {
		super(context);
		initView(context);
	}

	public ImageTextLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	public ImageTextLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView(context);
	}

	public void initView(Context context)
	{
		LayoutInflater infalInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = infalInflater.inflate(R.layout.prop_places, null);

		imageView = (ImageView) view.findViewById(R.id.image);
		titleView = (TextView) view.findViewById(R.id.title);
		countView = (TextView) view.findViewById(R.id.count);
		countView.setVisibility(View.GONE);
		addView(view);
	}

	public void setValues(PlacePropertyBean placePropertyBean)
	{
		imageView.setImageResource(placePropertyBean.getResId());
		titleView.setText(placePropertyBean.getTitle());
		//countView.setText(String.valueOf(placePropertyBean.getCount()));
	}
	
	
}
