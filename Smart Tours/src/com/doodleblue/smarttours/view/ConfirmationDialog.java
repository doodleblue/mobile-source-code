package com.doodleblue.smarttours.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.doodleblue.smarttours.application.SmartTours;

public class ConfirmationDialog extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public interface ConfirmationDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);

        public void onDialogNegativeClick(DialogFragment dialog);
    }

    private String mMsg;
    private int mPositiveButtonLabelId;
    private int mNegativeButtonLabelId;
    private int mTitleId;
    private int mIconId;
    private ConfirmationDialogListener mListener;

    public static ConfirmationDialog create(int title, int message,
                                            int positiveButtonLabel, int negativeButtonLabelId) {
        ConfirmationDialog dialog = new ConfirmationDialog();
        dialog.mTitleId = title;
        dialog.mMsg = SmartTours.getContext().getString(message);
        dialog.mPositiveButtonLabelId = positiveButtonLabel;
        dialog.mNegativeButtonLabelId = negativeButtonLabelId;
        return dialog;
    }

    public static ConfirmationDialog create(int title, String message,
                                            int positiveButtonLabel, int negativeButtonLabelId) {
        ConfirmationDialog dialog = new ConfirmationDialog();
        dialog.mTitleId = title;
        dialog.mMsg = message;
        dialog.mPositiveButtonLabelId = positiveButtonLabel;
        dialog.mNegativeButtonLabelId = negativeButtonLabelId;
        return dialog;
    }

    public ConfirmationDialog() {
    }

    public void setIcon(int iconId) {
        this.mIconId = iconId;
    }

    public void setListener(ConfirmationDialogListener pListener) {
        mListener = pListener;
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(mTitleId);
        builder.setMessage(mMsg)
                .setPositiveButton(mPositiveButtonLabelId,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                mListener
                                        .onDialogPositiveClick(ConfirmationDialog.this);
                            }
                        })
                .setNegativeButton(mNegativeButtonLabelId,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                mListener
                                        .onDialogNegativeClick(ConfirmationDialog.this);
                            }
                        });
        if (mIconId > 0)
            builder.setIcon(mIconId);
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (mListener != null)
            return;

        try {
            mListener = (ConfirmationDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ConfirmationDialogListener");
        }
    }

}
