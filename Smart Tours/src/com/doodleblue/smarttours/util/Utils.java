package com.doodleblue.smarttours.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.smarttoursnz.guide.R;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/18/13
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class Utils {


	private static final String TAG = "Utils";

	//decodes image and scales it to reduce memory consumption
	public static synchronized Bitmap getImageFromString(String path){
		Log.i("Utils","getImageFromString: "+path);
		try {
			File f = new File(AppConstants.ROOT_DIRECTORY+path);

			//Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);

			//The new size we want to scale to
			final int REQUIRED_SIZE=70;

			//Find the correct scale value. It should be the power of 2.
			int scale=1;
			while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
				scale*=2;

			//Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Log.i("Utils","FileNotFoundException");
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.i("Utils","Exception");
		}
		return BitmapFactory.decodeResource(DatabaseHelper.getInstance().getResources(), R.drawable.i_am_here_1);
	}



	/*//TODO:Remove this method
    public static Bitmap getImageFromFile(File file){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=70;

            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, o2);
        } catch (FileNotFoundException e) {}
        return BitmapFactory.decodeResource(DatabaseHelper.getInstance().getResources(), R.drawable.i_am_here_1);
    }*/

	public static void close(InputStream stream) {
		if(stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void close(OutputStream stream) {
		if(stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static InputStream retrieveStream(String url) {
		Log.i("retrieveStream", url);
		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet(url);
		try {
			HttpResponse getResponse = client.execute(getRequest);
			final int statusCode = getResponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Log.w(TAG,
						"Error " + statusCode + " for URL " + url);
				return null;
			}
			HttpEntity getResponseEntity = getResponse.getEntity();
			return getResponseEntity.getContent();
		}
		catch (IOException e) {
			getRequest.abort();
			Log.w(TAG, "Error for URL " + url, e);
		}
		catch (Exception e) {
			getRequest.abort();
			Log.e(TAG, "Error for URL " + url, e);
		}

		return null;
	}


	public static boolean downloadItem(String id, String serverPath, String localPath){
		try {
			try {
				Log.i(TAG, "id: "+id);
				Log.i(TAG, "serverPath: "+serverPath);
				Log.i(TAG, "localPath: "+localPath);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(TextUtils.isEmpty(serverPath)){
				return false;
			}
			//create a new file, specifying the path, and the filename
			//which we want to save the file as.
			String path = AppConstants.ROOT_DIRECTORY+localPath;
			String root = path.substring(0,path.lastIndexOf("/"));
			File dir = new File(root);
			if(!dir.exists()){
				dir.mkdirs();
			};
			String fileName = path.substring(path.lastIndexOf("/"),path.length());
			File file = new File(root,fileName);  //SDCardRoot,"1.jpg"
			
			if(!file.exists()){

				//set the download URL, a url that points to a file on the internet
				//this is the file to be downloaded
				if(localPath == null) return false;
				//URL url = new URL("http://www.weenysoft.com/images/icons/free-image-to-pdf-converter.png");//ToDo: Change this to serverPath
				URL url = new URL(serverPath);

				//create the new connection
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

				//set up some things on the connection
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);

				//and connect!
				urlConnection.connect();

				//this is the total size of the file
				int totalSize = urlConnection.getContentLength();

				if(totalSize == 0) { 
					file.delete();
					return false;
				}

				if(file.length() == totalSize){
					Log.i(TAG, "File Already Exists: "+file.getAbsolutePath());
					return false;
				}
				//this will be used to write the downloaded data into the file we created
				FileOutputStream fileOutput = new FileOutputStream(file);
				//this will be used in reading the data from the internet
				InputStream inputStream = urlConnection.getInputStream();

				//variable to store total downloaded bytes
				int downloadedSize = 0;

				//create a buffer...
				byte[] buffer = new byte[1024];
				int bufferLength = 0; //used to store a temporary size of the buffer

				//now, read through the input buffer and write the contents to the file
				while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
					//add the data in the buffer to the file in the file output stream (the file on the sd card
					fileOutput.write(buffer, 0, bufferLength);
					//add up the size so we know how much is downloaded
					downloadedSize += bufferLength;
					//this is where you would do something to report the progress, like this maybe
					//updateProgress(downloadedSize, totalSize);

				}
				//close the output stream when done
				Utils.close(inputStream);
				Utils.close(fileOutput);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

	public static String addStringAsInteger(String src, String dest){

		String result = null;

		try {
			result = String.valueOf(Integer.parseInt(src)+Integer.parseInt(dest));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = src;
		}

		return result;
	}

	public static synchronized Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		if(bitmap!=null)
			try {
				Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
						.getHeight(), android.graphics.Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(output);

				final int color = 0xff424242;
				final Paint paint = new Paint();
				final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
				final RectF rectF = new RectF(rect);
				final float roundPx = pixels;

				paint.setAntiAlias(true);
				canvas.drawARGB(0, 0, 0, 0);
				paint.setColor(color);
				try {
					canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
				canvas.drawBitmap(bitmap, rect, rect, paint);

				return output;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return BitmapFactory.decodeResource(DatabaseHelper.getInstance().getResources(), R.drawable.i_am_here_1);
	}

}
