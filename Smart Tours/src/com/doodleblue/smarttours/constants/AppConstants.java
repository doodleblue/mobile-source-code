package com.doodleblue.smarttours.constants;

import android.os.Environment;
import android.support.v4.app.Fragment;

import java.io.File;

import com.doodleblue.smarttours.application.SmartTours;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/4/13
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppConstants {
    public static final String ROUTES = "routes";
    public static final String MAPS = "maps";
    public static final String THINGS_TO_DO = "things_to_do_selector";
    public static final String SETTINGS = "settings";

    public static final String DOWNLOADS = "downloads";
    public static final String MY_FAVOURITE = "my_favourite";
    public static final String SELECT_ROUTES = "select_route";

    public static final String ABOUT = "about";
    public static final String SYNCHRONIZATION = "synchronization";
    public static final int DEFAULT_ZOOM_LEVEL = 15;

    public static final String BASE_URL             = "http://smarttoursnz.com/";
    
    public static final String GET_ALL_ROUTES_URL   = BASE_URL + "searchroutes.php";
    public static final String GET_SEGMENTS_URL     = BASE_URL + "getsegments.php"; //?segment_id=
    public static final String SYNC_URL             = BASE_URL + "sync.php?" +
            "segment_id=value1" +
            "&segment_lastmodified=value2" +
            "&activity_id=value3" +
            "&activity_lastmodified=value4" +
            "&stop_id=value5" +
            "&stop_lastmodified=value6" +
            "&route_id=value7" +
            "&route_lastmodified=value8";
    
    //public static final String BASE_URL_ASSET		= "http://192.168.1.150/smart/wp-content/uploads";
    public static final String BASE_URL_IMAGE 		= "http://smarttoursnz.com/wp-content/uploads";
    public static final String BASE_URL_BASEMAP		=  BASE_URL + "wp-content/uploads/2014/02/nz-test_2012-11-30_205334.mbtiles";
    
    
    public static final String FILE_DIR = "smartToursDb1"; 
    public static final String ROOT_DIRECTORY = SmartTours.getPrivateFilePath() + File.separator;
    /*public static final String ROOT_DIRECTORY = Environment.getExternalStorageDirectory()
            + File.separator + FILE_DIR
            + File.separator;
    */
    public static final String BASE_MBTILE = "base.mbtiles";
	public static final String URL_TERMS_OF_USE = "http://wiki.openstreetmap.org/wiki/Legal_FAQ";
	public static final String URL_CONTRIBUTORS = "http://wiki.openstreetmap.org/wiki/Contributors";
	public static final String URL_DEVELOPERS = "http://www.doodleblue.com";
	
	//http://smarttoursnz.com/wp-content/uploads/2014/02/nz-test_2012-11-30_205334.mbtiles
	
		
		
	
	

    public static boolean IS_FIRST_TIME;
    
    

    //public static final String DUMMY_ROUTE_URL = "http://upload.wikimedia.org/wikipedia/commons/8/8c/Arizona_State_Route_51_portrait.png";
    //public static final String DUMMY_THUMB_URL = "http://torus.math.uiuc.edu/jms/Images/IMU-logo/transp/IMU-logo-ptg.png";
    //public static final String DUMMY_WIDE_URL = "http://s3.amazonaws.com/production.mediajoint.prx.org/public/comatose_files/4456/prx-logo_large.png";
}
