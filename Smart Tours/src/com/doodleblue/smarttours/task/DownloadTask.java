package com.doodleblue.smarttours.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;
import com.doodleblue.smarttours.application.SmartTours;
import com.doodleblue.smarttours.model.*;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class DownloadTask extends AsyncTask<String,String,Object> {

    private Context mContext;

    public DownloadTask(Context context){
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override //pass null to download all or route id to download specific route
    protected Object doInBackground(String... params) {
        for(Route route: DatabaseHelper.getInstance(mContext).fetchRoutes(new String[]{params[0]})){
            //Tours
            for(Tours tour:DatabaseHelper.getInstance(mContext).fetchTours(route.getId())){
                downloadItem(tour.getId(),tour.getImage(),tour.getImageLocal());
            }

            //Segments
            for(SegmentEntry segmentEntry:DatabaseHelper.getInstance(mContext).fetchSegmentEntry(route.getId())){
                //Segments
                for(Segment segment:DatabaseHelper.getInstance(mContext).fetchSegment(segmentEntry.getSegmentId())){
                    downloadItem(segment.getId(),segment.getImage(),segment.getImageLocal());
                    //downloadItem(segment.getId(),segment.getAudio(),segment.getAudioLocal()); ToDo: enable this to download audio and mbtiles
                    //downloadItem(segment.getId(),segment.getMbTiles(),segment.getMbTilesLocal());
                }

                //Stops
                for(Stops stop:DatabaseHelper.getInstance(mContext).fetchStops(segmentEntry.getSegmentId())){
                    downloadItem(stop.getId(),stop.getImage(),stop.getImageLocal());
                }

                //Activity
                for(Activity activity:DatabaseHelper.getInstance(mContext).fetchActivity(segmentEntry.getSegmentId())){
                    downloadItem(activity.getId(),activity.getImage(),activity.getImageLocal());
                    downloadItem(activity.getId(),activity.getImageWide(),activity.getImageWideLocal());
                }
            }
            /*Downloaded downloaded = new Downloaded();
            downloaded.setRouteId(route.getId());
            DatabaseHelper.getInstance(mContext).insertOrUpdateDownloaded(downloaded);*/
        }
        return null;
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        Toast.makeText(SmartTours.getContext(),"Route Downloaded",Toast.LENGTH_SHORT).show();
    }


    //key = route, id=1
    private boolean downloadItem(String id, String serverPath, String localPath){ //synchronized
        try {
            if(localPath == null) return false;
            //set the download URL, a url that points to a file on the internet
            //this is the file to be downloaded

            URL url = new URL("http://www.weenysoft.com/images/icons/free-image-to-pdf-converter.png");//ToDo: Change this to serverPath
            //URL url = new URL(serverPath);

            //create the new connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            //set up some things on the connection
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            //and connect!
            urlConnection.connect();

            //set the path where we want to save the file
            //in this case, going to save it on the root directory of the
            //sd card.
            File SDCardRoot = Environment.getExternalStorageDirectory();
            //create a new file, specifying the path, and the filename
            //which we want to save the file as.
            String path = localPath;
            String root = path.substring(0,path.lastIndexOf("/"));
            File dir = new File(root);
            if(dir.exists() == false){
                dir.mkdirs();
            };
            String fileName = path.substring(path.lastIndexOf("/"),path.length());
                /*Log.i("DownloadTask","root: "+root);
                Log.i("DownloadTask","fileName: "+fileName);*/
            File file = new File(root,fileName);  //SDCardRoot,"1.jpg"

            //this will be used to write the downloaded data into the file we created
            FileOutputStream fileOutput = new FileOutputStream(file);
            //this will be used in reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();
            //this is the total size of the file
            int totalSize = urlConnection.getContentLength();
            //variable to store total downloaded bytes
            int downloadedSize = 0;

            //create a buffer...
            byte[] buffer = new byte[1024];
            int bufferLength = 0; //used to store a temporary size of the buffer

            //now, read through the input buffer and write the contents to the file
            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                //add the data in the buffer to the file in the file output stream (the file on the sd card
                fileOutput.write(buffer, 0, bufferLength);
                //add up the size so we know how much is downloaded
                downloadedSize += bufferLength;
                //this is where you would do something to report the progress, like this maybe
                //updateProgress(downloadedSize, totalSize);

            }
            //close the output stream when done
            Utils.close(inputStream);
            Utils.close(fileOutput);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }


}
