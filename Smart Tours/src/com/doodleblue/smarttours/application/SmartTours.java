package com.doodleblue.smarttours.application;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 12/9/13
 * Time: 5:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmartTours extends Application {

    private static Context mContext;
    private static ConnectivityManager mConnectivityManager;
    private static String mPrivateFilePath;
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        initSingletons();
        
    }

    private void initSingletons(){
        DatabaseHelper.initInstance(mContext);
        Log.i("App","initSingletons!!!");
        Log.i("App","initSingletons!!!");
        Log.i("App","initSingletons!!!");
        mConnectivityManager =
                (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public static Context getContext() {
        return mContext;
    }
    
    public static boolean isNetworkConnected(){
    	NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                              activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
    
    public static String getPrivateFilePath(){
    	if(TextUtils.isEmpty(mPrivateFilePath))
    		mPrivateFilePath = mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    	return mPrivateFilePath;
    }
    
    
     
    
}
