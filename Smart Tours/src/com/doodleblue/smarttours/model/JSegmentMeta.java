package com.doodleblue.smarttours.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/26/13
 * Time: 11:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class JSegmentMeta {

    public String id;

    @SerializedName("route_id")
    public String routeId;

    //public String direction;  Todo: Uncomment this after getting this webservice response
    //public String order;

}
