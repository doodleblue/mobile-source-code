package com.doodleblue.smarttours.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 12/7/13
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class JAudioPoints {

    public String id;
    
    @SerializedName("startsegment")
    public String startSegment;
    
    @SerializedName("endsegment")
    public String endSegment;
    
    @SerializedName("associatesegment")
    public String associateSegment;
    public String latitude;
    public String longitude;
    
    @SerializedName("radius_for")
    public String radiusFwd;
    
    @SerializedName("radius_rev")
    public String radiusRev;
    
    @SerializedName("audio_fwd")
    public String audioFwd;
    
    @SerializedName("audio_rev")
    public String audioRev;
    
    @SerializedName("audio_main")
    public String audioMain;
    public String status;
    public String author;
    
    @SerializedName("lastmodified")
    public String lastModified;

}
