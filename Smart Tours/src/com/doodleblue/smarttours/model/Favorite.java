package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/13/13
 * Time: 12:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class Favorite {

    private String id;
    private String routeId;
    private boolean isCommentaryPurchased;

    public Favorite(){

    }

    public Favorite(String routeId, boolean commentaryPurchased) {
        this.routeId = routeId;
        isCommentaryPurchased = commentaryPurchased;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public boolean isCommentaryPurchased() {
        return isCommentaryPurchased;
    }

    public void setCommentaryPurchased(String commentaryPurchased) {
        isCommentaryPurchased = Boolean.parseBoolean(commentaryPurchased);
    }
}
