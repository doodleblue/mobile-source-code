package com.doodleblue.smarttours.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/23/13
 * Time: 12:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class JTours {
    public String id;
    public String name;
    public String image;
    public String imagesize;
    public String duration;
    public String commentary;
    public String distance;
    public String highlights;
    public String description;

    @SerializedName("route_id")
    public String routeId;

    public String start;
    public String finish;
    public String status;
    public String author;

    @SerializedName("lastmodified")
    public String lastModified;

}
