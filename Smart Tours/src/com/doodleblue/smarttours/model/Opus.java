package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/29/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class Opus {
	
    private String id;
    private String associateSegment;
    private String startSegment;
    private String endSegment;
    
    private String latitude;
    private String longitude;
    
    private String audioForward;
    private String audioReverse;
    private String audioMain;
    
    private String audioForwardLocal;
    private String audioReverseLocal;
    private String audioMainLocal;
    
    private String audioForwardSize;
    private String audioReverseSize;
    private String audioMainSize;
    
    private String radiusForward;
    private String radiusReverse;
    private String radiusMain;
    
    private String status;
    private String author;
    private String lastModified;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAssociateSegment() {
		return associateSegment;
	}
	public void setAssociateSegment(String associateSegment) {
		this.associateSegment = associateSegment;
	}
	public String getStartSegment() {
		return startSegment;
	}
	public void setStartSegment(String startSegment) {
		this.startSegment = startSegment;
	}
	public String getEndSegment() {
		return endSegment;
	}
	public void setEndSegment(String endSegment) {
		this.endSegment = endSegment;
	}
	public String getLatitude() {
		return latitude;
	}
	public Double getLatitudeInDouble() {
		return Double.parseDouble(latitude);
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public Double getLongitudeInDouble() {
		return Double.parseDouble(longitude);
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getAudioForward() {
		return audioForward;
	}
	public void setAudioForward(String audioForward) {
		this.audioForward = audioForward;
	}
	public String getAudioReverse() {
		return audioReverse;
	}
	public void setAudioReverse(String audioReverse) {
		this.audioReverse = audioReverse;
	}
	public String getAudioMain() {
		return audioMain;
	}
	public void setAudioMain(String audioMain) {
		this.audioMain = audioMain;
	}
	public String getAudioForwardLocal() {
		return audioForwardLocal;
	}
	public void setAudioForwardLocal(String audioForwardLocal) {
		this.audioForwardLocal = audioForwardLocal;
	}
	public String getAudioReverseLocal() {
		return audioReverseLocal;
	}
	public void setAudioReverseLocal(String audioReverseLocal) {
		this.audioReverseLocal = audioReverseLocal;
	}
	public String getAudioMainLocal() {
		return audioMainLocal;
	}
	public void setAudioMainLocal(String audioMainLocal) {
		this.audioMainLocal = audioMainLocal;
	}
	public String getRadiusForward() {
		return radiusForward;
	}
	public int getRadiusForwardInSeconds() {
		try{
			return Integer.parseInt(radiusForward);
		} catch(Exception e) {
			return 0;
		}
	}
	public void setRadiusForward(String radiusForward) {
		this.radiusForward = radiusForward;
	}
	public String getRadiusReverse() {
		return radiusReverse;
	}
	public int getRadiusReverseInSeconds() {
		try{
			return Integer.parseInt(radiusReverse);
		} catch(Exception e) {
			return 0;
		}
	}
	public void setRadiusReverse(String radiusReverse) {
		this.radiusReverse = radiusReverse;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getLastModified() {
		return lastModified;
	}
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}
	public String getAudioForwardSize() {
		return audioForwardSize;
	}
	public void setAudioForwardSize(String audioForwardSize) {
		this.audioForwardSize = audioForwardSize;
	}
	public String getAudioReverseSize() {
		return audioReverseSize;
	}
	public void setAudioReverseSize(String audioReverseSize) {
		this.audioReverseSize = audioReverseSize;
	}
	public String getAudioMainSize() {
		return audioMainSize;
	}
	public void setAudioMainSize(String audioMainSize) {
		this.audioMainSize = audioMainSize;
	}
	public String getRadiusMain() {
		return radiusMain;
	}
	public void setRadiusMain(String radiusMain) {
		this.radiusMain = radiusMain;
	}
    

    
}
