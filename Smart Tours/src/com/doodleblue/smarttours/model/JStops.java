package com.doodleblue.smarttours.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/23/13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class JStops {
    public String id;

    @SerializedName("segment_id")
    public String segmentId;

    public String name;
    public String types;
    public String address;

    @SerializedName("timeneeded")
    public String timeNeeded;

    public String highlights;
    public String description;
    public String status;
    public String author;
    public String cost;

    @SerializedName("lastmodified")
    public String lastModified;

    public String image;

    @SerializedName("imagesize")
    public String imageSize;

    public String image2;
    public String image2Size;
    public String latitude;
    public String longitude;


}
