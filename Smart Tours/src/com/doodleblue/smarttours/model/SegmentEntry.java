package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 11:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class SegmentEntry {

    private String id;
    private String routeId;
    private String segmentId;
    private String direction;
    private String order;
    private String lastModified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
}
