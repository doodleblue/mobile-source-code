package com.doodleblue.smarttours.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class Route {

    private String id;
    private String name;

    private String image;
    private String imageSize;
    private String imageLocal;

    private String image2;
    private String image2Size;
    private String image2Local;

    private String productId;
    private String startName;
    private String endName;
    private String start;
    private String end;
    private String minDay;
    private String maxDay;
    private String distance;
    private String highlights;
    private String lastModified;
    private String description;
    
    private String segmentMeta;
    
    private boolean isFavourite;
    

    private String nPOI;
    private String nStops;
    private String nPlaces;
    
    private String zone;
    private String isCommentaryPurchased;
    
    
    public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getIsCommentaryPurchased() {
		return isCommentaryPurchased;
	}

	public void setIsCommentaryPurchased(String isCommentaryPurchased) {
		this.isCommentaryPurchased = isCommentaryPurchased;
	}

	public String getSegmentMeta() {
		return segmentMeta;
	}

	public void setSegmentMeta(String segmentMeta) {
		this.segmentMeta = segmentMeta;
	}

	

	public String getnPOI() {
        return nPOI;
    }

    public void setnPOI(String nPOI) {
        this.nPOI = nPOI;
    }

    public String getnStops() {
        return nStops;
    }

    public void setnStops(String nStops) {
        this.nStops = nStops;
    }

    public String getnPlaces() {
        return nPlaces;
    }

    public void setnPlaces(String nPlaces) {
        this.nPlaces = nPlaces;
    }

    public String getStartName() {
        return startName;
    }

    public void setStartName(String startName) {
        this.startName = startName;
    }

    public String getEndName() {
        return endName;
    }

    public void setEndName(String endName) {
        this.endName = endName;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }



    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMinDay() {
        return minDay;
    }

    public void setMinDay(String minDay) {
        this.minDay = minDay;
    }

    public String getMaxDay() {
        return maxDay;
    }

    public void setMaxDay(String maxDay) {
        this.maxDay = maxDay;
    }

    public String getImageLocal() {
        return imageLocal;
    }

    public void setImageLocal(String imageLocal) {
        this.imageLocal = imageLocal;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage2Size() {
        return image2Size;
    }

    public void setImage2Size(String image2Size) {
        this.image2Size = image2Size;
    }

    public String getImage2Local() {
        return image2Local;
    }

    public void setImage2Local(String image2Local) {
        this.image2Local = image2Local;
    }
}
