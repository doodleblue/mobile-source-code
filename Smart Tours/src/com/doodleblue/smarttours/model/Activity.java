package com.doodleblue.smarttours.model;

import android.text.TextUtils;

import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class Activity {

    private String id;

    private String segmentId;
    private String userId;
    private String placeId;
    private String type;
    private String time;
    private String title;

    private String subtitle;
    private String price;
    private String latitude;
    private String longitude;
    private String description;

    private String facility;
    private String highlights;
    private String operator;

    private String cuisine;
    private String ratings;
    private String duration;

    private String image;
    private String imageSize;
    private String imageLocal;
    private String imageWide;
    private String imageWideSize;
    private String imageWideLocal;
    private String lastModified;

    private String innerAddOnTypes;

    private String email;

    private String placeName;

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInnerAddOnTypes() {
        return innerAddOnTypes;
    }

    public void setInnerAddOnTypes(String innerAddOnTypes) {
        this.innerAddOnTypes = innerAddOnTypes;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public String getImageWideSize() {
        return imageWideSize;
    }

    public void setImageWideSize(String imageWideSize) {
        this.imageWideSize = imageWideSize;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    private static int priceImageResId      = R.drawable.dollor_img;

    public static int getRateImageResId() {
        return rateImageResId;
    }

    private static int rateImageResId      = R.drawable.rateit_img;
    private static int durationImageResId   = R.drawable.things_time_img;

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getPriceImageResId() {
        return priceImageResId;
    }

    public int getDurationImageResId() {
        return durationImageResId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLatitude() {
        return latitude;
    }
    
    public Double getLatitudeInDouble() {
		if(!TextUtils.isEmpty(latitude))
			return Double.parseDouble(latitude);
		else
			return (double) 0;
	}

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }
    
    public Double getLongitudeInDouble() {
		if(!TextUtils.isEmpty(longitude))
			return Double.parseDouble(longitude);
		else
			return (double) 0;
	}

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImageLocal() {
        return imageLocal;
    }

    public void setImageLocal(String imageLocal) {
        this.imageLocal = imageLocal;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageWide() {
        return imageWide;
    }

    public void setImageWide(String imageWide) {
        this.imageWide = imageWide;
    }

    public String getImageWideLocal() {
        return imageWideLocal;
    }

    public void setImageWideLocal(String imageWideLocal) {
        this.imageWideLocal = imageWideLocal;
    }
}
