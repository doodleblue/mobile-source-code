package com.doodleblue.smarttours.model;

import java.util.List;

public class JSyncSet {
	
	public List<JSegments> segment;
    public List<JActivity> activity;
    public List<JStops> stops;
    public List<JRoutes> routes;
    public List<JTours> tours;
    
}
