package com.doodleblue.smarttours.model;

import android.text.TextUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class Segment {
	private String id;
	private String name;
	private String isPlace;
	private String latitude;
	private String longitude;
	private String description;
	private String audio;
	private String audioSize;
	private String audioLocal;
	private String coordinates;
	private String mbTiles;
	private String mbTilesSize;
	private String mbTilesLocal;
	private String lastModified;
	private String image;
	private String imageSize;
	private String imageLocal;
	private String islandType;
	private String hyperLink;
	
	private String nStops;
	private String nActivities;
	
	


	public String getnStops() {
		return nStops;
	}

	public void setnStops(int i) {
		this.nStops = String.valueOf(i);
	}

	public String getnActivities() {
		return nActivities;
	}

	public void setnActivities(int i) {
		this.nActivities = String.valueOf(i);
	}

	public String getHyperLink() {
		return hyperLink;
	}

	public void setHyperLink(String hyperLink) {
		this.hyperLink = hyperLink;
	}

	public String getAudioSize() {
		return audioSize;
	}

	public void setAudioSize(String audioSize) {
		this.audioSize = audioSize;
	}

	public String getMbTilesSize() {
		return mbTilesSize;
	}

	public void setMbTilesSize(String mbTilesSize) {
		this.mbTilesSize = mbTilesSize;
	}

	public String getImageSize() {
		return imageSize;
	}

	public void setImageSize(String imageSize) {
		this.imageSize = imageSize;
	}

	public String getIslandType() {
		return islandType;
	}

	public void setIslandType(String islandType) {
		this.islandType = islandType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageLocal() {
		return imageLocal;
	}

	public void setImageLocal(String imageLocal) {
		this.imageLocal = imageLocal;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String isPlace() {
		/*boolean tag = false;
		try{
			tag = Boolean.parseBoolean(isPlace);
		}
		catch(final Exception e){
			e.printStackTrace();
		}*/
		return isPlace;
	}

	public void setPlace(String place) {
		isPlace = place;
	}

	public String getLatitude() {
		return latitude;
	}

	public Double getLatitudeInDouble() {
		if(!TextUtils.isEmpty(latitude))
			return Double.parseDouble(latitude);
		else
			return (double) 0;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public Double getLongitudeInDouble() {
		if(!TextUtils.isEmpty(longitude))
			return Double.parseDouble(longitude);
		else
			return (double) 0;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAudio() {
		return audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public String getMbTiles() {
		return mbTiles;
	}

	public void setMbTiles(String mbTiles) {
		this.mbTiles = mbTiles;
	}

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public String getAudioLocal() {
		return audioLocal;
	}

	public void setAudioLocal(String audioLocal) {
		this.audioLocal = audioLocal;
	}

	public String getMbTilesLocal() {
		return mbTilesLocal;
	}

	public void setMbTilesLocal(String mbTilesLocal) {
		this.mbTilesLocal = mbTilesLocal;
	}

	public void setnStops(String string) {
		nStops = string;
	}

	public void setnActivities(String string) {
		nActivities = string;
	}
}
