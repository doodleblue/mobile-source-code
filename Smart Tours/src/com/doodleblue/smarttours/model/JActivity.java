package com.doodleblue.smarttours.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/23/13
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class JActivity {

    @SerializedName("_id")
    public String id;
    public String email;

    @SerializedName("segment_id")
    public String segmentId;

    @SerializedName("place_id")
    public String placeId;

    public String latitude;
    public String longitude;
    public String facility;
    public String highlights;
    public String operator;
    public String type;
    public String place;
    public String title;

    @SerializedName("subtitle")
    public String subTitle;

    public String price;
    public String info1;
    public String info2;
    public String info3;
    public String info4;
    public String info5;
    public String phone;

    @SerializedName("other_phone")
    public String otherPhone;

    public String web;
    public String description;
    public String image;
    public String image2;
    public String imagesize;
    public String image2size;
    public String location;
    public String status;
    public String author;
    public String date;

    @SerializedName("date_gmt")
    public String dateGmt;

    public String address;
    public String promovideo;

}
