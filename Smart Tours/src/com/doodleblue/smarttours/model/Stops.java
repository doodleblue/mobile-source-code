package com.doodleblue.smarttours.model;

import android.text.TextUtils;

import com.smarttoursnz.guide.R;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class Stops {

    private String id;
    private String segmentId;
    private String name;
    private String type;

    private String address;
    private String timeNeeded;
    private String highlights;
    private String description;

    private String image;
    private String imageSize;
    private String imageLocal;
    private String imageWide;
    private String imageWideSize;
    private String imageWideLocal;

    private String lastModified;

    private String latitude;
    private String longitude;

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public String getImageWideSize() {
        return imageWideSize;
    }

    public void setImageWideSize(String imageWideSize) {
        this.imageWideSize = imageWideSize;
    }

    public String getLatitude() {
        return latitude;
    }
    
    public Double getLongitudeInDouble() {
		if(!TextUtils.isEmpty(longitude))
			return Double.parseDouble(longitude);
		else
			return (double) 0;
	}
    
    public Double getLatitudeInDouble() {
		if(!TextUtils.isEmpty(latitude))
			return Double.parseDouble(latitude);
		else
			return (double) 0;
	}

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTimeNeeded() {
        return timeNeeded;
    }

    public void setTimeNeeded(String timeNeeded) {
        this.timeNeeded = timeNeeded;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageWide() {
        return imageWide;
    }

    public void setImageWide(String imageWide) {
        this.imageWide = imageWide;
    }

    public String getImageWideLocal() {
        return imageWideLocal;
    }

    public void setImageWideLocal(String imageWideLocal) {
        this.imageWideLocal = imageWideLocal;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageLocal() {
        return imageLocal;
    }

    public void setImageLocal(String imageLocal) {
        this.imageLocal = imageLocal;
    }

    public static final String[] TYPES = new String[]{
            "Lookouts",
            "Picnic Areas",
            "Walks",
            "Toilets"
    };

    public static final int[] TYPE_DRAWABLES = new int[]{
            R.drawable.lookout_unselect,
            R.drawable.picnic_unselect,
            R.drawable.walks_unselect,
            R.drawable.toilet_unselect
    };
}
