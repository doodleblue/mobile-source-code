package com.doodleblue.smarttours.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/23/13
 * Time: 12:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class JSegments {
    public String id;
    public String name;

    @SerializedName("isplace")
    public String isPlace;

    public String latitude;
    public String langitude;
    public String description;
    public String audio;
    public String coorinates;

    @SerializedName("mbtiles")
    public String mbTiles;

    public String image;

    @SerializedName("lastmodified")
    public String lastModified;

    public String status;
    public String author;
    public String island;

}
