package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/12/13
 * Time: 4:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class ActivityCount {

    private int nPaidActivity;
    private int nAccomodation;
    private int nFoodAndDrink;
    private int nAttraction;
    private int nOther;

    public ActivityCount(int nPaidActivity, int nAccomodation, int nFoodAndDrink, int nAttraction, int nOther) {
        this.nPaidActivity = nPaidActivity;
        this.nAccomodation = nAccomodation;
        this.nFoodAndDrink = nFoodAndDrink;
        this.nAttraction = nAttraction;
        this.nOther = nOther;
    }

    public int getnPaidActivity() {
        return nPaidActivity;
    }

    public void setnPaidActivity(int nPaidActivity) {
        this.nPaidActivity = nPaidActivity;
    }

    public int getnAccomodation() {
        return nAccomodation;
    }

    public void setnAccomodation(int nAccomodation) {
        this.nAccomodation = nAccomodation;
    }

    public int getnFoodAndDrink() {
        return nFoodAndDrink;
    }

    public void setnFoodAndDrink(int nFoodAndDrink) {
        this.nFoodAndDrink = nFoodAndDrink;
    }

    public int getnAttraction() {
        return nAttraction;
    }

    public void setnAttraction(int nAttraction) {
        this.nAttraction = nAttraction;
    }

    public int getnOther() {
        return nOther;
    }

    public void setnOther(int nOther) {
        this.nOther = nOther;
    }
}
