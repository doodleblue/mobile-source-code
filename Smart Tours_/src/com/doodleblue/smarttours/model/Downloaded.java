package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/16/13
 * Time: 1:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class Downloaded {
    private String id;
    private String routeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }
}
