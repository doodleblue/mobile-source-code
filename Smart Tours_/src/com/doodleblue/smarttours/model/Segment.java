package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class Segment {
    private String id;
    private String name;
    private String isPlace;
    private String latitude;
    private String longitude;
    private String description;
    private String audio;
    private String audioLocal;
    private String coordinates;
    private String mbTiles;
    private String mbTilesLocal;
    private String lastModified;
    private String image;
    private String imageLocal;
    private String islandType;

    public String getIslandType() {
        return islandType;
    }

    public void setIslandType(String islandType) {
        this.islandType = islandType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageLocal() {
        return imageLocal;
    }

    public void setImageLocal(String imageLocal) {
        this.imageLocal = imageLocal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String isPlace() {
        return isPlace;
    }

    public void setPlace(String place) {
        isPlace = place;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getMbTiles() {
        return mbTiles;
    }

    public void setMbTiles(String mbTiles) {
        this.mbTiles = mbTiles;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getAudioLocal() {
        return audioLocal;
    }

    public void setAudioLocal(String audioLocal) {
        this.audioLocal = audioLocal;
    }

    public String getMbTilesLocal() {
        return mbTilesLocal;
    }

    public void setMbTilesLocal(String mbTilesLocal) {
        this.mbTilesLocal = mbTilesLocal;
    }
}
