package com.doodleblue.smarttours.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/23/13
 * Time: 2:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class JGetSegments {
    public List<JSegments> segment;
    public List<JActivity> activity;
    public List<JStops> stops;
}
