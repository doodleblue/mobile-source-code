package com.doodleblue.smarttours.model;

public class PlacePropertyBean {


    public String placeName;
	private String title;
	private int resId;
	private String count;
	
	public PlacePropertyBean(String title, int resId, String count) {
		super();
		this.title = title;
		this.resId = resId;
		this.count = count;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getResId() {
		return resId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}
	
}
