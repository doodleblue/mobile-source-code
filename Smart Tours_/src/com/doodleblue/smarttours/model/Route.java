package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class Route {

    private String id;
    private String name;
    private String image;
    private String imageLocal;
    private String imageMedium;
    private String imageMediumLocal;
    private String imageLarge;
    private String imageLargeLocal;
    private String imageWide;
    private String imageWideLocal;
    private String productId;
    private String startName;
    private String endName;
    private String start;
    private String end;
    private String minDay;
    private String maxDay;
    private String distance;
    private String highlights;
    private String lastModified;
    private String description;
    private boolean isFavourite;

    public String getStartName() {
        return startName;
    }

    public void setStartName(String startName) {
        this.startName = startName;
    }

    public String getEndName() {
        return endName;
    }

    public void setEndName(String endName) {
        this.endName = endName;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }



    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMinDay() {
        return minDay;
    }

    public void setMinDay(String minDay) {
        this.minDay = minDay;
    }

    public String getMaxDay() {
        return maxDay;
    }

    public void setMaxDay(String maxDay) {
        this.maxDay = maxDay;
    }

    public String getImageLocal() {
        return imageLocal;
    }

    public void setImageLocal(String imageLocal) {
        this.imageLocal = imageLocal;
    }

    public String getImageMedium() {
        return imageMedium;
    }

    public void setImageMedium(String imageMedium) {
        this.imageMedium = imageMedium;
    }

    public String getImageMediumLocal() {
        return imageMediumLocal;
    }

    public void setImageMediumLocal(String imageMediumLocal) {
        this.imageMediumLocal = imageMediumLocal;
    }

    public String getImageLarge() {
        return imageLarge;
    }

    public void setImageLarge(String imageLarge) {
        this.imageLarge = imageLarge;
    }

    public String getImageLargeLocal() {
        return imageLargeLocal;
    }

    public void setImageLargeLocal(String imageLargeLocal) {
        this.imageLargeLocal = imageLargeLocal;
    }

    public String getImageWide() {
        return imageWide;
    }

    public void setImageWide(String imageWide) {
        this.imageWide = imageWide;
    }

    public String getImageWideLocal() {
        return imageWideLocal;
    }

    public void setImageWideLocal(String imageWideLocal) {
        this.imageWideLocal = imageWideLocal;
    }
}
