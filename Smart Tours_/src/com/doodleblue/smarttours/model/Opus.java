package com.doodleblue.smarttours.model;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/29/13
 * Time: 3:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class Opus {
    private String id;
    private String associateSegment;
    private String start;
    private String end;
    private String latitude;
    private String longitude;
    private String audio;
    private String radius;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssociateSegment() {
        return associateSegment;
    }

    public void setAssociateSegment(String associateSegment) {
        this.associateSegment = associateSegment;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getLatitude() {
        return latitude;
    }

    public double getLatitudeInDouble() {
        return Double.parseDouble(latitude);
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public double getLongitudeInDouble() {
        return Double.parseDouble(longitude);
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getRadius() {
        return radius;
    }

    public double getRadiusInDouble() {
        return Double.parseDouble(radius);
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }
}
