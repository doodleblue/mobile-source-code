package com.doodleblue.smarttours.constants;

import android.os.Environment;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/4/13
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppConstants {
    public static final String ROUTES = "routes";
    public static final String MAPS = "maps";
    public static final String THINGS_TO_DO = "things_to_do_selector";
    public static final String SETTINGS = "settings";

    public static final String DOWNLOADS = "downloads";
    public static final String MY_FAVOURITE = "my_favourite";
    public static final String SELECT_ROUTES = "select_route";

    public static final String ABOUT = "about";
    public static final String SYNCHRONIZATION = "synchronization";
    public static final int DEFAULT_ZOOM_LEVEL = 12;

    public static final String BASE_URL             = "http://192.168.1.150/smart/";
    public static final String GET_ALL_ROUTES_URL   = "http://192.168.1.150/smart/searchroutes.php";
    public static final String GET_SEGMENTS_URL     = "http://192.168.1.150/smart/getsegments.php?segment_id=";
    public static final String SYNC_URL             = "http://192.168.1.150/smart/sync.php?" +
            "segment_id=value1" +
            "&activity_id=value2" +
            "&stop_id=value3" +
            "&route_id=value4";

    public static final String FILE_DIR = "smartToursDb1";
    public static final String ROOT_DIRECTORY = Environment.getExternalStorageDirectory()
            + File.separator + FILE_DIR
            + File.separator;

    public static boolean IS_FIRST_TIME;
}
