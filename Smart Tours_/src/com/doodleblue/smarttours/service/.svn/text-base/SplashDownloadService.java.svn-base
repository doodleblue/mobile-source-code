package com.doodleblue.smarttours.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import com.doodleblue.smarttours.activity.MainActivity;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.*;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/29/13
 * Time: 10:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class SplashDownloadService extends IntentService {

    private static final String MY_CONTEXT = "context";

    public SplashDownloadService(){
        super("SplashDownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //Code to fetch Routes
        InputStream inputStream = retrieveStream(AppConstants.GET_ALL_ROUTES_URL);
        Gson gson = new Gson();
        Reader reader = new InputStreamReader(inputStream);
        JSearchRoutes response = gson.fromJson(reader, JSearchRoutes.class);

        for(JRoutes jRoutes:response.routes){
            Route route = new Route();
            route.setId(jRoutes.id);
            route.setName(jRoutes.name);
            route.setImage(jRoutes.image);
            route.setImageWide(jRoutes.image2);
            route.setProductId(jRoutes.productId);
            route.setStart(jRoutes.start);
            route.setEnd(jRoutes.end);
            route.setMinDay(jRoutes.minDay);
            route.setMaxDay(jRoutes.maxDay);
            route.setDistance(jRoutes.distance);
            route.setHighlights(jRoutes.highlights);
            route.setLastModified(jRoutes.lastModified);
            route.setDescription(jRoutes.description);
            route.setImageLocal(AppConstants.ROOT_DIRECTORY+"route" + File.separator + jRoutes.id + File.separator + "thumb.png");
            route.setImageWideLocal(AppConstants.ROOT_DIRECTORY+"route" + File.separator + jRoutes.id + File.separator + "wide.png");
            DatabaseHelper.getInstance().insertOrUpdateRoute(route);
            downloadItem(route.getId(),route.getImage(),route.getImageLocal());
            downloadItem(route.getId(),route.getImage(),route.getImageLocal());
        }

        for(JSegmentMeta segmentMeta:response.segments){
            SegmentEntry segmentEntry = new SegmentEntry();
            segmentEntry.setId(segmentMeta.id);
            segmentEntry.setSegmentId(segmentMeta.id);
            segmentEntry.setRouteId(segmentMeta.routeId);
            //segmentEntry.setDirection(segmentMeta.direction); Todo: Uncomment this after getting this webservice response
            //segmentEntry.setOrder(segmentMeta.order);
            DatabaseHelper.getInstance().insertOrUpdateSegmentEntry(segmentEntry);
        }

        for(JTours jTours:response.tours){
            Tours tour = new Tours();
            tour.setId(jTours.id);
            tour.setName(jTours.name);
            tour.setImage(jTours.image);
            tour.setImageLocal(AppConstants.ROOT_DIRECTORY+"tour"+ File.separator + jTours.id + File.separator + "thumb.png");
            tour.setDuration(jTours.duration);
            tour.setCommentary(jTours.commentary);
            tour.setDistance(jTours.distance);
            tour.setHighlights(jTours.highlights);
            tour.setDescription(jTours.description);
            tour.setRouteId(jTours.routeId);
            tour.setStart(jTours.start);
            tour.setEnd(jTours.finish);
            tour.setLastModified(jTours.lastModified);
            DatabaseHelper.getInstance().insertOrUpdateTours(tour);
        }

        try {
            reader.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Code to fetch Segments
        ArrayList<SegmentEntry> segmentEntries = DatabaseHelper.getInstance().fetchSegmentEntry(null);
        for(SegmentEntry segmentEntry:segmentEntries){
            inputStream = retrieveStream(AppConstants.GET_SEGMENTS_URL+segmentEntry.getSegmentId());
            gson = new Gson();
            reader = new InputStreamReader(inputStream);
            JGetSegments responseSegment = gson.fromJson(reader, JGetSegments.class);

            for(JSegments jSegments:responseSegment.segment){
                Segment segment = new Segment();
                segment.setId(jSegments.id);
                segment.setName(jSegments.name);
                segment.setPlace(jSegments.isPlace);
                segment.setIslandType(jSegments.island);
                segment.setLatitude(jSegments.latitude);
                segment.setLongitude(jSegments.langitude);
                segment.setDescription(jSegments.description);
                segment.setAudio(jSegments.audio);
                segment.setAudioLocal(AppConstants.ROOT_DIRECTORY+"segment" + File.separator + jSegments.id + File.separator + "audio.mp3");
                segment.setCoordinates(jSegments.coorinates);
                segment.setMbTiles(jSegments.mbTiles);
                segment.setMbTilesLocal(AppConstants.ROOT_DIRECTORY+"segment" + File.separator + jSegments.id + File.separator + "tiles.mbtiles");
                segment.setImage(jSegments.image);
                segment.setImageLocal(AppConstants.ROOT_DIRECTORY+"segment" + File.separator + jSegments.id + File.separator + "thumb.png");
                segment.setLastModified(jSegments.lastModified);
                DatabaseHelper.getInstance().insertOrUpdateSegment(segment);
            }

            for(JActivity jActivity:responseSegment.activity){
                Activity activity = new Activity();
                activity.setId(jActivity.id);
                activity.setType(jActivity.type);
                activity.setTitle(jActivity.title);
                activity.setSubtitle(jActivity.type);
                activity.setSegmentId(jActivity.segmentId);
                activity.setPlaceId(jActivity.placeId);
                activity.setLatitude(jActivity.latitude);
                activity.setLongitude(jActivity.longitude);
                activity.setDescription(jActivity.description);
                activity.setHighlights(jActivity.info2);
                activity.setFacility(jActivity.info2);
                activity.setCuisine(jActivity.info2);
                activity.setDuration(jActivity.info1);
                activity.setPrice(jActivity.price);
                activity.setTime(jActivity.info3);
                activity.setImage(jActivity.image);
                activity.setImageLocal(AppConstants.ROOT_DIRECTORY+"activity" + File.separator + jActivity.id + File.separator + "thumb.png");
                activity.setImageWide(jActivity.image2);
                activity.setImageWideLocal(AppConstants.ROOT_DIRECTORY+"activity" + File.separator + jActivity.id + File.separator + "wide.png");
                activity.setLastModified(jActivity.date);

                DatabaseHelper.getInstance().insertOrUpdateActivity(activity);
                for(int i=0;i<3;i++){
                    ActivityAccessory activityAccessory = new ActivityAccessory();
                    activityAccessory.setActivityId(jActivity.id);
                    activityAccessory.setType(jActivity.type);
                    switch (i){
                        case 0:
                            activityAccessory.setValue(jActivity.phone);
                            break;
                        case 1:
                            activityAccessory.setValue(jActivity.web);
                            break;
                        case 2:
                            activityAccessory.setValue(jActivity.otherPhone);
                            break;
                    }
                    DatabaseHelper.getInstance().insertOrUpdateActivityAccessory(activityAccessory);
                }
            }

            for(JStops jStops:responseSegment.stops){
                Stops stops = new Stops();
                stops.setId(jStops.id);
                stops.setName(jStops.name);
                stops.setSegmentId(jStops.segmentId);
                stops.setType(jStops.type);
                stops.setAddress(jStops.address);
                stops.setTimeNeeded(jStops.timeNeeded);
                stops.setHighlights(jStops.highlights);
                stops.setDescription(jStops.description);
                stops.setLastModified(jStops.lastModified);
                stops.setImage(jStops.image);
                stops.setImageLocal(AppConstants.ROOT_DIRECTORY+"stop"+ File.separator + jStops.id + File.separator + "thumb.png");
                DatabaseHelper.getInstance().insertOrUpdateStops(stops);
            }

            try {
                reader.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    //key = route, id=1
    private boolean downloadItem(String id, String serverPath, String localPath){ //synchronized
        try {
            //set the download URL, a url that points to a file on the internet
            //this is the file to be downloaded
            if(localPath == null) return false;
            URL url = new URL("http://www.weenysoft.com/images/icons/free-image-to-pdf-converter.png");//ToDo: Change this to serverPath
            //URL url = new URL(serverPath);

            //create the new connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            //set up some things on the connection
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            //and connect!
            urlConnection.connect();

            //set the path where we want to save the file
            //in this case, going to save it on the root directory of the
            //sd card.
            File SDCardRoot = Environment.getExternalStorageDirectory();
            //create a new file, specifying the path, and the filename
            //which we want to save the file as.
            String path = localPath;
            String root = path.substring(0,path.lastIndexOf("/"));
            File dir = new File(root);
            if(dir.exists() == false){
                dir.mkdirs();
            };
            String fileName = path.substring(path.lastIndexOf("/"),path.length());
                /*Log.i("DownloadTask","root: "+root);
                Log.i("DownloadTask","fileName: "+fileName);*/
            File file = new File(root,fileName);  //SDCardRoot,"1.jpg"

            //this will be used to write the downloaded data into the file we created
            FileOutputStream fileOutput = new FileOutputStream(file);
            //this will be used in reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();
            //this is the total size of the file
            int totalSize = urlConnection.getContentLength();
            //variable to store total downloaded bytes
            int downloadedSize = 0;

            //create a buffer...
            byte[] buffer = new byte[1024];
            int bufferLength = 0; //used to store a temporary size of the buffer

            //now, read through the input buffer and write the contents to the file
            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                //add the data in the buffer to the file in the file output stream (the file on the sd card
                fileOutput.write(buffer, 0, bufferLength);
                //add up the size so we know how much is downloaded
                downloadedSize += bufferLength;
                //this is where you would do something to report the progress, like this maybe
                //updateProgress(downloadedSize, totalSize);

            }
            //close the output stream when done
            Utils.close(inputStream);
            Utils.close(fileOutput);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.ResponseReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(broadcastIntent);

        return true;
    }

    private InputStream retrieveStream(String url) {
        Log.i("retrieveStream", url);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(url);
        try {
            HttpResponse getResponse = client.execute(getRequest);
            final int statusCode = getResponse.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w(getClass().getSimpleName(),
                        "Error " + statusCode + " for URL " + url);
                return null;
            }
            HttpEntity getResponseEntity = getResponse.getEntity();
            return getResponseEntity.getContent();
        }
        catch (IOException e) {
            getRequest.abort();
            Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
        }
        return null;
    }


}
