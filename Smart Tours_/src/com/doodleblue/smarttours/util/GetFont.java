package com.doodleblue.smarttours.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/6/13
 * Time: 2:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetFont {
    public static final int HELVITICA_CONDENSED = 1;
    public static final int HELVITICA_OT1 = 2;
    public static final int HELVITICA_TT = 3;

    private static GetFont ourInstance;

    private static  Context mContext;
    private static Typeface mTypefaceCondensed;
    private static Typeface mTypefaceOT1;
    private static Typeface mTypefaceTT;


    public static GetFont getInstance(Context context) {
        mContext = context;
        if(ourInstance == null)
            ourInstance = new GetFont();
        if(mTypefaceCondensed == null)
            mTypefaceCondensed = Typeface.createFromAsset(mContext.getAssets(),"fonts/Helvetica-Condensed_0.otf");
        if(mTypefaceOT1 == null)
            mTypefaceOT1 = Typeface.createFromAsset(mContext.getAssets(),"fonts/Helvetica-Condensed_0.otf");
        if(mTypefaceTT == null)
            mTypefaceTT = Typeface.createFromAsset(mContext.getAssets(),"fonts/Helvetica Bold.ttf");
        return ourInstance;
    }

    public Typeface getFont(int type)
    {
        switch (type)
        {
            case HELVITICA_CONDENSED:
            {
                return mTypefaceCondensed;
            }
            case HELVITICA_OT1:
            {
                return mTypefaceOT1;
            }
            case HELVITICA_TT:
            {
                return mTypefaceTT;
            }
            default:
                return mTypefaceCondensed;
        }
    }


}
