package com.doodleblue.smarttours.sqlite.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.*;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/11/13
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    private static DatabaseHelper sDatabaseHelper = null;
    private static Context mContext;
    public static DatabaseHelper getInstance(Context context)
    {
        mContext = context;
        if(sDatabaseHelper==null)
            sDatabaseHelper = new DatabaseHelper(context);
        return sDatabaseHelper;
    }
    public static DatabaseHelper getInstance()
    {
        if(sDatabaseHelper==null && mContext!=null)
            sDatabaseHelper = new DatabaseHelper(mContext);
        return sDatabaseHelper;
    }
    private DatabaseHelper(Context context) {

       super(context, AppConstants.ROOT_DIRECTORY + DATABASE_NAME, null, DATABASE_VERSION);

        //super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //To change body of implemented methods use File | Settings | File Templates.

        db.execSQL(CREATE_TABLE_SEGMENT);
        db.execSQL(CREATE_TABLE_ROUTE);
        db.execSQL(CREATE_TABLE_SEGMENT_ENTRY);
        db.execSQL(CREATE_TABLE_TOURS);
        db.execSQL(CREATE_TABLE_STOPS);
        db.execSQL(CREATE_TABLE_ACTIVITY);
        db.execSQL(CREATE_TABLE_ACTIVITY_ACCESSORY);
        db.execSQL(CREATE_TABLE_FAVORITE);
        db.execSQL(CREATE_TABLE_DOWNLOADED);
        db.execSQL(CREATE_TABLE_OPUS);

        Log.i("DatabaseHelper",CREATE_TABLE_SEGMENT);
        Log.i("DatabaseHelper",CREATE_TABLE_ROUTE);
        Log.i("DatabaseHelper",CREATE_TABLE_TOURS);
        Log.i("DatabaseHelper",CREATE_TABLE_STOPS);
        Log.i("DatabaseHelper",CREATE_TABLE_ACTIVITY);
        Log.i("DatabaseHelper",CREATE_TABLE_ACTIVITY_ACCESSORY);
        Log.i("DatabaseHelper",CREATE_TABLE_FAVORITE);
        Log.i("DatabaseHelper",CREATE_TABLE_DOWNLOADED);
        Log.i("DatabaseHelper",CREATE_TABLE_OPUS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //To change body of implemented methods use File | Settings | File Templates.
    }




    //INSERT
    public long insertOrUpdateSegment(Segment segment)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID, segment.getId());
        values.put(KEY_NAME, segment.getName());
        values.put(KEY_IS_PLACE, segment.isPlace());
        values.put(KEY_ISLAND_TYPE, segment.getIslandType());
        values.put(KEY_LAT, segment.getLatitude());
        values.put(KEY_LONG, segment.getLongitude());
        values.put(KEY_DESC, segment.getDescription());
        values.put(KEY_AUDIO, segment.getAudio());
        values.put(KEY_AUDIO_LOCAL, segment.getAudioLocal());
        values.put(KEY_COORDINATES, segment.getCoordinates());
        values.put(KEY_MB_TILES,segment.getMbTiles());
        values.put(KEY_MB_TILES_LOCAL,segment.getMbTilesLocal());
        values.put(KEY_LAST_MODIFIED,segment.getLastModified());
        values.put(KEY_IMAGE,segment.getImage());
        values.put(KEY_IMAGE_LOCAL,segment.getImageLocal());


        // insert row
        return db.insertWithOnConflict(TABLE_SEGMENT, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateRoute(Route route)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID, route.getId());
        values.put(KEY_NAME, route.getName());

        values.put(KEY_IMAGE, route.getImage());
        values.put(KEY_IMAGE_LOCAL, route.getImageLocal());
        values.put(KEY_IMAGE_WIDE, route.getImageWide());
        values.put(KEY_IMAGE_WIDE_LOCAL, route.getImageWideLocal());

        values.put(KEY_IMAGE_MEDIUM, route.getImageMedium());
        values.put(KEY_IMAGE_MEDIUM_LOCAL, route.getImageMediumLocal());
        values.put(KEY_IMAGE_LARGE, route.getImageLarge());
        values.put(KEY_IMAGE_LARGE_LOCAL, route.getImageLargeLocal());
        values.put(KEY_PROD_ID, route.getProductId());
        values.put(KEY_START, route.getStart());
        values.put(KEY_END, route.getEnd());
        values.put(KEY_MIN_DAY, route.getMinDay());
        values.put(KEY_MAX_DAY, route.getMaxDay());
        values.put(KEY_DISTANCE, route.getDistance());
        values.put(KEY_HIGHLIGHTS,route.getHighlights());
        values.put(KEY_LAST_MODIFIED,route.getLastModified());
        values.put(KEY_DESC, route.getDescription());

        // insert row
        return db.insertWithOnConflict(TABLE_ROUTE, null, values,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateSegmentEntry(SegmentEntry segmentEntry)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID, segmentEntry.getId());
        values.put(KEY_ROUTE_ID, segmentEntry.getRouteId());
        values.put(KEY_SEGMENT_ID, segmentEntry.getSegmentId());
        values.put(KEY_DIRECTION, segmentEntry.getDirection());
        values.put(KEY_ORDER, segmentEntry.getOrder());
        values.put(KEY_LAST_MODIFIED,segmentEntry.getLastModified());

        // insert row
        return  db.insertWithOnConflict(TABLE_SEGMENT_ENTRY, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateTours(Tours tours)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID, tours.getId());
        values.put(KEY_NAME, tours.getName());
        values.put(KEY_IMAGE, tours.getImage());
        values.put(KEY_IMAGE_LOCAL, tours.getImageLocal());
        values.put(KEY_IMAGE_WIDE, tours.getImageWide());
        values.put(KEY_IMAGE_WIDE_LOCAL, tours.getImageWideLocal());
        values.put(KEY_DURATION, tours.getDuration());
        values.put(KEY_COMMENTARY, tours.getCommentary());
        values.put(KEY_DISTANCE, tours.getDistance());
        values.put(KEY_HIGHLIGHTS,tours.getHighlights());
        values.put(KEY_DESC, tours.getDescription());
        values.put(KEY_ROUTE_ID, tours.getRouteId());
        values.put(KEY_START, tours.getStart());
        values.put(KEY_END, tours.getEnd());
        values.put(KEY_LAST_MODIFIED,tours.getLastModified());

        // insert row
        return  db.insertWithOnConflict(TABLE_TOURS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateStops(Stops stops)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID, stops.getId());
        values.put(KEY_SEGMENT_ID, stops.getSegmentId());
        values.put(KEY_NAME, stops.getName());
        values.put(KEY_TYPE, stops.getType());
        values.put(KEY_ADDRESS, stops.getAddress());
        values.put(KEY_TIME_NEEDED, stops.getTimeNeeded());
        values.put(KEY_HIGHLIGHTS,stops.getHighlights());
        values.put(KEY_DESC, stops.getDescription());
        values.put(KEY_IMAGE, stops.getImage());
        values.put(KEY_IMAGE_LOCAL, stops.getImageLocal());
        values.put(KEY_IMAGE_WIDE, stops.getImageWide());
        values.put(KEY_IMAGE_WIDE_LOCAL, stops.getImageWideLocal());
        values.put(KEY_LAST_MODIFIED, stops.getLastModified());

        // insert row
        return  db.insertWithOnConflict(TABLE_STOPS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }


    public long insertOrUpdateActivity(Activity activity)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID, activity.getId());
        values.put(KEY_SEGMENT_ID, activity.getSegmentId());
        values.put(KEY_USER_ID, activity.getUserId());
        values.put(KEY_PLACE_ID, activity.getPlaceId());
        values.put(KEY_TYPE, activity.getType());

        values.put(KEY_TITLE, activity.getTitle());
        values.put(KEY_SUB_TITLE, activity.getSubtitle());
        values.put(KEY_PRICE, activity.getPrice());
        values.put(KEY_LAT, activity.getLatitude());
        values.put(KEY_LONG,activity.getLongitude());

        values.put(KEY_DESC, activity.getDescription());
        values.put(KEY_TIME, activity.getTime());
        values.put(KEY_FACILITY, activity.getDescription());
        values.put(KEY_HIGHLIGHTS, activity.getHighlights());
        values.put(KEY_OPERATOR, activity.getOperator());

        values.put(KEY_CUISINE, activity.getCuisine());
        values.put(KEY_DURATION, activity.getDuration());
        values.put(KEY_RATINGS, activity.getRatings());
        values.put(KEY_IMAGE, activity.getImage());
        values.put(KEY_IMAGE_LOCAL, activity.getImageLocal());

        values.put(KEY_IMAGE_WIDE, activity.getImageWide());
        values.put(KEY_IMAGE_WIDE_LOCAL, activity.getImageWideLocal());




        // insert row
        return  db.insertWithOnConflict(TABLE_ACTIVITY, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateActivityAccessory(ActivityAccessory activityAccessory)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        //values.put(KEY_ID, activityAccessory.getId());
        values.put(KEY_ACTIVITY_ID, activityAccessory.getActivityId());
        values.put(KEY_TYPE, activityAccessory.getType());
        values.put(KEY_VALUE, activityAccessory.getValue());

        // insert row
        return  db.insertWithOnConflict(TABLE_ACTIVITY_ACCESSORY, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateFavorite(Favorite favorite)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        //values.put(KEY_ID, favorite.getId());
        values.put(KEY_ROUTE_ID, favorite.getRouteId());
        values.put(KEY_IS_COMMENTARY_PURCHASED, favorite.isCommentaryPurchased());

        // insert row
        return  db.insertWithOnConflict(TABLE_FAVORITE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateDownloaded(Downloaded downloaded)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ROUTE_ID, downloaded.getRouteId());

        // insert row
        return  db.insertWithOnConflict(TABLE_DOWNLOADED, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public long insertOrUpdateOpus(Opus opus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put(KEY_ID, opus.getId()); //Auto increment Primary Key
        values.put(KEY_ASSOCIATE_SEGMENT, opus.getAssociateSegment());
        values.put(KEY_START, opus.getStart());
        values.put(KEY_END, opus.getEnd());
        values.put(KEY_LAT, opus.getLatitude());
        values.put(KEY_LONG, opus.getLongitude());
        values.put(KEY_AUDIO, opus.getAudio());
        values.put(KEY_RADIUS, opus.getRadius());

        // insert row
        return  db.insertWithOnConflict(TABLE_OPUS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    //DELETE
    public void deleteRecord(String tableName, String colName, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tableName, colName + " = ?",
                new String[] { id });
    }

    /*public Opus fetchOpus(String latitude, String longitude) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(latitude)&&!TextUtils.isEmpty(longitude))
            cursor = db.query(TABLE_OPUS, null, KEY_LAT + " = " + latitude + " AND " + KEY_LONG + " = " +longitude, null, null, null, null);
        else
            cursor = db.query(TABLE_OPUS, null,null,null, null, null, null);

        Opus opus = null;
        if(cursor.moveToFirst()) {

            opus = new Opus();

            opus.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
            opus.setAssociateSegment(cursor.getString(cursor.getColumnIndex(KEY_ASSOCIATE_SEGMENT)));
            opus.setStart(cursor.getString(cursor.getColumnIndex(KEY_START)));
            opus.setEnd(cursor.getString(cursor.getColumnIndex(KEY_END)));
            opus.setLatitude(cursor.getString(cursor.getColumnIndex(KEY_LAT)));
            opus.setLongitude(cursor.getString(cursor.getColumnIndex(KEY_LONG)));
            opus.setAudio(cursor.getString(cursor.getColumnIndex(KEY_AUDIO)));
            opus.setRadius(cursor.getString(cursor.getColumnIndex(KEY_RADIUS)));

        }
        return opus;

    }*/

    public ArrayList<Opus> fetchOpus(String latitude, String longitude) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(latitude)&&!TextUtils.isEmpty(longitude))
            cursor = db.query(TABLE_OPUS, null, KEY_LAT + " = " + latitude + " AND " + KEY_LONG + " = " +longitude, null, null, null, null);
        else
            cursor = db.query(TABLE_OPUS, null,null,null, null, null, null);

        Opus opus = null;
        ArrayList<Opus> arrayList = new ArrayList<Opus>();
        if(cursor.moveToFirst()) {
            do {
                opus = new Opus();

                opus.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                opus.setAssociateSegment(cursor.getString(cursor.getColumnIndex(KEY_ASSOCIATE_SEGMENT)));
                opus.setStart(cursor.getString(cursor.getColumnIndex(KEY_START)));
                opus.setEnd(cursor.getString(cursor.getColumnIndex(KEY_END)));
                opus.setLatitude(cursor.getString(cursor.getColumnIndex(KEY_LAT)));
                opus.setLongitude(cursor.getString(cursor.getColumnIndex(KEY_LONG)));
                opus.setAudio(cursor.getString(cursor.getColumnIndex(KEY_AUDIO)));
                opus.setRadius(cursor.getString(cursor.getColumnIndex(KEY_RADIUS)));
                arrayList.add(opus);

            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<Opus> fetchOpus(String start) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(start))
            cursor = db.query(TABLE_OPUS, null, KEY_START + " = " +start, null, null, null, null);
        else
            cursor = db.query(TABLE_OPUS, null,null,null, null, null, null);

        Opus opus = null;
        ArrayList<Opus> arrayList = new ArrayList<Opus>();
        if(cursor.moveToFirst()) {
            do {
                opus = new Opus();

                opus.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                opus.setAssociateSegment(cursor.getString(cursor.getColumnIndex(KEY_ASSOCIATE_SEGMENT)));
                opus.setStart(cursor.getString(cursor.getColumnIndex(KEY_START)));
                opus.setEnd(cursor.getString(cursor.getColumnIndex(KEY_END)));
                opus.setLatitude(cursor.getString(cursor.getColumnIndex(KEY_LAT)));
                opus.setLongitude(cursor.getString(cursor.getColumnIndex(KEY_LONG)));
                opus.setAudio(cursor.getString(cursor.getColumnIndex(KEY_AUDIO)));
                opus.setRadius(cursor.getString(cursor.getColumnIndex(KEY_RADIUS)));
                arrayList.add(opus);

            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    //FETCH
    public ArrayList<Route> fetchRoutes(String routeId) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor routeCursor = null;

        if(!TextUtils.isEmpty(routeId))
            routeCursor = db.query(TABLE_ROUTE, null,  KEY_ID + " = ?", new String[]{routeId}, null, null, null);
        else
            routeCursor = db.query(TABLE_ROUTE, null,null,null, null, null, null);

        ArrayList<Route> routeArrayList = new ArrayList<Route>();
        if(routeCursor.moveToFirst()) {
            do {
                Route route = new Route();

                route.setId(routeCursor.getString(routeCursor.getColumnIndex(KEY_ID)));
                route.setName(routeCursor.getString(routeCursor.getColumnIndex(KEY_NAME)));
                route.setImage(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE)));
                route.setImageLocal(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE_LOCAL)));
                route.setImageMedium(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE_MEDIUM)));
                route.setImageMediumLocal(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE_MEDIUM_LOCAL)));
                route.setImageLarge(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE_LARGE)));
                route.setImageLargeLocal(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE_LARGE_LOCAL)));
                route.setImageWide(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE_WIDE)));
                route.setImageWideLocal(routeCursor.getString(routeCursor.getColumnIndex(KEY_IMAGE_WIDE_LOCAL)));
                route.setProductId(routeCursor.getString(routeCursor.getColumnIndex(KEY_PROD_ID)));
                route.setStart(routeCursor.getString(routeCursor.getColumnIndex(KEY_START)));
                route.setEnd(routeCursor.getString(routeCursor.getColumnIndex(KEY_END)));
                route.setMinDay(routeCursor.getString(routeCursor.getColumnIndex(KEY_MIN_DAY)));
                route.setMaxDay(routeCursor.getString(routeCursor.getColumnIndex(KEY_MAX_DAY)));
                route.setDistance(routeCursor.getString(routeCursor.getColumnIndex(KEY_DISTANCE)));
                route.setHighlights(routeCursor.getString(routeCursor.getColumnIndex(KEY_HIGHLIGHTS)));
                route.setLastModified(routeCursor.getString(routeCursor.getColumnIndex(KEY_LAST_MODIFIED)));
                route.setDescription(routeCursor.getString(routeCursor.getColumnIndex(KEY_DESC)));

                route.setStartName(fetchSegmentName(routeCursor.getString(routeCursor.getColumnIndex(KEY_START))));
                route.setEndName(fetchSegmentName(routeCursor.getString(routeCursor.getColumnIndex(KEY_END))));

                routeArrayList.add(route);
            }while (routeCursor.moveToNext());
        }
        return routeArrayList;
    }
    public String fetchSegmentName(String segmentID) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(segmentID))
            cursor = db.query(TABLE_SEGMENT, null, KEY_ID + " = ?", new String[]{segmentID}, null, null, null);
        else
            cursor = db.query(TABLE_SEGMENT, null,null,null, null, null, null);

        if(cursor.moveToFirst())
            return cursor.getString(cursor.getColumnIndex(KEY_NAME));
        else
            return null;

    }
    public ArrayList<Segment> fetchPlaceSegment() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        cursor = db.query(TABLE_SEGMENT, null, KEY_IS_PLACE + " = ?", new String[]{"TRUE"}, null, null, null);

        ArrayList<Segment> arrayList = new ArrayList<Segment>();
        if(cursor.moveToFirst()) {
            do {
                Segment segment = new Segment();

                segment.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                segment.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                segment.setPlace(cursor.getString(cursor.getColumnIndex(KEY_IS_PLACE)));
                segment.setIslandType(cursor.getString(cursor.getColumnIndex(KEY_ISLAND_TYPE)));
                segment.setLatitude(cursor.getString(cursor.getColumnIndex(KEY_LAT)));
                segment.setLongitude(cursor.getString(cursor.getColumnIndex(KEY_LONG)));
                segment.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESC)));
                segment.setAudio(cursor.getString(cursor.getColumnIndex(KEY_AUDIO)));
                segment.setAudioLocal(cursor.getString(cursor.getColumnIndex(KEY_AUDIO_LOCAL)));
                segment.setCoordinates(cursor.getString(cursor.getColumnIndex(KEY_COORDINATES)));
                segment.setMbTiles(cursor.getString(cursor.getColumnIndex(KEY_MB_TILES)));
                segment.setMbTilesLocal(cursor.getString(cursor.getColumnIndex(KEY_MB_TILES_LOCAL)));
                segment.setLastModified(cursor.getString(cursor.getColumnIndex(KEY_LAST_MODIFIED)));
                segment.setImage(cursor.getString(cursor.getColumnIndex(KEY_IMAGE)));
                segment.setImageLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_LOCAL)));

                arrayList.add(segment);
            }while (cursor.moveToNext());
        }
        return arrayList;
    }
    public ArrayList<Segment> fetchSegment(String segmentID) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(segmentID))
            cursor = db.query(TABLE_SEGMENT, null, KEY_ID + " = ?", new String[]{segmentID}, null, null, null);
        else
            cursor = db.query(TABLE_SEGMENT, null,null,null, null, null, null);

        ArrayList<Segment> arrayList = new ArrayList<Segment>();
        if(cursor.moveToFirst()) {
            do {
                Segment segment = new Segment();

                segment.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                segment.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                segment.setPlace(cursor.getString(cursor.getColumnIndex(KEY_IS_PLACE)));
                segment.setIslandType(cursor.getString(cursor.getColumnIndex(KEY_ISLAND_TYPE)));
                segment.setLatitude(cursor.getString(cursor.getColumnIndex(KEY_LAT)));
                segment.setLongitude(cursor.getString(cursor.getColumnIndex(KEY_LONG)));
                segment.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESC)));
                segment.setAudio(cursor.getString(cursor.getColumnIndex(KEY_AUDIO)));
                segment.setAudioLocal(cursor.getString(cursor.getColumnIndex(KEY_AUDIO_LOCAL)));
                segment.setCoordinates(cursor.getString(cursor.getColumnIndex(KEY_COORDINATES)));
                segment.setMbTiles(cursor.getString(cursor.getColumnIndex(KEY_MB_TILES)));
                segment.setMbTilesLocal(cursor.getString(cursor.getColumnIndex(KEY_MB_TILES_LOCAL)));
                segment.setLastModified(cursor.getString(cursor.getColumnIndex(KEY_LAST_MODIFIED)));
                segment.setImage(cursor.getString(cursor.getColumnIndex(KEY_IMAGE)));
                segment.setImageLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_LOCAL)));

                arrayList.add(segment);
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<SegmentEntry> fetchSegmentEntry(String routeID) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(routeID))
            cursor = db.query(TABLE_SEGMENT_ENTRY, null,  KEY_ROUTE_ID + " = ?", new String[]{routeID}, null, null, null);
        else
            cursor = db.query(TABLE_SEGMENT_ENTRY, null,null,null, null, null, null);

        ArrayList<SegmentEntry> arrayList = new ArrayList<SegmentEntry>();
        if(cursor.moveToFirst()) {
            do {
                SegmentEntry segmentEntry = new SegmentEntry();

                segmentEntry.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                segmentEntry.setRouteId(cursor.getString(cursor.getColumnIndex(KEY_ROUTE_ID)));
                segmentEntry.setSegmentId(cursor.getString(cursor.getColumnIndex(KEY_SEGMENT_ID)));
                segmentEntry.setDirection(cursor.getString(cursor.getColumnIndex(KEY_DIRECTION)));
                segmentEntry.setOrder(cursor.getString(cursor.getColumnIndex(KEY_ORDER)));
                segmentEntry.setLastModified(cursor.getString(cursor.getColumnIndex(KEY_LAST_MODIFIED)));

                arrayList.add(segmentEntry);
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<String> fetchDownloaded(String routeID) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(routeID))
            cursor = db.query(TABLE_DOWNLOADED, null,  KEY_ROUTE_ID + " = ?", new String[]{routeID}, null, null, null);
        else
            cursor = db.query(TABLE_DOWNLOADED, null,null,null, null, null, null);

        ArrayList<String> arrayList = new ArrayList<String>();
        if(cursor.moveToFirst()) {
            do {
                arrayList.add(cursor.getString(cursor.getColumnIndex(KEY_ROUTE_ID)));
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<Tours> fetchTours(String routeID) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(routeID))
            cursor = db.query(TABLE_TOURS, null, KEY_ROUTE_ID + " = ?", new String[]{routeID}, null, null, null);
        else
            cursor = db.query(TABLE_TOURS, null,null,null, null, null, null);

        ArrayList<Tours> arrayList = new ArrayList<Tours>();
        if(cursor.moveToFirst()) {
            do {
                Tours tours = new Tours();

                tours.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                tours.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                tours.setImage(cursor.getString(cursor.getColumnIndex(KEY_IMAGE)));
                tours.setImageLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_LOCAL)));
                tours.setDuration(cursor.getString(cursor.getColumnIndex(KEY_DURATION)));
                tours.setCommentary(cursor.getString(cursor.getColumnIndex(KEY_COMMENTARY)));
                tours.setDistance(cursor.getString(cursor.getColumnIndex(KEY_DISTANCE)));
                tours.setHighlights(cursor.getString(cursor.getColumnIndex(KEY_HIGHLIGHTS)));
                tours.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESC)));
                tours.setRouteId(cursor.getString(cursor.getColumnIndex(KEY_ROUTE_ID)));
                tours.setStart(cursor.getString(cursor.getColumnIndex(KEY_START)));
                tours.setEnd(cursor.getString(cursor.getColumnIndex(KEY_END)));
                tours.setImageWide(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_WIDE)));
                tours.setImageWideLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_WIDE_LOCAL)));

                tours.setStartName(fetchSegmentName(cursor.getString(cursor.getColumnIndex(KEY_START))));
                tours.setEndName(fetchSegmentName(cursor.getString(cursor.getColumnIndex(KEY_END))));

                arrayList.add(tours);
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<Stops> fetchStops(String segmentId) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(segmentId))
            cursor = db.query(TABLE_STOPS, null, KEY_SEGMENT_ID + " = ?", new String[]{segmentId}, null, null, null);
        else
            cursor = db.query(TABLE_STOPS, null,null,null, null, null, null);

        ArrayList<Stops> arrayList = new ArrayList<Stops>();
        if(cursor.moveToFirst()) {
            do {
                Stops stops = new Stops();

                stops.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                stops.setSegmentId(cursor.getString(cursor.getColumnIndex(KEY_SEGMENT_ID)));
                stops.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                stops.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                stops.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                stops.setTimeNeeded(cursor.getString(cursor.getColumnIndex(KEY_TIME_NEEDED)));
                stops.setHighlights(cursor.getString(cursor.getColumnIndex(KEY_HIGHLIGHTS)));
                stops.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESC)));
                stops.setImage(cursor.getString(cursor.getColumnIndex(KEY_IMAGE)));
                stops.setImageLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_LOCAL)));
                stops.setImageWide(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_WIDE)));
                stops.setImageWideLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_WIDE_LOCAL)));

                arrayList.add(stops);
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<Activity> fetchActivity(String segmentId) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(segmentId))
            cursor = db.query(TABLE_ACTIVITY, null,  KEY_SEGMENT_ID + " = " +segmentId, null, null, null, null);
        else
            cursor = db.query(TABLE_ACTIVITY, null,null,null, null, null, null);

        ArrayList<Activity> arrayList = new ArrayList<Activity>();
        if(cursor.moveToFirst()) {
            do {
                Activity activity = new Activity();

                activity.setId("1");//activity.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));//TODO: mani
                activity.setSegmentId(""+cursor.getInt(cursor.getColumnIndex(KEY_SEGMENT_ID)));
                activity.setUserId(""+cursor.getInt(cursor.getColumnIndex(KEY_USER_ID)));
                activity.setPlaceId(""+cursor.getInt(cursor.getColumnIndex(KEY_PLACE_ID)));
                activity.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));

                activity.setTitle(cursor.getString(cursor.getColumnIndex(KEY_TITLE)));
                activity.setSubtitle(cursor.getString(cursor.getColumnIndex(KEY_SUB_TITLE)));
                activity.setPrice(cursor.getString(cursor.getColumnIndex(KEY_PRICE)));
                activity.setTime(cursor.getString(cursor.getColumnIndex(KEY_TIME)));
                activity.setLatitude(cursor.getString(cursor.getColumnIndex(KEY_LAT)));

                activity.setLongitude(cursor.getString(cursor.getColumnIndex(KEY_LONG)));
                activity.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESC)));
                activity.setFacility(cursor.getString(cursor.getColumnIndex(KEY_FACILITY)));
                activity.setHighlights(cursor.getString(cursor.getColumnIndex(KEY_HIGHLIGHTS)));
                activity.setOperator(cursor.getString(cursor.getColumnIndex(KEY_OPERATOR)));

                activity.setCuisine(cursor.getString(cursor.getColumnIndex(KEY_CUISINE)));
                activity.setDuration(cursor.getString(cursor.getColumnIndex(KEY_DURATION)));
                activity.setRatings(cursor.getString(cursor.getColumnIndex(KEY_RATINGS)));
                activity.setImage(cursor.getString(cursor.getColumnIndex(KEY_IMAGE)));
                activity.setImageLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_LOCAL)));

                activity.setImageWide(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_WIDE)));
                activity.setImageWideLocal(cursor.getString(cursor.getColumnIndex(KEY_IMAGE_WIDE_LOCAL)));


                arrayList.add(activity);
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ActivityCount fetchActivityCount(String segmentId) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(segmentId))
            cursor = db.query(TABLE_ACTIVITY, null,KEY_SEGMENT_ID+" = "+segmentId, null , null, null, null);
        else
            cursor = db.query(TABLE_ACTIVITY, null,null,null, null, null, null);

        int n1 = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;

        if(cursor.moveToFirst()) {
            do {

                if(cursor.getString(cursor.getColumnIndex(KEY_TYPE)).equals("Paid Activity")){
                    n1++;
                }
                else  if(cursor.getString(cursor.getColumnIndex(KEY_TYPE)).equals("Free Activity")){
                    n2++;
                }
                else  if(cursor.getString(cursor.getColumnIndex(KEY_TYPE)).equals("Food & Drink")){
                    n3++;
                }
                else  if(cursor.getString(cursor.getColumnIndex(KEY_TYPE)).equals("Accommodation")){
                    n4++;
                }
                else  if(cursor.getString(cursor.getColumnIndex(KEY_TYPE)).equals("Other")){
                    n5++;
                }
        }while (cursor.moveToNext());
        }
        return new ActivityCount(n1,n2,n3,n4,n5);
    }

    public ArrayList<ActivityAccessory> fetchActivityAccessory(String activityID) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(activityID))
            cursor = db.query(TABLE_ACTIVITY_ACCESSORY, null, KEY_ACTIVITY_ID + " = ?", new String[]{activityID}, null, null, null);
        else
            cursor = db.query(TABLE_ACTIVITY_ACCESSORY, null,null,null, null, null, null);

        ArrayList<ActivityAccessory> arrayList = new ArrayList<ActivityAccessory>();
        if(cursor.moveToFirst()) {
            do {
                ActivityAccessory activityAccessory = new ActivityAccessory();

                activityAccessory.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                activityAccessory.setActivityId(cursor.getString(cursor.getColumnIndex(KEY_ACTIVITY_ID)));
                activityAccessory.setType(cursor.getString(cursor.getColumnIndex(KEY_TYPE)));
                activityAccessory.setValue(cursor.getString(cursor.getColumnIndex(KEY_VALUE)));

                arrayList.add(activityAccessory);
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public Favorite fetchFavorite(String routeID) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;

        if(!TextUtils.isEmpty(routeID))
            cursor = db.query(TABLE_FAVORITE, null, KEY_ROUTE_ID + " = ?", new String[]{routeID}, null, null, null);
        else
            cursor = db.query(TABLE_FAVORITE, null,null,null, null, null, null);

        Favorite favorite = null;
        if(cursor.moveToFirst()) {
            //do {
                favorite = new Favorite();

                favorite.setId(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                favorite.setRouteId(cursor.getString(cursor.getColumnIndex(KEY_ROUTE_ID)));
                favorite.setCommentaryPurchased(cursor.getString(cursor.getColumnIndex(KEY_IS_COMMENTARY_PURCHASED)));

            //}while (cursor.moveToNext());
        }
        return favorite;
    }

    // Database Version
    private static final int DATABASE_VERSION = 1;


    // Database Name
    private static final String DATABASE_NAME = "smartToursDb";

    // Table Names
    public static final String TABLE_SEGMENT = "segment";
    public static final String TABLE_ROUTE = "route";
    public static final String TABLE_SEGMENT_ENTRY = "segment_entry";
    public static final String TABLE_TOURS = "tours";
    public static final String TABLE_STOPS = "stops";
    public static final String TABLE_ACTIVITY = "activity";
    public static final String TABLE_ACTIVITY_ACCESSORY = "activity_accessory";
    public static final String TABLE_FAVORITE = "favorite";
    public static final String TABLE_DOWNLOADED = "downloaded";
    public static final String TABLE_OPUS = "opus";




    // TABLE_SEGMENT - column names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_IS_PLACE = "is_place";
    private static final String KEY_ISLAND_TYPE = "island_type";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LONG = "longitude";
    private static final String KEY_DESC = "description";
    private static final String KEY_AUDIO = "audio";
    private static final String KEY_AUDIO_LOCAL = "audio_local";
    private static final String KEY_COORDINATES = "coordinates";
    private static final String KEY_MB_TILES = "mb_tiles";
    private static final String KEY_MB_TILES_LOCAL = "mb_tiles_local";
    private static final String KEY_LAST_MODIFIED = "last_modified";



    // TABLE_ROUTE - column names
    private static final String KEY_PROD_ID = "product_id";
    private static final String KEY_START = "start";
    private static final String KEY_END = "end";
    private static final String KEY_MIN_DAY = "minDays";
    private static final String KEY_MAX_DAY = "maxDays";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_DISTANCE = "distance";
    private static final String KEY_HIGHLIGHTS = "highlights";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_IMAGE_LOCAL = "image_local";
    private static final String KEY_IMAGE_MEDIUM = "image_medium";
    private static final String KEY_IMAGE_MEDIUM_LOCAL = "image_medium_local";
    private static final String KEY_IMAGE_LARGE = "image_large";
    private static final String KEY_IMAGE_LARGE_LOCAL = "image_large_local";
    private static final String KEY_IMAGE_WIDE = "image_wide";
    private static final String KEY_IMAGE_WIDE_LOCAL = "image_wide_local";


    // TABLE_SEGMENT_ENTRY - column names
    public static final String KEY_ROUTE_ID = "route_id";
    private static final String KEY_SEGMENT_ID = "segment_id";
    private static final String KEY_DIRECTION = "direction";
    private static final String KEY_ORDER = "direction_order";

    // TABLE_TOURS - column names
    private static final String KEY_COMMENTARY = "commentary";

    // TABLE_STOPS - column names
    private static final String KEY_TYPE = "type";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_TIME_NEEDED = "time_needed";

    //TABLE_ACTIVITY - column names
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_PLACE_ID = "place_id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_SUB_TITLE = "subtitle";
    private static final String KEY_TIME = "time";
    private static final String KEY_PRICE = "price";
    private static final String KEY_FACILITY = "facility";
    private static final String KEY_OPERATOR = "operator";
    private static final String KEY_RATINGS = "ratings";
    private static final String KEY_CUISINE = "cuisine";

    //TABLE_ACTIVITY_ACCESSORY - column names
    private static final String KEY_ACTIVITY_ID = "activity_id";
    private static final String KEY_VALUE = "value";

    //TABLE_FAVORITE - column names
    private static final String KEY_IS_COMMENTARY_PURCHASED= "is_commentary_purchased";

    // TABLE_OPUS - column names
    private static final String KEY_ASSOCIATE_SEGMENT = "associate_segment";
    private static final String KEY_RADIUS = "radius";

    private static final String CREATE_TABLE_OPUS = "CREATE TABLE "
            + TABLE_OPUS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_ASSOCIATE_SEGMENT + " TEXT,"
            + KEY_AUDIO + " TEXT,"
            + KEY_START+ " TEXT,"
            + KEY_END+ " TEXT,"
            + KEY_LAT + " TEXT,"
            + KEY_LONG + " TEXT,"
            + KEY_RADIUS + " TEXT"
            + ")";

    private static final String CREATE_TABLE_SEGMENT = "CREATE TABLE "
            + TABLE_SEGMENT + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_IS_PLACE+ " TEXT,"
            + KEY_ISLAND_TYPE+ " TEXT,"
            + KEY_LAT + " TEXT,"
            + KEY_LONG + " TEXT,"
            + KEY_DESC + " TEXT,"
            + KEY_AUDIO + " TEXT,"
            + KEY_AUDIO_LOCAL + " TEXT,"
            + KEY_COORDINATES + " TEXT,"
            + KEY_MB_TILES+ " TEXT,"
            + KEY_MB_TILES_LOCAL+ " TEXT,"
            + KEY_IMAGE + " TEXT,"
            + KEY_IMAGE_LOCAL + " TEXT,"
            + KEY_LAST_MODIFIED + " TEXT"
            + ")";

    private static final String CREATE_TABLE_ROUTE = "CREATE TABLE "
            + TABLE_ROUTE + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_PROD_ID + " TEXT,"
            + KEY_START + " TEXT,"
            + KEY_END + " TEXT,"
            + KEY_MIN_DAY + " TEXT,"
            + KEY_MAX_DAY + " TEXT,"
            + KEY_DISTANCE + " TEXT,"
            + KEY_HIGHLIGHTS + " TEXT,"
            + KEY_LAST_MODIFIED + " TEXT,"
            + KEY_DESC + " TEXT,"
            + KEY_IMAGE + " TEXT,"
            + KEY_IMAGE_LOCAL + " TEXT,"
            + KEY_IMAGE_MEDIUM + " TEXT,"
            + KEY_IMAGE_MEDIUM_LOCAL + " TEXT,"
            + KEY_IMAGE_LARGE + " TEXT,"
            + KEY_IMAGE_LARGE_LOCAL + " TEXT,"
            + KEY_IMAGE_WIDE + " TEXT,"
            + KEY_IMAGE_WIDE_LOCAL + " TEXT"
            + ")";

    private static final String CREATE_TABLE_SEGMENT_ENTRY = "CREATE TABLE "
            + TABLE_SEGMENT_ENTRY + "("
            + KEY_ID + " INTEGER,"
            + KEY_ROUTE_ID + " TEXT,"
            + KEY_SEGMENT_ID + " INTEGER PRIMARY KEY,"
            + KEY_DIRECTION + " TEXT,"
            + KEY_ORDER + " TEXT,"
            + KEY_LAST_MODIFIED + " TEXT"
            + ")";

    private static final String CREATE_TABLE_TOURS = "CREATE TABLE "
            + TABLE_TOURS + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_DURATION + " TEXT,"
            + KEY_COMMENTARY+ " TEXT,"
            + KEY_DISTANCE + " TEXT,"
            + KEY_HIGHLIGHTS + " TEXT,"
            + KEY_DESC + " TEXT,"
            + KEY_ROUTE_ID + " INTEGER,"
            + KEY_START + " TEXT,"
            + KEY_END + " TEXT,"
            + KEY_IMAGE_WIDE + " TEXT,"
            + KEY_IMAGE_WIDE_LOCAL + " TEXT,"
            + KEY_LAST_MODIFIED + " TEXT,"
            + KEY_IMAGE + " TEXT,"
            + KEY_IMAGE_LOCAL + " TEXT"
            + ")";

    private static final String CREATE_TABLE_STOPS = "CREATE TABLE "
            + TABLE_STOPS + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_SEGMENT_ID + " TEXT,"
            + KEY_NAME + " TEXT,"
            + KEY_TYPE + " TEXT,"
            + KEY_ADDRESS + " TEXT,"
            + KEY_TIME_NEEDED + " TEXT,"
            + KEY_HIGHLIGHTS + " TEXT,"
            + KEY_DESC + " TEXT,"
            + KEY_IMAGE_WIDE + " TEXT,"
            + KEY_IMAGE_WIDE_LOCAL + " TEXT,"
            + KEY_LAST_MODIFIED + " TEXT,"
            + KEY_IMAGE + " TEXT,"
            + KEY_IMAGE_LOCAL + " TEXT"
            + ")";

    private static final String CREATE_TABLE_ACTIVITY = "CREATE TABLE "
            + TABLE_ACTIVITY + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_SEGMENT_ID + " TEXT,"
            + KEY_USER_ID + " TEXT,"
            + KEY_PLACE_ID + " TEXT,"
            + KEY_TYPE + " TEXT,"
            + KEY_TITLE + " TEXT,"
            + KEY_SUB_TITLE + " TEXT,"
            + KEY_LAT + " TEXT,"
            + KEY_LONG + " TEXT,"
            + KEY_TIME + " TEXT,"
            + KEY_PRICE + " TEXT,"
            + KEY_RATINGS + " TEXT,"
            + KEY_DURATION + " TEXT,"
            + KEY_OPERATOR + " TEXT,"
            + KEY_CUISINE + " TEXT,"
            + KEY_FACILITY + " TEXT,"
            + KEY_HIGHLIGHTS + " TEXT,"
            + KEY_DESC + " TEXT,"
            + KEY_IMAGE_WIDE + " TEXT,"
            + KEY_IMAGE_WIDE_LOCAL + " TEXT,"
            + KEY_LAST_MODIFIED + " TEXT,"
            + KEY_IMAGE + " TEXT,"
            + KEY_IMAGE_LOCAL + " TEXT"
            + ")";

    private static final String CREATE_TABLE_ACTIVITY_ACCESSORY = "CREATE TABLE "
            + TABLE_ACTIVITY_ACCESSORY + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_ACTIVITY_ID + " TEXT,"
            + KEY_TYPE + " TEXT,"
            + KEY_VALUE + " TEXT"
            + ")";

    private static final String CREATE_TABLE_FAVORITE = "CREATE TABLE "
            + TABLE_FAVORITE + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_ROUTE_ID + " TEXT,"
            + KEY_IS_COMMENTARY_PURCHASED + " TEXT"
            + ")";


    private static final String CREATE_TABLE_DOWNLOADED = "CREATE TABLE "
            + TABLE_DOWNLOADED + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_ROUTE_ID + " TEXT"
            + ")";
}
