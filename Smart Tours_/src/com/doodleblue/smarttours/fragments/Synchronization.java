package com.doodleblue.smarttours.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.JSearchRoutes;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.GetFont;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class Synchronization extends BaseContainerFragment implements View.OnClickListener{



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.sync, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        Button syncButton = (Button) mRootView.findViewById(R.id.sync);
        syncButton.setTypeface(GetFont.getInstance(getActivity()).getFont(GetFont.HELVITICA_TT));
        syncButton.setOnClickListener(this);


    }

    class SyncTask extends AsyncTask<String,String,Object> {

        private Context mContext;

        public SyncTask(Context context){
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(String... params) {

            //Sync Route
            for(Route route: DatabaseHelper.getInstance(mActivity).fetchRoutes(null)){
                String url = AppConstants.SYNC_URL;
                try {
                    url = url.replace("value1", URLEncoder.encode("", "UTF-8")); //segment_id
                    url = url.replace("value2",URLEncoder.encode("", "UTF-8")); //activity_id
                    url = url.replace("value3",URLEncoder.encode("", "UTF-8")); //stop_id
                    url = url.replace("value4",URLEncoder.encode(route.getId(), "UTF-8")); //route_id
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                InputStream inputStream = retrieveStream(url);
                Gson gson = new Gson();
                Reader reader = new InputStreamReader(inputStream);
                JSearchRoutes response = gson.fromJson(reader, JSearchRoutes.class);




            }


            return null;
        }

        private InputStream retrieveStream(String url) {
            Log.i("retrieveStream", url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet(url);
            try {
                HttpResponse getResponse = client.execute(getRequest);
                final int statusCode = getResponse.getStatusLine().getStatusCode();
                if (statusCode != HttpStatus.SC_OK) {
                    Log.w(getClass().getSimpleName(),
                            "Error " + statusCode + " for URL " + url);
                    return null;
                }
                HttpEntity getResponseEntity = getResponse.getEntity();
                return getResponseEntity.getContent();
            }
            catch (IOException e) {
                getRequest.abort();
                Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sync:
                SyncTask syncTask = new SyncTask(mActivity);
                syncTask.execute();
                break;
        }
    }

    /*private void replaceFragment() {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new RouteDetail(), true);
    }*/
}
