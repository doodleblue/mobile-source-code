package com.doodleblue.smarttours.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.util.BaseContainerFragment;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class About extends BaseContainerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.about, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        ((TextView)mActivity.findViewById(R.id.header_text)).setText("Settings");
        ((TextView)mRootView.findViewById(R.id.about_text)).setText(
                mActivity.getResources().getString(R.string.lorem_ipsum)+" \n\n"+
                        mActivity.getResources().getString(R.string.lorem_ipsum_more));
    }

    /*private void replaceFragment() {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new RouteDetail(), true);
    }*/
}
