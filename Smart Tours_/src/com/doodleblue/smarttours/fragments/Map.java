package com.doodleblue.smarttours.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.capricorn.ArcMenu;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.Opus;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.PlayerUtils;
import com.doodleblue.smarttours.view.POIInfoWindow;
import com.doodleblue.smarttours.view.SmartMapView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.bonuspack.overlays.ExtendedOverlayItem;
import org.osmdroid.bonuspack.overlays.ItemizedOverlayWithBubble;
import org.osmdroid.bonuspack.routing.GoogleRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 7:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class Map extends BaseContainerFragment implements
        MediaPlayer.OnCompletionListener,
        SeekBar.OnSeekBarChangeListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationListener,
        View.OnClickListener
{
    private ArrayList<Opus> mOpuses = new ArrayList<Opus>();
    private ArrayList<Opus> mRelevantOpuses = new ArrayList<Opus>();
    private MediaPlayer mMediaPlayer;
    private SeekBar mSeekBar;
    private Handler mHandler;
    private ImageButton mPlayButton;
    private int mSeekForwardTime = 5000; // 5000 milliseconds
    private int mSeekBackwardTime = 5000; // 5000 milliseconds
    public static ArrayList<POI> mPOIs;
    private ArrayList<OverlayItem> mOverlayItemArray;
    private PlayerState mPlayerState = PlayerState.UNINITIALIZED;

    public enum PlayerState {
        INITIALIZED,
        UNINITIALIZED,
        PAUSED,
        PLAYING,
        STOPPED
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.map, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initAudioView();
    }

    private void initView() {

        mLocationClient = new LocationClient(mActivity,this,this);

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();

        // Use high accuracy
        mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        mLocationClient.connect();

        ArcMenu activitiesFilter = (ArcMenu) mRootView.findViewById(R.id.activity_filter);
        ArcMenu stopsFilter = (ArcMenu) mRootView.findViewById(R.id.stop_filter);

        activitiesFilter.setCenterImage(R.drawable.map_info_btn);
        stopsFilter.setCenterImage(R.drawable.map_stop_btn);

        initArcMenu(activitiesFilter, ACTIVITY_DRAWABLES);
        initArcMenu(stopsFilter, STOP_DRAWABLES);

        osmMap = (SmartMapView) mRootView.findViewById(R.id.mapview);
        osmMap.getController().setCenter(new GeoPoint(-45.02876,168.65623));
        osmMap.getController().setZoom(AppConstants.DEFAULT_ZOOM_LEVEL);
        osmMap.setBuiltInZoomControls(true);

        final ArrayList<ExtendedOverlayItem> poiItems = new ArrayList<ExtendedOverlayItem>();
        poiMarkers = new ItemizedOverlayWithBubble<ExtendedOverlayItem>(mActivity,
                poiItems, osmMap, new POIInfoWindow(osmMap));

        final ArrayList<ExtendedOverlayItem> poiItemsTemp = new ArrayList<ExtendedOverlayItem>();
        places = new ItemizedOverlayWithBubble<ExtendedOverlayItem>(mActivity,
                poiItemsTemp, osmMap, new POIInfoWindow(osmMap));

        osmMap.getOverlays().add(poiMarkers);
        getRoadAsync();

        mOverlayItemArray = new ArrayList<OverlayItem>();
        DefaultResourceProxyImpl defaultResourceProxyImpl
                = new DefaultResourceProxyImpl(mActivity);
        MyItemizedIconOverlay myItemizedIconOverlay
                = new MyItemizedIconOverlay(
                mOverlayItemArray, null, defaultResourceProxyImpl);
        osmMap.getOverlays().add(myItemizedIconOverlay);

        /*osmMap.setOnZoomChangedListener(new SmartMapView.onZoomChangedListener() {
            @Override
            public void onZoomChanged(ArrayList<POI> arrayList, int type) {
                Log.i("SmartMapView","onZoomChanged");
                if(mPOIs!=null){
                    updateUIWithPOI(mPOIs, 1);
                }
            }
        });*/

        FetchRecordTask task = new FetchRecordTask();
        task.execute();
    }

    public class FetchRecordTask extends AsyncTask<String,String,ArrayList<Opus>>{

        @Override
        protected ArrayList<Opus> doInBackground(String... params) {
            mOpuses = DatabaseHelper.getInstance(mActivity).fetchOpus(null,null);
            return mOpuses;
        }
        @Override
        protected void onPostExecute(ArrayList<Opus> opuses) {
            mRelevantOpuses = opuses;
        }
    }

    public class FetchRelevantTask extends AsyncTask<String,String,ArrayList<Opus>>{

        @Override
        protected ArrayList<Opus> doInBackground(String... params) {
            ArrayList<Opus> opuses = DatabaseHelper.getInstance(mActivity).fetchOpus(params[0]);//ToDo: incomplete code
            return opuses;
        }

        @Override
        protected void onPostExecute(ArrayList<Opus> opuses) {
            mRelevantOpuses = opuses;
        }
    }

    private void replaceFragment() {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Tab1AddOnFragment(), true);
    }

    private void initArcMenu(ArcMenu menu, final int[] itemDrawables) {
        final int itemCount = itemDrawables.length;
        for (int i = 0; i < itemCount; i++) {
            ImageView item = new ImageView(mActivity);
            item.setImageResource(itemDrawables[i]);
            item.setTag(itemDrawables[i]);

            final int position = i;
            menu.addItem(item, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //Toast.makeText(mActivity, "position:" + position, Toast.LENGTH_SHORT).show();
                    getPOIAsync(itemDrawables[position]);
                }
            });
        }
    }

    private static final int[] ACTIVITY_DRAWABLES = { R.drawable.map_paidbtn, R.drawable.map_attractbtn,
            R.drawable.map_foodbtn, R.drawable.map_accomodbtn, R.drawable.map_otherbtn };

    private static final int[] STOP_DRAWABLES = { R.drawable.map_lookoutbtn, R.drawable.map_picnicbtn,
            R.drawable.map_walkbtn, R.drawable.map_toiletbtn };


    public static ItemizedOverlayWithBubble<ExtendedOverlayItem> poiMarkers;
    ItemizedOverlayWithBubble<ExtendedOverlayItem> places;

    void getPOIAsync(int tag){
        poiMarkers.removeAllItems();
        new POITask().execute(tag);
    }

    private class POITask extends AsyncTask<Object, Void, ArrayList<POI>> {
        Integer mTag;
        protected ArrayList<POI> doInBackground(Object... params) {
            mTag = (Integer)params[0];

            if (mTag==null){
                return null;
            } else
            {

                //TODO: sample code starts here
                /*GeoNamesPOIProvider poiProvider = new GeoNamesPOIProvider("mkergall");
                //ArrayList<POI> pois = poiProvider.getPOICloseTo(point, 30, 20.0);
                //Get POI inside the bounding box of the current map view:
                BoundingBoxE6 bb = osmMap.getBoundingBox();
                ArrayList<POI> pois = poiProvider.getPOIInside(bb, 30);*/

                /*for (int i=0; i<1; i++){
                    POI poi = new POI(POI.POI_SERVICE_GEONAMES_WIKIPEDIA);
                    poi.mLocation = new GeoPoint(13,80);
                    poi.mCategory = "Sample Category";
                    poi.mType = "Sample Type";
                    poi.mDescription = "Sample Summary";
                    poi.mThumbnailPath = "Sample thumbnail";
				*//* This makes loading too long.
				 * Thumbnail loading will be done only when needed, with POI.getThumbnail()
				if (poi.mThumbnailPath != null){
					poi.mThumbnail = BonusPackHelper.loadBitmap(poi.mThumbnailPath);
				}
				*//*
                    poi.mUrl = "Sample URL";
                    if (poi.mUrl != null)
                        poi.mUrl = "http://" + poi.mUrl;
                    poi.mRank = 0;
                    //other attributes: distance?
                    pois.add(poi);
                }*/
                //TODO: code ends here

                ArrayList<POI> pois = new ArrayList<POI>();
                ArrayList<com.doodleblue.smarttours.model.Activity> activityList
                        = DatabaseHelper.getInstance(mActivity).fetchActivity(null); //TODO: get all activities here...
                for (com.doodleblue.smarttours.model.Activity activity : activityList){
                    POI poi = null;
                    switch (mTag)
                    {
                        case R.drawable.map_paidbtn:
                        {
                            poi = new POI(POI.PAID_ACTIVITIES);
                            break;
                        }
                        case R.drawable.map_attractbtn:
                        {
                            poi = new POI(POI.ATTRACTIONS);
                            break;
                        }
                        case R.drawable.map_foodbtn:
                        {
                            poi = new POI(POI.FOOD_AND_DRINK);
                            break;
                        }
                        case R.drawable.map_accomodbtn:
                        {
                            poi = new POI(POI.ACCOMODATIONS);
                            break;
                        }
                        case R.drawable.map_lookoutbtn:
                        {
                            poi = new POI(POI.LOOK_OUT);
                            break;
                        }
                        case R.drawable.map_picnicbtn:
                        {
                            poi = new POI(POI.PICNIC);
                            break;
                        }
                        case R.drawable.map_walkbtn:
                        {
                            poi = new POI(POI.WALKS);
                            break;
                        }
                        case R.drawable.toilet_select:
                        {
                            poi = new POI(POI.TOILET);
                            break;
                        }

                    }


                    if(poi!=null)
                    {
                        poi.mLocation = new GeoPoint(
                                Double.parseDouble(activity.getLatitude()),
                                Double.parseDouble(activity.getLongitude())
                                    ); //(45.02876,168.65623);
                        poi.mCategory = "Sample Category";
                        poi.mType = activity.getType(); //"Sample Type";
                        poi.mDescription = activity.getDescription();  //"Sample Summary";
                        poi.mThumbnailPath =  activity.getImageLocal(); //"Sample thumbnail";
				/* This makes loading too long.
				 * Thumbnail loading will be done only when needed, with POI.getThumbnail()
				if (poi.mThumbnailPath != null){
					poi.mThumbnail = BonusPackHelper.loadBitmap(poi.mThumbnailPath);
				}
				*/
                        pois.add(poi);
                    }
                }
                return pois;

            }
        }
        protected void onPostExecute(ArrayList<POI> pois) {
            mPOIs = pois;
            if (mTag.equals("")){
                //no search, no message
            } else if (mPOIs == null){
                Toast.makeText(mActivity, "Technical issue when getting "+mTag+ " POI.", Toast.LENGTH_LONG).show();
            } /*else {
                Toast.makeText(mActivity, ""+mPOIs.size()+" "+mTag+ " entries found", Toast.LENGTH_LONG).show();
                if (mTag.equals("flickr")||mTag.startsWith("picasa")||mTag.equals("wikipedia"))
                    startAsyncThumbnailsLoading(mPOIs);
            }*/
            updateUIWithPOI(mPOIs, mTag);
        }
    }

    void updateUIWithPOI(ArrayList<POI> pois,Integer type){
        if (pois != null){
            for (POI poi:pois){
                ExtendedOverlayItem poiMarker = new ExtendedOverlayItem(poi.mType, poi.mDescription,poi.mLocation, mActivity);
                Drawable marker = null;

                switch (poi.mServiceId)
                {
                    case POI.PAID_ACTIVITIES:
                    {
                        marker = getResources().getDrawable(R.drawable.map_paid_img);
                        break;
                    }
                    case POI.ATTRACTIONS:
                    {
                        marker = getResources().getDrawable(R.drawable.map_attract_img);
                        break;
                    }
                    case POI.FOOD_AND_DRINK:
                    {
                        marker = getResources().getDrawable(R.drawable.map_food_img);
                        break;
                    }
                    case POI.ACCOMODATIONS:
                    {
                        marker = getResources().getDrawable(R.drawable.map_accomod_img);
                        break;
                    }
                    case POI.OTHER:
                    {
                        marker = getResources().getDrawable(R.drawable.map_otherbtn);
                        break;
                    }
                    case POI.LOOK_OUT:
                    {
                        marker = getResources().getDrawable(R.drawable.map_lookout_img);
                        break;
                    }
                    case POI.PICNIC:
                    {
                        marker = getResources().getDrawable(R.drawable.map_picnic_img);
                        break;
                    }
                    case POI.WALKS:
                    {
                        marker = getResources().getDrawable(R.drawable.map_walks_img);
                        break;
                    }
                    case POI.TOILET:
                    {
                        marker = getResources().getDrawable(R.drawable.map_toilet_img);
                        break;
                    }
                }

                poiMarker.setSubDescription(poi.mCategory);
                poiMarker.setMarker(marker);
               /* if (poi.mServiceId == POI.POI_SERVICE_NOMINATIM){
                    poiMarker.setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
                } else {
                    poiMarker.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
                }*/
                poiMarker.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
                //thumbnail loading moved in POIInfoWindow.onOpen for better performances.
                poiMarker.setRelatedObject(poi);

                int count = 0;
                switch (osmMap.getZoomLevel()){
                    case 10:
                        count = 10;
                        break;
                    case 11:
                        count = 15;
                        break;
                    case 12:
                        count = 20;
                        break;
                    case 13:
                        count = 25;
                        break;
                    case 14:
                        count = 30;
                        break;
                    case 15:
                        count = 35;
                        break;
                    case 16:
                        count = 40;
                        break;
                    case 17:
                        count = 45;
                        break;
                    case 18:
                        count = 50;
                        break;
                    default:
                        count = 1;
                        break;
                }

                /*if(osmMap.getBoundingBox().contains(poi.mLocation))
                    poiMarkers.addItem(poiMarker);*/

                if(osmMap.getBoundingBox().contains(poi.mLocation))
                    if(poiMarkers!=null){
                        if(poiMarkers.size()< count)
                            poiMarkers.addItem(poiMarker);
                        else
                            break;
                    }
            }
        }
        osmMap.invalidate();

    }

    //------------ Route and Directions

    protected ItemizedOverlayWithBubble<ExtendedOverlayItem> roadNodeMarkers;

    private void putRoadNodes(Road road){
       /* roadNodeMarkers.removeAllItems();
        Drawable marker = getResources().getDrawable(R.drawable.marker_node);
        int n = road.mNodes.size();
        TypedArray iconIds = getResources().obtainTypedArray(R.array.direction_icons);
        for (int i=0; i<n; i++){
            RoadNode node = road.mNodes.get(i);
            String instructions = (node.mInstructions==null ? "" : node.mInstructions);
            ExtendedOverlayItem nodeMarker = new ExtendedOverlayItem("Step " + (i+1), instructions,node.mLocation, mActivity.getApplicationContext());
            nodeMarker.setSubDescription(road.getLengthDurationText(node.mLength, node.mDuration));
            nodeMarker.setMarkerHotspot(OverlayItem.HotspotPlace.CENTER);
            nodeMarker.setMarker(marker);
            int iconId = iconIds.getResourceId(node.mManeuverType, R.drawable.ic_empty);
            if (iconId != R.drawable.ic_empty){
                Drawable icon = getResources().getDrawable(iconId);
                nodeMarker.setImage(icon);
            }
            roadNodeMarkers.addItem(nodeMarker);
        }*/
    }

    void updateUIWithRoad(Road road){
        if(roadNodeMarkers!=null)
            roadNodeMarkers.removeAllItems();

        List<Overlay> mapOverlays = osmMap.getOverlays();
        if (roadOverlay != null){
            mapOverlays.remove(roadOverlay);
        }
        if (road == null)
            return;
        if (road.mStatus == Road.STATUS_DEFAULT)
            Toast.makeText(osmMap.getContext(), "We have a problem to get the route", Toast.LENGTH_SHORT).show();
        roadOverlay = RoadManager.buildRoadOverlay(road, osmMap.getContext());
        Overlay removedOverlay = mapOverlays.set(0, roadOverlay);
        //we set the road overlay at the "bottom", just above the MapEventsOverlay,
        //to avoid covering the other overlays.
        mapOverlays.add(removedOverlay);
        putRoadNodes(road);
        osmMap.invalidate();
    }

    /**
     * Async task to get the road in a separate thread.
     */
    private class UpdateRoadTask extends AsyncTask<Object, Void, Road> {
        protected Road doInBackground(Object... params) {
            @SuppressWarnings("unchecked")
            ArrayList<GeoPoint> waypoints = (ArrayList<GeoPoint>)params[0];
            RoadManager roadManager = new GoogleRoadManager();

            return roadManager.getRoad(waypoints);
        }

        protected void onPostExecute(Road result) {
            mRoad = result;
            updateUIWithRoad(result);
        }
    }


    public void getRoadAsync(){
        mRoad = null;
        GeoPoint roadStartPoint = null;
        GeoPoint startPoint = new GeoPoint(-44.935641,168.83612);
        GeoPoint destinationPoint = new GeoPoint(-44.990298,168.855003);

        /*GeoPoint startPoint = new GeoPoint(13.105561,80.292868);
        GeoPoint destinationPoint = new GeoPoint(13.005478,80.21855);*/

        if (startPoint != null){
            roadStartPoint = startPoint;
        }
        if (roadStartPoint == null || destinationPoint == null){
            updateUIWithRoad(mRoad);
            return;
        }
        ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>(2);
        waypoints.add(roadStartPoint);
        //add intermediate via points:
        for (GeoPoint p:viaPoints){
            waypoints.add(p);
        }
        waypoints.add(destinationPoint);
        new UpdateRoadTask().execute(waypoints);
    }


    private void initAudioView(){

        mMediaPlayer = new MediaPlayer();
        mHandler = new Handler();

        // Listeners
        mSeekBar = (SeekBar)mRootView.findViewById(R.id.songProgressBar);
        mPlayButton = (ImageButton)mRootView.findViewById(R.id.btnPlay);
        mSeekBar.setOnSeekBarChangeListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        mSeekBar.setEnabled(false);
        //playSong(0);


        mPlayButton.setOnClickListener(this);

        ((ImageButton)mRootView.findViewById(R.id.btnRewind))
                .setOnClickListener(this);


        ((ImageButton)mRootView.findViewById(R.id.btnForward))
                .setOnClickListener(this);

        ((ImageButton)mRootView.findViewById(R.id.btnStop))
                .setOnClickListener(this);
    }

    public void  playSong(String path){
        // Play song
        try {
            File baseDir = Environment.getExternalStorageDirectory();
            //String audioPath = baseDir.getAbsolutePath()  + File.separator +  "smartToursDb/segment/sample.mp3";

            String audioPath;

            if(path!=null)
                audioPath = baseDir.getAbsolutePath() + "/smartToursDb1/Audio/02 Queenstown to Mt Cook/" + path;
            else
                audioPath = baseDir.getAbsolutePath()  + "/smartToursDb/segment/sample.mp3";

            Log.i("Map", "Playing Audio Path: "+audioPath);

            FileInputStream fis = new FileInputStream(audioPath);
            FileDescriptor fd = fis.getFD();

            if(mMediaPlayer==null)
                mMediaPlayer = new MediaPlayer();
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(fd);
            mPlayerState = PlayerState.INITIALIZED;
            mMediaPlayer.prepare();
            mMediaPlayer.start();
            mPlayerState = PlayerState.PLAYING;

            // Changing Play Button Image to pause image
            mPlayButton.setImageResource(R.drawable.comment_pause_btn);

            // set Progress bar values
            mSeekBar.setProgress(0);
            mSeekBar.setMax(100);

            // Updating progress bar
            updateProgressBar();

        }
        catch (Exception e) {
            Log.i("Map","audio File Not Found");
            e.printStackTrace();
        }/* catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Log.i("SmartMapView","audio File Not Found");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        //To change body of implemented methods use File | Settings | File Templates.
        // Changing Play Button Image to pause image
        mPlayButton.setImageResource(R.drawable.audio_play_button_selector);
        mMediaPlayer.release();
        mMediaPlayer = null;
        mPlayerState = PlayerState.UNINITIALIZED;
    }

    /**
     * Update timer on seekbar
     * */
    public void updateProgressBar() {
        if (mMediaPlayer!=null)
            mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            if (mMediaPlayer!=null){
                long totalDuration = mMediaPlayer.getDuration();
                long currentDuration = mMediaPlayer.getCurrentPosition();

                // Displaying Total Duration time
                ((TextView)mRootView.findViewById(R.id.songTotalDurationLabel))
                        .setText("" + PlayerUtils.getInstance().milliSecondsToTimer(totalDuration));

                // Displaying time completed playing
                ((TextView)mRootView.findViewById(R.id.songCurrentDurationLabel))
                        .setText("" + PlayerUtils.getInstance().milliSecondsToTimer(currentDuration));

                // Updating progress bar
                int progress = (int)(PlayerUtils.getInstance().getProgressPercentage(currentDuration, totalDuration));
                //Log.d("Progress", ""+progress);
                mSeekBar.setProgress(progress);

                // Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 100);
            }
        }
    };

    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        if(mMediaPlayer!=null){
            int totalDuration = mMediaPlayer.getDuration();
            int currentPosition = PlayerUtils.getInstance().progressToTimer(seekBar.getProgress(), totalDuration);

            // forward or backward to certain seconds
            mMediaPlayer.seekTo(currentPosition);

            // update timer progress again
            updateProgressBar();
        }
    }

    protected ArrayList<GeoPoint> viaPoints = new ArrayList<GeoPoint>();
    private SmartMapView osmMap;
    protected Road mRoad;
    protected PathOverlay roadOverlay;

    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    // Define an object that holds accuracy and frequency parameters
    LocationRequest mLocationRequest;

    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationClient mLocationClient;

    private FetchRelevantTask mFetchRelevantTask;
    private String mAudioStartPoint;


    @Override
    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        //Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
        updateLoc(location);
        boolean isStartPointChanged = false;
        for(Opus opus:mRelevantOpuses){
            Double calculatedRadius = Math.pow(2 * (opus.getLatitudeInDouble() - location.getLatitude()), 2) +
                    Math.pow(opus.getLongitudeInDouble() - location.getLongitude(), 2);

            boolean isWithinRadius = calculatedRadius < opus.getRadiusInDouble();
            //Log.i("Map", "Math: "+ calculatedRadius);
            //Log.i("Map", "getRadius: "+opus.getRadiusInDouble());
            //Log.i("Map", "isWithinRadius: "+isWithinRadius);
            Log.i("Map","isPlaying ordinal: "+mPlayerState.ordinal());
            if(isWithinRadius && mPlayerState.ordinal() != 3){
                Log.i("Map","I'm in... Playing...");
                playSong(opus.getAudio());
                if(!opus.getStart().equals(mAudioStartPoint)){
                    mAudioStartPoint = opus.getStart();
                    isStartPointChanged = true;
                }
                break;
            }
        }
        if(mAudioStartPoint!=null && isStartPointChanged) {
            mFetchRelevantTask = new FetchRelevantTask();
            mFetchRelevantTask.execute(mAudioStartPoint);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Display the connection status
        Toast.makeText(mActivity, "Connected",Toast.LENGTH_SHORT).show();
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
        Log.i("Map","Current Location: "+mLocationClient.getLastLocation());
    }

    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(mActivity, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        mActivity,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            showErrorDialog(connectionResult.getErrorCode());
            Log.w("Map","onConnectionFailed: ErrorCode = "+connectionResult.getErrorCode());
        }
    }

    // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    @Override
    public void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :

                switch (resultCode) {

                    case Activity.RESULT_OK :
                    /**
                     * Try the request again
                     */
                        break;
                }
        }
    }

    /*private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(mActivity);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            // Get the error code
            //int errorCode = connectionResult.getErrorCode();
            showErrorDialog(connectionResult.getErrorCode());
        }
    }*/

    private void showErrorDialog(int errorCode){
        // Get the error dialog from Google Play services
        Dialog errorDialog;
        errorDialog = GooglePlayServicesUtil.getErrorDialog(
                errorCode,
                mActivity,
                CONNECTION_FAILURE_RESOLUTION_REQUEST);
        // If Google Play services can provide an error dialog
        if (errorDialog != null) {
            // Create a new DialogFragment for the error dialog
            ErrorDialogFragment errorFragment =
                    new ErrorDialogFragment();
            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);
            // Show the error dialog in the DialogFragment
            //errorFragment.show(getSupportFragmentManager(), "Location Updates");
            errorFragment.show(mActivity.getSupportFragmentManager(),"Location Updates");

        }
    }

    private void updateLoc(Location loc){
        GeoPoint locGeoPoint = new GeoPoint(loc.getLatitude(), loc.getLongitude());
        osmMap.getController().setCenter(locGeoPoint);
        setOverlayLoc(loc);
        osmMap.invalidate();
    }

    private void setOverlayLoc(Location overlayloc){
        GeoPoint overlocGeoPoint = new GeoPoint(overlayloc);
        //---
        mOverlayItemArray.clear();
        OverlayItem newMyLocationItem = new OverlayItem(
                "My Location", "My Location", overlocGeoPoint);
        mOverlayItemArray.add(newMyLocationItem);
        //---
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnPlay:
                switch (mPlayerState){
                    case UNINITIALIZED:
                    case STOPPED:
                        playSong(null);
                        mPlayerState = PlayerState.PLAYING;
                        break;
                    case PLAYING:
                        mMediaPlayer.pause();
                        mPlayButton.setImageResource(R.drawable.audio_play_button_selector);
                        mPlayerState = PlayerState.PAUSED;
                        break;
                    case PAUSED:
                        mMediaPlayer.start();
                        mPlayButton.setImageResource(R.drawable.comment_pause_btn);
                        mPlayerState = PlayerState.PLAYING;
                        break;
                }
                break;
            case R.id.btnStop:
                /*if(mLocationClient.isConnected())
                    mLocationClient.disconnect();*/
                switch (mPlayerState){
                    case PLAYING:
                        mMediaPlayer.seekTo(0);
                        mMediaPlayer.stop();
                        mPlayButton.setImageResource(R.drawable.audio_play_button_selector);
                        mPlayerState = PlayerState.STOPPED;
                        break;
                }
                break;
            case R.id.btnForward:
                switch (mPlayerState){
                    case PLAYING:
                        int currentPosition = mMediaPlayer.getCurrentPosition();
                        // check if seekForward time is lesser than song duration
                        if(currentPosition + mSeekForwardTime <= mMediaPlayer.getDuration()){
                            // forward song
                            mMediaPlayer.seekTo(currentPosition + mSeekForwardTime);
                        }else{
                            // forward to end position
                            mMediaPlayer.seekTo(mMediaPlayer.getDuration());
                        }
                        break;
                }
                break;
            case R.id.btnRewind:
                switch (mPlayerState){
                    case PLAYING:
                        // get current song position
                        int currentPosition = mMediaPlayer.getCurrentPosition();
                        // check if seekBackward time is greater than 0 sec
                        if(currentPosition - mSeekBackwardTime >= 0){
                            // forward song
                            mMediaPlayer.seekTo(currentPosition - mSeekBackwardTime);
                        }else{
                            // backward to starting position
                            mMediaPlayer.seekTo(0);
                        }
                        break;
                }
                break;
        }
    }

    private class MyItemizedIconOverlay extends ItemizedIconOverlay<OverlayItem> {

        public MyItemizedIconOverlay(
                List<OverlayItem> pList,
                org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener<OverlayItem> pOnItemGestureListener,
                ResourceProxy pResourceProxy) {
            super(pList, pOnItemGestureListener, pResourceProxy);
        }

        @Override
        public void draw(Canvas canvas, org.osmdroid.views.MapView mapview, boolean arg2) {
            // TODO Auto-generated method stub
            super.draw(canvas, mapview, arg2);

            if(!mOverlayItemArray.isEmpty()){

                //overlayItemArray have only ONE element only, so I hard code to get(0)
                GeoPoint in = mOverlayItemArray.get(0).getPoint();

                Point out = new Point();
                osmMap.getProjection().toPixels(in, out);

                Bitmap bm = BitmapFactory.decodeResource(getResources(),
                        R.drawable.i_am_here_1  );
                canvas.drawBitmap(bm,
                        out.x - bm.getWidth()/2,  //shift the bitmap center
                        out.y - bm.getHeight()/2,  //shift the bitmap center
                        null);
            }
        }

        @Override
        public boolean onSingleTapUp(MotionEvent event, MapView mapView) {
            // TODO Auto-generated method stub
            //return super.onSingleTapUp(event, mapView);
            return false;
        }
    }
}
