package com.doodleblue.smarttours.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.util.BaseContainerFragment;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/14/13
 * Time: 10:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class Web extends BaseContainerFragment {

    private String mUrl;

    public Web(String url){
        mUrl = url;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.web,
                container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView(){
        WebView webView = (WebView)mRootView.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(mUrl);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }
}
