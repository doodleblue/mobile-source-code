package com.doodleblue.smarttours.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.model.ActivityAccessory;
import com.doodleblue.smarttours.model.ThingsToDoDetailBean;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;

import java.io.File;
import java.util.ArrayList;

public class Description extends BaseContainerFragment implements View.OnClickListener{

    private ThingsToDoDetailBean mDescriptionBean = null;
    private int mDescriptionType;
    private String freeCallValue;
    private String callValue;
    private String webValue;

    public Description(ThingsToDoDetailBean bean, int type)
    {
        mDescriptionBean = bean;
        mDescriptionType = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.things_to_do_description,
                container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fetchActivityAccessory(mDescriptionBean.getId());
    }


    private void fetchActivityAccessory(String actyId){
        FetchActivityAccessoryTask task = new FetchActivityAccessoryTask();
        task.execute(new String[]{actyId});
    }

    public class FetchActivityAccessoryTask extends AsyncTask<String,String,ArrayList<ActivityAccessory> > {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        protected ArrayList<ActivityAccessory> doInBackground(String... params) {

            return DatabaseHelper.getInstance(mActivity).fetchActivityAccessory(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<ActivityAccessory> arrayList) {
            super.onPostExecute(arrayList);
            //if(arrayList!=null&&arrayList.size()>0)
            {
                setupCustomLists(arrayList);
            }
        }
    }

    private void setupCustomLists(ArrayList<ActivityAccessory> arrayList) {

        ((Button)mRootView.findViewById(R.id.freeCall)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.call)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.web)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.viewOnMap)).setOnClickListener(this);

        if(arrayList!=null)
            for(ActivityAccessory activityAccessory:arrayList)
            {
                if(activityAccessory.getType().equals("1")){
                    freeCallValue = activityAccessory.getValue();
                } else if(activityAccessory.getType().equals("2")){
                    webValue = activityAccessory.getValue();
                } else if(activityAccessory.getType().equals("3")){
                    callValue = activityAccessory.getValue();
                }
            }
        ((TextView)mRootView.findViewById(R.id.title)).setText(mDescriptionBean.getTitle());

        File file = new File(mDescriptionBean.getCoverImageLargeLocal());
        if(file.exists())
            ((ImageView)mRootView.findViewById(R.id.route_image)).setImageBitmap(Utils.getImageFromFile(file));
        switch (mDescriptionType)
        {
            case 0:     //info

                break;

            case 1:     //paid act
                ((TextView)mRootView.findViewById(R.id.subTitle)).setText(mDescriptionBean.getOperator());
                ((TextView)mRootView.findViewById(R.id.hours)).setText(mDescriptionBean.getDuration());
                ((TextView)mRootView.findViewById(R.id.price)).setText(mDescriptionBean.getPrice());
                ((TextView)mRootView.findViewById(R.id.dateTime)).setText(mDescriptionBean.getOpenedTime());
                ((TextView)mRootView.findViewById(R.id.highlightContent)).setText(mDescriptionBean.getHighlights());
                ((TextView)mRootView.findViewById(R.id.descriptionContent)).setText(mDescriptionBean.getDescription());
                break;
            case 2:     //Attractions
                ((TextView)mRootView.findViewById(R.id.subTitle)).setText(mDescriptionBean.getOperator());
                ((TextView)mRootView.findViewById(R.id.hours)).setText(mDescriptionBean.getDuration());
                ((TextView)mRootView.findViewById(R.id.price)).setText(mDescriptionBean.getPrice());
                ((TextView)mRootView.findViewById(R.id.dateTime)).setText(mDescriptionBean.getOpenedTime());
                ((TextView)mRootView.findViewById(R.id.highlightContent)).setText(mDescriptionBean.getHighlights());
                ((TextView)mRootView.findViewById(R.id.descriptionContent)).setText(mDescriptionBean.getDescription());
                break;
            case 3:     //Food & Drinks
                ((TextView)mRootView.findViewById(R.id.subTitle)).setText(mDescriptionBean.getSubType());
                ((TextView)mRootView.findViewById(R.id.hours)).setVisibility(View.INVISIBLE);
                ((TextView)mRootView.findViewById(R.id.price)).setText(mDescriptionBean.getPrice());
                ((TextView)mRootView.findViewById(R.id.dateTime)).setText(mDescriptionBean.getOpenedTime());
                ((TextView)mRootView.findViewById(R.id.highlightTitle)).setText("Cuisine");
                ((TextView)mRootView.findViewById(R.id.highlightContent)).setText(mDescriptionBean.getCuisine());
                ((TextView)mRootView.findViewById(R.id.descriptionContent)).setText(mDescriptionBean.getDescription());

                break;
            case 4:     //Accommodation
                ((TextView)mRootView.findViewById(R.id.subTitle)).setText(mDescriptionBean.getSubType());
                ((TextView)mRootView.findViewById(R.id.hours)).setText(mDescriptionBean.getRating());
                ((TextView)mRootView.findViewById(R.id.hours)).setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.rateit_img, 0, 0, 0);
                ((TextView)mRootView.findViewById(R.id.price)).setText(mDescriptionBean.getPrice());
                ((TextView)mRootView.findViewById(R.id.dateTime)).setVisibility(View.GONE);
                ((TextView)mRootView.findViewById(R.id.highlightTitle)).setText("Facilities");
                ((TextView)mRootView.findViewById(R.id.highlightContent)).setText(mDescriptionBean.getFacility());
                ((TextView)mRootView.findViewById(R.id.descriptionContent)).setText(mDescriptionBean.getDescription());
                break;
            case 5:     //Others
                ((TextView)mRootView.findViewById(R.id.subTitle)).setText(mDescriptionBean.getSubType());
                ((TextView)mRootView.findViewById(R.id.hours)).setVisibility(View.GONE);
                ((TextView)mRootView.findViewById(R.id.price)).setVisibility(View.GONE);
                ((TextView)mRootView.findViewById(R.id.dateTime)).setText(mDescriptionBean.getOpenedTime());
                ((TextView)mRootView.findViewById(R.id.highlightContent)).setText(mDescriptionBean.getHighlights());
                ((TextView)mRootView.findViewById(R.id.descriptionContent)).setText(mDescriptionBean.getDescription());
                break;
        }
    }

    private void replaceFragment(String url) {
        //TODO: any navigation of level deeper will go here
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Web(url), true);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.freeCall:
                if(!TextUtils.isEmpty(freeCallValue)){
                    showDevMsg(freeCallValue);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+freeCallValue));
                    mActivity.startActivity(intent);
                }

                break;
            case R.id.call:
                if(!TextUtils.isEmpty(callValue) ) {
                    showDevMsg(callValue);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+callValue));
                    mActivity.startActivity(intent);
                }
                break;
            case R.id.web:
                if(!TextUtils.isEmpty(webValue) ){
                    showDevMsg(webValue);
                    replaceFragment(webValue);
                }
                break;
            case R.id.viewOnMap:
                showDevMsg();
                break;
        }
    }
}
