package com.doodleblue.smarttours.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.model.Stops;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/27/13
 * Time: 7:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class StopDetail extends BaseContainerFragment implements View.OnClickListener {

    private Stops mStop;
    private int mStopType;

    private String freeCallValue;
    private String callValue;
    private String webValue;

    StopDetail(Stops stops, int type){
        mStop = stops;
        mStopType = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.stops_description,
                container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView(){
        ((Button)mRootView.findViewById(R.id.freeCall)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.call)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.web)).setOnClickListener(this);
        ((Button)mRootView.findViewById(R.id.viewOnMap)).setOnClickListener(this);

        ((TextView)mRootView.findViewById(R.id.title)).setText(mStop.getName());
        File file = new File(mStop.getImageLocal());    //   /mnt/sdcard/smartToursDb1/route/2/thumb.png
        if(file.exists())
            ((ImageView)mRootView.findViewById(R.id.cover)).setImageBitmap(Utils.getImageFromFile(file));

        ((TextView)mRootView.findViewById(R.id.hours)).setText(mStop.getTimeNeeded());
        ((TextView)mRootView.findViewById(R.id.dateTime)).setText(mStop.getSegmentId()); //Todo: retrieve Segment Name
        ((TextView)mRootView.findViewById(R.id.highlightContent)).setText(mStop.getHighlights());
        ((TextView)mRootView.findViewById(R.id.descriptionContent)).setText(mStop.getDescription());

    }

    private void replaceFragment(String url) {
        //TODO: any navigation of level deeper will go here
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Web(url), true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.freeCall:
                if(!TextUtils.isEmpty(freeCallValue)){
                    showDevMsg(freeCallValue);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + freeCallValue));
                    mActivity.startActivity(intent);
                }

                break;
            case R.id.call:
                if(!TextUtils.isEmpty(callValue) ) {
                    showDevMsg(callValue);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+callValue));
                    mActivity.startActivity(intent);
                }
                break;
            case R.id.web:
                if(!TextUtils.isEmpty(webValue) ){
                    showDevMsg(webValue);
                    replaceFragment(webValue);
                }
                break;
            case R.id.viewOnMap:
                showDevMsg();
                break;
        }
    }
}
