package com.doodleblue.smarttours.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.adapter.CustomArrayAdapter;
import com.doodleblue.smarttours.model.Favorite;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.task.DownloadTask;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.view.SmartTextView;
import com.meetme.android.horizontallistview.HorizontalListView;

import java.util.ArrayList;


/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/4/13
 * Time: 6:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class SearchRoute extends BaseContainerFragment implements View.OnClickListener {

    private ArrayList<String> mStartPointList;
    private ArrayList<String> mDestPointList;
    private Spinner mStartSpinner;
    private Spinner mDestSpinner;
    private CustomArrayAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.select_route, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        mStartSpinner = (Spinner)mRootView.findViewById(R.id.startSpinner);
        mDestSpinner = (Spinner)mRootView.findViewById(R.id.destSpinner);
        fetchRouteRecord();
        ((ImageView)mRootView.findViewById(R.id.download)).setOnClickListener(this);

    }

    private void replaceFragment(Route route) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new RouteDetail(route), true);
    }

    private void fetchRouteRecord()
    {
        FetchRecordTask task = new FetchRecordTask();
        task.execute();
    }



    public class FetchRecordTask extends AsyncTask<String,String,ArrayList<Route> > {

        private ArrayAdapter<String> startAdapter;
        private ArrayAdapter<String> destAdapter;


        FetchRecordTask(){
            mStartPointList = new ArrayList<String>();
            mDestPointList = new ArrayList<String>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Route> doInBackground(String... params) {
            ArrayList<Route> arrayList = DatabaseHelper.getInstance(mActivity).fetchRoutes(null);

            for(Route route:arrayList){
                String start = DatabaseHelper.getInstance(mActivity).fetchSegmentName(route.getStart());
                String dest = DatabaseHelper.getInstance(mActivity).fetchSegmentName(route.getEnd());
                if(start!=null) {
                    if(!mStartPointList.contains(start))
                        mStartPointList.add(start);
                }
                if(dest!=null) {
                    if(!mDestPointList.contains(dest))
                        mDestPointList.add(dest);
                }

                Favorite favoriteArrayList = DatabaseHelper.getInstance(mActivity).fetchFavorite(route.getId());
                if(favoriteArrayList!=null)
                {
                    route.setFavourite(true);
                }
                else
                    route.setFavourite(false);
            }
            return arrayList;
        }

        @Override
        protected void onPostExecute(ArrayList<Route>  routeArrayList) {
            super.onPostExecute(routeArrayList);
            if(routeArrayList!=null&&routeArrayList.size()>0)
            {
                setupCustomLists(routeArrayList);
            }
            if(mStartPointList.size()>0){
                if(startAdapter == null){
                    startAdapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_dropdown_item_1line, mStartPointList);
                    startAdapter.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);
                    mStartSpinner.setAdapter(startAdapter);
                } else {
                    startAdapter.notifyDataSetChanged();
                }

            }
            if(mDestPointList.size()>0){
                if(destAdapter==null){
                    destAdapter = new ArrayAdapter<String>(mActivity,
                            android.R.layout.simple_spinner_dropdown_item, mDestPointList);
                    destAdapter.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);
                    mDestSpinner.setAdapter(destAdapter);
                }else {
                    destAdapter.notifyDataSetChanged();
                }
            }
            try{
                int routeCount = mAdapter.getCount();
                ((TextView)mRootView.findViewById(R.id.result_header)).setText(String.valueOf(routeCount));
                ((TextView)mRootView.findViewById(R.id.routes_value)).setText(String.valueOf(routeCount));
            } catch (final Exception e){
                e.printStackTrace();
            }

        }
    }

    private void setupCustomLists(ArrayList<Route> routeArrayList) {
        // Make an array adapter using the built in android layout to render a list of strings
        mAdapter = new CustomArrayAdapter(mActivity, routeArrayList);

        // Assign adapter to HorizontalListView
        ((HorizontalListView)mRootView.findViewById(R.id.routes_list)).setAdapter(mAdapter);
        ((HorizontalListView)mRootView.findViewById(R.id.routes_list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                replaceFragment(mAdapter.getItem(position));
            }
        });

        ((SmartTextView)mRootView.findViewById(R.id.start_point)).setOnClickListener(this);
        ((SmartTextView)mRootView.findViewById(R.id.destination_point)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.start_point:
                mStartSpinner.performClick();
                break;
            case R.id.destination_point:
                mDestSpinner.performClick();
                break;
            case R.id.download:
                Toast.makeText(mActivity, getResources().getString(R.string.download_alert), Toast.LENGTH_SHORT).show();
                DownloadTask downloadTask = new DownloadTask(mActivity);
                downloadTask.execute(new String[]{null});
                break;
        }
    }


}
