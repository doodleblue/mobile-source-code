package com.doodleblue.smarttours.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.util.BaseContainerFragment;

public class Tab1ContainerFragment extends BaseContainerFragment {
	
	private boolean mIsViewInited;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.e("test", "tab 1 oncreateview");
		return inflater.inflate(R.layout.container_fragment, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.e("test", "tab 1 container on activity created");
		if (!mIsViewInited) {
			mIsViewInited = true;
			initView();
		}
	}
	
	private void initView() {
		Log.e("test", "tab 1 init view");
		replaceFragment(new Tab1Fragment(), false);
	}

    public static class SelectRoute extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.tab_fragment, null);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            initView();
        }

        private void initView() {
            ((TextView)getView().findViewById(R.id.tab_text)).setText("Tab 1");
            Button button = (Button) getView().findViewById(R.id.tab_btn);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replaceFragment();
                }
            });
        }

        private void replaceFragment() {
            ((BaseContainerFragment)getParentFragment()).replaceFragment(new Tab1AddOnFragment(), true);
        }

    }
}
