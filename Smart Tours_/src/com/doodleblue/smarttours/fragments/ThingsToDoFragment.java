package com.doodleblue.smarttours.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.adapter.ThingsToDoMainAdapter;
import com.doodleblue.smarttours.model.ABC;
import com.doodleblue.smarttours.model.Activity;
import com.doodleblue.smarttours.model.ActivityCount;
import com.doodleblue.smarttours.model.Segment;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ThingsToDoFragment extends BaseContainerFragment {

    protected static final String TAG = "ThingsToDoFragment";
    private ExpandableListView expandableListView;
    private ThingsToDoMainAdapter thingsToDoMainAdapter;
    private FetchRecordTask mTask;

    private List<String> listDataHeader;
    private HashMap<String, ArrayList<ABC>> listDataChild;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.things_to_do_main,
                container, false);

        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView()
    {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, ArrayList<ABC>>();



        expandableListView = (ExpandableListView)mRootView.findViewById(R.id.expandableListView1);
        thingsToDoMainAdapter = new ThingsToDoMainAdapter(getActivity(), listDataHeader, listDataChild);
        expandableListView.setAdapter(thingsToDoMainAdapter);
        expandableListView.setGroupIndicator(null);
        expandableListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                replaceFragment(thingsToDoMainAdapter
                        .getChild(groupPosition, childPosition)
                        .segmentId,
                        DatabaseHelper.getInstance(mActivity)
                                .fetchActivity(
                                        thingsToDoMainAdapter
                                                .getChild(groupPosition, childPosition)
                                                .segmentId
                                )
                );
                return true;
            }
        });

        mTask = new FetchRecordTask();
        mTask.execute();
    }

    public class FetchRecordTask extends AsyncTask<String,String,ArrayList<Activity>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Activity> doInBackground(String... params) {

            ArrayList<Segment> segmentArrayList = DatabaseHelper.getInstance(mActivity).fetchPlaceSegment();
            ArrayList<ABC> abcArrayListNorth = new ArrayList<ABC>();
            ArrayList<ABC> abcArrayListSouth = new ArrayList<ABC>();

            for(Segment segment:segmentArrayList)
            {
                ActivityCount activityCount = DatabaseHelper.getInstance(mActivity).fetchActivityCount(segment.getId());
                ABC abc = new ABC();

                abc.placeName = segment.getName();

                abc.accomodationCount =  String.valueOf(activityCount.getnAccomodation());
                abc.paidActivityCount =  String.valueOf(activityCount.getnPaidActivity());
                abc.attractionCount =  String.valueOf(activityCount.getnAttraction());
                abc.otherCount =  String.valueOf(activityCount.getnOther());
                abc.foodDrinkCount =  String.valueOf(activityCount.getnFoodAndDrink());

                abc.segmentId =  String.valueOf(segment.getId());
                abc.imageLocal = segment.getImageLocal();


                if(segment.getIslandType().equals("North")){
                    if(!listDataHeader.contains("North Island")){
                        listDataHeader.add("North Island");
                        listDataChild.put("North Island",abcArrayListNorth);
                    }
                    abcArrayListNorth.add(abc);

                } else if (segment.getIslandType().equals("South")){
                    if(!listDataHeader.contains("South Island")){
                        listDataHeader.add("South Island");
                        listDataChild.put("South Island",abcArrayListSouth);
                    }
                    abcArrayListSouth.add(abc);

                }

            }
            //if(abcArrayListNorth.size()!=0)
               // listDataChild.put("North Island",abcArrayListNorth);
            //if(abcArrayListSouth.size()!=0)
               // listDataChild.put("South Island",abcArrayListSouth);
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Activity>  arrayList) {
            super.onPostExecute(arrayList);

            //setupCustomLists(arrayList);

            if(thingsToDoMainAdapter == null)
            {
                thingsToDoMainAdapter = new ThingsToDoMainAdapter(getActivity(), listDataHeader, listDataChild);
                expandableListView.setAdapter(thingsToDoMainAdapter);
            }
            else
            {
                thingsToDoMainAdapter.notifyDataSetChanged();
            }
        }
    }


    private void replaceFragment(String segmentId, ArrayList<Activity> arrayList) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new ThingsToDoDetailFragment(segmentId,arrayList), true);
    }

}
