package com.doodleblue.smarttours.fragments;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.model.Favorite;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.task.DownloadTask;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.doodleblue.smarttours.view.CoverFlow;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 1:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class Favourite extends BaseContainerFragment implements View.OnClickListener{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.favourite, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {

        //Fetch available favorite records
        fetchRouteRecord();
        ((Button)mRootView.findViewById(R.id.download)).setOnClickListener(this);
    }

    private void fetchRouteRecord()
    {
        FetchRecordTask task = new FetchRecordTask();
        task.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.download:
                Toast.makeText(mActivity,getResources().getString(R.string.download_alert),Toast.LENGTH_SHORT).show();
                Route route = (Route) ((CoverFlow)mRootView.findViewById(R.id.favourite_rotes)).getAdapter().getItem(
                        ((CoverFlow)mRootView.findViewById(R.id.favourite_rotes)).getSelectedItemPosition());
                DownloadTask downloadTask = new DownloadTask(mActivity);
                downloadTask.execute(new String[]{route.getId()});

                break;
        }
    }

    public class FetchRecordTask extends AsyncTask<String,String,ArrayList<Route> > {

        private ArrayList<Route> favouriteRoutes = null;

        public FetchRecordTask(){
            favouriteRoutes = new ArrayList<Route>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Route> doInBackground(String... params) {

            ArrayList<Route> arrayList = DatabaseHelper.getInstance(mActivity).fetchRoutes(null);

            for(Route route:arrayList){

                Favorite favorite = DatabaseHelper.getInstance(mActivity).fetchFavorite(route.getId());
                if(favorite!=null)
                {
                    favouriteRoutes.add(route);
                }
            }
            return favouriteRoutes;
        }

        @Override
        protected void onPostExecute(ArrayList<Route>  routeArrayList) {
            super.onPostExecute(routeArrayList);
            if(routeArrayList!=null&&routeArrayList.size()>0)
            {
                setupCustomLists(routeArrayList);
            }
        }
    }

    private void setupCustomLists(ArrayList<Route>  routeArrayList){

        CoverFlow coverFlow = (CoverFlow) mRootView.findViewById(R.id.favourite_rotes);
        ImageAdapter coverImageAdapter =  new ImageAdapter(mActivity, routeArrayList);
        //FavouriteSwipeAdapter coverImageAdapter = new FavouriteSwipeAdapter(mActivity, routeArrayList);
        coverFlow.setAdapter(coverImageAdapter);
        coverFlow.setSpacing(-25);
        coverFlow.setSelection(0, true);
        coverFlow.setAnimationDuration(1000);
        coverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Route route = (Route) parent.getAdapter().getItem(position);
                replaceFragment(route);

            }
        });

        //TODO: Initially setOnItemSelectedListener is not calling by default.

        coverFlow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Route route = (Route) parent.getAdapter().getItem(position);
                ((TextView)mRootView.findViewById(R.id.tour_spot)).setText(route.getName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void replaceFragment(Route route) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new TourStopList(route), true);
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private  ArrayList<Route>  mList;

        public ImageAdapter(Context c, ArrayList<Route>  list) {
            mContext = c;
            mList = list;
        }

        public int getCount() {
            return mList.size();
        }

        public Route getItem(int position) {
            return mList.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @SuppressWarnings("deprecation")
		public View getView(int position, View convertView, ViewGroup parent) {

        	//Use this code if you want to load from resources
        	ImageView i = new ImageView(mContext);
            File file = new File(mList.get(position).getImageLocal());
            if(file.exists())
                i.setImageBitmap(Utils.getImageFromFile(file));
            //i.setImageResource(R.drawable.route_img_big);
            //i.setLayoutParams(new CoverFlow.LayoutParams(200,250)); //200,250
            //i.setLayoutParams(new CoverFlow.LayoutParams(200,250)); //200,250
            i.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            //Make sure we set anti-aliasing otherwise we get jaggies
            BitmapDrawable drawable = (BitmapDrawable) i.getDrawable();
            drawable.setAntiAlias(true);

            return i;
        }
        /** Returns the size (0.0f to 1.0f) of the views
         * depending on the 'offset' to the center. */
        public float getScale(boolean focused, int offset) {
        /* Formula: 1 / (2 ^ offset) */
            return Math.max(0, 1.0f / (float)Math.pow(2, Math.abs(offset)));
        }

    }
}
