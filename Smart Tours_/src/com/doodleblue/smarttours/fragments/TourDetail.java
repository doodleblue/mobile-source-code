package com.doodleblue.smarttours.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.model.Tours;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.doodleblue.smarttours.view.Info;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class TourDetail extends BaseContainerFragment implements View.OnClickListener{

    private Tours mTours;

    public TourDetail(Tours tours){
        mTours = tours;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView =  inflater.inflate(R.layout.favorite_detail, null);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        File file = new File(mTours.getImageLocal());
        if(file.exists())
            ((ImageView)mRootView.findViewById(R.id.route_image)).setImageBitmap(Utils.getImageFromFile(file));
        ((TextView)mRootView.findViewById(R.id.fav_name)).setText(mTours.getName());
        ((TextView)mRootView.findViewById(R.id.start_value)).setText(mTours.getStartName());
        ((TextView)mRootView.findViewById(R.id.dest_value)).setText(mTours.getEndName());
        ((Info)mRootView.findViewById(R.id.duration)).setText(mTours.getDuration());
        ((Info)mRootView.findViewById(R.id.distance)).setText(mTours.getDistance());
        //((Info)mRootView.findViewById(R.id.stops)).setText(); //TODO: Calculation logic here
        ((TextView)mRootView.findViewById(R.id.highlights_value)).setText(mTours.getHighlights());
        ((TextView)mRootView.findViewById(R.id.description_value)).setText(mTours.getDescription());
        ((Button)mRootView.findViewById(R.id.view_on_map)).setOnClickListener(this);
    }

    private void replaceFragment() {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(new Tab1AddOnFragment(), true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.view_on_map:
            {
                showDevMsg();
                break;
            }
        }
    }
}
