package com.doodleblue.smarttours.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.view.CoverFlow;

import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Doodle
 * Date: 11/13/13
 * Time: 3:47 PM
 * To change this template use File | Settings | File Templates.
 */

public class FavouriteSwipeAdapter extends ArrayAdapter<Route> {

    int mGalleryItemBackground;
    private Context mContext;
    private FileInputStream fis;
    private ArrayList<Route> mRouteArrayList;
    private LayoutInflater mInflater;

    public FavouriteSwipeAdapter(Context context, ArrayList<Route> list) {
        super(context, R.layout.custom_data_view, list);
        mContext = context;
        mRouteArrayList = list;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mRouteArrayList.size();
    }

    @Override
    public Route getItem(int position) {
        return mRouteArrayList.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.custom_data_view, parent, false);

            holder = new Holder();
            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            holder.route = (ImageView) convertView.findViewById(R.id.route_map);
            holder.favourite = (CheckBox) convertView.findViewById(R.id.favourite);
        }
        holder.favourite.setVisibility(View.GONE);
        holder.route.setImageDrawable(mContext.getResources().getDrawable(R.drawable.route_img_big));
        holder.textView.setText(getItem(position).getName());

        holder.route.setLayoutParams(new CoverFlow.LayoutParams(200, 250));
        holder.route.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        //Make sure we set anti-aliasing otherwise we get jaggies
        BitmapDrawable drawable = (BitmapDrawable) holder.route.getDrawable();
        drawable.setAntiAlias(true);

        return holder.route;
    }

    /** View holder for the views we need access to */
    private static class Holder {
        public TextView textView;
        public CheckBox favourite;
        public ImageView route;
    }
}
