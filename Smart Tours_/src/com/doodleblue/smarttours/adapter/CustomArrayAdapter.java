package com.doodleblue.smarttours.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.model.Favorite;
import com.doodleblue.smarttours.model.Route;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.Utils;

import java.io.File;
import java.util.ArrayList;

/** An array adapter that knows how to render views when given CustomData classes */
public class CustomArrayAdapter extends ArrayAdapter<Route> {
    private LayoutInflater mInflater;
    private ArrayList<Route> mRouteArrayList;
    private Context mContext;

    public CustomArrayAdapter(Context context,ArrayList<Route> routeArrayList) {
        super(context, R.layout.custom_data_view, routeArrayList);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRouteArrayList = routeArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;

        if (convertView == null) {
            // Inflate the view since it does not exist
            convertView = mInflater.inflate(R.layout.custom_data_view, parent, false);

            // Create and save off the holder in the tag so we get quick access to inner fields
            // This must be done for performance reasons
            holder = new Holder();
            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            holder.route = (ImageView) convertView.findViewById(R.id.route_map);
            holder.favourite = (CheckBox) convertView.findViewById(R.id.favourite);
            holder.favourite.setOnCheckedChangeListener(checkListener);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        // Populate the text
        holder.favourite.setTag(getItem(position));

        holder.favourite.setChecked(getItem(position).isFavourite());
        holder.textView.setText(getItem(position).getName());


        //holder.route.setImageURI(getItem(position).getImage());
        //holder.route.setImageDrawable(mContext.getResources().getDrawable(R.drawable.route_img_big));

        File file = new File(getItem(position).getImageLocal());
        if(file.exists())
            holder.route.setImageBitmap(Utils.getImageFromFile(file));
        return convertView;
    }

    @Override
    public int getCount() {
        return mRouteArrayList.size();
    }

    @Override
    public Route getItem(int position) {
        return mRouteArrayList.get(position);    //To change body of overridden methods use File | Settings | File Templates.
    }

    /** View holder for the views we need access to */
    private static class Holder {
        public TextView textView;
        public CheckBox favourite;
        public ImageView route;
    }

    private CompoundButton.OnCheckedChangeListener checkListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton checkboxView, boolean isChecked) {
            Route route = (Route) checkboxView.getTag();
            String routeId = route.getId();

            //Query routeId to Favorite
            if(isChecked){
                DatabaseHelper.getInstance(mContext).insertOrUpdateFavorite(new Favorite(routeId,false)); //TODO: Update while in-app purchase is introduced
                route.setFavourite(true);
            }
            else {
                DatabaseHelper.getInstance(mContext).deleteRecord(DatabaseHelper.TABLE_FAVORITE,DatabaseHelper.KEY_ROUTE_ID,routeId);
                route.setFavourite(false);
            }


        }
    };




}
