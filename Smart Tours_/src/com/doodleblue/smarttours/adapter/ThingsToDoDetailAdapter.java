package com.doodleblue.smarttours.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.model.PlacePropertyBean;
import com.doodleblue.smarttours.model.ThingsToDoDetailBean;
import com.doodleblue.smarttours.util.Utils;
import com.doodleblue.smarttours.view.ImageTextLayout;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ThingsToDoDetailAdapter extends BaseExpandableListAdapter {

	private static final String TAG = "ThingsToDoDetailAdapter";
	private Context _context;
	private ArrayList<PlacePropertyBean> _listHeader;
	private ArrayList<ArrayList<ThingsToDoDetailBean>> _listChild;
    private MediaPlayer mMediaPlayer;
    private boolean isMediaInitiated;

	public ThingsToDoDetailAdapter(Context context, ArrayList<PlacePropertyBean> listHeader, ArrayList<ArrayList<ThingsToDoDetailBean>> listChild) {

		this._context = context;
		this._listHeader = listHeader;
		this._listChild = listChild;


	}

	@Override
	public ThingsToDoDetailBean getChild(int groupPosition, int childPosition) {
		return _listChild.get(groupPosition).get(childPosition);
	}

	@Override
	public int getChildType(int groupPosition, int childPosition) {
		ThingsToDoDetailBean bean = getChild(groupPosition, childPosition);
		if(bean.getChildType().equals("Information")){
			return 0;
		}else if(bean.getChildType().equals("Paid Activities")){
			return 1;
		}else if(bean.getChildType().equals("Attractions")){
			return 2;
		}else if(bean.getChildType().equals("Food & Drink")){
			return 3;
		}else if(bean.getChildType().equals("Accommodation")){
			return 4;
		}else if(bean.getChildType().equals("Other")){
			return 5;
		}
		return 0;
	}

	@Override
	public int getChildTypeCount() {
		return 6;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final ThingsToDoDetailBean bean = getChild(groupPosition, childPosition);
		LayoutInflater infalInflater = (LayoutInflater) this._context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		switch (getChildType(groupPosition, childPosition)) {
		case 0:
			if (convertView == null) {
				convertView = infalInflater.inflate(R.layout.things_to_do_group2_info, null);
                if(mMediaPlayer == null){
                    mMediaPlayer = new MediaPlayer();
                }
			}
			break;
		case 1:
			if (convertView == null) {
				convertView = infalInflater.inflate(R.layout.things_to_do_group2_acty, null);
			}
			break;

		case 2:
			if (convertView == null) {
				convertView = infalInflater.inflate(R.layout.things_to_do_group2_acty, null);
			}
			break;

		case 3:
			if (convertView == null) {
				convertView = infalInflater.inflate(R.layout.things_to_do_group2_acc, null);
			}
			break;

		case 4:
			if (convertView == null) {
				convertView = infalInflater.inflate(R.layout.things_to_do_group2_acc, null);
			}
			break;

		case 5:
			if (convertView == null) {
				convertView = infalInflater.inflate(R.layout.things_to_do_group2_acc, null);
			}
			break;
		}
		
		switch (getChildType(groupPosition, childPosition)) {
		case 0:
			ImageView coverImage = (ImageView) convertView.findViewById(R.id.image);
			TextView title = (TextView) convertView.findViewById(R.id.title);
			ImageView audioInfo = (ImageView) convertView.findViewById(R.id.audioInfo);
			TextView content = (TextView) convertView.findViewById(R.id.content);

            File file = new File(bean.getCoverImageLocal());
            if(file.exists())
                coverImage.setImageBitmap(Utils.getImageFromFile(file));
			title.setText(bean.getTitle());
			audioInfo.setImageResource(bean.getAudioImageResId());
			content.setText(bean.getDescription());
            audioInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mMediaPlayer!=null){
                        if(!isMediaInitiated){
                            isMediaInitiated = true;
                            playSong(bean.getAudioLocal());
                        } else if(mMediaPlayer.isPlaying())
                            mMediaPlayer.pause();
                        else
                            mMediaPlayer.start();
                    }
                }
            });
			break;
		case 1:
			coverImage = (ImageView) convertView.findViewById(R.id.image);
			title = (TextView) convertView.findViewById(R.id.title);
			TextView highlights = (TextView) convertView.findViewById(R.id.highlight);
			TextView operator = (TextView) convertView.findViewById(R.id.type);
			ImageTextLayout time = (ImageTextLayout) convertView.findViewById(R.id.time);
			ImageTextLayout price = (ImageTextLayout) convertView.findViewById(R.id.price);

            File filePaid = new File(bean.getCoverImageLocal());
            if(filePaid.exists())
                coverImage.setImageBitmap(Utils.getImageFromFile(filePaid));

			title.setText(bean.getTitle());
			highlights.setText(bean.getHighlightsWithTitle());
			operator.setText(bean.getOperator());
			time.setValues(new PlacePropertyBean(
					bean.getDuration(), 
					bean.getDurationImageResId(), 
					String.valueOf(0)));
			price.setValues(new PlacePropertyBean(
					bean.getPrice(), 
					bean.getPriceImageResId(),
                    String.valueOf(0)));
			break;

		case 2:
            coverImage = (ImageView) convertView.findViewById(R.id.image);
            title = (TextView) convertView.findViewById(R.id.title);
            highlights = (TextView) convertView.findViewById(R.id.highlight);
            operator = (TextView) convertView.findViewById(R.id.type);
            time = (ImageTextLayout) convertView.findViewById(R.id.time);
            price = (ImageTextLayout) convertView.findViewById(R.id.price);

            File fileAttr = new File(bean.getCoverImageLocal());
            if(fileAttr.exists())
                coverImage.setImageBitmap(Utils.getImageFromFile(fileAttr));
            title.setText(bean.getTitle());
            highlights.setText(bean.getHighlights());
            operator.setText(bean.getOperator());
            time.setValues(new PlacePropertyBean(
                    bean.getDuration(),
                    bean.getDurationImageResId(),
                    String.valueOf(0)));
            price.setValues(new PlacePropertyBean(
                    bean.getPrice(),
                    bean.getPriceImageResId(),
                    String.valueOf(0)));
			break;

		case 3:
			coverImage = (ImageView) convertView.findViewById(R.id.image);
			title = (TextView) convertView.findViewById(R.id.title);
            TextView cuisine = (TextView) convertView.findViewById(R.id.highlight);
			TextView type = (TextView) convertView.findViewById(R.id.type);
			price = (ImageTextLayout) convertView.findViewById(R.id.price);

            ((TextView)convertView.findViewById(R.id.rate)).setVisibility(View.GONE);

            File fileFood = new File(bean.getCoverImageLocal());
            if(fileFood.exists())
                coverImage.setImageBitmap(Utils.getImageFromFile(fileFood));
			title.setText(bean.getTitle());
            cuisine.setText(bean.getCuisineWithTitle());
			type.setText(bean.getSubTypeWithTitle());
			price.setValues(new PlacePropertyBean(
					bean.getPrice(), 
					bean.getPriceImageResId(),
                    String.valueOf(0)));
			
			break;

		case 4:
			coverImage = (ImageView) convertView.findViewById(R.id.image);
			title = (TextView) convertView.findViewById(R.id.title);
            TextView facility  = (TextView) convertView.findViewById(R.id.highlight);
			type = (TextView) convertView.findViewById(R.id.type);
			TextView rate = (TextView) convertView.findViewById(R.id.rate);
			price = (ImageTextLayout) convertView.findViewById(R.id.price);

            File fileAcc = new File(bean.getCoverImageLocal());
            if(fileAcc.exists())
                coverImage.setImageBitmap(Utils.getImageFromFile(fileAcc));
			title.setText(bean.getTitle());
            facility.setText(bean.getFacilityWithTitle());
			type.setText(bean.getSubTypeWithTitle());
			rate.setText(bean.getRatingWithTitle());
			price.setValues(new PlacePropertyBean(
					bean.getPrice(), 
					bean.getPriceImageResId(),
                    String.valueOf(0)));
			break;

		case 5:
			coverImage = (ImageView) convertView.findViewById(R.id.image);
			title = (TextView) convertView.findViewById(R.id.title);
            highlights = (TextView) convertView.findViewById(R.id.highlight);
			type = (TextView) convertView.findViewById(R.id.type);

            price = (ImageTextLayout) convertView.findViewById(R.id.price);
            price.setVisibility(View.GONE);
            convertView.findViewById(R.id.rate).setVisibility(View.GONE);
            File fileOther = new File(bean.getCoverImageLocal());
            if(fileOther.exists())
                coverImage.setImageBitmap(Utils.getImageFromFile(fileOther));
			title.setText(bean.getTitle());
            highlights.setText(bean.getHighlightsWithTitle());
			type.setText(bean.getSubTypeWithTitle());

			break;
		
		}
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return _listChild.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return _listHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return _listHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		PlacePropertyBean headerItem = (PlacePropertyBean) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.things_to_do_group2, null);
		}
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.title);
		lblListHeader.setText(headerItem.getTitle());

		ImageView imageView = (ImageView) convertView
				.findViewById(R.id.image);
		imageView.setImageResource(headerItem.getResId());

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

    public void  playSong(String path){
        // Play song
        try {
            String audioPath = AppConstants.ROOT_DIRECTORY + path;

            Log.i("playSong", "Audio Path: " + audioPath);

            FileInputStream fis = new FileInputStream(audioPath);
            FileDescriptor fd = fis.getFD();

            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(fd);
            mMediaPlayer.prepare();
            mMediaPlayer.start();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Log.i("ThingsToDoDetailAdapter","audio File Not Found");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
