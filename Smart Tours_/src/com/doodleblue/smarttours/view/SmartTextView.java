package com.doodleblue.smarttours.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.util.GetFont;

//com.doodleblue.smarttours.view.SmartTextView

public class SmartTextView extends TextView {

    private Context mContext;

	public SmartTextView(Context context) {
		super(context);
        mContext = context;
	}
	
	public SmartTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyCustomFont, 0, 0);
        int data = ta.getInteger(R.styleable.MyCustomFont_font_type,1);
        ta.recycle();

        init(data);
    }

    public SmartTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyCustomFont, 0, 0);
        int data = ta.getInteger(R.styleable.MyCustomFont_font_type,1);
        ta.recycle();

        init(data);
    }

    private void init(int type) {
        switch (type)
        {
            case 1:
            {
                setTypeface(GetFont.getInstance(mContext).getFont(GetFont.HELVITICA_CONDENSED));
                break;
            }
            case 2:
            {
                setTypeface(GetFont.getInstance(mContext).getFont(GetFont.HELVITICA_TT));
                break;
            }
        }

    }


}
