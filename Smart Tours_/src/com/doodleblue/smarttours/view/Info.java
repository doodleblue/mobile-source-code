package com.doodleblue.smarttours.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.doodleblue.smarttours.R;

/**
 * Created with IntelliJ IDEA.
 * User: ruby
 * Date: 11/5/13
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class Info extends FrameLayout {
    public Info(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    public Info(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyCustomElement, 0, 0);

        String data = ta.getString(R.styleable.MyCustomElement_text);
        int id =ta.getResourceId(R.styleable.MyCustomElement_top_image,R.drawable.heart_unselect);

        ta.recycle();

        initView(context);

        setData(id,data);
    }

    public Info(Context context) {
        super(context);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.info_view, null);
        topImage = (ImageView) view.findViewById(R.id.top_image);
        textView = (TextView)view.findViewById(R.id.bottom_text);
        addView(view);
    }

    private ImageView topImage;
    private TextView textView;

    public void setData(int id,String data)
    {
        topImage.setImageResource(id);
        textView.setText(data);
    }

    public void setText(String data)
    {
        //topImage.setImageResource(id);
        textView.setText(data);
    }
}