package com.doodleblue.smarttours.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.doodleblue.smarttours.R;
import com.doodleblue.smarttours.constants.AppConstants;
import com.doodleblue.smarttours.fragments.Map;
import com.doodleblue.smarttours.fragments.RouteLevelOne;
import com.doodleblue.smarttours.fragments.SettingsContainer;
import com.doodleblue.smarttours.fragments.ThingsToDoContainer;
import com.doodleblue.smarttours.model.*;
import com.doodleblue.smarttours.service.SplashDownloadService;
import com.doodleblue.smarttours.sqlite.helper.DatabaseHelper;
import com.doodleblue.smarttours.util.BaseContainerFragment;
import com.doodleblue.smarttours.util.Utils;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends FragmentActivity {

	private FragmentTabHost mTabHost;
    private SharedPreferences mSharedPreferences;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bottom_tabs);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //initData();
        initializeTabs();
	}

    public void initializeTabs() {

        final TabHost.TabSpec routeTabTag       = mTabHost.newTabSpec(AppConstants.ROUTES);
        final TabHost.TabSpec mapTabTag         = mTabHost.newTabSpec(AppConstants.MAPS);
        final TabHost.TabSpec thingstodoTabTag  = mTabHost.newTabSpec(AppConstants.THINGS_TO_DO);
        final TabHost.TabSpec settingsTabTag    = mTabHost.newTabSpec(AppConstants.SETTINGS);

        mTabHost.addTab(
                setIndicator(
                    getApplicationContext(),
                    routeTabTag,
                    R.drawable.route_tab_selector
                ),
                RouteLevelOne.class,
                null);

        mTabHost.addTab(setIndicator(getApplicationContext(), mapTabTag, R.drawable.map_tab_selector),
                Map.class, null);

        mTabHost.addTab(setIndicator(getApplicationContext(), thingstodoTabTag, R.drawable.things_to_do_selector),
                ThingsToDoContainer.class, null);

        mTabHost.addTab(setIndicator(getApplicationContext(), settingsTabTag, R.drawable.settings_tab_selector),
                SettingsContainer.class, null);

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if(routeTabTag.getTag().equals(tabId)){
                    ((TextView)findViewById(R.id.header_text))
                            .setText(getResources().getString(R.string.route_tab));
                }else if(mapTabTag.getTag().equals(tabId)){
                    ((TextView)findViewById(R.id.header_text))
                            .setText(getResources().getString(R.string.map_tab));
                }else if(thingstodoTabTag.getTag().equals(tabId)){
                    ((TextView)findViewById(R.id.header_text))
                            .setText(getResources().getString(R.string.things_to_do_tab));
                }else if(settingsTabTag.getTag().equals(tabId)){
                    ((TextView)findViewById(R.id.header_text))
                            .setText(getResources().getString(R.string.settings_tab));
                }
            }
        });
    }

    public TabHost.TabSpec setIndicator(Context ctx,TabHost.TabSpec spec, int resid) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.tabs_icon, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
        imageView.setImageResource(resid);
        return spec.setIndicator(view);
    }

    public void initData(){

        /*FetchRecordTask fetchRecordTask = new FetchRecordTask(MainActivity.this);
        fetchRecordTask.execute();*/

        Intent intent = new Intent(this, SplashDownloadService.class);
        startService(intent);

    }

    private void savePreferences(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void savePreferences(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public class FetchRecordTask extends AsyncTask<String,String,String > {
        private ProgressDialog dialog;
        private Context mContext;

        FetchRecordTask(Context context){
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*dialog = new ProgressDialog(MainActivity.this);
            dialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {

            //Code to fetch Routes
            InputStream inputStream = retrieveStream(AppConstants.GET_ALL_ROUTES_URL);
            Gson gson = new Gson();
            Reader reader = new InputStreamReader(inputStream);
            JSearchRoutes response = gson.fromJson(reader, JSearchRoutes.class);

            for(JRoutes jRoutes:response.routes){
                Route route = new Route();
                route.setId(jRoutes.id);
                route.setName(jRoutes.name);
                route.setImage(jRoutes.image);
                route.setImageWide(jRoutes.image2);
                route.setProductId(jRoutes.productId);
                route.setStart(jRoutes.start);
                route.setEnd(jRoutes.end);
                route.setMinDay(jRoutes.minDay);
                route.setMaxDay(jRoutes.maxDay);
                route.setDistance(jRoutes.distance);
                route.setHighlights(jRoutes.highlights);
                route.setLastModified(jRoutes.lastModified);
                route.setDescription(jRoutes.description);
                route.setImageLocal(AppConstants.ROOT_DIRECTORY+"route" + File.separator + jRoutes.id + File.separator + "thumb.png");
                route.setImageWideLocal(AppConstants.ROOT_DIRECTORY+"route" + File.separator + jRoutes.id + File.separator + "wide.png");
                DatabaseHelper.getInstance(MainActivity.this).insertOrUpdateRoute(route);
                downloadItem(route.getId(),route.getImage(),route.getImageLocal());
                downloadItem(route.getId(),route.getImage(),route.getImageLocal());
            }

            for(JSegmentMeta segmentMeta:response.segments){
                SegmentEntry segmentEntry = new SegmentEntry();
                segmentEntry.setId(segmentMeta.id);
                segmentEntry.setSegmentId(segmentMeta.id);
                segmentEntry.setRouteId(segmentMeta.routeId);
                //segmentEntry.setDirection(segmentMeta.direction); Todo: Uncomment this after getting this webservice response
                //segmentEntry.setOrder(segmentMeta.order);
                DatabaseHelper.getInstance(MainActivity.this).insertOrUpdateSegmentEntry(segmentEntry);
            }

            for(JTours jTours:response.tours){
                Tours tour = new Tours();
                tour.setId(jTours.id);
                tour.setName(jTours.name);
                tour.setImage(jTours.image);
                tour.setImageLocal(AppConstants.ROOT_DIRECTORY+"tour"+ File.separator + jTours.id + File.separator + "thumb.png");
                tour.setDuration(jTours.duration);
                tour.setCommentary(jTours.commentary);
                tour.setDistance(jTours.distance);
                tour.setHighlights(jTours.highlights);
                tour.setDescription(jTours.description);
                tour.setRouteId(jTours.routeId);
                tour.setStart(jTours.start);
                tour.setEnd(jTours.finish);
                tour.setLastModified(jTours.lastModified);
                DatabaseHelper.getInstance(MainActivity.this).insertOrUpdateTours(tour);
            }

            try {
                reader.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Code to fetch Segments
            ArrayList<SegmentEntry> segmentEntries = DatabaseHelper.getInstance(MainActivity.this).fetchSegmentEntry(null);
            for(SegmentEntry segmentEntry:segmentEntries){
                inputStream = retrieveStream(AppConstants.GET_SEGMENTS_URL+segmentEntry.getSegmentId());
                gson = new Gson();
                reader = new InputStreamReader(inputStream);
                JGetSegments responseSegment = gson.fromJson(reader, JGetSegments.class);

                for(JSegments jSegments:responseSegment.segment){
                    Segment segment = new Segment();
                    segment.setId(jSegments.id);
                    segment.setName(jSegments.name);
                    segment.setPlace(jSegments.isPlace);
                    segment.setIslandType(jSegments.island);
                    segment.setLatitude(jSegments.latitude);
                    segment.setLongitude(jSegments.langitude);
                    segment.setDescription(jSegments.description);
                    segment.setAudio(jSegments.audio);
                    segment.setAudioLocal(AppConstants.ROOT_DIRECTORY+"segment" + File.separator + jSegments.id + File.separator + "audio.mp3");
                    segment.setCoordinates(jSegments.coorinates);
                    segment.setMbTiles(jSegments.mbTiles);
                    segment.setMbTilesLocal(AppConstants.ROOT_DIRECTORY+"segment" + File.separator + jSegments.id + File.separator + "tiles.mbtiles");
                    segment.setImage(jSegments.image);
                    segment.setImageLocal(AppConstants.ROOT_DIRECTORY+"segment" + File.separator + jSegments.id + File.separator + "thumb.png");
                    segment.setLastModified(jSegments.lastModified);
                    DatabaseHelper.getInstance(MainActivity.this).insertOrUpdateSegment(segment);
                }

                for(JActivity jActivity:responseSegment.activity){
                    Activity activity = new Activity();
                    activity.setId(jActivity.id);
                    activity.setType(jActivity.type);
                    activity.setTitle(jActivity.title);
                    activity.setSubtitle(jActivity.type);
                    activity.setSegmentId(jActivity.segmentId);
                    activity.setPlaceId(jActivity.placeId);
                    activity.setLatitude(jActivity.latitude);
                    activity.setLongitude(jActivity.longitude);
                    activity.setDescription(jActivity.description);
                    activity.setHighlights(jActivity.info2);
                    activity.setFacility(jActivity.info2);
                    activity.setCuisine(jActivity.info2);
                    activity.setDuration(jActivity.info1);
                    activity.setPrice(jActivity.price);
                    activity.setTime(jActivity.info3);
                    activity.setImage(jActivity.image);
                    activity.setImageLocal(AppConstants.ROOT_DIRECTORY+"activity" + File.separator + jActivity.id + File.separator + "thumb.png");
                    activity.setImageWide(jActivity.image2);
                    activity.setImageWideLocal(AppConstants.ROOT_DIRECTORY+"activity" + File.separator + jActivity.id + File.separator + "wide.png");
                    activity.setLastModified(jActivity.date);

                /*if (jActivity.type.equals("Paid Activity")) {

                } else if (jActivity.type.equals("Free Activity")) {

                } else if (jActivity.type.equals("Food & Drink")) {

                } else if (jActivity.type.equals("Accommodation")) {

                } else if (jActivity.type.equals("Other")) {

                }*/

                    DatabaseHelper.getInstance(MainActivity.this).insertOrUpdateActivity(activity);
                    for(int i=0;i<3;i++){
                        ActivityAccessory activityAccessory = new ActivityAccessory();
                        activityAccessory.setActivityId(jActivity.id);
                        activityAccessory.setType(jActivity.type);
                        switch (i){
                            case 0:
                                activityAccessory.setValue(jActivity.phone);
                                break;
                            case 1:
                                activityAccessory.setValue(jActivity.web);
                                break;
                            case 2:
                                activityAccessory.setValue(jActivity.otherPhone);
                                break;
                        }
                        DatabaseHelper.getInstance(MainActivity.this).insertOrUpdateActivityAccessory(activityAccessory);
                    }
                }

                for(JStops jStops:responseSegment.stops){
                    Stops stops = new Stops();
                    stops.setId(jStops.id);
                    stops.setName(jStops.name);
                    stops.setSegmentId(jStops.segmentId);
                    stops.setType(jStops.type);
                    stops.setAddress(jStops.address);
                    stops.setTimeNeeded(jStops.timeNeeded);
                    stops.setHighlights(jStops.highlights);
                    stops.setDescription(jStops.description);
                    stops.setLastModified(jStops.lastModified);
                    stops.setImage(jStops.image);
                    stops.setImageLocal(AppConstants.ROOT_DIRECTORY+"stop"+ File.separator + jStops.id + File.separator + "thumb.png");
                    DatabaseHelper.getInstance(MainActivity.this).insertOrUpdateStops(stops);
                }

                try {
                    reader.close();
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        //key = route, id=1
        private boolean downloadItem(String id, String serverPath, String localPath){ //synchronized
            try {
                //set the download URL, a url that points to a file on the internet
                //this is the file to be downloaded
                if(localPath == null) return false;
                URL url = new URL("http://www.weenysoft.com/images/icons/free-image-to-pdf-converter.png");//ToDo: Change this to serverPath
                //URL url = new URL(serverPath);

                //create the new connection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                //set up some things on the connection
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);

                //and connect!
                urlConnection.connect();

                //set the path where we want to save the file
                //in this case, going to save it on the root directory of the
                //sd card.
                File SDCardRoot = Environment.getExternalStorageDirectory();
                //create a new file, specifying the path, and the filename
                //which we want to save the file as.
                String path = localPath;
                String root = path.substring(0,path.lastIndexOf("/"));
                File dir = new File(root);
                if(dir.exists() == false){
                    dir.mkdirs();
                };
                String fileName = path.substring(path.lastIndexOf("/"),path.length());
                /*Log.i("DownloadTask","root: "+root);
                Log.i("DownloadTask","fileName: "+fileName);*/
                File file = new File(root,fileName);  //SDCardRoot,"1.jpg"

                //this will be used to write the downloaded data into the file we created
                FileOutputStream fileOutput = new FileOutputStream(file);
                //this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();
                //this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                //variable to store total downloaded bytes
                int downloadedSize = 0;

                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0; //used to store a temporary size of the buffer

                //now, read through the input buffer and write the contents to the file
                while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                    //add the data in the buffer to the file in the file output stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    //add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    //this is where you would do something to report the progress, like this maybe
                    //updateProgress(downloadedSize, totalSize);

                }
                //close the output stream when done
                Utils.close(inputStream);
                Utils.close(fileOutput);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(String  routeArrayList) {
            super.onPostExecute(routeArrayList);
            /*DownloadTask downloadTask = new DownloadTask(mContext);
            downloadTask.execute(new String[]{"1"});*/
            /*if(dialog.isShowing())
                dialog.dismiss();;*/
        }
    }

    private InputStream retrieveStream(String url) {
        Log.i("retrieveStream",url);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(url);
        try {
            HttpResponse getResponse = client.execute(getRequest);
            final int statusCode = getResponse.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w(getClass().getSimpleName(),
                        "Error " + statusCode + " for URL " + url);
                return null;
            }
            HttpEntity getResponseEntity = getResponse.getEntity();
            return getResponseEntity.getContent();
        }
        catch (IOException e) {
            getRequest.abort();
            Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        boolean isPopFragment = false;
        String currentTabTag = mTabHost.getCurrentTabTag();
        if (currentTabTag.equals(AppConstants.ROUTES)) {
            RouteLevelOne fragment3 = (RouteLevelOne) getSupportFragmentManager().findFragmentByTag(AppConstants.ROUTES);
            isPopFragment = ((BaseContainerFragment)fragment3.getChildFragmentManager().findFragmentByTag(fragment3.getCurrentTab())).popFragment();
        } else if (currentTabTag.equals(AppConstants.THINGS_TO_DO)) {
            isPopFragment = ((BaseContainerFragment)getSupportFragmentManager().findFragmentByTag(AppConstants.THINGS_TO_DO)).popFragment();
        }
        if (!isPopFragment) {
            finish();
        }
    }

    public class ResponseReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "MESSAGE_PROCESSED";
        @Override
        public void onReceive(Context context, Intent intent) {

            // Update UI, new "message" processed by SimpleIntentService
            //intent.getStringExtra(SplashDownloadService.PARAM_OUT_MSG);
        }
    }

}
